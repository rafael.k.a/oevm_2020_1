package com.company;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class ASMCode {
    ArrayList<String> arrayOfVariables = new ArrayList<>();
    HashMap<String, String> arrayOfStringVariables = new HashMap<>();

    String starting = "format PE console\n" +
            "\n" + "entry start\n" +
            "\n" + "include 'win32a.inc'\n" +
            "\n" + "section '.data' data readable writable\n";
    String variables = "\tspaceStr db '%d', 0\n" +
            "\tdopStr db '%d', 0ah, 0\n";
    String code = "section '.code' code readable executable\n" +
            "\n" + "\t start:\n";
    String ending = "\t finish:\n" +
            "\n" + "\t\t call [getch]\n" +
            "\n" + "\t\t call [ExitProcess]\n" +
            "\n" + "section '.idata' import data readable\n" +
            "\n" + "\t library kernel, 'kernel32.dll',\\\n" +
            "\t msvcrt, 'msvcrt.dll'\n" +
            "\n" + "\t import kernel,\\\n" +
            "\t ExitProcess, 'ExitProcess'\n" +
            "\n" + "\t import msvcrt,\\\n" +
            "\t printf, 'printf',\\\n" +
            "\t scanf, 'scanf',\\\n" +
            "\t getch, '_getch'";

    public void create(){
        addAllVariables();
        try(FileWriter writer = new FileWriter("asmProg.ASM", false))
        {
            writer.write(starting);
            writer.write(variables);
            writer.write(code);
            writer.write(ending);
            writer.flush();
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
    }
    public void addToVariables(ArrayList<String> arrayList) {
        arrayOfVariables = arrayList;
    }
    public void addAllVariables() {
        for (String string : arrayOfVariables) {
            variables += "\t" + string + " dd ?\n";
        }
        for (String key: arrayOfStringVariables.keySet()) {
            variables += "\t" + key + " db '" + arrayOfStringVariables.get(key) + "', 0\n";
        }
    }
    public void addToCodeWrite(String string) {
        arrayOfStringVariables.put("str" + (arrayOfStringVariables.size() + 1), string);

        code += "\t\t push " + "str" + arrayOfStringVariables.size() + "\n" +
                "\t\t call [printf]\n";
    }
    public void addToCodeReadLn(String string) {
        if(arrayOfVariables.contains(string)){
            code += "\t\t push " + string + "\n" +
                    "\t\t push spaceStr\n" +
                    "\t\t call [scanf]\n\n";
        }
    }
    public void addToCodeOperation(String res, String firstNum, String operator, String secondNum) {
        if (arrayOfVariables.contains(res) && arrayOfVariables.contains(firstNum) && arrayOfVariables.contains(secondNum)) {

            switch (operator) {
                case "+":
                    code += "\t\t mov ecx, [" +  firstNum + "]\n" +
                            "\t\t add ecx, [" +  secondNum + "]\n" +
                            "\t\t mov [" + res + "], ecx\n";
                    break;
                case "-":
                    code += "\t\t mov ecx, [" +  firstNum + "]\n" +
                            "\t\t sub ecx, [" +  secondNum + "]\n" +
                            "\t\t mov [" + res + "], ecx\n";
                    break;
                case "*":
                    code += "\t\t mov ecx, [" +  firstNum + "]\n" +
                            "\t\t imul ecx, [" +  secondNum + "]\n" +
                            "\t\t mov [" + res + "], ecx\n";
                    break;
                case "/":
                    code += "\t\t mov eax, [" +  firstNum + "]\n" +
                            "\t\t mov ecx, [" +  secondNum + "]\n" +
                            "\t\t div ecx\n" +
                            "\t\t mov [" + res + "], eax\n";
                    break;
            }
        }
    }
    public void addToCodeWriteLn(String string) {
        if (arrayOfVariables.contains(string)){
            code += "\t\t push [" + string + "]\n" +
                    "\t\t push dopStr\n" +
                    "\t\t call [printf]\n\n";
        }
    }
}
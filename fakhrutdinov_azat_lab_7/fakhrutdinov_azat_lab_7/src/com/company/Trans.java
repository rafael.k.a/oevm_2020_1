package com.company;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Trans {

    ASMCode asmCode;
    private String processingString;
    private String blockOfVars;
    private String blockOfCods;
    private ArrayList<String> arrayVarsString = new ArrayList<>();
    private ArrayList<String> arrayCodsString = new ArrayList<>();
    private ArrayList<String> arrayVars = new ArrayList<>();

    public Trans(String processingString, ASMCode asmCode) {
        this.processingString = processingString;
        this.asmCode = asmCode;
    }
    public void transBody() {
        Pattern patternOfVariables = Pattern.compile("(?<=var)[\\s\\S]*(?=begin)");
        Matcher matcherOfVariables = patternOfVariables.matcher(processingString);
        while (matcherOfVariables.find()) {
            blockOfVars = processingString.substring(matcherOfVariables.start(), matcherOfVariables.end());
        }
        Pattern patternOfCode = Pattern.compile("(?<=begin)[\\s\\S]*(?=end.)");
        Matcher matcherOfCode = patternOfCode.matcher(processingString);
        while (matcherOfCode.find()) {
            blockOfCods = processingString.substring(matcherOfCode.start(), matcherOfCode.end());
        }
        transVar();
        transCode();
    }

    public void transVar(){
        Pattern patternOfVariables = Pattern.compile("([a-zA-Z]([a-zA-Z0-9_]*[\\s]*,[\\s]*)*)([a-zA-Z][a-zA-Z0-9_]*)[\\s]*:[\\s]*integer[\\s]*;");
        Matcher matcherOfVariables = patternOfVariables.matcher(blockOfVars);
        while (matcherOfVariables.find()) {
            arrayVarsString.add(blockOfVars.substring(matcherOfVariables.start(), matcherOfVariables.end()));
        }
        transVarsInteger();
    }

    public void transVarsInteger(){
        Pattern patternOfVariables = Pattern.compile("[a-zA-Z][a-zA-Z0-9_]*");
        Matcher matcherOfVariables;
        String var;
        for (String string : arrayVarsString) {
            matcherOfVariables = patternOfVariables.matcher(string);
            while (matcherOfVariables.find()) {
                var = string.substring(matcherOfVariables.start(), matcherOfVariables.end());
                if (!var.equals("integer")) { arrayVars.add(var); }
            }
        }
        asmCode.addToVariables(arrayVars);
    }

    public void transCode(){
        Pattern patternOfCods = Pattern.compile("[a-zA-Z].*;");
        Matcher matcherOfCods = patternOfCods.matcher(blockOfCods);
        while (matcherOfCods.find()) {
            arrayCodsString.add(blockOfCods.substring(matcherOfCods.start(), matcherOfCods.end()));
        }
        createCode();
    }

    public void createCode(){
        Pattern patternForWrite = Pattern.compile("write\\('(.*)'\\);");
        Pattern patternForWriteLn = Pattern.compile("writeln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");

        Pattern patternForReadLn = Pattern.compile("readln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");
        Pattern patternForOperation = Pattern.compile("([a-z][a-zA-Z0-9]*)[\\s]*[\\s]*:=[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*([+\\-*/])[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*;");
        for (String string : arrayCodsString) {
            if(string.matches(patternForWrite.toString())){
                Matcher matcherForCode = patternForWrite.matcher(string);
                if (matcherForCode.find()) {
                    asmCode.addToCodeWrite(matcherForCode.group(1));
                }
            }
            else if(string.matches(patternForReadLn.toString())){
                Matcher matcherForCode = patternForReadLn.matcher(string);
                if (matcherForCode.find()) {
                    asmCode.addToCodeReadLn(matcherForCode.group(1));
                }
            }
            else if(string.matches(patternForOperation.toString())){
                Matcher matcherForCode = patternForOperation.matcher(string);

                if (matcherForCode.find()) {
                    asmCode.addToCodeOperation(matcherForCode.group(1), matcherForCode.group(2), matcherForCode.group(3), matcherForCode.group(4));
                }
            }
            else if(string.matches(patternForWriteLn.toString())){
                Matcher matcherForCode = patternForWriteLn.matcher(string);

                if (matcherForCode.find()) {
                    asmCode.addToCodeWriteLn(matcherForCode.group(1));
                }
            }
        }
    }
}
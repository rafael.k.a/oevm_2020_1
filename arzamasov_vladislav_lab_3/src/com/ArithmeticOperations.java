package com;

public class ArithmeticOperations {
    String firstNum;
    String secondNum;
    char operation;
    private static String _result = "";

    public ArithmeticOperations(String firstNumber, String secondNumber, char math_operation, int ishodSS) {
        firstNum = convertTOSecondNumSys(firstNumber, ishodSS);
        secondNum = convertTOSecondNumSys(secondNumber, ishodSS);
        operation = math_operation;
    }

    public ArithmeticOperations(String firstNumber, String secondNumber, char math_operation) {
        firstNum = firstNumber;
        secondNum = secondNumber;
        operation = math_operation;
        result();
    }

    public String result() {
        String rez = "";
        switch (operation) {
            case '+':
                swapValue(firstNum, secondNum);
                addZeroSecondValue(firstNum, secondNum);
                rez = plusOper(firstNum, secondNum);
                break;
            case '-':
                swapValue(firstNum, secondNum);
                addZeroSecondValue(firstNum, secondNum);
                rez = minusOper(firstNum, secondNum);
                break;
            case '*':
                swapValue(firstNum, secondNum);
                rez = umnozOper(firstNum, secondNum);
                break;
            case '/':
                rez = delenOper(firstNum, secondNum);
                break;
        }
        return rez;
    }

    private String plusOper(String fnum, String snum) {
        char[] firstValueArray = new StringBuffer(fnum).reverse().toString().toCharArray();
        char[] secondValueArray = new StringBuffer(snum).reverse().toString().toCharArray();
        boolean flag = false;
        for (int i = 0; i < secondValueArray.length; i++) {
            if (firstValueArray[i] == '0' && secondValueArray[i] == '0') {
                if (flag == false) {
                    flag = false;
                    _result += "0";
                } else if (flag == true) {
                    flag = false;
                    _result += "1";
                }
            } else if ((firstValueArray[i] == '1' && secondValueArray[i] == '0') || (firstValueArray[i] == '0' && secondValueArray[i] == '1')) {
                if (flag == false) {
                    flag = false;
                    _result += "1";
                } else if (flag == true) {
                    flag = true;
                    _result += "0";
                }
            } else if (firstValueArray[i] == '1' && secondValueArray[i] == '1') {
                if (flag == false) {
                    flag = true;
                    _result += "0";
                } else if (flag == true) {
                    flag = true;
                    _result += "1";
                }
            }
        }
        for (int i = firstValueArray.length - 1; i < firstValueArray.length; i++) {
            if (firstValueArray[i] == 0) {
                if (flag == false) {
                    flag = false;
                    _result += "0";
                } else if (flag == true) {
                    flag = false;
                    _result += "1";
                }
            } else if (firstValueArray[i] == 1) {
                if (flag == false) {
                    flag = false;
                    _result = "1";
                } else if (flag == true) {
                    flag = true;
                    _result += "0";
                }
            }
        }
        if (flag == true) {
            _result += "10";
        }
        _result = new StringBuffer(_result).reverse().toString();
        return _result;
    }

    private String minusOper(String fnum, String snum) {
        char[] firstValueArray = new StringBuffer(fnum).toString().toCharArray();
        char[] secondValueArray = new StringBuffer(snum).toString().toCharArray();
        boolean flag = false;
        for (int i = secondValueArray.length - 1; i > -1; i--) {
            if (firstValueArray[i] == '0' && secondValueArray[i] == '0') {
                if (flag == false) {
                    flag = false;
                    _result += "0";
                } else if (flag == true) {
                    flag = true;
                    _result += "1";
                }
            } else if (firstValueArray[i] == '1' && secondValueArray[i] == '0') {
                if (flag == false) {
                    flag = false;
                    _result += "1";
                } else if (flag == true) {
                    flag = false;
                    _result += "0";
                }
            } else if (firstValueArray[i] == '0' && secondValueArray[i] == '1') {
                if (flag == false) {
                    flag = true;
                    _result += "1";
                } else if (flag == true) {
                    flag = true;
                    _result += "0";
                }
            } else if (firstValueArray[i] == '1' && secondValueArray[i] == '1') {
                if (flag == false) {
                    flag = false;
                    _result += "0";
                } else if (flag == true) {
                    flag = true;
                    _result += "1";
                }
            }
        }
        _result = new StringBuffer(_result).reverse().toString();
        return _result;
    }

    private String umnozOper(String fnum, String snum) {
        char[] secondNumArray = new StringBuffer(snum).reverse().toString().toCharArray();
        String helpResult = "0";
        int helpSymbol = 0;
        for (int i = 0; i < secondNumArray.length; i++) {
            if (secondNumArray[i] == '1') {
                for (int j = 0; j < i - helpSymbol; j++) {
                    fnum += "0";
                }
                new ArithmeticOperations (fnum, helpResult,'+');
                helpResult = _result;
                helpSymbol = i;
            }
        }
        return helpResult;
    }

    private String delenOper(String fnum, String snum) {
        String result = "0";
        String comparison = snum;
        while (compare(fnum, comparison)) {
            result = add(result, "1");
            comparison = add(comparison, snum);
        }
        return result;
    }

    private static int decimalNum(String chislo, int iss) {
        char[] chisloar = chislo.toCharArray();
        int znach = 0;
        int num = 0;
        for (int i = 0; i < chisloar.length; i++) {
            if(chisloar[i] >= '0' && chisloar[i] <= '9') {
                num = chisloar[i] - '0';
            }
            else if (chisloar[i] >= 'A' && chisloar[i] <= 'F'){
                num = chisloar[i] - 'A' + 10;
            }
            znach += num * Math.pow(iss, chisloar.length - 1 - i);
        }
        return znach;
    }

    private static String convertTOSecondNumSys(String chislo, int iss) {
        int tenSS = decimalNum(chislo, iss);
        String rez = "";
        int ost;
        do {
            ost = tenSS % 2;
            tenSS = tenSS / 2;
            if (ost < 10)
                rez = ost + rez;
            else {
                rez = (char) (ost + 'A' - 10) + rez;
            }
        }
        while (tenSS / 2 != 0);
        if (tenSS > 0 && tenSS < 10)
            rez = tenSS + rez;
        else if (tenSS > 10) {
            rez = (char) (tenSS + 'A' - 10) + rez;
        }
        return rez;
    }

    private void swapValue(String firstValue, String secondValue) {
        String temporaryString = "";
        if (firstValue.length() < secondValue.length()) {
            temporaryString = firstValue;
            firstValue = secondValue;
            secondValue = temporaryString;
        }
        firstNum = firstValue;
        secondNum = secondValue;
    }

    private void addZeroSecondValue(String firstValue, String secondValue) {
        int col = firstValue.length() - secondValue.length();
        while (col > 0) {
            secondValue = "0" + secondValue;
            col--;
        }
        firstNum = firstValue;
        secondNum = secondValue;
    }
    public boolean compare(String strFirstNumber, String strSecondNumber) {
        char[] strFirstArray = new StringBuilder(strFirstNumber).reverse().toString().toCharArray();
        char[] strSecondArray = new StringBuilder(strSecondNumber).reverse().toString().toCharArray();

        if (strFirstArray.length > strSecondArray.length) {
            return true;
        }
        if (strFirstArray.length < strSecondArray.length) {
            return false;
        }
        if (strFirstArray.length == strSecondArray.length) {

            for (int i = 0; i < strFirstArray.length; i++) {

                if (strFirstArray[i] == '1' && strSecondArray[i] == '0') {
                    return true;
                }
                if (strFirstArray[i] == '0' && strSecondArray[i] == '1') {
                    return false;
                }
            }
        }
        return true;
    }
    public String add(String strFirstNumber, String strSecondNumber) {
        char[] firstDigitArray = strFirstNumber.toCharArray();
        char[] secondDigitArray = strSecondNumber.toCharArray();
        StringBuilder result = new StringBuilder();
        int transfer = 0;
        int i = 0;
        char charZero = '0';
        char charOne = '1';

        while (i < secondDigitArray.length) {
            if (transfer == 0) {
                if (firstDigitArray[i] == charZero && secondDigitArray[i] == charZero) {
                    result.append("0");
                    transfer = 0;
                }
                if ((firstDigitArray[i] == charZero && secondDigitArray[i] == charOne) || (firstDigitArray[i] == charOne && secondDigitArray[i] == charZero)) {
                    result.append("1");
                    transfer = 0;
                }
                if (firstDigitArray[i] == charOne && secondDigitArray[i] == charOne) {
                    result.append("0");
                    transfer = 1;
                }
            } else {
                if (firstDigitArray[i] == charZero && secondDigitArray[i] == charZero) {
                    result.append("1");
                    transfer = 0;
                }
                if ((firstDigitArray[i] == charZero && secondDigitArray[i] == charOne) || (firstDigitArray[i] == charOne && secondDigitArray[i] == charZero)) {
                    result.append("0");
                    transfer = 1;
                }
                if (firstDigitArray[i] == '1' && secondDigitArray[i] == charOne) {
                    result.append("1");
                    transfer = 1;
                }
            }
            i++;
        }

        while (i < firstDigitArray.length) {
            if (firstDigitArray[i] == charZero && transfer == 0) {
                result.append("0");
                transfer = 0;
            }
            if ((firstDigitArray[i] == charZero && transfer == 1) || (firstDigitArray[i] == '1' && transfer == 0)) {
                result.append("1");
                transfer = 0;
            }
            if (firstDigitArray[i] == charOne && transfer == 1) {
                result.append("0");
                transfer = 1;
            }
            i++;
        }

        if (transfer == 1) {
            result.append("1");
        }
        return result.toString();
    }
}

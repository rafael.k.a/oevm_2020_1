package EVM_Lab_3;

import java.util.Scanner;

public class Main {

    static final int COLUMN = 16;
    static final int SYMBOL = 5;

    public static void main(String[] args) {
        System.out.println("Вывести ДНФ нажмите 1");
        System.out.println("Вывести КНФ нажмите 2");
        Scanner number = new Scanner(System.in);
        int form = number.nextInt();
        int[][] table = new int[COLUMN][SYMBOL];
        System.out.println(" A  B  C  D  f");
        Operations.printTable(table);
        if (form == 1) {
            Operations.getDNF(table);
        } else {
            Operations.getKNF(table);
        }
    }
}

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ReadAndWrite {

    public String readFileAsString(String fileName) throws Exception {
        String data = "";
        data = new String( Files.readAllBytes( Paths.get( fileName ) ) );
        return data.replaceAll( ";", ";\n" );
    }

    public File writeFile(String path, String a) throws IOException {
        String newString = a;
        File file = new File( path );
        FileWriter fw = null;
        try {
            fw = new FileWriter( file );
            fw.write( newString );
        } catch (IOException e) {
            e.printStackTrace();
        }
        fw.close();
        return file;
    }
}

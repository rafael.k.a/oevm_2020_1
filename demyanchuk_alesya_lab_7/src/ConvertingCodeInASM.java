import java.util.HashMap;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConvertingCodeInASM {

    public String convert(String pas) {
        LinkedList<String> listVariable = new LinkedList<>();
        LinkedList<String> listVariableInt = new LinkedList<>();
        LinkedList<String> listCode = new LinkedList<>();
        HashMap<String, String> mapVariableString = new HashMap<>();
        String strVariable = "";
        String strCode = "";
        String stringCodeProgramAsm = "format PE console\n\n" + "entry start\n\n" + "include 'win32a.inc'\n\n" +
                "section '.data' data readable writable\n\n" + "\tstrSpace db '%d', 0\n" + "\tstrDop db '%d', 0ah, 0\n";
        Pattern patternVariables = Pattern.compile( "(?<=var)[\\s\\S]*(?=begin)" );
        Matcher matcherVariables = patternVariables.matcher( pas );

        while (matcherVariables.find()) {
            strVariable = pas.substring( matcherVariables.start(), matcherVariables.end() );
        }

        patternVariables = Pattern.compile( "([a-zA-Z]([a-zA-Z0-9_]*[\\s]*,[\\s]*)*)([a-zA-Z][a-zA-Z0-9_]*)[\\s]*:[\\s]*integer[\\s]*;" );
        matcherVariables = patternVariables.matcher( strVariable );

        while (matcherVariables.find()) {
            listVariable.add( strVariable.substring( matcherVariables.start(), matcherVariables.end() ) );
        }

        patternVariables = Pattern.compile( "[a-zA-Z][a-zA-Z0-9_]*" );

        for (int i = 0; i < listVariable.size(); i++) {
            matcherVariables = patternVariables.matcher( listVariable.get( i ) );
            while (matcherVariables.find()) {
                if (!listVariable.get( i ).substring( matcherVariables.start(), matcherVariables.end() ).equals( "integer" )) {
                    listVariableInt.add( listVariable.get( i ).substring( matcherVariables.start(), matcherVariables.end() ) );
                }
            }
        }

        for (int i = 0; i < listVariableInt.size(); i++) {
            stringCodeProgramAsm += "\t" + listVariableInt.get( i ) + " dd ?\n";
        }

        Pattern patternCode = Pattern.compile( "(?<=begin)[\\s\\S]*(?=end.)" );
        Matcher matcherCode = patternCode.matcher( pas );

        while (matcherCode.find()) {
            strCode = pas.substring( matcherCode.start(), matcherCode.end() );
        }

        patternCode = Pattern.compile( "[a-zA-Z].*;" );
        matcherCode = patternCode.matcher( strCode );

        while (matcherCode.find()) {
            listCode.add( strCode.substring( matcherCode.start(), matcherCode.end() ) );
        }

        Pattern patternWrite = Pattern.compile( "write\\('(.*)'\\);" );
        Pattern patternWriteLn = Pattern.compile( "writeln\\(([a-zA-Z][a-zA-Z0-9]*)\\);" );
        Pattern patternReadLn = Pattern.compile( "readln\\(([a-zA-Z][a-zA-Z0-9]*)\\);" );
        Pattern patternOperation = Pattern.compile( "([a-z][a-zA-Z0-9]*)[\\s]*[\\s]*:=[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*([+\\-*/])[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*;" );

        strCode = "";

        for (int i = 0; i < listCode.size(); i++) {
            if (listCode.get( i ).matches( patternWrite.toString() )) {
                matcherCode = patternWrite.matcher( listCode.get( i ) );
                if (matcherCode.find()) {
                    mapVariableString.put( "str" + (mapVariableString.size() + 1), matcherCode.group( 1 ) );
                    strCode += "\tpush " + "str" + mapVariableString.size() + "\n" +
                            " \tcall [printf]\n";
                }
            } else if (listCode.get( i ).matches( patternReadLn.toString() )) {
                matcherCode = patternReadLn.matcher( listCode.get( i ) );
                if (matcherCode.find()) {
                    if (listVariableInt.contains( matcherCode.group( 1 ) )) {
                        strCode += "\tpush " + matcherCode.group( 1 ) + "\n" +
                                "\tpush strSpace\n" +
                                "\tcall [scanf]\n\n";
                    }
                }
            } else if (listCode.get( i ).matches( patternOperation.toString() )) {
                matcherCode = patternOperation.matcher( listCode.get( i ) );
                if (matcherCode.find()) {
                    String strResult = matcherCode.group( 1 );
                    String strFirstNum = matcherCode.group( 2 );
                    String strSecondNum = matcherCode.group( 4 );
                    String strOperation = matcherCode.group( 3 );
                    ;
                    if (listVariableInt.contains( strResult ) && listVariableInt.contains( strFirstNum ) && listVariableInt.contains( strSecondNum )) {
                        switch (strOperation) {
                            case "+":
                                strCode += "\tmov ecx, [" + strFirstNum + "]\n" +
                                        "\tadd ecx, [" + strSecondNum + "]\n" +
                                        "\t mov [" + strResult + "], ecx\n";
                                break;
                            case "-":
                                strCode += "\tmov ecx, [" + strFirstNum + "]\n" +
                                        "\tsub ecx, [" + strSecondNum + "]\n" +
                                        "\tmov [" + strResult + "], ecx\n";
                                break;
                            case "*":
                                strCode += "\tmov ecx, [" + strFirstNum + "]\n" +
                                        "\timul ecx, [" + strSecondNum + "]\n" +
                                        "\tmov [" + strResult + "], ecx\n";
                                break;
                            case "/":
                                strCode += "\tmov eax, [" + strFirstNum + "]\n" +
                                        "\tmov ecx, [" + strSecondNum + "]\n" +
                                        "\tdiv ecx\n" +
                                        "\tmov [" + strResult + "], eax\n";
                                break;
                        }
                    }
                }
            } else if (listCode.get( i ).matches( patternWriteLn.toString() )) {
                matcherCode = patternWriteLn.matcher( listCode.get( i ) );
                if (matcherCode.find()) {
                    if (listVariableInt.contains( matcherCode.group( 1 ) )) {
                        strCode += "\tpush [" + matcherCode.group( 1 ) + "]\n" +
                                "\tpush strDop\n" +
                                "\tcall [printf]\n\n";
                    }
                }
            }
        }

        for (String key : mapVariableString.keySet()) {
            stringCodeProgramAsm += "\t" + key + " db '" + mapVariableString.get( key ) + "', 0\n";
        }

        stringCodeProgramAsm += "\nsection '.code' code readable executable\n\n" + "\tstart:\n" + strCode +
                "\tfinish:\n" +
                "\n" +
                "\tcall [getch]\n" +
                "\n" +
                "\tcall [ExitProcess]\n" +
                "\n" +
                "section '.idata' import data readable\n" +
                "\n" +
                "\tlibrary kernel, 'kernel32.dll',\\\n" +
                "\t\tmsvcrt, 'msvcrt.dll'\n" +
                "\n" +
                "\timport kernel,\\\n" +
                "\t\tExitProcess, 'ExitProcess'\n" +
                "\n" +
                "\timport msvcrt,\\\n" +
                "\t\tprintf, 'printf',\\\n" +
                "\t\tscanf, 'scanf',\\\n" +
                "\t\tgetch, '_getch'";

        return stringCodeProgramAsm;
    }
}

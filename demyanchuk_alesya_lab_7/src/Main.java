import java.io.File;

public class Main {

    public static void main(String[] args) throws Exception {
        ReadAndWrite readAndWrite = new ReadAndWrite();
        ConvertingCodeInASM convertingCodeInASM = new ConvertingCodeInASM();
        File fileProgram = readAndWrite.writeFile("src/demyanchuk_alesya_lab7.ASM",convertingCodeInASM.convert(readAndWrite.readFileAsString("src/demyanchuk_alesya_lab7.pas")));
        System.out.println("Файл для нашей программы создан. Путь к файлу:" + fileProgram.getAbsolutePath());
    }
}

﻿# Лабораторная работа 7
## ИСЭбд-21
## Зиновьев Степан

## **[URL видео.](https://drive.google.com/file/d/1kqmIU0Igi03VEMcYIfpOdDBbHvx4sxVW/view?usp=sharing)**

## Задание: разработать транслятор программ Pascal-FASM с проверкой синтаксиса и семантики исходной программы

## Пример работы:
**[До запуска программы](https://drive.google.com/file/d/1D3zXE6jXF0Ll0ZwktQx7uCxHkLlVlJce/view?usp=sharing)**
**[После работы программы](https://drive.google.com/file/d/1z_1sPIQgV5-HObbcvxjWkgDEWAnljDjK/view?usp=sharing)**

## Спасибо за внимание.
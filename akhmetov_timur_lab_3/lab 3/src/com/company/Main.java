package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
	    System.out.println("Введите исходную систему счисления: ");
        int base = sc.nextInt();
        System.out.println("Введите первое число в исходной системе счисления: ");
        String first = sc.next();
        System.out.println("Введите второе число в исходной системе счисления: ");
        String second = sc.next();
        Calc calc = new Calc(first, second, base);
    }
}

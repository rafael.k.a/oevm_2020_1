package com.company;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Translation translation = new Translation();
        Scanner scan = new Scanner(System.in);
        StringBuffer StrBuf = new StringBuffer();

        System.out.printf("Введите начальную систему счисления: ");
        String start_cc = scan.nextLine();
        System.out.printf("Введите конечную систему счисления: ");
        String finish_cc = scan.nextLine();
        System.out.printf("Введите число: ");
        String count = scan.nextLine();

        int start = Integer.parseInt(start_cc);
        int finish = Integer.parseInt(finish_cc);
        translation.transfer(start, count);
        if (translation.eror == 0) {
            translation.endTransfer(finish, StrBuf);
        }
    }
}

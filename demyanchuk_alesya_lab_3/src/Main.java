import java.util.Scanner;

public class Main {
    private static Calculator calculator;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int sourceSystem;
        String numberOne = "", numberTwo = "", action = "";
        System.out.print("Введите начальную СС(от 2 до 16): ");
        sourceSystem = scanner.nextInt();
        scanner.nextLine();
        System.out.print("Введите число1 (допускаются буквы A-F): ");
        numberOne += scanner.next();
        System.out.print("Введите число2 (допускаются буквы A-F): ");
        numberTwo += scanner.next();
        System.out.print("Введите действие: ");
        action += scanner.next();
        calculator = new Calculator(numberOne, numberTwo, action);
        System.out.print(calculator.returnResult(sourceSystem));
    }
}

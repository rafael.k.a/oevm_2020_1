public class ClassSupport {

        static final char[] BASES = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

        public static char[] toUpper(String finNumberStr, char[] finNumberCh) {
            for (int i = 0; i < finNumberStr.length(); i++) {
                if (finNumberCh[i] >= 'a' && finNumberCh[i] <= 'z') {
                    finNumberCh[i] -= ' ';
                }
            }
            return finNumberCh;
        }

        public static int convertToTen(String finNumberStr, char[] finNumberCh, int finNumberInt, int initialSystem) {
            for (int i = finNumberStr.length() - 1; i >= 0; i--) {
                for (int j = 0; j < 16; j++) {
                    if (finNumberCh[i] == BASES[j]) {
                        finNumberInt += j * Math.pow(initialSystem, finNumberStr.length() - i - 1);
                    }
                }
            }
            return finNumberInt;
        }

        public static String convertToBinary(int number, String number2Base) {
            while (number != 0) {
                number2Base += number % 2;
                number /= 2;
            }

            return new StringBuffer(number2Base).reverse().toString();
        }

        public static String operations(char[] operation, String firstNum, String secondNum, String result, int firstNumberInt, int secondNumberInt) throws Exception {
            if (operation[0] == '+') {
                result = BinaryOperations.addition(firstNum, secondNum);
                return result;
            } else if (operation[0] == '-') {
                result = BinaryOperations.subtraction(firstNum, secondNum, firstNumberInt, secondNumberInt);
                return result;
            } else if (operation[0] == '*') {
                result = BinaryOperations.multiply(firstNum, secondNumberInt);
                return result;
            } else if (operation[0] == '/') {
                result = BinaryOperations.divide(firstNumberInt, secondNumberInt);
                return result;
            }
            throw new Exception ();
        }
}

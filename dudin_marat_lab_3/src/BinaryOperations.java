public class BinaryOperations {

    public static String addition(String firstNumberBin, String secondNumberBin) {
        String result = "";
        String digitPresently = "0";
        int digitInMemory = 0;
        int firstNumberLen = firstNumberBin.length() - 1;
        int secondNumberLen = secondNumberBin.length() - 1;

        while (firstNumberLen >= 0 || secondNumberLen >= 0 || digitInMemory == 1) {
            int firstNum = 0;
            int secondNum = 0;

            if (firstNumberLen < 0) {
                firstNum = 0;
            }
            else if (firstNumberBin.charAt(firstNumberLen) == '1') {
                firstNum = 1;
            }
            else {
                firstNum = 0;
            }
            if (secondNumberLen < 0) {
                secondNum = 0;
            }
            else if (secondNumberBin.charAt(secondNumberLen) == '1') {
                secondNum = 1;
            }
            else {
                secondNum = 0;
            }

            int outcome = firstNum + secondNum + digitInMemory;

            switch (outcome) {
                case 0:
                    digitInMemory = 0;
                    digitPresently = "0";
                    break;
                case 1:
                    digitInMemory = 0;
                    digitPresently = "1";
                    break;
                case 2:
                    digitInMemory = 1;
                    digitPresently = "0";
                    break;
                case 3:
                    digitInMemory = 1;
                    digitPresently = "1";
                    break;
            }
            firstNumberLen--;
            secondNumberLen--;
            result += digitPresently;
        }
        return new StringBuffer(result).reverse().toString();
    }

    public static String subtraction(String firstNumber, String secondNumber, int firstNumberInt, int secondNumberInt) {
        String result = "";
        String _1 = "1";
        int cnt = firstNumber.length() - secondNumber.length();
        char[] additionalCode = new char[firstNumber.length()];

        for (int i = 0; i < cnt; i++) {
            additionalCode[i] = '1';
        }
        for (int i = cnt; i < additionalCode.length; ++i) {
            if (secondNumber.charAt(i - cnt) == '1') {
                additionalCode[i] = '0';
            } else if (secondNumber.charAt(i - cnt) == '0') {
                additionalCode[i] = '1';
            }
        }
        String additionalCodeStr = String.valueOf(additionalCode);
        additionalCodeStr = addition(additionalCodeStr, _1);
        String outcome = addition(additionalCodeStr, firstNumber);

        if (outcome.length() > 1) {
            boolean zero = true;
            for (int i = 1; i < outcome.length(); i++) {
                if (outcome.charAt(i) == '1') {
                    zero = false;
                }
                else {
                    if (zero) {
                        continue;
                    }
                }
                result += outcome.charAt(i);
            }
        }
        if (secondNumberInt > firstNumberInt) {
            result = "-" + result;
        }
        return result;
    }

    public static String multiply(String firstNum, int secondNumberInt) {
        String result = "";
        for (int i = 0; i < secondNumberInt; i++) {
            result = addition(result, firstNum);
        }
        return result;
    }

    public static String divide(int firstNumberInt, int secondNumberInt) {
        String finResult = "";
        int cnt = 0;
        for (int i = firstNumberInt; i >= secondNumberInt; i -= secondNumberInt) {
            cnt++;
        }
        finResult = ClassSupport.convertToBinary(cnt, finResult);
        return finResult;
    }
}


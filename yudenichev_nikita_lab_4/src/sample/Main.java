package sample;

import java.util.Random;
import java.util.Scanner;

public class Main
{
    public static void main(String[] args)
    {
        int sizeCol=5;
        int sizeStr=16;
        int numberForm;
        Random random= new Random();
        Scanner scanner= new Scanner(System.in);

        int[][] table = {
                {0,0,0,0,random.nextInt(2)},
                {0,0,0,1,random.nextInt(2)},
                {0,0,1,0,random.nextInt(2)},
                {0,0,1,1,random.nextInt(2)},
                {0,1,0,0,random.nextInt(2)},
                {0,1,0,1,random.nextInt(2)},
                {0,1,1,0,random.nextInt(2)},
                {0,1,1,1,random.nextInt(2)},
                {1,0,0,0,random.nextInt(2)},
                {1,0,0,1,random.nextInt(2)},
                {1,0,1,0,random.nextInt(2)},
                {1,0,1,1,random.nextInt(2)},
                {1,1,0,0,random.nextInt(2)},
                {1,1,0,1,random.nextInt(2)},
                {1,1,1,0,random.nextInt(2)},
                {1,1,1,1,random.nextInt(2)},
        };

        System.out.print("ДНФ - 1\nКНФ - 2\nВведите номер: ");
        numberForm=scanner.nextInt();

        if (numberForm==1)
        {
            Helper helper =new Helper(numberForm,sizeCol,sizeStr,table);
        }
        else if (numberForm==2)
        {
            Helper helper =new Helper(numberForm,sizeCol,sizeStr,table);
        }
    }
}


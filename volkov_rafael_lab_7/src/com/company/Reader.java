package com.company;


import java.io.FileReader;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Reader {
    private static int newVariablesCount = 0;

    public static void readPascal(String name, Interpreter interpreter) {
        try (FileReader reader = new FileReader(name)) {
            Scanner scanner = new Scanner(reader);
            StringBuilder textStringBuilder = new StringBuilder();
            while (scanner.hasNextLine()) {
                textStringBuilder.append(scanner.nextLine()).append("\n");
            }
            String code = textStringBuilder.toString();
            codeProcessing(code, interpreter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void codeProcessing(String code, Interpreter interpreter) {
        variableProcessing(code, interpreter);
        commandProcessing(code, interpreter);
    }

    private static void variableProcessing(String code, Interpreter interpreter) {
        Pattern patternVariablesString = Pattern.compile("(?<=var\\n)[\\w\\W]*(?=\\nbegin)");
        Matcher matcherVariablesString = patternVariablesString.matcher(code);

        if (matcherVariablesString.find()) {
            String variables = matcherVariablesString.group();

            Pattern patternVariables = Pattern.compile("\\b[a-zA-Z][\\w]*\\b");
            Matcher matcherVariables = patternVariables.matcher(variables);

            while (matcherVariables.find()) {
                if (!matcherVariables.group().equals("integer")) {
                    interpreter.addVariable(matcherVariables.group() + " dd ?\n");
                }
            }
        }
    }

    private static void commandProcessing(String code, Interpreter interpreter) {
        Pattern patternCommandsString = Pattern.compile("(?<=begin\\n)[\\w\\W]*(?=\\nend.)");
        Matcher matcherCommandString = patternCommandsString.matcher(code);
        if (matcherCommandString.find()) {
            String commands = matcherCommandString.group();

            Pattern patternCommands = Pattern.compile("[\\w][\\w /,=*+:()'-]*;");
            Matcher matcherCommands = patternCommands.matcher(commands);

            while (matcherCommands.find()) {

                String command = matcherCommands.group();
                String valueWithoutQuotes = null;
                if (command.contains("(")) {
                    valueWithoutQuotes = command.substring(command.indexOf('(') + 1, command.indexOf(')'));
                }

                if (command.contains("readln")) {
                    interpreter.addReadCommand(valueWithoutQuotes);
                } else if (command.contains("write")) {
                    writeProcessing(command, interpreter, valueWithoutQuotes);
                } else if (command.contains(":=")) {
                    operationProcessing(command, interpreter);
                }
            }
        }
    }

    private static void writeProcessing(String command, Interpreter interpreter, String valueWithoutQuotes) {
        String endVariable = "', 0\n";
        String endVariableNewLine = ", 0dh, 0ah, 0\n";
        if (command.contains("'")) {
            String value = command.substring(command.indexOf('(') + 2, command.indexOf(')') - 1);
            String newVariable = "string" + newVariablesCount + " db '" + value;

            if (command.contains("writeln")) {
                newVariable += endVariableNewLine;
            } else {
                newVariable += endVariable;
            }

            interpreter.addVariable(newVariable);
            interpreter.addWriteCommand("string" + newVariablesCount);
            newVariablesCount++;
        } else {
            if (command.contains("writeln")) {
                interpreter.addWritelnCommand(valueWithoutQuotes);
            } else {
                interpreter.addWriteCommand(valueWithoutQuotes);
            }
        }
    }

    private static void operationProcessing(String command, Interpreter interpreter) {
        String result = command.substring(0, command.indexOf(":") - 1);
        if (command.contains("+")) {
            String firstElement = command.substring(command.indexOf("=") + 2, command.indexOf("+") - 1);
            String secondElement = command.substring(command.indexOf("+") + 2, command.length() - 1);
            interpreter.addMathsCommand(result + " " + firstElement + " " + secondElement + " add");
        } else if (command.contains("-")) {
            String firstElement = command.substring(command.indexOf("=") + 2, command.indexOf("-") - 1);
            String secondElement = command.substring(command.indexOf("-") + 2, command.length() - 1);
            interpreter.addMathsCommand(result + " " + firstElement + " " + secondElement + " sub");
        } else if (command.contains("*")) {
            String firstElement = command.substring(command.indexOf("=") + 2, command.indexOf("*") - 1);
            String secondElement = command.substring(command.indexOf("*") + 2, command.length() - 1);
            interpreter.addMathsCommand(result + " " + firstElement + " " + secondElement + " imul");
        } else if (command.contains("div")) {
            String firstElement = command.substring(command.indexOf("=") + 2, command.indexOf("div") - 1);
            String secondElement = command.substring(command.indexOf("div") + 4, command.length() - 1);
            interpreter.addMathsCommand(result + " " + firstElement + " " + secondElement + " div");
        }
    }
}

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        PascalReader pascalReader = new PascalReader();
        AssemblerWriter assemblerWriter = new AssemblerWriter();
        PascalParser pascalParser = new PascalParser(
                pascalReader.readPascalFile(),
                assemblerWriter
        );

        pascalParser.parsePascalFile();
        assemblerWriter.createAsm();
    }
}
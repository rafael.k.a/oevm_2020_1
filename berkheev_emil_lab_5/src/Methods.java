import java.awt.*;

public class Methods {

    private final int sizeI = 16;
    private final int sizeJ = 5;

    private final int width = 30;
    private final int height = 30;

    private int X = 10;
    private int Y = 10;
    private int formDefiner;

    public void draw(int array[][], Graphics g) {

        for (int i = 0; i < sizeJ - 1; i++) {
            g.drawRect(X + width * i, Y, width, height);
            g.drawString("X" + (i + 1), X * 2 + width * i, Y * 3);
        }

        g.drawRect(X + width * (sizeJ - 1), Y, width, height);
        g.drawString("F", X + 10 + width * (sizeJ - 1), Y + 20);

        Y += 30;
        for (int i = 0; i < sizeI; i++) {
            for (int j = 0; j < sizeJ; j++) {
                g.drawRect(X + width * j, Y + height * i, width, height);
                g.drawString(array[i][j] + "", X + 10 + width * j, Y + 20 + height * i);
            }
        }
        if (MainWindow._KNF == true) {
            formDefiner = 0;
            drawNormalForm(array, g);
        }

        if (MainWindow._DNF == true) {
            formDefiner = 1;
            drawNormalForm(array, g);
        }

        Y -= 30;
    }

    private void drawNormalForm(int array[][], Graphics g) {
        if (array[MainWindow._step][sizeJ - 1] == formDefiner) {
            g.setColor(Color.GREEN);
        } else {
            g.setColor(Color.RED);
        }
        for (int j = 0; j < sizeJ; j++) {
            g.drawRect(X + width * j, Y + height * MainWindow._step, width, height);
            g.drawString(array[MainWindow._step][j] + "", X + 10 + width * j, Y + 20 + height * MainWindow._step);
        }
    }

    public String normalForm(int array[][], boolean definer) {
        int X4 = 3;
        int F = 4;
        int cell = -1;
        String minorOperator="";
        String majorOperator="";

        StringBuilder strBuilder = new StringBuilder();
        if (!definer) {
            cell = 1;
            minorOperator=" * ";
            majorOperator=" ) + ";
        } else {
            cell = 0;
            minorOperator=" + ";
            majorOperator=" ) * ";
        }

        if (array[MainWindow._step][F] == cell) {
            strBuilder.append("( ");
            for (int j = 0; j < X4; j++) {
                if (array[MainWindow._step][j] == cell) {
                    strBuilder.append("X").append(j + 1).append(minorOperator);
                } else {
                    strBuilder.append("-X").append(j + 1).append(minorOperator);
                }
            }
            if (array[MainWindow._step][X4] == cell) {
                strBuilder.append("X4");
            } else {
                strBuilder.append("-X4");
            }
            strBuilder.append(majorOperator);

            return strBuilder.append("\n").toString();
        }

        return "";
    }

}


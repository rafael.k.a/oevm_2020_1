import javax.swing.*;
import java.awt.*;

public class DrawPanel extends JPanel {
    final int NUMBER_OF_ROWS = 16;
    final int NUMBER_OF_COLUMNS = 5;
    final int SIZE_OF_CELL = 32;
    final int TOP_MARGIN = 65;
    final int LEFT_MARGIN = 10;
    final int FONT_SIZE = 22;
    final int LAST_STEP = 16;
    private boolean chooseDNF = false;
    private int step = 0;
    private TruthTable truthTable;
    private FormConverter formConverter;

    public DrawPanel() {
        truthTable = new TruthTable(NUMBER_OF_ROWS, NUMBER_OF_COLUMNS);
        formConverter = new FormConverter(NUMBER_OF_COLUMNS);
    }

    /*
    Метод для обновления таблицы истинности
     */
    public void updateTable() {
        truthTable = new TruthTable(NUMBER_OF_ROWS, NUMBER_OF_COLUMNS);
    }

    public void drawTable(Graphics g) {
        g.setColor(new Color(0, 0, 159));
        g.setFont(new Font("Consolas", Font.BOLD, FONT_SIZE));
        g.drawString("Таблица истинности", 5, 15);
        g.setColor(Color.BLACK);
        for (int k = 0; k < NUMBER_OF_COLUMNS; k++) {
            if (k != NUMBER_OF_COLUMNS - 1) {
                g.drawRect(LEFT_MARGIN + SIZE_OF_CELL * k, TOP_MARGIN - SIZE_OF_CELL, SIZE_OF_CELL, SIZE_OF_CELL);
                g.setFont(new Font("Consolas", Font.BOLD, FONT_SIZE));
                g.drawString("X" + (k + 1), LEFT_MARGIN + SIZE_OF_CELL * k + LEFT_MARGIN / 3, TOP_MARGIN - SIZE_OF_CELL / 3);
            } else {
                g.drawRect(LEFT_MARGIN + SIZE_OF_CELL * k, TOP_MARGIN - SIZE_OF_CELL, SIZE_OF_CELL, SIZE_OF_CELL);
                g.setFont(new Font("Consolas", Font.BOLD, FONT_SIZE));
                g.drawString("F", LEFT_MARGIN + SIZE_OF_CELL * k + LEFT_MARGIN, TOP_MARGIN - SIZE_OF_CELL / 3);
            }
        }
        for (int i = 0; i < NUMBER_OF_ROWS; i++) {
            for (int j = 0; j < NUMBER_OF_COLUMNS; j++) {
                g.setFont(new Font("Consolas", Font.PLAIN, FONT_SIZE));
                g.drawRect(LEFT_MARGIN + SIZE_OF_CELL * j, TOP_MARGIN + SIZE_OF_CELL * i, SIZE_OF_CELL, SIZE_OF_CELL);
                g.drawString(truthTable.getTruthTable()[i][j] + "", LEFT_MARGIN + SIZE_OF_CELL * j + LEFT_MARGIN, TOP_MARGIN + SIZE_OF_CELL * (i + 1) - LEFT_MARGIN);
            }
        }
    }

    public boolean isChooseDNF() {
        return chooseDNF;
    }

    public void setChooseDNF(boolean chooseDNF) {
        this.chooseDNF = chooseDNF;
    }

    /*
    Метод для отрисовки ДНФ до определённого шага
     */
    public void drawDNF(Graphics g) {
        g.setColor(Color.RED);
        g.drawLine(LEFT_MARGIN, TOP_MARGIN + SIZE_OF_CELL * step, LEFT_MARGIN + SIZE_OF_CELL * 5, TOP_MARGIN + SIZE_OF_CELL * step);
        int inner_margin = 1;
        for (int i = 1; i <= step; i++) {
            if (!formConverter.bringToDNF(truthTable.getTruthTable(), i).equals("-1")) {
                g.setFont(new Font("Consolas", Font.PLAIN, FONT_SIZE));
                g.setColor(Color.BLACK);
                g.drawString(formConverter.bringToDNF(truthTable.getTruthTable(), i), 210, TOP_MARGIN + 30 * inner_margin);
                inner_margin++;
                if (formConverter.getFirstStringIndex() == -1) {
                    formConverter.setFirstStringIndex(i);
                }
            }
        }
    }

    /*
    Метод для отрисовки КНФ до определённого шага
     */
    public void drawCNF(Graphics g) {
        g.setColor(Color.RED);
        g.drawLine(LEFT_MARGIN, TOP_MARGIN + SIZE_OF_CELL * step, LEFT_MARGIN + SIZE_OF_CELL * 5, TOP_MARGIN + SIZE_OF_CELL * step);
        int inner_margin = 1;
        for (int i = 1; i <= step; i++) {
            if (!formConverter.bringToCNF(truthTable.getTruthTable(), i).equals("-1")) {
                g.setFont(new Font("Consolas", Font.PLAIN, FONT_SIZE));
                g.setColor(Color.BLACK);
                g.drawString(formConverter.bringToCNF(truthTable.getTruthTable(), i), 210, TOP_MARGIN + 30 * inner_margin);
                inner_margin++;
                if (formConverter.getFirstStringIndex() == -1) {
                    formConverter.setFirstStringIndex(i);
                }
            }
        }
    }

    /*
    Метод для увелечения шага
     */
    public void increaseStep() {
        step++;
    }

    /*
    Мето для сбрасывания значения шага и индекса
     */
    public void resetStep() {
        step = 0;
        formConverter.setFirstStringIndex(-1);
    }

    /*
    Проверка на последний шаг
     */
    public boolean isLastStep() {
        return step == LAST_STEP;
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(new Color(238, 238, 238));
        g2.fillRect(0, 0, 600, 600);

        drawTable(g2);

        if (step > 0) {
            if (isChooseDNF()) {
                g.setColor(new Color(0, 0, 159));
                g.setFont(new Font("Consolas", Font.BOLD, FONT_SIZE));
                g.drawString("ДНФ:", 230, 55);
                drawDNF(g2);
            } else {
                g.setColor(new Color(0, 0, 159));
                g.setFont(new Font("Consolas", Font.BOLD, FONT_SIZE));
                g.drawString("КНФ:", 230, 55);
                drawCNF(g2);
            }
        }
    }
}
package com.company;

import javax.swing.*;
import java.awt.*;

public class Panel extends JPanel {
    private TruthTable truthTable;
    private Font stringFont;
    private String selectedForm = " ";
    private int changeX;
    private int changeY;
    private String[][] toStringTruthTable;
    private String[] hatOfThruthTable;

    public void setSelectedForm(String selectedForm) {
        this.selectedForm = selectedForm;
    }

    public String getSelectedForm() {
        return selectedForm;
    }

    public void paint(Graphics g) {
        drawTruthT(g);
        if (selectedForm.equals("DNF")) {
            changeY = 70;
            changeX = 10;
            for (int i = 0; i < truthTable.getCountOfStrings(); i++) {
                for (int j = 0; j < truthTable.getCountOfColumns(); j++) {
                    if (TruthTable.truthT[i][truthTable.getCountOfColumns() - 1] == 1) {
                        g.setColor(new Color(72, 199, 89));
                        g.drawString(toStringTruthTable[i][j], changeX, changeY);
                    } else {
                        g.setColor(new Color(222, 23, 23));
                        g.drawString(toStringTruthTable[i][j], changeX, changeY);
                    }
                    changeX += 50;
                }
                changeX = 10;
                changeY += 40;
            }
        }
        if (selectedForm.equals("KNF")) {
            changeY = 70;
            changeX = 10;
            for (int i = 0; i < truthTable.getCountOfStrings(); i++) {
                for (int j = 0; j < truthTable.getCountOfColumns(); j++) {
                    if (TruthTable.truthT[i][truthTable.getCountOfColumns() - 1] == 0) {
                        g.setColor(new Color(72, 199, 89));
                        g.drawString(toStringTruthTable[i][j], changeX, changeY);
                    } else {
                        g.setColor(new Color(222, 23, 23));
                        g.drawString(toStringTruthTable[i][j], changeX, changeY);
                    }
                    changeX += 50;
                }
                changeX = 10;
                changeY += 40;
            }
        }
    }

    public void drawTruthT(Graphics g) {
        changeY = 70;
        changeX = 10;
        hatOfThruthTable = new String[]{"x1", "x2", "x3", "x4", "F"};
        truthTable = new TruthTable();
        toStringTruthTable = new String[truthTable.getCountOfStrings()][truthTable.getCountOfColumns()];
        stringFont = new Font("Times New Roman", Font.PLAIN, 30);
        g.setFont(stringFont);
        for (int i = 0; i < truthTable.getCountOfColumns(); i++) {
            g.drawString(hatOfThruthTable[i], changeX, changeY - 30);
            changeX += 50;
        }
        changeX = 10;
        for (int i = 0; i < truthTable.getCountOfStrings(); i++) {
            for (int j = 0; j < truthTable.getCountOfColumns(); j++) {
                toStringTruthTable[i][j] = String.valueOf(TruthTable.truthT[i][j]);
            }
        }
        for (int i = 0; i < truthTable.getCountOfStrings(); i++) {
            for (int j = 0; j < truthTable.getCountOfColumns(); j++) {
                g.drawString(toStringTruthTable[i][j], changeX, changeY);
                changeX += 50;
            }
            changeX = 10;
            changeY += 40;
        }
    }
}

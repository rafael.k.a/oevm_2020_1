package com.company;

import javax.swing.*;
import java.awt.*;

public class Frame {

    private JButton buttonDNF;
    private JButton buttonCNF;
    private JButton buttonNext;
    private JTextArea resultArea;
    private JTextArea statusArea;
    private JFrame frame;
    private Panel panel;
    private Minimalization minimalization;
    private TruthTable truthT;
    private Font resultFont;

    public void initialization() {
        minimalization = new Minimalization();
        truthT = new TruthTable();
        buttonNext = new JButton("Тык!");
        buttonNext.setBounds(650, 50, 90, 30);
        buttonNext.setEnabled(false);
        buttonNext.addActionListener(e -> {
            if (panel.getSelectedForm().equals("DNF")) {
                minimalization.setIterations(minimalization.getIterations() + 1);
                if (minimalization.getIterations() == truthT.getCountOfStrings() - 1) {
                    buttonNext.setEnabled(false);
                }
                minimalization.dNF(TruthTable.truthT);
                resultArea.setText(minimalization.getStringR());
                if (minimalization.getIterations() == 15 && TruthTable.truthT[minimalization.getIterations()][truthT.getCountOfColumns() - 1] != 1) {
                    resultArea.append("()");
                }
                if (TruthTable.truthT[minimalization.getIterations()][truthT.getCountOfColumns() - 1] == 1) {
                    statusArea.setText("Строка " + (minimalization.getIterations() + 1) + " подходит");
                } else {
                    statusArea.setText("Строка " + (minimalization.getIterations() + 1) + " не подходит");
                }
            } else if (panel.getSelectedForm().equals("KNF")) {
                minimalization.setIterations(minimalization.getIterations() + 1);
                if (minimalization.getIterations() == truthT.getCountOfStrings() - 1) {
                    buttonNext.setEnabled(false);
                }
                minimalization.сonjunctiveNormalForm(TruthTable.truthT);
                resultArea.setText(minimalization.getStringR());
                if (minimalization.getIterations() == 15 && TruthTable.truthT[minimalization.getIterations()][truthT.getCountOfColumns() - 1] != 0) {
                    resultArea.append("()");
                }
                if (TruthTable.truthT[minimalization.getIterations()][truthT.getCountOfColumns() - 1] == 0) {
                    statusArea.setText("Строка " + (minimalization.getIterations() + 1) + " подходит");
                } else {
                    statusArea.setText("Строка " + (minimalization.getIterations() + 1) + " не подходит");
                }
            }
            frame.repaint();
        });
        buttonDNF = new JButton("ДНФ");
        buttonDNF.setBounds(510, 50, 65, 30);
        buttonDNF.addActionListener(e -> {
            buttonNext.setEnabled(true);
            buttonDNF.setEnabled(false);
            buttonCNF.setEnabled(false);
            panel.setSelectedForm("DNF");
            frame.repaint();

        });
        buttonCNF = new JButton("КНФ");
        buttonCNF.setBounds(580, 50, 65, 30);
        buttonCNF.addActionListener(e -> {
            buttonNext.setEnabled(true);
            buttonDNF.setEnabled(false);
            buttonCNF.setEnabled(false);
            panel.setSelectedForm("KNF");
            frame.repaint();
        });

        resultFont = new Font("Times New Roman", Font.PLAIN, 15);
        resultArea = new JTextArea();
        resultArea.setBounds(500, 100, 300, 400);
        resultArea.setEnabled(false);
        resultArea.setFont(resultFont);
        resultArea.setBackground(new Color(238, 238, 238));
        resultArea.setDisabledTextColor(Color.BLACK);
        resultArea.setLineWrap(true);
        resultArea.setWrapStyleWord(true);

        statusArea = new JTextArea();
        statusArea.setBounds(510, 650, 150, 50);
        statusArea.setEnabled(false);
        statusArea.setFont(resultFont);
        statusArea.setBackground(new Color(238, 238, 238));
        statusArea.setDisabledTextColor(Color.BLACK);
        statusArea.setLineWrap(true);
        statusArea.setWrapStyleWord(true);

    }

    public Frame() {
        panel = new Panel();
        frame = new JFrame("Lab5_VS");
        frame.setSize(900, 900);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setResizable(false);
        initialization();
        frame.getContentPane().add(buttonDNF);
        frame.getContentPane().add(buttonCNF);
        frame.getContentPane().add(resultArea);
        frame.getContentPane().add(statusArea);
        frame.getContentPane().add(buttonNext);
        frame.getContentPane().add(panel);
        panel.setBounds(0, 0, 900, 900);
        frame.repaint();
    }
}

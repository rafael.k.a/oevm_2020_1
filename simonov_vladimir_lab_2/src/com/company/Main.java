package com.company;
import java.util.Scanner;
import java.util.InputMismatchException;

public class Main {
    public static void main(String[] args) {
        // write your code here
        Scanner in = new Scanner(System.in);
        String num = " ";
        int isxSistem = 0;
        int konSistem = 0;

            try {
                isxSistem = in.nextInt();
                konSistem = in.nextInt();
                num = in.next();
            } catch (InputMismatchException e) {
                System.out.printf("Введите другую с.с. ");
                return;
            }

            in.close();
            Per per = new Per(isxSistem, konSistem, num);
            per.translateToEnd();
            System.out.print(per.getResult());
        }
    }


import javax.swing.*;
import java.awt.*;

public class TableOfTruePanel extends JPanel {

    Minimize minimize;
    public TableOfTruePanel() {
        minimize = new Minimize();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        minimize.drawTable(g);
    }
}

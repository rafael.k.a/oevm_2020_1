import java.awt.*;
import java.util.Random;

public class Minimize {
    private final static int ROWS_NUMBER = 16;
    private final static int COLS_NUMBER = 5;
    private final static char[] LETTERS = {'A', 'B', 'C', 'D'};
    static int[][] tableOfTrue = new int[ROWS_NUMBER][COLS_NUMBER];
    private final static int COLS_NUMBERS = 6;
    private final static int ROWS_NUMBERS = 17;
    private final static int CELL_WIDTH = 30;
    private final static int CELL_HEIGHT = 30;
    private int X = 5;
    private int Y = 5;
    String result = "";

    public void fillTable() {
        Random random = new Random();
        for(int i = 0; i < ROWS_NUMBER; i++) {
            for(int j = 0; j < COLS_NUMBER; j++){
                tableOfTrue[i][j] = random.nextInt(2);
            }
        }
    }

    public void printTable() {
        for(int i = 0; i < ROWS_NUMBER; i++) {
            for(int j = 0; j < COLS_NUMBER; j++){
                System.out.print(tableOfTrue[i][j] + " ");
            }
            System.out.println();
        }
    }

    public String makeDNF() {
        result = "";
        for(int i = 0; i < ROWS_NUMBER; i++) {
            if(tableOfTrue[i][COLS_NUMBER - 1] == 1) {
                if(result != "") {
                    result += " + ";
                }
                result += "(";
                 for(int j = 0; j < COLS_NUMBER - 1; j++){
                    if(tableOfTrue[i][j] == 0) {
                        result += "(-" + LETTERS[j] + ")";
                    } else {
                        result += LETTERS[j];
                    }
                    if(j < COLS_NUMBER - 2) {
                        result += " * ";
                    }
                }
                result += ")";
            }
        }
        return result;
    }

    public String makeCNF() {
        result = "";
        for(int i = 0; i < ROWS_NUMBER; i++) {
            if(tableOfTrue[i][COLS_NUMBER - 1] == 0) {
                if(result != "") {
                    result += " * ";
                }
                result += "(";
                for(int j = 0; j < COLS_NUMBER - 1; j++){
                    if(tableOfTrue[i][j] == 1) {
                        result += "(-" + LETTERS[j] + ")";
                    } else {
                        result += LETTERS[j];
                    }
                    if(j < COLS_NUMBER - 2) {
                        result += " + ";
                    }
                }
                result += ")";
            }
        }
        return result;
    }

    public void drawTable(Graphics g) {
        g.setFont(new Font("Courier", Font.BOLD, 18));
        g.drawString("A", X + CELL_WIDTH * 1 + (CELL_WIDTH / 3), (Y + (0 * CELL_HEIGHT)) + (CELL_HEIGHT));
        g.drawString("B", X + CELL_WIDTH * 2 + (CELL_WIDTH / 3), (Y + (0 * CELL_HEIGHT)) + (CELL_HEIGHT));
        g.drawString("C", X + CELL_WIDTH * 3 + (CELL_WIDTH / 3), (Y + (0 * CELL_HEIGHT)) + (CELL_HEIGHT));
        g.drawString("D", X + CELL_WIDTH * 4 + (CELL_WIDTH / 3), (Y + (0 * CELL_HEIGHT)) + (CELL_HEIGHT));
        g.drawString("F", X + CELL_WIDTH * 5 + (CELL_WIDTH / 3), (Y + (0 * CELL_HEIGHT)) + (CELL_HEIGHT));
        for(int i = 0; i < ROWS_NUMBERS; i++) {
            if (i > 0) {
                g.drawString(Integer.toString(i), X + CELL_WIDTH * 0 + (CELL_WIDTH / 3), (Y + (i * CELL_HEIGHT)) + (CELL_HEIGHT));
            }
            for(int j = 0; j < COLS_NUMBERS; j++) {
                g.drawLine(X + CELL_WIDTH * j, (Y + (i * CELL_HEIGHT)), X + CELL_WIDTH * (j + 1), (Y + (i * CELL_HEIGHT)));
                g.drawLine(X + CELL_WIDTH * j, (Y + (i * CELL_HEIGHT)) + CELL_HEIGHT, X + CELL_WIDTH * (j + 1), (Y + (i * CELL_HEIGHT)) + CELL_HEIGHT);
                g.drawLine(X + CELL_WIDTH * j, (Y + (i * CELL_HEIGHT)), X + CELL_WIDTH * j, (Y + (i * CELL_HEIGHT)) + CELL_HEIGHT);
                g.drawLine(X + CELL_WIDTH * (j + 1), (Y + (i * CELL_HEIGHT)), X + CELL_WIDTH * (j + 1), (Y + (i * CELL_HEIGHT)) + CELL_HEIGHT);

                if(i > 0 && j > 0) {
                    g.drawString(Integer.toString(tableOfTrue[i - 1][j - 1]), X + CELL_WIDTH * j + (CELL_WIDTH / 3), (Y + (i * CELL_HEIGHT)) + (CELL_HEIGHT));
                }
            }
        }
    }

    public void drawResult(Graphics g) {
        g.drawString(result, 5,5);
        result = "";
    }
}

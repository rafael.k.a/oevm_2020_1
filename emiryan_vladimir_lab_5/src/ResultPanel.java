import javax.swing.*;
import java.awt.*;

public class ResultPanel extends JPanel {

    Minimize minimize;
    String result;
    public ResultPanel(Minimize minimize) {
        this.minimize = minimize;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.drawString(minimize.result, 5,15);
    }
}

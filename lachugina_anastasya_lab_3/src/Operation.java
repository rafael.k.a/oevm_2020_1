import java.util.Arrays;

public class Operation {

    public String add(char[] firstOperand, char[] secondOperand) {
        StringBuilder sb = new StringBuilder();
        char currentDigit;
        int discharge = 0;
        for (int i = firstOperand.length - 1, j = secondOperand.length - 1; i >= 0 || j >= 0 || discharge == 1; --i, --j) {
            int firstDigit;
            int secondDigit;
            if (i < 0) {
                firstDigit = 0;
            } else if (firstOperand[i] == '0') {
                firstDigit = 0;
            } else {
                firstDigit = 1;
            }
            if (j < 0) {
                secondDigit = 0;
            } else if (secondOperand[j] == '0') {
                secondDigit = 0;
            } else {
                secondDigit = 1;
            }
            int currentSum = firstDigit + secondDigit + discharge;
            if (currentSum == 0) {
                currentDigit = '0';
                discharge = 0;
            } else if (currentSum == 1) {
                currentDigit = '1';
                discharge = 0;
            } else if (currentSum == 2) {
                currentDigit = '0';
                discharge = 1;
            } else {
                currentDigit = '1';
                discharge = 1;
            }
            sb.insert(0, currentDigit);
        }
        return sb.toString();
    }

    public String subtract(char[] firstOperand, char[] secondOperand) {
        boolean sign = false;
        if (Converter.convertTo10(firstOperand, 2) < Converter.convertTo10(secondOperand, 2)) {
            sign = true;
            char[] tmp = firstOperand;
            firstOperand = secondOperand;
            secondOperand = tmp;
        } else if (Converter.convertTo10(firstOperand, 2) == Converter.convertTo10(secondOperand, 2)) {
            return "0";
        }
        StringBuilder result = new StringBuilder();
        int differenceInLength = firstOperand.length - secondOperand.length;
        char[] reverseNumber = new char[firstOperand.length];

        for (int i = 0; i < reverseNumber.length; i++) {
            if (i < differenceInLength) {
                reverseNumber[i] = '1';
            } else if (secondOperand[i - differenceInLength] == '0') {
                reverseNumber[i] = '1';
            } else {
                reverseNumber[i] = '0';
            }
        }
        char[] addCode = add(reverseNumber, new char[]{'1'}).toCharArray();
        char[] addSum = add(addCode, firstOperand).toCharArray();

        boolean additionalZero = true;
        for (int i = 1; i < addSum.length; i++) {
            if (addSum[i] == '1') {
                additionalZero = false;
            } else if (additionalZero) {
                continue;
            }
            result.append(addSum[i]);
        }
        if (sign) result.insert(0, '-');
        return result.toString();
    }

    public String divide(char[] firstOperand, char[] secondOperand) {
        int result = 0;
        if (Converter.convertTo10(firstOperand, 2) < Converter.convertTo10(secondOperand, 2)) return "0";
        while (Converter.convertTo10(firstOperand, 2) >= Converter.convertTo10(secondOperand, 2) && !Arrays.equals(firstOperand, new char[]{'0'})) {
            firstOperand = (subtract(firstOperand, secondOperand).toCharArray());
            result++;
        }
        return Converter.convertToFinal(result);
    }

    public String multiply(char[] firstOperand, char[] secondOperand) {
        char[] result = {};
        for (int i = 0; i < Converter.convertTo10(secondOperand, 2); i++) {
            result = (add(result, firstOperand).toCharArray());
        }
        return new String(result);
    }
}

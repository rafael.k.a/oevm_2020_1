import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {
            System.out.print("Введите исходную систему счисления (от 2 до 16): ");
            int initialSN = scanner.nextInt();
            if (initialSN > 16 || initialSN < 2) throw new NotValidateException();
            System.out.print("Введите первый операнд: ");
            char[] firstOperandInInitialSN = scanner.next().toUpperCase().toCharArray();
            System.out.print("Введите второй операнд: ");
            char[] secondOperandInInitialSN = scanner.next().toUpperCase().toCharArray();
            int code = 0;
            while (code == 0) {
                System.out.println("Введите код операции (1 - сложение, 2 - вычитание, 3 - умножение, 4 - деление): ");
                try {
                    code = scanner.nextInt();
                    if (code < 1 || code > 4) throw new NumberFormatException();
                } catch (NumberFormatException e) {
                    System.out.println("Неверный формат данных. Введите число от 1 до 4");
                }
            }
            char[] firstOperand = Converter.convert(initialSN, firstOperandInInitialSN).toCharArray();
            char[] secondOperand = Converter.convert(initialSN, secondOperandInInitialSN).toCharArray();
            Operation operation = new Operation();
            String result = "";
            switch (code) {
                case 1:
                    result = operation.add(firstOperand, secondOperand);
                    break;
                case 2:
                    result = operation.subtract(firstOperand, secondOperand);
                    break;
                case 3:
                    result = operation.multiply(firstOperand, secondOperand);
                    break;
                default:
                    result = operation.divide(firstOperand, secondOperand);
                    break;
            }
            System.out.println("Результат вычисления: " + result);
        } catch (NotValidateException e) {
            System.out.println(e.getMessage());
        }
    }
}

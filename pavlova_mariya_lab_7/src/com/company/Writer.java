package com.company;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class Writer {

    ArrayList<String> arrayVariables = new ArrayList<>();
    HashMap<String, String> stringVariables = new HashMap<>();

    String starting = "format PE console\n" +
            "\n" +
            "entry Start\n" +
            "\n" +
            "include 'win32a.inc'\n" +
            "\n" +
            "section '.data' data readable writable\n";

    String variables = "\tspaceStr db '%d', 0\n" +
            "\tstrDop db '%d', 0ah, 0\n";

    String code = "section '.code' code readable executable\n" +
            "\n" +
            "\tStart:\n";

    String finishing = "\tfinish:\n" +
            "\n" +
            "\t\tcall [getch]\n" +
            "\n" +
            "\t\tcall [ExitProcess]\n" +
            "\n" +
            "section '.idata' import data readable\n" +
            "\n" +
            "\tlibrary kernel, 'kernel32.dll',\\\n" +
            "\t\tmsvcrt, 'msvcrt.dll'\n" +
            "\n" +
            "\timport kernel,\\\n" +
            "\t\tExitProcess, 'ExitProcess'\n" +
            "\n" +
            "\timport msvcrt,\\\n" +
            "\t\tprintf, 'printf',\\\n" +
            "\t\tscanf, 'scanf',\\\n" +
            "\t\tgetch, '_getch'";

    public void write() {
        addVariables();

        try (FileWriter myWriter = new FileWriter("OEVM_lab7.ASM", false))
        {
            myWriter.write(starting);
            myWriter.write(variables);
            myWriter.write(code);
            myWriter.write(finishing);
            myWriter.flush();
        }
        catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void addToVariables(ArrayList<String> arrayList) {
        arrayVariables = arrayList;
    }

    public void addVariables() {
        for (String string : arrayVariables) {
            variables += "\t" + string + " dd ?\n";
        }

        for (String key: stringVariables.keySet()) {
            variables += "\t" + key + " db '" + stringVariables.get(key) + "', 0\n";
        }
    }

    public void addReadLn(String string) {
        if (arrayVariables.contains(string)) {
            code += "\t\tpush " + string + "\n" +
                    "\t\tpush spaceStr\n" +
                    "\t\tcall [scanf]\n\n";
        }
    }

    public void addWrite(String string) {
        stringVariables.put("string" + (stringVariables.size() + 1), string);
        code += "\t\tpush " + "string" + stringVariables.size() + "\n" +
                "\t\tcall [printf]\n";
    }

    public void addOperation(String result, String firstNumber, String operator, String secondNumber) {
        if (arrayVariables.contains(result) && arrayVariables.contains(firstNumber) && arrayVariables.contains(secondNumber)) {
            switch (operator) {
                case "+":
                    code += "\t\tmov ecx, [" +  firstNumber + "]\n" +
                            "\t\tadd ecx, [" +  secondNumber + "]\n" +
                            "\t\tmov [" + result + "], ecx\n";
                    break;
                case "-":
                    code += "\t\tmov ecx, [" +  firstNumber + "]\n" +
                            "\t\tsub ecx, [" +  secondNumber + "]\n" +
                            "\t\tmov [" + result + "], ecx\n";
                    break;
                case "*":
                    code += "\t\tmov ecx, [" +  firstNumber + "]\n" +
                            "\t\timul ecx, [" +  secondNumber + "]\n" +
                            "\t\tmov [" + result + "], ecx\n";
                    break;
                case "/":
                    code += "\t\tmov eax, [" +  firstNumber + "]\n" +
                            "\t\tcdq\n" +
                            "\t\tmov ecx, [" +  secondNumber + "]\n" +
                            "\t\tdiv ecx\n" +
                            "\t\tmov [" + result + "], eax\n";
                    break;
            }
        }
    }

    public void addWriteLn(String string) {
        if (arrayVariables.contains(string)) {
            code += "\t\tpush [" + string + "]\n" +
                    "\t\tpush strDop\n" +
                    "\t\tcall [printf]\n\n";
        }
    }
}

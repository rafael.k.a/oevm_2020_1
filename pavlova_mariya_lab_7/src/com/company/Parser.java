package com.company;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {

    private ArrayList<String> stringVariables = new ArrayList<>();
    private ArrayList<String> stringCode = new ArrayList<>();
    private ArrayList<String> variables = new ArrayList<>();
    private String blockData;
    private String blockCode;
    private Writer myWriter;
    private String currentString;

    public Parser(String currentString, Writer writer) {
        this.currentString = currentString;
        this.myWriter = writer;
    }

    public void createCode(){
        Pattern ptrReaderLn = Pattern.compile("readln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");
        Pattern ptrOper = Pattern.compile("([a-z][a-zA-Z0-9]*)[\\s]*[\\s]*:=[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*([+\\-*/])[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*;");
        Pattern ptrWriter = Pattern.compile("write\\('(.*)'\\);");
        Pattern ptrWriterLn = Pattern.compile("writeln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");

        for (String string : stringCode) {
            if (string.matches(ptrWriter.toString())) {
                Matcher matcherCode = ptrWriter.matcher(string);
                if (matcherCode.find()) {
                    myWriter.addWrite(matcherCode.group(1));
                }
            }
            else if (string.matches(ptrReaderLn.toString())) {
                Matcher matcherCode = ptrReaderLn.matcher(string);

                if (matcherCode.find()) {
                    myWriter.addReadLn(matcherCode.group(1));
                }
            }
            else if (string.matches(ptrOper.toString())) {
                Matcher matcherCode = ptrOper.matcher(string);

                if (matcherCode.find()) {
                    myWriter.addOperation(matcherCode.group(1), matcherCode.group(2), matcherCode.group(3), matcherCode.group(4));
                }
            }
            else if (string.matches(ptrWriterLn.toString())) {
                Matcher matcherCode = ptrWriterLn.matcher(string);
                if (matcherCode.find()) {
                    myWriter.addWriteLn(matcherCode.group(1));
                }
            }
        }
    }

    public void parse() {
        Pattern patternVariables = Pattern.compile("(?<=var)[\\s\\S]*(?=begin)");
        Matcher matcherVariables = patternVariables.matcher(currentString);
        while (matcherVariables.find()) {
            blockData = currentString.substring(matcherVariables.start(), matcherVariables.end());
        }
        Pattern patternCode = Pattern.compile("(?<=begin)[\\s\\S]*(?=end.)");
        Matcher matcherCode = patternCode.matcher(currentString);
        while (matcherCode.find()) {
            blockCode = currentString.substring(matcherCode.start(), matcherCode.end());
        }
        parseStringsVariables();
        parseStringsCode();
    }

    public void parseStringsVariables() {
        Pattern patternVariables = Pattern.compile("([a-zA-Z]([a-zA-Z0-9_]*[\\s]*,[\\s]*)*)([a-zA-Z][a-zA-Z0-9_]*)[\\s]*:[\\s]*integer[\\s]*;");
        Matcher matcherVariables = patternVariables.matcher(blockData);

        while (matcherVariables.find()) {
            stringVariables.add(blockData.substring(matcherVariables.start(), matcherVariables.end()));
        }
        parseIntegerVariables();
    }

    public void parseIntegerVariables() {
        Pattern patternVariables = Pattern.compile("[a-zA-Z][a-zA-Z0-9_]*");
        Matcher matcherVariables;
        String var;
        for (String string : stringVariables) {
            matcherVariables = patternVariables.matcher(string);

            while (matcherVariables.find()) {
                var = string.substring(matcherVariables.start(), matcherVariables.end());
                if (!var.equals("integer")) { variables.add(var); }
            }
        }
        myWriter.addToVariables(variables);
    }

    public void parseStringsCode(){
        Pattern stringPattern = Pattern.compile("[a-zA-Z].*;");
        Matcher stringMatcher = stringPattern.matcher(blockCode);
        while (stringMatcher.find()) {
            stringCode.add(blockCode.substring(stringMatcher.start(), stringMatcher.end()));
        }
        createCode();
    }
}


### Павлова Мария ПИбд-22
# Лабораторная работа №7 "Изучение методов трансляции"

#### Задание
Разработать транслятор программ Pascal-FASM. Предусмотреть проверку синтаксиса и семантики исходной программы. Результатом работы транслятора является программа для ассемблера FASM, идентичная по функционалу исходной программе. Полученная программа должна компилироваться и выполняться без ошибок.
Исходная программа на языке программирования Pascal имеет вид:

```
var
	x, y: integer;
	res1, res2, res3, res4: integer;
begin
	write(‘input x: ’); readln(x);
	write(‘input y: ’); readln(y);
	res1 := x + y; write(‘x + y = ’); writeln(res1);
	res2 := x - y; write(‘x - y = ’); writeln(res2);
	res3 := x * y; write(‘x * y = ’); writeln(res3);
	res4 := x / y; write(‘x / y = ’); writeln(res4);
end.
```

#### Запуск
Найти в проекте Main.java и запустить

#### Технологии 
IntelliJ IDEA Community
Java 
Ассемблер FASM
 

#### Видео 
[Открыть](https://www.dropbox.com/s/42pqd46f72rgtv8/7%20%D0%BB%D0%B0%D0%B1%D0%BE%D1%80%D0%B0%D1%82%D0%BE%D1%80%D0%BD%D0%B0%D1%8F%20%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%B0.avi?dl=0)



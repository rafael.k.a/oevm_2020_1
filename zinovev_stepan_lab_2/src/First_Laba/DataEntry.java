package First_Laba;

import java.util.Scanner;

public class DataEntry {
    int[] intCheckOutMassive = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    char[] charCheckOutMassive = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    Scanner scanner = new Scanner(System.in);

    // Исходная сс
    int from;

    // Требуемая сс
    int to;

    // Число
    String value;
    char[] valueInCharMassive;
    int[] valueInIntMassive;

    public void initialization() {
        System.out.println("Введите исходную систему счисления: ");
        from = scanner.nextInt();

        System.out.println("Введите конечную систему счисления: ");
        to = scanner.nextInt();

        System.out.println("Введите число: ");
        value = scanner.next();

        valueInCharMassive = value.toCharArray();
        valueInIntMassive = new int[valueInCharMassive.length];

    }

    public int[] getIntCheckOutMassive() {
        return intCheckOutMassive;
    }

    public char[] getCharCheckOutMassive() {
        return charCheckOutMassive;
    }

    public Scanner getScanner() {
        return scanner;
    }

    public int getFrom() {
        return from;
    }

    public int getTo() {
        return to;
    }

    public String getValue() {
        return value;
    }

    public char[] getValueInCharMassive() {
        return valueInCharMassive;
    }

    public int[] getValueInIntMassive() {
        return valueInIntMassive;
    }
}

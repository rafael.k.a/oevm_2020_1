# Лабораторная работа № 7

## Выполнил студент группы ИСЭбд-21, Ашанин Борис

##### [Видео с примером работы программы](https://youtu.be/gL07mFa-zNs)

###### Задание:

Разработать транслятор программ Pascal-FASM. Предусмотреть
проверку синтаксиса и семантики исходной программы. Результатом
работы транслятора является программа для ассемблера FASM,
идентичная по функционалу исходной программе. Полученная
программа должна компилироваться и выполняться без ошибок.
Исходная программа на языке программирования Pascal имеет вид:

```scss /*
    var
    x, y: integer;
    res1, res2, res3, res4: integer;
    begin
    write(‘input x: ’); readln(x);
    write(‘input y: ’); readln(y);
    res1 := x + y; write(‘x + y = ’); writeln(res1);
    res2 := x - y; write(‘x - y = ’); writeln(res2);
    res3 := x * y; write(‘x * y = ’); writeln(res3);
    res4 := x / y; write(‘x / y = ’); writeln(res4);
    end.
```
##### Результат:

```scss /*
format PE console

entry start

        include 'win32a.inc'

section '.data' data readable writable

        spaceString db '%d', 0
        strSymbol db 'Enter symbol:', 0
        sum db 'X + Y = %d', 0dh, 0ah, 0 
        subtraction  db 'X - Y = %d', 0dh, 0ah, 0
        multiplication db 'X * Y = %d', 0dh, 0ah, 0
        dividing db 'X / Y = %d', 0dh, 0ah, 0
        newLine db '%d', 0ah, 0
        x dd ?
        y dd ?
        res1 dd ?
        res2 dd ?
        res3 dd ?
        res4 dd ?

section '.code' data readable executable

        start:

        push strSymbol
        call [printf]

        push x
        push spaceString
        call [scanf]

        push strSymbol
        call [printf]

        push y
        push spaceString
        call [scanf]

        mov ecx, [x]
        add ecx, [y]
        mov [res1], ecx

        mov ecx, [x]
        sub ecx, [y]
        mov [res2], ecx

        mov ecx, [x]
        imul ecx, [y]
        mov [res3], ecx

        push [res1]
        push sum
        call [printf]

        push [res2]
        push subtraction
        call [printf]

        push [res3]
        push multiplication
        call [printf]

        push [res4]
        push dividing
        call [printf]

        call [getch]
        push NULL
        call[ExitProcess]

section '.idata' import data readable

        library kernel, 'kernel32.dll',\
                msvcrt, 'msvcrt.dll'

        import kernel,\
                ExitProcess, 'ExitProcess'

        import msvcrt,\
                printf, 'printf',\
                scanf, 'scanf',\
                getch, '_getch'

```
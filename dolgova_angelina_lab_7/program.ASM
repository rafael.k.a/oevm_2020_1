format PE console

entry start

	include 'win32a.inc'

section '.data' data readable writable

	spaceString db '%d', 0
	newLine db '%d', 0ah, 0
	x dd ?
	y dd ?
	res1 dd ?
	res2 dd ?
	res3 dd ?
	res4 dd ?

section '.code' data readable executable

	start:

	push x
	push spaceString
	call [scanf]

	push y
	push spaceString
	call [scanf]

	mov ecx, [x]
	add ecx, [y]
	mov [res1], ecx

	push [res1]
	push newLine
	call [printf]

	mov ecx, [x]
	sub ecx, [y]
	mov [res2], ecx

	push [res2]
	push newLine
	call [printf]

	mov ecx, [x]
	imul ecx, [y]
	mov [res3], ecx

	push [res3]
	push newLine
	call [printf]

	push [res4]
	push newLine
	call [printf]

	call [getch]
	push NULL
	call[ExitProcess]

section '.idata' import data readable

	library kernel, 'kernel32.dll',\
		msvcrt, 'msvcrt.dll'

	import kernel,\
		ExitProcess, 'ExitProcess'

	import msvcrt,\
		printf, 'printf',\
		scanf, 'scanf',\
		getch, '_getch'
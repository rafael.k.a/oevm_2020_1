## Лабораторная работа №7 "Изучение методов трансляции"

Выполнила студентка группы **ПИбд-22 Долгова Ангелина**


### Задание:

Разработать транслятор программ Pascal-FASM. Предусмотреть
проверку синтаксиса и семантики исходной программы. Результатом
работы транслятора является программа для ассемблера FASM,
идентичная по функционалу исходной программе. Полученная
программа должна компилироваться и выполняться без ошибок.

---------------------------------------------------------------

**Тесты:**

*Паскаль:*
```
var
x, y: integer;
res1, res2, res3, res4: integer;
begin
write(‘input x: ’); readln(x);
write(‘input y: ’); readln(y);
res1 := x + y; write(‘x + y = ’); writeln(res1);
res2 := x - y; write(‘x - y = ’); writeln(res2);
res3 := x * y; write(‘x * y = ’); writeln(res3);
res4 := x / y; write(‘x / y = ’); writeln(res4);
end.
```

*Ассемблер:*
```
format PE console

entry start

	include 'win32a.inc'

section '.data' data readable writable

	spaceString db '%d', 0
	newLine db '%d', 0ah, 0
	x dd ?
	y dd ?
	res1 dd ?
	res2 dd ?
	res3 dd ?
	res4 dd ?

section '.code' data readable executable

	start:

	push x
	push spaceString
	call [scanf]

	push y
	push spaceString
	call [scanf]

	mov ecx, [x]
	add ecx, [y]
	mov [res1], ecx

	push [res1]
	push newLine
	call [printf]

	mov ecx, [x]
	sub ecx, [y]
	mov [res2], ecx

	push [res2]
	push newLine
	call [printf]

	mov ecx, [x]
	imul ecx, [y]
	mov [res3], ecx

	push [res3]
	push newLine
	call [printf]

	push [res4]
	push newLine
	call [printf]

	call [getch]
	push NULL
	call[ExitProcess]

section '.idata' import data readable

	library kernel, 'kernel32.dll',\
		msvcrt, 'msvcrt.dll'

	import kernel,\
		ExitProcess, 'ExitProcess'

	import msvcrt,\
		printf, 'printf',\
		scanf, 'scanf',\
		getch, '_getch'
```

---------------------------------------------------------------

[Видео c демонстрацией работы](https://goo.su/3eDF)

package com.company;

import java.util.Scanner;

public class Helper {
    void checkBase(String message, Scanner sc, Converter converter, boolean flagIn){
        while (true) {
            System.out.println(message);
            try {
                int number = sc.nextInt();
                if(number<2 || number>16){
                    System.out.println("Неверно! Число должно быть в диапозоне (2-16)!");
                }
                else {
                    //Use correct data
                    if(flagIn) {
                        converter.setBaseIn(number);
                    }
                    else {
                        converter.setBaseOut(number);
                    }
                    break;
                }
            }
            catch (Exception e){
                System.out.println("Вы ввели не число!");
                sc.next();
            }
        }
    }

    void checkNum(String message, Scanner sc, Converter converter){
        while (true) {
            System.out.println(message);
            try {
                converter.setNumIn(sc.next().toUpperCase());
                break;
            }
            catch (Exception e){
                System.out.println("Вы ввели не число!");
                sc.next();
            }
        }
    }
}

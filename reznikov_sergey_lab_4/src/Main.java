import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        BooleanTable table = new BooleanTable();
        char[][] excel = table.getTable();

        for (int i = 0; i < excel.length; i++) {
            for (int k = 0; k < table.ELEMENTS_NUMBER; k++) {
                System.out.print("  " + excel[i][k]);
            }
            System.out.print(" = "+excel[i][table.ELEMENTS_NUMBER]);
            System.out.println();
        }

        System.out.println("1 - СКНФ, 2 - СДНФ, 3 - СКНФ + СДНФ");
        Scanner sc = new Scanner(System.in);
        int choice = sc.nextInt();

        if(choice==1){
            System.out.println(table.getKForm());
        }
        else if (choice==2){
            System.out.println(table.getDForm());
        }
        else if (choice==3){
            System.out.println(table.getKForm());
            System.out.println(table.getDForm());
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab_5;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 *
 * @author Ной
 */
public class Draw extends JPanel{  
    private static String contact;
	    private final int[][] matView;
	    public Draw(int[][] matriz) {
	        matView = matriz;
	    }

	    @Override
	    public void paint(Graphics g) {
	        super.paint(g);
	        MyClassDraw.dTable(matView, contact, g);
	    }
	    public static void setContact(String setContact) {
	        contact = setContact;
	    }  
}

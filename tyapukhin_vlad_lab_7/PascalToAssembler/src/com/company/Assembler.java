package com.company;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class Assembler {

    ArrayList<String> arrayOfVars = new ArrayList<>();
    HashMap<String, String> arrayOfVarsString = new HashMap<>();

    String starting = "format PE console\n" +
            "\n" + "entry start\n" +
            "\n" + "include 'win32a.inc'\n" +
            "\n" + "section '.data' data readable writable\n";

    String variables = "\n\tspaceStr db '%d', 0\n";

    String code = "\nsection '.code' code readable executable\n" +
            "\n" + "\t start:\n";

    String ending = "\t finish:\n" +
            "\n" + "\t\t call [getch]\n" +
            "\n" + "\t\t call [ExitProcess]\n" +
            "\n" + "section '.idata' import data readable\n" +
            "\n" + "\t library kernel, 'kernel32.dll',\\\n" +
            "\t msvcrt, 'msvcrt.dll'\n" +
            "\n" + "\t import kernel,\\\n" +
            "\t ExitProcess, 'ExitProcess'\n" +
            "\n" + "\t import msvcrt,\\\n" +
            "\t printf, 'printf',\\\n" +
            "\t scanf, 'scanf',\\\n" +
            "\t getch, '_getch'";

    public void createCode(){
        addAllVars();

        try(FileWriter writer = new FileWriter("assemblerProgram.ASM", false)) {
            writer.write(starting);
            writer.write(variables);
            writer.write(code);
            writer.write(ending);
            writer.flush();
        } catch(IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void addToVars(ArrayList<String> arrayList) {
        arrayOfVars = arrayList;
    }

    public void addAllVars() {
        for (String string : arrayOfVars) {
            variables += "\t" + string + " dd ?\n";
        }

        for (String key: arrayOfVarsString.keySet()) {
            if(!key.equals("str1") && !key.equals("str2")) {
                variables += "\t" + key + " db '" + arrayOfVarsString.get(key) + "%d', 0ah, 0\n";
            } else {
                variables += "\t" + key + " db '" + arrayOfVarsString.get(key) + "', 0\n";
            }
        }
    }

    public void addToCodeWrite(String string) {
        arrayOfVarsString.put("str" + (arrayOfVarsString.size() + 1), string);
        code += "\t\t push " + "str" + arrayOfVarsString.size() + "\n" +
                "\t\t call [printf]\n\n";
    }

    public void addToCodeRead(String string) {
        if(arrayOfVars.contains(string)){
            code += "\t\t push " + string + "\n" +
                    "\t\t push spaceStr\n" +
                    "\t\t call [scanf]\n\n";
        }
    }

    public void addToCodeOperation(String res, String firstNum, String operator, String secondNum) {
        if (arrayOfVars.contains(res) && arrayOfVars.contains(firstNum) && arrayOfVars.contains(secondNum)) {
            switch (operator) {
                case "+":
                    code += "\t\t mov ecx, [" +  firstNum + "]\n" +
                            "\t\t add ecx, [" +  secondNum + "]\n" +
                            "\t\t push ecx\n";
                    break;
                case "-":
                    code += "\t\t mov ecx, [" +  firstNum + "]\n" +
                            "\t\t sub ecx, [" +  secondNum + "]\n" +
                            "\t\t push ecx\n";
                    break;
                case "*":
                    code += "\t\t mov ecx, [" +  firstNum + "]\n" +
                            "\t\t imul ecx, [" +  secondNum + "]\n" +
                            "\t\t push ecx\n";
                    break;
                case "/":
                    code += "\t\t mov eax, [" +  firstNum + "]\n" +
                            "\t\t mov ecx, [" +  secondNum + "]\n" +
                            "\t\t div ecx\n" +
                            "\t\t push eax\n";
                    break;
            }
        }
    }
}
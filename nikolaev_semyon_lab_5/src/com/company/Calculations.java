public class Calculations {
    private int sizeI = 16;

    public String reduceFunction(String type, int[][] array) {
        StringBuilder result = new StringBuilder();
        switch (type) {
            case "DNF":
                for (int i = 0; i < sizeI; i++) {
                    result.append(DNF(i, array));
                }
                result.setLength(result.length() - 3);
                return result.toString();
            case "KNF":
                for (int i = 0; i < sizeI; i++) {
                    result.append(KNF(i, array));
                }
                result.setLength(result.length() - 3);
                return result.toString();
        }
        return "";
    }

    private String DNF(int step, int[][] array) {
        int X1 = 0;
        int X4 = 3;
        int F = 4;

        StringBuilder strBuilder = new StringBuilder();

        if (array[step][F] == 1) {
            strBuilder.append("( ");
            for (int j = X1; j < X4; j++) {
                if (array[step][j] == 1) {
                    strBuilder.append("X").append(j + 1).append(" * ");
                } else {
                    strBuilder.append("-X").append(j + 1).append(" * ");
                }
            }
            if (array[step][X4] == 1) {
                strBuilder.append("X4");
            } else {
                strBuilder.append("-X4");
            }
            strBuilder.append(" ) + ");
            return strBuilder.append("\n").toString();
        }
        return "";
    }

    private String KNF(int step, int[][] array) {
        int X1 = 0;
        int X4 = 3;
        int F = 4;

        StringBuilder strBuilder = new StringBuilder();

        if (array[step][F] == 0) {
            strBuilder.append("( ");
            for (int j = X1; j < X4; j++) {
                if (array[step][j] == 0) {
                    strBuilder.append("X").append(j + 1).append(" + ");
                } else {
                    strBuilder.append("-X").append(j + 1).append(" + ");
                }
            }
            if (array[step][X4] == 0) {
                strBuilder.append("X4");
            } else {
                strBuilder.append("-X4");
            }
            strBuilder.append(" ) * ");
            return strBuilder.append("\n").toString();
        }
        return "";
    }
}
import java.util.Scanner;

public class Main {

    public static void main(String[] args){
        Methods methods = new Methods();
        Scanner in = new Scanner(System.in);
        System.out.print("Введите начальную систему счисления: ");
        methods.setSS1(in.nextInt());
        System.out.print("Введите конечную систему счисления: ");
        methods.setSS2(in.nextInt());
        System.out.print("Введите число: ");
        methods.setNumber(in.next());
        System.out.print(methods.program());
    }
}
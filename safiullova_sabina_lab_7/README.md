# ИСЭбд-21 Сафиуллова Сабина
#### *Лабораторная работа №7*
Видео тут: https://drive.google.com/file/d/16nyLmy0Cf0mvK-tbSqgfuA0aOXq3zl5f/view?usp=sharing

#### Запуск:

Чтобы запустить программу в рамках лабароторной работы №7, необходимо перейти по пути
> *Lab_7\src\com\company*

и открыть файл *Main.java*

#### Технологии:

В качестве языка программирования был выбран язык ***Java***

В качестве среды разработки ***IntelliJ IDEA***

#### Зачем? 

Эта программа является транслятором Pascal-FASM. Результатом
работы транслятора является программа для ассемблера FASM,
идентичная по функционалу исходной программе
Эта программа создана для перевода целого положительного числа из одной системы счисления в другую.

Исходная программа на языке программирования Pascal имеет вид:
```
var
    x, y: integer;
    res1, res2, res3, res4: integer;
begin
    write(‘input x: ’); readln(x);
    write(‘input y: ’); readln(y);
    res1 := x + y; write(‘x + y = ’); writeln(res1);
    res2 := x - y; write(‘x - y = ’); writeln(res2);
    res3 := x * y; write(‘x * y = ’); writeln(res3);
    res4 := x / y; write(‘x / y = ’); writeln(res4);
end.
```

После запуска программы в случае успешного выполнения выводится "Process finished with exit code 0". Перейдя в корневую папку можно увидеть сгенерированный файл с расширением "ProgramAssembler.ASM".

 Запустить его можно в среде FASM и проверить работу программы.

#### Реализация
Имеется 4 класса с разным функционалом: 
- Main
- ReaderPascal
- CodeDivider
- WriterAssembler

Класс Main:
Cоздаются экземпляры указанных классов и вызываются методы.

Класс ReaderPascal:
В первую очередь программа обращается к данному классу, здесь происходит считывание исходного файла с названием "Program.pas".

Класс CodeDivider:
Здесь описаны методы, которые разделяют исходный код по "группам": переменные, тело. 

Класс WriterAssembler:
Имеются переменные типа string, в котором хранятся "шаблоны" секций программы на ассемблере. Метод, который записывает исходные результаты в файл "ProgramAssembler.ASM".

#### Что получается?

файл "ProgramAssembler.ASM" выглядит следующим образом:

```
format PE console
entry start

include 'win32a.inc'

section '.idata' import data readable

        library kernel, 'kernel32.dll',\
                msvcrt, 'msvcrt.dll'

        import kernel,\
                ExitProcess, 'ExitProcess'

        import msvcrt, printf, 'printf',\
                scanf, 'scanf',\
                getch, '_getch'

section '.data' data readable writable

        spaceStr db '%d', 0
        dopStr db '%d', 0ah, 0

        x dd ?
        y dd ?
        res1 dd ?
        res2 dd ?
        res3 dd ?
        res4 dd ?
        str5 db 'x * y = ', 0
        str6 db 'x / y = ', 0
        str3 db 'x + y = ', 0
        str4 db 'x - y = ', 0
        str1 db 'input x: ', 0
        str2 db 'input y: ', 0

section '.code' code readable executable

        start:
                push str1
                call [printf]
                push x
                push spaceStr
                call [scanf]

                push str2
                call [printf]
                push y
                push spaceStr
                call [scanf]

                mov ecx, [x]
                add ecx, [y]
                mov [res1], ecx
                push str3
                call [printf]
                push [res1]
                push dopStr
                call [printf]

                mov ecx, [x]
                sub ecx, [y]
                mov [res2], ecx
                push str4
                call [printf]
                push [res2]
                push dopStr
                call [printf]

                mov ecx, [x]
                imul ecx, [y]
                mov [res3], ecx
                push str5
                call [printf]
                push [res3]
                push dopStr
                call [printf]

                mov eax, [x]
                cdq
                mov ecx, [y]
                idiv ecx
                mov [res4], eax
                push str6
                call [printf]
                push [res4]
                push dopStr
                call [printf]


                call [getch]

                push 0
                call [ExitProcess] 
    ```


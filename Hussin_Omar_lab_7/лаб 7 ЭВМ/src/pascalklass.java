import java.io.FileReader;
import java.io.IOException;

public class pascalklass {
	public String read() {
		StringBuilder str = new StringBuilder();

		try {
			FileReader reader = new FileReader("Pascal.pas");
			int ch;
			while ((ch = reader.read()) != -1) {

				str.append((char)ch);
			}
			reader.close();
		}

		catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

		return str.toString().replaceAll(";", ";\n");
	}

}

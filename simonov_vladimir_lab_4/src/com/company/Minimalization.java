package com.company;

public class Minimalization {

    private StringBuffer buffer;
    private TruthTable truthT;

    //ДНФ
    public void convertToDNF(int[][] array) {
        truthT = new TruthTable();
        buffer = new StringBuffer();
        buffer.append("(");
        for (int i = 0; i < truthT.getCountOfStrings(); i++) {
            if (array[i][truthT.getCountOfColumns() - 1] == 1) {
                for (int j = 0; j < truthT.getCountOfColumns() - 1; j++) {
                    if (array[i][j] == 0) {
                        if (j == truthT.getCountOfColumns() - 2) {
                            buffer.append("-x").append(j + 1);
                        } else {
                            buffer.append("-x").append(j + 1).append(" * ");
                        }
                    }
                    if (array[i][j] == 1) {
                        if (j == truthT.getCountOfColumns() - 2) {
                            buffer.append("x").append(j + 1);
                        } else {
                            buffer.append("x").append(j + 1).append(" * ");
                        }
                    }
                }
                if (i == truthT.getCountOfStrings() - 1) {
                    buffer.append(" )");
                } else {
                    buffer.append(" ) + ").append(" (");
                }
            }
        }
        System.out.println(buffer);
    }

    //КНФ
    public void convertToCNF(int[][] array) {
        truthT = new TruthTable();
        buffer = new StringBuffer();
        buffer.append("(");
        for (int i = 0; i < truthT.getCountOfStrings(); i++) {
            if (array[i][truthT.getCountOfColumns() - 1] == 0) {
                for (int j = 0; j < truthT.getCountOfColumns() - 1; j++) {
                    if (array[i][j] == 0) {
                        if (j == truthT.getCountOfColumns() - 2) {
                            buffer.append("x").append(j + 1);
                        } else {
                            buffer.append("x").append(j + 1).append(" + ");
                        }
                    }
                    if (array[i][j] == 1) {
                        if (j == truthT.getCountOfColumns() - 2) {
                            buffer.append("-x").append(j + 1);
                        } else {
                            buffer.append("-x").append(j + 1).append(" + ");
                        }
                    }
                }
                if (i == truthT.getCountOfStrings() - 1) {
                    buffer.append(" )");
                } else {
                    buffer.append(" ) * ").append(" ( ");
                }
            }
        }
        System.out.println(buffer);
    }
}

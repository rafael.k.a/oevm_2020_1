package com.company;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Minimalization minimalization = new Minimalization();
        TruthTable truthTable = new TruthTable();
        System.out.println("x1 x2 x3 x4 F");
        truthTable.printTruthTable();
        int choise = 0;
        while (choise < 1 || choise > 2) {
            System.out.println("\n Выберите приведение: ");
            System.out.println("  1. Дизъюктивная нормальная форма");
            System.out.println("  2. Конъюктивная нормальная форма");
            choise = sc.nextInt();
        }
        if (choise == 1) {
            minimalization.convertToDNF(truthTable.truthT);
        }
        if (choise == 2) {
            minimalization.convertToCNF(truthTable.truthT);
        }
    }
}

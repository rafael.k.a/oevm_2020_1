import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class MainForm {
    private final JTextArea _tbResult = new JTextArea();
    private final JButton _btnDNF = new JButton("ДНФ");
    private final JButton _btnKNF = new JButton("КНФ");

    private final int[][] _generatedArray = DNFandKNF.generateArray();
    private final JPanel _tableVisualization = new TableVisualization(_generatedArray);

    public static boolean KNF;
    public static boolean DNF;

    public MainForm() {
        setParams();
    }

    public static void main(String[] args) {
        MainForm form = new MainForm();
    }

    private void setParams() {
        //параметры формы
        JFrame _form = new JFrame();
        _form.setTitle("Визуализация ДНФ и КНФ");
        _form.setBounds(100, 100, 400, 600);
        _form.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        _form.getContentPane().setLayout(null);
        _form.setVisible(true);

        //параметры таблицы
        _tableVisualization.setBounds(0, 0, 180, 525);
        _form.getContentPane().add(_tableVisualization);

        //параметры textBox'a с результатом
        _tbResult.setBounds(220, 10, 200, 600);
        _tbResult.setEditable(false);
        _form.getContentPane().add(_tbResult);

        //параметры кнопки _btnDNF
        _btnDNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                _btnDNF.setEnabled(false);
                _btnKNF.setEnabled(false);

                DNF = true;
                _tbResult.setText((DNFandKNF.DNF(_generatedArray)));
                _tableVisualization.repaint();
            }
        });
        _btnDNF.setBounds(10, 525, 65, 30);
        _form.getContentPane().add(_btnDNF);

        //параметры кнопки _btnKNF
        _btnKNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                _btnKNF.setEnabled(false);
                _btnDNF.setEnabled(false);

                KNF = true;
                _tbResult.setText((DNFandKNF.KNF(_generatedArray)));
                _tableVisualization.repaint();
            }
        });
        _btnKNF.setBounds(95, 525, 65, 30);
        _form.getContentPane().add(_btnKNF);
    }
}

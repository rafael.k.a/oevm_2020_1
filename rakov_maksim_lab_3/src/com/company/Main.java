package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Exception {
        System.out.print("Введите систему счисления: ");
        Scanner scanner = new Scanner(System.in);
        int notation = scanner.nextInt();
        System.out.print("Введите первое число: ");
        Scanner scanner1 = new Scanner(System.in);
        char[] firstNumber = scanner1.nextLine().toCharArray();
        System.out.print("Введите второе число: ");
        Scanner scanner2 = new Scanner(System.in);
        char[] secondNumber = scanner2.nextLine().toCharArray();
        System.out.print("Введите арифметическую операцию: (возможные операции следующие : +, -, *, /) ");
        char operation = (char) System.in.read();

        long firstDecimal = Helper.convertToDecimal(firstNumber, notation);
        long secondDecimal = Helper.convertToDecimal(secondNumber, notation);
        char[] firstBinary = (Helper.convertToBinary(firstDecimal)).toCharArray();
        char[] secondBinary = (Helper.convertToBinary(secondDecimal)).toCharArray();

        if (operation == '+') {
            System.out.println(BinaryArithmetic.sum(firstBinary, secondBinary));
        }
        if (operation == '-') {
            if (firstDecimal > secondDecimal) {
                System.out.println(BinaryArithmetic.subtract(firstBinary, secondBinary));
            }
            else {
                System.out.println("Поменяйте числа местами");
            }
        }
        if (operation == '*') {
            System.out.println(BinaryArithmetic.multiply(firstBinary, secondBinary));
        }
        if (operation == '/') {

            if (firstDecimal > secondDecimal) {
                System.out.println(BinaryArithmetic.divide(firstBinary, secondBinary));
            }
            else {
                System.out.println("Поменяйте числа местами ");
            };
        }
    }
}

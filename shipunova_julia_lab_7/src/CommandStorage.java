import java.util.ArrayList;

public class CommandStorage {
    private final ArrayList<String> commandList;
    private final ArrayList<String> varList;
    private final ArrayList<String> commandsToReadList;
    private final ArrayList<String> commandsToWriteList;
    private final ArrayList<String> commandsToWritelnList;
    private final ArrayList<String> commandsToMathList;

    public CommandStorage() {
        commandList = new ArrayList<>();
        varList = new ArrayList<>();
        commandsToReadList = new ArrayList<>();
        commandsToWriteList = new ArrayList<>();
        commandsToWritelnList = new ArrayList<>();
        commandsToMathList = new ArrayList<>();
    }

    public void addVar(String var) {
        varList.add(var);
    }

    public String removeVar(int index) {
        return varList.remove(index);
    }

    public boolean hasVar() {
        return !varList.isEmpty();
    }

    public void addCommandToRead(String var) {
        commandsToReadList.add(var);
        commandList.add("read");
    }

    public void addCommandToWrite(String text) {
        commandsToWriteList.add(text);
        commandList.add("write");
    }

    public void addCommandToWriteln(String text) {
        commandsToWritelnList.add(text);
        commandList.add("writeln");
    }

    public void addCommandToMath(String operation) {
        commandsToMathList.add(operation);
        commandList.add("math");
    }

    public boolean hasCommand() {
        return !commandList.isEmpty();
    }

    public String removeCommand() {
        return commandList.remove(0);
    }

    public String removeReadCommand() {
        return commandsToReadList.remove(0);
    }

    public String removeWriteCommand() {
        return commandsToWriteList.remove(0);
    }

    public String removeWritelnCommand() {
        return commandsToWritelnList.remove(0);
    }

    public String removeMathsCommand() {
        return commandsToMathList.remove(0);
    }
}

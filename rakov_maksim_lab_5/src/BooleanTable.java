import java.awt.*;

public class BooleanTable {

    char[][] table;
    public final int item_numbers = 4;
    final int size_string = 40;
    final int size_column = 60;
    final private char[] words = {'a', 'b', 'c', 'd', 'e', 'f', 'g'};

    private char colorField = '2';

    public int checkXSize() {
        int field =(item_numbers+1)*size_column;
        return field+10;
    }

    public int checkYSize() {
        int field = (int)(Math.pow(2, item_numbers)+1)*size_string;
        return field+10;
    }

    public void changeNumbers(){
        table = new char[(int) Math.pow(2, item_numbers)][];

        for (int i = 0; i < table.length; i++) {
            table[i] = new char[item_numbers + 1];
            for (int k = 0; k < item_numbers + 1; k++) {
                String tmp = "" + (int) (Math.random() * 2);
                table[i][k] = tmp.toCharArray()[0];
            }
        }
        colorField='2';
    }

    public BooleanTable() {
        table = new char[(int) Math.pow(2, item_numbers)][];

        for (int i = 0; i < table.length; i++) {
            table[i] = new char[item_numbers + 1];
            for (int k = 0; k < item_numbers + 1; k++) {
                String tmp = "" + (int) (Math.random() * 2);
                table[i][k] = tmp.toCharArray()[0];
            }
        }
    }

    public String makeKForm() {
        StringBuffer sb = new StringBuffer("");
        sb.append("f(");
        for(int i=0;i<item_numbers;i++){
            sb.append(words[i] + ", ");
        }
        sb.delete(sb.length()-2,sb.length());
        sb.append(") = \n");

        for (int i = 0; i < table.length; i++) {
            if (table[i][item_numbers] == '0') {
                sb.append("(");
                for (int k = 0; k < item_numbers; k++) {
                    if (table[i][k] == '1') {
                        sb.append("-" + words[k]);
                    } else {
                        sb.append(words[k]);
                    }
                    if (k != item_numbers - 1) sb.append(" + ");
                }
                sb.append(") * \n * ");
            }
        }
        sb.delete(sb.length() - 6, sb.length());
        return sb.toString();
    }

    public String makeDForm() {
        StringBuffer sb = new StringBuffer("");
        sb.append("f(");
        for(int i=0;i<item_numbers;i++){
            sb.append(words[i] + ", ");
        }
        sb.delete(sb.length()-2,sb.length());
        sb.append(") = \n");

        for (int i = 0; i < table.length; i++) {
            if (table[i][item_numbers] == '1') {
                sb.append("(");
                for (int k = 0; k < item_numbers; k++) {
                    if (table[i][k] == '0') {
                        sb.append("-" + words[k]);
                    } else {
                        sb.append(words[k]);
                    }
                    if (k != item_numbers - 1) sb.append(" * ");
                }
                sb.append(") + \n + ");
            }
        }

        sb.delete(sb.length() - 6, sb.length());
        return sb.toString();
    }

    public void draw(Graphics g) {
        g.setColor(Color.BLACK);
        g.setFont(new Font("Courier", Font.BOLD + Font.ITALIC, 16));
        int i = 0;
        while(i<item_numbers){
            g.drawRect(i * size_column, 0, size_column, size_string);
            i++;
        }
        g.drawRect(i * size_column, 0, size_column, size_string);

        for (int k = 0; k < table.length; k++) {
            for (int t = 0; t < table[k].length; t++) {
                g.drawRect(t * size_column, size_string+k*size_string, size_column, size_string);
            }
        }

        g.setColor(Color.GREEN);
        for (int k = 0; k < table.length; k++) {
            if(table[k][item_numbers]==colorField) {
                for (int t = 0; t < table[k].length; t++) {
                    g.drawRect(t * size_column, size_string + k * size_string, size_column, size_string);
                }
            }
        }

        g.setColor(Color.BLACK);
        g.setFont(new Font("Courier", Font.BOLD + Font.ITALIC, 16));
        i = 0;
        while(i<item_numbers){
            g.drawString("" + words[i], (int) (i * size_column + 0.5 * size_column), (int) size_string/2);
            i++;
        }
        g.drawString("" + words[i], (int) (i * size_column + 0.5 * size_column), (int) size_string/2);

        for (int k = 0; k < table.length; k++) {
            for (int t = 0; t < table[k].length; t++) {
                g.drawString("" + table[k][t], (int) (t * size_column + 0.5 * size_column), (int) (k+1)*size_string+size_string/2);
            }
        }

    }

    public void formKColor() {
        colorField='0';
    }

    public void formDColor() {
        colorField='1';
    }
}

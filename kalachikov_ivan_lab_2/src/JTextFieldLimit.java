package src;

import javax.swing.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;

public class JTextFieldLimit extends JTextField {
    private final int limit;
    private final int status;

    public JTextFieldLimit(int limit, int status) {
        super();
        this.limit = limit;
        this.status = status;
    }

    @Override
    protected Document createDefaultModel() {
        return new LimitDocument();
    }

    private class LimitDocument extends PlainDocument {

        @Override
        public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {
            if (str == null) return;

            String DIGITS = "\\d+";

            if (status == 1 && (getLength() + str.length()) <= limit && str.matches(DIGITS)) {
                super.insertString(offset, str, attr);
            }
            else if (status == 2 && (getLength() + str.length()) <= limit) {
                super.insertString(offset, str, attr);
            }
        }
    }
}

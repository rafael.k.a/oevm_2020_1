import javax.swing.*;

public class Frame {
    Calculations calculations = new Calculations();

    private JFrame frame;
    private MyPanel myPanel;
    private JTextArea reducedFunctions= new JTextArea();
    private JButton buttonNewArray = new JButton("New array");
    private JButton buttonDNF = new JButton("DNF");
    private JButton buttonKNF = new JButton("KNF");

    Frame() {
        frame = new JFrame();
        frame.setBounds(100, 100, 380, 540);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setResizable(false);
        frame.setLayout(null);

        reducedFunctions.setBounds(180, 130, 150, 360);
        frame.getContentPane().add(reducedFunctions);

        myPanel = new MyPanel();
        myPanel.setBounds(0, 0, 180, 500);
        frame.getContentPane().add(myPanel);

        buttonNewArray.addActionListener(e -> createNewArray());
        buttonNewArray.setBounds(180, 10, 150, 30);
        frame.getContentPane().add(buttonNewArray);

        buttonDNF.addActionListener(e -> cutToDNF());
        buttonDNF.setBounds(180, 50, 150, 30);
        frame.getContentPane().add(buttonDNF);

        buttonKNF.addActionListener(e -> cutToKNF());
        buttonKNF.setBounds(180, 90, 150, 30);
        frame.getContentPane().add(buttonKNF);
    }

    private void createNewArray() {
        reducedFunctions.setText("");
        myPanel.createNewArray();
        myPanel.repaint();
    }

    private void cutToKNF() {
        myPanel.setKNF();
        myPanel.repaint();
        reducedFunctions.setText(calculations.reduceFunction("KNF",myPanel.getArray()));
    }

    private void cutToDNF() {
        myPanel.setDNF();
        myPanel.repaint();
        reducedFunctions.setText(calculations.reduceFunction("DNF",myPanel.getArray()));
    }
}

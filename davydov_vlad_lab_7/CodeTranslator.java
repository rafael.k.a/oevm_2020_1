package com.company;

import java.util.ArrayList;

public class CodeTranslator {
    private final ArrayList<Operation> operationSequence;
    private final ArrayList<String> variableList;
    private final ArrayList<String> readOperationList;
    private final ArrayList<String> writeOperationList;
    private final ArrayList<String> writelnOperationList;
    private final ArrayList<String> mathsOperationList;

    public CodeTranslator() {
        operationSequence = new ArrayList<>();
        variableList = new ArrayList<>();
        readOperationList = new ArrayList<>();
        writeOperationList = new ArrayList<>();
        writelnOperationList = new ArrayList<>();
        mathsOperationList = new ArrayList<>();
    }

    public void addVariable(String var) {
        variableList.add(var);
    }

    public String removeVariable(int index) {
        return variableList.remove(index);
    }

    public boolean hasVariable() {
        return !variableList.isEmpty();
    }

    public void addReadOperation(String var) {
        readOperationList.add(var);
        operationSequence.add(Operation.READ);
    }

    public void addWriteOperation(String text) {
        writeOperationList.add(text);
        operationSequence.add(Operation.WRITE);
    }

    public void addWritelnOperation(String text) {
        writelnOperationList.add(text);
        operationSequence.add(Operation.WRITELN);
    }

    public void addMathsOperation(String operation) {
        mathsOperationList.add(operation);
        operationSequence.add(Operation.MATHS);
    }

    public boolean hasOperation() {
        return !operationSequence.isEmpty();
    }

    public Operation removeOperation() {
        return operationSequence.remove(0);
    }

    public String removeReadOperation() {
        return readOperationList.remove(0);
    }

    public String removeWriteOperation() {
        return writeOperationList.remove(0);
    }

    public String removeWritelnOperation() {
        return writelnOperationList.remove(0);
    }

    public String removeMathsOperation() {
        return mathsOperationList.remove(0);
    }
}


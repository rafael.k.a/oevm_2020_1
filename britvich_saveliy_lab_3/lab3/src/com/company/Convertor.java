package com.company;

public class Convertor {
    private int firstScale = 0;
    private int secondScale = 2;
    private String number = "";

    public Convertor(int first, String num) {
        firstScale = first;
        number = num;
    }

    public int convertToDecimal() {
        number = new StringBuffer(number).reverse().toString(); // переворачиваем строку для удобного понимания
        char[] numberInCharArray = number.toCharArray();
        int numberInDecimal = 0;
        int degree = 0;
        int numberBetweenConversion = 0;
        int maxSizeOfScale = 16;
        int minSizeOfScale = 2;

        if (firstScale < minSizeOfScale || firstScale > maxSizeOfScale) {
            System.out.println("Вы ввели неправильную систему счисления");
            return -1;
        }
        if (number == "") {
            System.out.println("Вы не ввели число!");
            return -1;
        }

        for (int i = 0; i < numberInCharArray.length; i++) {
            if (numberInCharArray[i] >= '0' && numberInCharArray[i] <= '9') {
                numberBetweenConversion = (int) (numberInCharArray[i] - 48);
            } else if (numberInCharArray[i] >= 'A' && numberInCharArray[i] <= 'F' && numberInCharArray[i] - 64 + 10 <= firstScale) {
                numberBetweenConversion = numberInCharArray[i] - 64 + 10;
            } else {
                System.out.println("Вы ввели неправильное число");
                return -1;
            }

            numberInDecimal += numberBetweenConversion * (int) Math.pow(firstScale, degree);
            degree++;
        }

        return numberInDecimal;
    }

    public StringBuilder convertToBinaryScale(int numberInDecimal) {
        StringBuilder stringBuilder = new StringBuilder();

        if (numberInDecimal == 0) {
            return stringBuilder.append(0);
        }

        while (numberInDecimal != 0) {
            int step = numberInDecimal % secondScale;
            stringBuilder.append(step);
            numberInDecimal /= secondScale;
        }
        stringBuilder.reverse();
        return stringBuilder;
    }

}

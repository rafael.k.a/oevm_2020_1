package com.company;

public class Calc {
    private final char zero = '0';
    private final char one = '1';

    private int firstNumber = 0;
    private int secondNumber = 0;
    private int trans = 0;

    private String firstNum;
    private String secondNum;
    private char action;

    private String firstNumToEndNS;
    private String secondNumToEndNS;

    public Calc(int startNS, String firstNumber, String secondNumber, char action) {
        this.firstNum = firstNumber;
        this.secondNum = secondNumber;
        this.action = action;
        transTo10NS(startNS);
        transToNS();
    }
    private Boolean compare(String strFirst, String strSecond) {
        char[] strArrayFirst = new StringBuilder(strFirst).reverse().toString().toCharArray();
        char[] strArrayTwo = new StringBuilder(strSecond).reverse().toString().toCharArray();
        if (strArrayFirst.length > strArrayTwo.length) {
            return true;
        }
        if (strArrayFirst.length < strArrayTwo.length) {
            return false;
        }
        if (strArrayFirst.length == strArrayTwo.length) {
            for (int i = 0; i < strArrayFirst.length; i++) {
                if (strArrayFirst[i] == one && strArrayTwo[i] == zero) {
                    return true;
                }
                if (strArrayFirst[i] == zero && strArrayTwo[i] == one) {
                    return false;
                }
            }
        }
        return true;
    }
    private void transTo10NS(int startNS) {
        char[] firstNumArray = firstNum.toCharArray();
        char[] secondNumArray = secondNum.toCharArray();
        for (int i = 0; i < firstNumArray.length; i++) {
            int temp;
            if (firstNumArray[i] >= '0' && firstNumArray[i] <= '9') {
                temp = firstNumArray[i] - '0';
            } else {
                temp = 10 + firstNumArray[i] - 'A';
            }
            firstNumber += temp * Math.pow(startNS, firstNumArray.length - i - 1);
        }
        for (int i = 0; i < secondNumArray.length; i++) {
            int temp;
            if (secondNumArray[i] >= '0' && secondNumArray[i] <= '9') {
                temp = secondNumArray[i] - '0';
            } else {
                temp = 10 + secondNumArray[i] - 'A';
            }
            secondNumber += temp * Math.pow(startNS, secondNumArray.length - i - 1);
        }
    }
    private void transToNS() {
        StringBuilder strFirstNum = new StringBuilder();
        StringBuilder strSecondNum = new StringBuilder();
        if (firstNumber == 0) {
            strFirstNum.append("0");
        }
        for (int i = 0; firstNumber > 0; i++) {
            strFirstNum.append((char) (firstNumber % 2 + '0'));
            firstNumber /= 2;
        }
        firstNumToEndNS = strFirstNum.toString();
        if (secondNumber == 0) {
            strSecondNum.append("0");
        }
        for (int i = 0; secondNumber > 0; i++) {
            strSecondNum.append((char) (secondNumber % 2 + '0'));
            secondNumber /= 2;
        }
        secondNumToEndNS = strSecondNum.toString();
    }
    public String calc() {
        StringBuilder str = new StringBuilder();
        switch (action) {
            case '+':
                str.append(add(firstNumToEndNS, secondNumToEndNS));
                return str.reverse().toString();
            case '-':
                str.append(subtract(firstNumToEndNS, secondNumToEndNS)).reverse();
                return delZero(str.toString());
            case '*':
                str.append(multiplic(firstNumToEndNS, secondNumToEndNS)).reverse();
                return str.toString();
            case '/':
                str.append(divide(firstNumToEndNS, secondNumToEndNS)).reverse();
                return str.toString();
        }
        throw new ArithmeticException("div by 0");
    }
    private String addFirst(char firstNum,char secondNum){
        if (trans == 0) {
            if (firstNum == zero && secondNum == zero) {
                trans = 0;
                return "0";
            }
            if ((firstNum == zero && secondNum == one) || (firstNum == one && secondNum == zero)) {
                trans = 0;
                return "1";
            }
            if (firstNum == one && secondNum == one) {
                trans = 1;
                return "0";
            }
        } else {
            if (firstNum == zero && secondNum == zero) {
                trans = 0;
                return "1";
            }
            if ((firstNum == zero && secondNum == one) || (firstNum == one && secondNum == zero)) {
                trans = 1;
                return "0";
            }
            if (firstNum == one && secondNum == one) {
                trans = 1;
                return "1";
            }
        }
        return "невозможная операция";
    }
    private String addSecond(char firstNum){
        if (firstNum == zero && trans == 0) {
            trans = 0;
            return "0";
        }
        if ((firstNum == zero && trans == 1) || (firstNum == one && trans == 0)) {
            trans = 0;
            return "1";
        }
        if (firstNum == one && trans == 1) {
            trans = 1;
            return "0";
        }
        return "невозможная операция";
    }
    private String add(String strFirst, String strSecond) {
        char[] firstNumArray = strFirst.toCharArray();
        char[] secondNumArray = strSecond.toCharArray();
        StringBuilder result = new StringBuilder();
        int i = 0;
        while (i < secondNumArray.length) {
            result.append( addFirst(firstNumArray[i],secondNumArray[i]));
            i++;
        }
        while (i < firstNumArray.length) {
            result.append( addSecond(firstNumArray[i]));
            i++;
        }
        if (trans == 1) {
            result.append("1");
        }
        return result.toString();
    }
    private String subtract(String strFirst, String strSecond) {
        StringBuilder result = new StringBuilder();
        strSecond = upend(strSecond);
        result.append(add(strFirst, strSecond));
        result.deleteCharAt(result.length() - 1);
        return result.toString();
    }

    private String multiplic(String strFirst, String strSecond) {
        char[] strArraySecond = strSecond.toCharArray();
        StringBuilder strArray1 = new StringBuilder(strFirst);
        String result = "0";
        int iIndex = 0;
        for (int i = 0; i < strArraySecond.length; i++) {
            if (strArraySecond[i] == one) {
                for (int j = 0; j < i - iIndex; j++) {
                    strArray1.insert(0, '0');
                }
                result = add(strArray1.toString(), result);
                iIndex = i;
            }
        }
        return result;
    }
    private String divide(String strFirst, String strSecond) {
        String comparison = strSecond;
        String result = "0";
        if (strSecond.toCharArray().length == 1 && strSecond.toCharArray()[0] != '1') {
            throw new ArithmeticException("division by 0");
        }
        while (compare(strFirst, comparison)) {
            result = add(result, "1");
            comparison = add(comparison, strSecond);
        }
        return result;
    }
    private String upend(String str) {
        StringBuilder string = new StringBuilder();
        StringBuilder result = new StringBuilder();
        char[] digitArray = str.toCharArray();
        int i = 0;
        while (i < secondNumToEndNS.length()) {
            if (digitArray[i] == one) {
                string.append("0");
            } else {
                string.append("1");
            }
            i++;
        }
        while (i < firstNumToEndNS.length()) {
            string.append("1");
            i++;
        }
        result.append(add(string.toString(), "1"));
        return result.toString();
    }
    private String delZero(String str) {
        StringBuilder result = new StringBuilder();
        char[] digitArray = str.toCharArray();
        int i = 0;
        while (digitArray[i] == zero) {
            i++;
            if (i == str.length()) {
                return "0";
            }
        }
        while (i < str.length()) {
            result.append(digitArray[i]);
            i++;
        }
        return result.toString();
    }
}
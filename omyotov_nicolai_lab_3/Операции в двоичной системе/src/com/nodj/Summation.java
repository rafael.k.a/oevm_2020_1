package com.nodj;

import java.util.ArrayList;
import java.util.Collections;

public class Summation {
    private final ArrayList<Integer> maxFigs;
    private final ArrayList<Integer> minFigs;

    Summation(ArrayList<Integer> firstNum, ArrayList<Integer> secondNum) {
        maxFigs = firstNum.size() > secondNum.size() ? new ArrayList<>(firstNum) : new ArrayList<>(secondNum);
        minFigs = firstNum.size() > secondNum.size() ? secondNum : firstNum;
    }

    public ArrayList<Integer> sum() {
        Collections.reverse(maxFigs);
        for (int i = 0; i < maxFigs.size(); i++) {
            int sum;
            if (i < minFigs.size()) {
                sum = maxFigs.get(i) + minFigs.get(minFigs.size() - i - 1);
            } else {
                sum = maxFigs.get(i);
            }
            if (sum == 2) {
                maxFigs.set(i, 0);
                if (maxFigs.size() > i + 1)
                    maxFigs.set(i + 1, maxFigs.get(i + 1) + 1);
                else {
                    maxFigs.add(1);
                }
            } else if (sum == 3) {
                maxFigs.set(i, 1);
                if (maxFigs.size() > i + 1)
                    maxFigs.set(i + 1, maxFigs.get(i + 1) + 1);
                else {
                    maxFigs.add(1);
                }
            } else {
                maxFigs.set(i, sum);
            }
        }
        Collections.reverse(maxFigs);
        return maxFigs;
    }
}

package com.nodj;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Converter converter = new Converter();
        Converter converter2 = new Converter();
        Scanner sc = new Scanner(System.in);
        Helper helper = new Helper();
        helper.checkBase("Укажите исходную систему счисления (2-16):", sc, converter, converter2);
        helper.checkNum("Укажите первое число в исходной системе счисления (целое):", sc, converter);
        helper.checkNum("Укажите второе число в исходной системе счисления (целое):", sc, converter2);
        Calculator calc = new Calculator(converter.convert(), converter2.convert());
        helper.checkOperation("Укажите арифметическую операцию (+, -, *, /):", sc, calc);
        calc.calculate();
        System.out.println(calc.toString());
    }
}

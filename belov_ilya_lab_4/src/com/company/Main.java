package com.company;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        final int N = 16;                   // количество строк
        final int M = 5;                    // количество столбцов
        int[][] truthTable = new int[N][M]; // таблица истинности

        int option = 0;
        Scanner in = new Scanner(System.in);
        while (option != 1 && option != 2) {
            System.out.printf("Введите 1 для рачёта ДНФ\nВведите 2 для рачёта КНФ\nВвод: ");
            option = in.nextInt();
        }
        truthTable = Helper.fillTruthTable(N, M, truthTable); // заполняем таблицу истинности
        Helper.printTruthTable(N, M, truthTable);             // выводим результат на экран

        if (option == 1) {
            System.out.print("\nДНФ: ");
            NormalForm.toDisNormalForm(N, M, truthTable);
        } else if (option == 2) {
            System.out.print("\nКНФ: ");
            NormalForm.toKonNormalForm(N, M, truthTable);
        }
    }

}

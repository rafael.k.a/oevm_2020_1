import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PascalFileReader {
    private ArrayList<String> variables = new ArrayList<>();
    private ArrayList<String> write = new ArrayList<>();
    private ArrayList<String> writeln = new ArrayList<>();
    private ArrayList<String> read = new ArrayList<>();
    private ArrayList<String> arithmeticCommands = new ArrayList<>();
    private String pascalFile;

    public void startReading(String name) throws FileNotFoundException {
        readPasFile(name);
        findVariables();
        findWriteStrings();
        findWritelnStrings();
        findReadStrings();
        findOperationStrings();
    }
    private void readPasFile(String name) throws FileNotFoundException {
        StringBuilder pascalFileBuilder = new StringBuilder();
        try (FileReader fr = new FileReader(name)) {
            BufferedReader reader = new BufferedReader(fr);
            String line = reader.readLine();
            while (line != null) {
                pascalFileBuilder.append(line).append("\n");
                line = reader.readLine();
            }
            pascalFile=pascalFileBuilder.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void findVariables() {
        Pattern patternVar_begin = Pattern.compile("(?<=var)[\\w\\W]*(?=begin)");
        Matcher matcherVar_begin = patternVar_begin.matcher(pascalFile);
        if (matcherVar_begin.find()) {
            String variables = matcherVar_begin.group();


            Pattern patternVariables = Pattern.compile("\\b[a-zA-Z][\\w]*\\b");
            Matcher matcherVariables = patternVariables.matcher(variables);

            while (matcherVariables.find()) {
                if (!matcherVariables.group().equals("integer")) {
                    this.variables.add(matcherVariables.group() + " dd ?");
                }
            }
        }
    }

    private void findWriteStrings() {
        Pattern patternWriteFile = Pattern.compile("(?<=begin\\n)[\\w\\W]*(?=\\nend.)");
        Matcher matcherWriteFile = patternWriteFile.matcher(pascalFile);
        if (matcherWriteFile.find()) {
            String output = matcherWriteFile.group();

            Pattern patternWrite = Pattern.compile("(?<=write)\\('(.*)'\\);");
            Matcher matcherWrite = patternWrite.matcher(output);

            while (matcherWrite.find()) {
                    this.write.add(matcherWrite.group());
            }
            clipping(write);
        }
    }
    private void findWritelnStrings() {
        Pattern patternWritelnFile = Pattern.compile("(?<=begin\\n)[\\w\\W]*(?=\\nend.)");
        Matcher matcherWritelnFile = patternWritelnFile.matcher(pascalFile);
        if (matcherWritelnFile.find()) {
            String output = matcherWritelnFile.group();

            Pattern patternWrite = Pattern.compile("(?<=writeln)\\((.*)\\);");
            Matcher matcherWrite = patternWrite.matcher(output);

            while (matcherWrite.find()) {
                this.writeln.add(matcherWrite.group());
            }
            clipping(writeln);
        }
    }
    private void findReadStrings() {
        Pattern patternReadFile = Pattern.compile("(?<=begin\\n)[\\w\\W]*(?=\\nend.)");
        Matcher matcherReadFile = patternReadFile.matcher(pascalFile);
        if (matcherReadFile.find()) {
            String input = matcherReadFile.group();

            Pattern patternRead = Pattern.compile("(?<=readln)\\((.*)\\);");
            Matcher matcherRead = patternRead.matcher(input);

            while (matcherRead.find()) {
                this.read.add(matcherRead.group());
            }
            clipping(read);
        }
    }
    private void findOperationStrings() {
        Pattern patternOperationFile = Pattern.compile("(?<=begin\\n)[\\w\\W]*(?=\\nend.)");
        Matcher matcherOperationFile = patternOperationFile.matcher(pascalFile);
        if (matcherOperationFile.find()) {
            String operations = matcherOperationFile.group();

            Pattern patternOperation = Pattern.compile("([\\w]*)[\\s]*:=[\\s]*([\\w]*)[\\s]*(['\\-+*/])[\\s]*([\\w]*)[\\s]*;");
            Matcher matcherOperation = patternOperation.matcher(operations);

            while (matcherOperation.find()) {

                this.arithmeticCommands.add(matcherOperation.group());
            }
        }
    }
    private void clipping (ArrayList<String> array) {
        for (int i = 0; i < array.size(); i++) {
            int start = array.get(i).indexOf("(")+1 ;
            int finish =  array.get(i).indexOf(")");
            String clip = array.get(i).substring(start,finish);
            array.set(i,clip);
        }
    }

    public ArrayList<String> getVariables() {
        return variables;
    }

    public ArrayList<String> getWrite() {
        return write;
    }

    public ArrayList<String> getWriteln() {
        return writeln;
    }

    public ArrayList<String> getRead() {
        return read;
    }

    public ArrayList<String> getArithmeticCommands() {
        return arithmeticCommands;
    }
}


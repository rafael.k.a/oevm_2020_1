format PE console

entry start

include 'win32a.inc'

section '.data' data readable writable
string0 db 'input x: ', 0
string1 db 'input y: ', 0
string2 db 'x + y = ', 0
string3 db 'x - y = ', 0
string4 db 'x * y = ', 0
string5 db 'x / y = ', 0
spaceStr db '%d', 0
dopStr db '%d', 0ah, 0 
x dd ?
y dd ?
res1 dd ?
res2 dd ?
res3 dd ?
res4 dd ?
section '.code' code readable executable
   start:
push string0
call [printf]
push x
push spaceStr
call [scanf]

push string1
call [printf]
push y
push spaceStr
call [scanf]

mov ecx, [x]
sub ecx, [y]
mov [res1], ecx
push string2
call [printf]
push [res1]
push dopStr
call [printf]

mov ecx, [x]
add ecx, [y]
mov [res2], ecx
push string3
call [printf]
push [res2]
push dopStr
call [printf]

mov ecx, [x]
imul ecx, [y]
mov [res3], ecx
push string4
call [printf]
push [res3]
push dopStr
call [printf]

mov eax, [x]
mov ecx, [y]
div ecx
mov [res4], eax
push string5
call [printf]
push [res4]
push dopStr
call [printf]

         finish:

                call [getch]

                call [ExitProcess]

section '.idata' import data readable

        library kernel, 'kernel32.dll',\
                msvcrt, 'msvcrt.dll'

        import kernel,\
               ExitProcess, 'ExitProcess'

        import msvcrt,\
               printf, 'printf',\
               scanf, 'scanf',\
               getch, '_getch'
package com.company;

public class ArithmeticOperations {
    public static String fold(char[] firstNumber, char[] secondNumber){
        String resultFold = "";
        int countFirst = firstNumber.length - 1;
        int countSecond = secondNumber.length - 1;
        int flag = 0;
        while(countFirst > -1) {
            if(countFirst == countSecond) {
                flag = 1;
            }
            if(countSecond < 0 && countFirst >= 0) {
                resultFold = functionOneFold(resultFold, firstNumber, secondNumber, countFirst);
            }
            else if (firstNumber[countFirst] == '0' && secondNumber[countSecond] == '0') {
                resultFold = '0' + resultFold;
                countFirst--;
                countSecond--;
            }
            else if ((firstNumber[countFirst] == '1' && secondNumber[countSecond] == '0') || (firstNumber[countFirst] == '0' && secondNumber[countSecond] == '1')) {
                resultFold = '1' + resultFold ;
                countFirst--;
                countSecond--;
            }
            else if (firstNumber[countFirst] == '1' && secondNumber[countSecond] == '1') {
                resultFold = '0' + resultFold;
                countFirst--;
                countSecond--;
                if (countSecond > -1) {
                    while (secondNumber[countSecond] == '1') {
                        if (firstNumber[countFirst] == '1') {
                            resultFold = '1' + resultFold;
                            countFirst--;
                            countSecond--;
                        }
                        else if (firstNumber[countFirst] == '0') {
                            resultFold = '0' + resultFold;
                            countFirst--;
                            countSecond--;
                        }
                        resultFold = functionTwoFold(resultFold, firstNumber, countFirst, flag);
                        if (countSecond < 0) {
                            break;
                        }
                    }
                }
                if(countFirst < 0 && countSecond < 0 && flag !=-1) {
                    resultFold = '1' + resultFold;
                    break;
                }
                else if(flag == -1) {
                    break;
                }
                if (firstNumber[countFirst] == '0') {
                    resultFold = '1' + resultFold;
                    countFirst--;
                    countSecond--;
                }
                else if (firstNumber[countFirst] == '1') {
                    resultFold = '0' + resultFold;
                    countFirst--;
                    if(countSecond < 0) {
                        countSecond++;
                    }
                    else {
                        if(secondNumber[countSecond - 1] == '1') {
                            secondNumber[countSecond - 1] = '0';
                        }
                        countSecond --;
                    }
                    if(countFirst < 0) {
                        resultFold = '1' + resultFold;
                        break;
                    }
                }
            }
        }
        return resultFold;
    }

    public static String functionOneFold(String resultFold, char[] firstNumber, char[] secondNumber, int countFirst){
        if(firstNumber[countFirst + 1] == '1' && firstNumber[countFirst] == '0') {
            resultFold = '1' + resultFold;
            countFirst--;
        }
        else if(firstNumber[countFirst + 1] == '1' && firstNumber[countFirst] == '1') {
            resultFold = '0' + resultFold;
            countFirst--;
            if(countFirst == -1) {
                resultFold = '1' + resultFold;
                countFirst--;
            }
        }
        else if(secondNumber[0] == '0') {
            resultFold = '0' + resultFold;
            if(countFirst > 0) {
                firstNumber[countFirst] = '0';
            }
        }
        while(countFirst > -1) {
            resultFold = firstNumber[countFirst] + resultFold;
            countFirst--;
        }
        return resultFold;
    }

    public static String functionTwoFold (String resultFold, char[] firstNumber, int countFirst, int flag){
        if (flag == 1 && countFirst == -1) {
            if (firstNumber[countFirst + 1] == '1' && firstNumber[countFirst + 2] == '1') {
                resultFold = '1' + resultFold;
                flag = -1;
            }
            else if (firstNumber[countFirst + 1] == '1' && firstNumber[countFirst + 2] == '0') {
                resultFold = '1' + resultFold;
            }
        }
        return resultFold;
    }

    public static String subtract (char[] firstNumber, char[] secondNumber){
        String resultSubstract = "";
        int countFirst = firstNumber.length - 1;
        int countSecond = secondNumber.length - 1;
        while(countSecond> -1) {
            if (countSecond == 0 && firstNumber[countFirst] == '1' && countFirst == 0) {
                firstNumber[countFirst] = '0';
                countFirst--;
                countSecond--;
            }
            else if (firstNumber[countFirst] == '0' && secondNumber[countSecond] == '0' || firstNumber[countFirst] == '1' && secondNumber[countSecond] == '1') {
                resultSubstract = '0' + resultSubstract ;
                countFirst--;
                countSecond--;
            } else if (firstNumber[countFirst] == '1' && secondNumber[countSecond] == '0') {
                resultSubstract = '1' + resultSubstract ;
                countFirst--;
                countSecond--;
            } else if (firstNumber[countFirst] == '0' && secondNumber[countSecond] == '1') {
                resultSubstract = '1' + resultSubstract;
                countFirst--;
                firstNumber = functionOneSubstract(firstNumber, countFirst);
                countSecond--;
            }
        }
        resultSubstract = functionTwoSubstract(resultSubstract, firstNumber, countFirst);
        return resultSubstract;
    }

    public static char[] functionOneSubstract(char[] firstNumber, int countFirst){
        int index = countFirst;
        while(firstNumber[index] == '0') {
            firstNumber[index] = '1';
            index--;
        }
        if(firstNumber[index] == '1') {
            firstNumber[index] = '0';
        }
        return firstNumber;
    }

    public static String functionTwoSubstract(String resultSubstract, char[] firstNumber, int countFirst){
        while(countFirst > 0) {
            resultSubstract = firstNumber[countFirst] + resultSubstract;
            countFirst--;
        }
        if(firstNumber[0]!= '0') {
            resultSubstract = '1' + resultSubstract;
        }
        return resultSubstract;
    }

    public static String multiply(char[] firstNumber, char[] secondNumber){
        long count = HelperClass.convertTo10Base(secondNumber, 2);
        char[] result = firstNumber;
        String constanta = new String(firstNumber);
        for(int i = 0; i < count - 1; i++){
            result = fold(result, constanta.toCharArray()).toCharArray();
        }
        String resultMultiply = new String(result);
        return resultMultiply;
    }

    public static String divide(char[] firstNumber, char[] secondNumber){
        long count = 0;
        while(firstNumber[0] != '0') {
            firstNumber = subtract(firstNumber, secondNumber).toCharArray();
            count++;
        }
        String resultDivide = new String(HelperClass.convertToFinalBase(count));
        return resultDivide;
    }
}

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try {
            Scanner input = new Scanner(System.in);

            System.out.print("Исходная система счисления: ");
            int origin_ss = input.nextInt();

            input.nextLine();

            System.out.print("Число1: ");
            char[] num_ch1 = input.nextLine().toCharArray();

            System.out.print("Число2: ");
            char[] num_ch2 = input.nextLine().toCharArray();

            System.out.print("Операция: ");
            String operation = input.next();

            input.close();

            Converter c = new Converter(origin_ss, num_ch1, num_ch2);
            StringBuffer num1 = new StringBuffer(c.convertToBinary(num_ch1));
            StringBuffer num2 = new StringBuffer(c.convertToBinary(num_ch2));
            Operations tr = new Operations(num1, num2, operation);
            System.out.printf("Результат: %s", tr.formResultString());
        }
        catch (Exception l) {
            System.out.println(l);
        }
    }
}

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try {
            Scanner input = new Scanner(System.in);

            System.out.print("Исходная система счисления: ");
            int origin_ss = input.nextInt();

            System.out.print("Конечная система счисления: ");
            int final_ss = input.nextInt();

            input.nextLine();

            System.out.print("Число в исходной системе счисления: ");
            char[] num_ch = input.nextLine().toCharArray();

            input.close();

            Conversion c = new Conversion(origin_ss, final_ss, num_ch);
            System.out.printf("Результат: %s", c.formResultString());

        } catch (Exception e) {
            System.out.println("Ошибка");
        }
    }
}

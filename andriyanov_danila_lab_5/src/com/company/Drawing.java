package com.company;

import java.awt.*;

public class Drawing {
    Property prop = new Property();

    private final int M = prop.getPropertyInt("M");
    private final int N = prop.getPropertyInt("N");
    private int width = prop.getPropertyInt("width");
    private int height = prop.getPropertyInt("height");

    private int startX = prop.getPropertyInt("X");
    private int startY = prop.getPropertyInt("Y");
    private int dopX = 15;
    private int dopY = 25;
    private static String[] variables= {"X1", "X2", "X3", "X4", "F"};

    public void draw(int truthTable[][], Graphics g) {

        for (int i = 0; i < M - 1; i++) {
            g.drawRect(startX + width * i, startY, width, height);
            g.drawString(variables[i], startX * 2 + width * i, startY * 3);
        }

        g.drawRect(startX + width * (M - 1), startY, width, height);
        g.drawString("F", startX + 10 + width * (M - 1), startY + 20);

        startY += height;

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                g.drawRect(startX + width * j, startY + height * i, width, height);
                g.drawString(Integer.toString(truthTable[i][j]), startX + dopX + width * j, startY + dopY + height * i);
            }
        }

        if (Main._CNF) {
            drawCNF(truthTable, g);
        }

        if (Main._DNF) {
            drawDNF(truthTable, g);
        }

        startY -= height;
    }

    private void drawCNF(int truthTable[][], Graphics g) {
        for (int i = 0; i < N; ++i) {
            if (truthTable[i][M - 1] == 0) {
                g.setColor(new Color(Integer.parseInt(prop.getPropertyString("mainColor"))));
            } else {
                g.setColor(new Color(Integer.parseInt(prop.getPropertyString("dopColor"))));
            }

            for (int j = 0; j < M; j++) {
                g.drawRect(startX + width * j, startY + height * i, width, height);
                g.drawString(Integer.toString(truthTable[i][j]), startX + dopX + width * j, startY + dopY + height * i);
            }
        }
    }

    private void drawDNF(int truthTable[][], Graphics g) {
        for (int i = 0; i < N; ++i) {
            if (truthTable[i][M - 1] == 1) {
                g.setColor(new Color(Integer.parseInt(prop.getPropertyString("mainColor"))));
            } else {
                g.setColor(new Color(Integer.parseInt(prop.getPropertyString("dopColor"))));
            }

            for (int j = 0; j < M; j++) {
                g.drawRect(startX + width * j, startY + height * i, width, height);
                g.drawString(Integer.toString(truthTable[i][j]), startX + dopX + width * j, startY + dopY + height * i);
            }
        }
    }

    public String convertToDNF(int truthTable[][], int n, int m) {
        boolean printPlus = false;
        StringBuilder strBuilder = new StringBuilder();
        int cnt = 0;

        for (int i = 0; i < n; ++i) {
            if (truthTable[i][m - 1] == 1) {
                if (printPlus) {
                    strBuilder.append(" + ");
                }
                strBuilder.append("(");
                printPlus = true;

                for (int j = 0; j < m - 1; ++j) {
                    if (truthTable[i][j] == 0) {
                        strBuilder.append("!");
                    }
                    strBuilder.append(variables[j]);
                    if (j < m - 2) {
                        strBuilder.append(" * ");
                    } else {
                        strBuilder.append(")");
                    }
                }
                if (cnt % 2 == 1) {
                    strBuilder.append("\n");
                }
                cnt++;
            }
        }
        return strBuilder.toString();
    }
    public String convertToCNF(int truthTable[][], int n, int m) {
        boolean star = false;
        StringBuilder strBuilder = new StringBuilder();
        int cnt = 0;

        for (int i = 0; i < n; ++i) {
            if (truthTable[i][m - 1] == 0) {
                if (star) {
                    strBuilder.append(" * ");
                }
                strBuilder.append("(");
                star = true;

                for (int j = 0; j < m - 1; ++j) {
                    if (truthTable[i][j] == 1) {
                        strBuilder.append("!");
                    }
                    strBuilder.append(variables[j]);
                    if (j < m - 2) {
                        strBuilder.append(" + ");
                    } else {
                        strBuilder.append(")");
                    }
                }
                if (cnt % 2 == 1) {
                    strBuilder.append("\n");
                }
                cnt++;
            }
        }
        return strBuilder.toString();
    }
}

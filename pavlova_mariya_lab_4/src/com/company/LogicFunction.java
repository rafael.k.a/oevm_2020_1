package com.company;

import java.util.Random;

public class LogicFunction {

    private static int lines;
    private static int countVariables;
    private static int functionValue;
    public LogicFunction(int countVariables) {
        this.countVariables = countVariables;
        lines = (int)Math.pow(2, countVariables);
        functionValue = countVariables;
    }

    public static String getDisjunctiveNormalForm(int[][] truthTable) {
        String result = "F: ";
        boolean isOther;
        for (int i = 0; i < lines; i++) {
            isOther = false;//переменная для проверки, последнее ли это было истинное выражение
            if (truthTable[i][functionValue] == 1) {
                result += "(";
                for (int j = 0; j < countVariables; j++) {
                    if (j != 0) {
                        result += "*";
                    }
                    if (truthTable[i][j] == 1) {
                        result += "(X" + j + ")";
                    }
                    if (truthTable[i][j] == 0) {
                        result += "(-X"+j+")";
                    }
                }
                result += ")";
                for (int k = i+1; k < lines; k++) {
                    if (truthTable[k][functionValue] == 1) {
                        isOther = true;
                        break;
                    }
                }
                if (isOther) {
                    result += "+";
                }
                result+="\n";//перевод на след строку для простоты восприятия
            }
        }
        return result;
    }

    public static String getConjunctiveNormalForm(int[][] truthTable) {
        String result = "F: ";
        for (int i = 0; i < lines; i++) {
            boolean isOther = false;//переменная для проверки, последнее ли это было ложное выражение
            if (truthTable[i][functionValue] == 0) {
                result += "(";
                for (int j = 0; j < countVariables; j++) {
                    if (j != 0) {
                        result += "+";
                    }
                    if (truthTable[i][j] == 1) {
                        result += "(-X"+j+")";
                    }
                    if (truthTable[i][j] == 0) {
                        result += "(X"+j+")";
                    }
                }
                result += ")";
                for (int k = i+1; k < lines; k++) {
                    if (truthTable[k][functionValue] == 0) {
                        isOther = true;
                        break;
                    }
                }
                if (isOther) {
                    result += "*";
                }
                result+="\n";//перевод на след. строку для простоты восприятия
            }
        }
        return result;
    }

    public static int[][] createTableTruth() {
        Random random = new Random();
        int[][] truthTable = {
                {0,0,0,0,random.nextInt(2)},
                {0,0,0,1,random.nextInt(2)},
                {0,0,1,0,random.nextInt(2)},
                {0,0,1,1,random.nextInt(2)},
                {0,1,0,0,random.nextInt(2)},
                {0,1,0,1,random.nextInt(2)},
                {0,1,1,0,random.nextInt(2)},
                {0,1,1,1,random.nextInt(2)},
                {1,0,0,0,random.nextInt(2)},
                {1,0,0,1,random.nextInt(2)},
                {1,0,1,0,random.nextInt(2)},
                {1,0,1,1,random.nextInt(2)},
                {1,1,0,0,random.nextInt(2)},
                {1,1,0,1,random.nextInt(2)},
                {1,1,1,0,random.nextInt(2)},
                {1,1,1,1,random.nextInt(2)},
        };
        return truthTable;
    }

    public static void print(int[][] tableTruth) {
        for (int i = 0; i < countVariables; i++) {
            int num = i + 1;
            System.out.print("X"+num+" ");
        }
        System.out.println("F");

        for (int i = 0; i < lines; i++) {
            for (int j = 0; j < countVariables+1; j++) {
                System.out.print(tableTruth[i][j] + "  ");
            }
            System.out.println();
        }
    }
}

import javax.swing.*;
import java.awt.event.*;

public class MyForm {

    Helper helper = new Helper();

    public static final int LENGTH = 4;

    public static final int ROWS = (int) Math.pow(2, LENGTH);

    private JFrame frame;
    private MyPanel myPanel;
    private JTextArea result= new JTextArea();

    private JButton buttonCreate = new JButton("Создать");
    private JButton buttonDNF = new JButton("ДНФ");
    private JButton buttonKNF = new JButton("КНФ");

    MyForm() {
        frame = new JFrame( "Минимизация булевых функций");
        frame.setBounds(300, 100, 400, 600);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setResizable(false);
        frame.setLayout(null);

        myPanel = new MyPanel();
        myPanel.setBounds(180, 50, 180, 500);
        frame.getContentPane().add(myPanel);
        myPanel.setVisible(false);

        result.setBounds(10, 140, 150, 250);
        frame.getContentPane().add(result);
        result.setVisible(false);

        buttonCreate.setBounds(190, 10, 150, 30);
        frame.getContentPane().add(buttonCreate);
        buttonCreate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                result.setText("");

                myPanel.createMatrix();
                myPanel.repaint();
                myPanel.setVisible(true);

                frame.setVisible(true);
                frame.setResizable(false);

                buttonDNF.setEnabled(true);
                buttonDNF.setVisible(true);

                buttonKNF.setVisible(true);
                buttonKNF.setEnabled(true);
            }
        });

        buttonDNF.setBounds(10, 60, 150, 30);
        frame.getContentPane().add(buttonDNF);
        buttonDNF.setVisible(false);
        buttonDNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                myPanel.setDNF();
                myPanel.repaint();

                result.setVisible(true);
                result.setText(null);
                result.setText(helper.toDisjunctiveForm(ROWS, LENGTH ,myPanel.getMatrix()));
            }
        });

        buttonKNF.setBounds(10, 100,  150, 30);
        frame.getContentPane().add(buttonKNF);
        buttonKNF.setVisible(false);
        buttonKNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                myPanel.setKNF();
                myPanel.repaint();

                result.setVisible(true);
                result.setText(null);
                result.setText(helper.toConjunctiveForm(ROWS, LENGTH ,myPanel.getMatrix()));
            }
        });
    }
}

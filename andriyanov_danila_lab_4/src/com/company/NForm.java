package com.company;

public class NForm {

    public static String[] vars= {"X1", "X2", "X3", "X4" };

    //днф
    public static void toDisjunctiveNForm(int n, int m, int[][] truthTable) {
        boolean plus = false;

        for (int i = 0; i < n; ++i) {
            if (truthTable[i][m - 1] == 1) {
                if (plus) {
                    System.out.print("+");
                }
                System.out.print("(");
                plus = true;

                for (int j = 0; j < m - 1; ++j) {
                    if (truthTable[i][j] == 0) {
                        System.out.print("!");
                    }
                    System.out.print(vars[j]);
                    if (j < m - 2) {
                        System.out.print("*");
                    }
                    else {
                        System.out.print(")");
                    }
                }
            }
        }
    }

    //кнф
    public static void toConjunctiveNForm(int n, int m, int[][] truthTable) {
        boolean star = false;

        for (int i = 0; i < n; ++i) {
            if (truthTable[i][m - 1] == 0) {
                if (star) {
                    System.out.print("*");
                }

                System.out.print("(");
                star = true;

                for (int j = 0; j < m - 1; ++j) {
                    if (truthTable[i][j] == 1) {
                        System.out.print("!");
                    }
                    System.out.print(vars[j]);
                    if (j < m - 2) {
                        System.out.print("+");
                    }
                    else {
                        System.out.print(")");
                    }
                }
            }
        }
    }
}



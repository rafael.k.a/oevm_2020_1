package com.company;

public class Main {

    public static void main(String[] args) {
        PascalStructure PascalCode = new PascalStructure();

        AssemblerStructure assemblerCode = new AssemblerStructure();

        Parsing Parser = new Parsing(PascalCode.read(), assemblerCode);
        Parser.parseBody();
        assemblerCode.create();
    }
}

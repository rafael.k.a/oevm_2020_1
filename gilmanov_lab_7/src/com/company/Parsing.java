package com.company;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parsing {
    AssemblerStructure assemblerStructure;

    private String processingString;

    private String Vars;
    private String Cods;

    private ArrayList<String> arrayVarsString = new ArrayList<>();
    private ArrayList<String> arrayCodsString = new ArrayList<>();
    private ArrayList<String> arrayVars = new ArrayList<>();

    public Parsing(String processingString, AssemblerStructure assemblerStructure) {

        this.processingString = processingString;
        this.assemblerStructure = assemblerStructure;

    }

    public void parseBody() {
        Pattern ofVariables = Pattern.compile("(?<=var)[\\s\\S]*(?=begin)");
        Matcher matcherOfVariables = ofVariables.matcher(processingString);

        while (matcherOfVariables.find()) {
            Vars = processingString.substring(matcherOfVariables.start(), matcherOfVariables.end());
        }

        Pattern ofCode = Pattern.compile("(?<=begin)[\\s\\S]*(?=end.)");
        Matcher matcherOfCode = ofCode.matcher(processingString);

        while (matcherOfCode.find()) {
          Cods = processingString.substring(matcherOfCode.start(), matcherOfCode.end());
        }
        parseVar();
        parseCode();
    }

    public void parseVar(){

        Pattern ofVariables = Pattern.compile("([a-zA-Z]([a-zA-Z0-9_]*[\\s]*,[\\s]*)*)([a-zA-Z][a-zA-Z0-9_]*)[\\s]*:[\\s]*integer[\\s]*;");
        Matcher matcherOfVariables = ofVariables.matcher(Vars);

        while (matcherOfVariables.find()) {
            arrayVarsString.add(Vars.substring(matcherOfVariables.start(), matcherOfVariables.end()));
        }
        parseVarsInteger();
    }

    public void parseVarsInteger(){
        Pattern ofVariables = Pattern.compile("[a-zA-Z][a-zA-Z0-9_]*");
        Matcher matcherOfVariables;
        String var;

        for (String string : arrayVarsString) {

            matcherOfVariables = ofVariables.matcher(string);

            while (matcherOfVariables.find()) {
                var = string.substring(matcherOfVariables.start(), matcherOfVariables.end());
                if (!var.equals("integer")) { arrayVars.add(var); }
            }
        }
        assemblerStructure.addToVariables(arrayVars);
    }

    public void parseCode(){

        Pattern ofCods = Pattern.compile("[a-zA-Z].*;");
        Matcher matcherOfCods = ofCods.matcher(Cods);
        while (matcherOfCods.find()) {

            arrayCodsString.add(Cods.substring(matcherOfCods.start(), matcherOfCods.end()));

        }

        createCode();
    }

    public void createCode(){

        Pattern forWrite = Pattern.compile("write\\('(.*)'\\);");
        Pattern forWriteLn = Pattern.compile("writeln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");

        Pattern forReadLn = Pattern.compile("readln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");
        Pattern forOperation = Pattern.compile("([a-z][a-zA-Z0-9]*)[\\s]*[\\s]*:=[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*([+\\-*/])[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*;");

        for (String string : arrayCodsString) {

            if(string.matches(forWrite.toString())){
                Matcher matcher = forWrite.matcher(string);

                if (matcher.find()) {
                    assemblerStructure.addToCodeWrite(matcher.group(1));
                }
            }

            else if(string.matches(forReadLn.toString())){
                Matcher matcher = forReadLn.matcher(string);

                if (matcher.find()) {
                    assemblerStructure.addToCodeReadLn(matcher.group(1));
                }
            }

            else if(string.matches(forOperation.toString())){
                Matcher matcher = forOperation.matcher(string);

                if (matcher.find()) {
                    assemblerStructure.addToCodeOperation(matcher.group(1), matcher.group(2), matcher.group(3), matcher.group(4));
                }
            }

            else if(string.matches(forWriteLn.toString())){
                Matcher matcher = forWriteLn.matcher(string);

                if (matcher.find()) {
                    assemblerStructure.addToCodeWriteLn(matcher.group(1));
                }
            }
        }
    }
}

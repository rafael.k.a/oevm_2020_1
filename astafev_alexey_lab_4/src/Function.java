import java.util.Random;

public class Function {
    static int sizeI = 16;
    static int sizeJ = 5;

    public void printArray(int[][] array){
        for(int i = 0;i<sizeI;i++){
            for(int j = 0;j<sizeJ;j++){
                System.out.print(array[i][j]+" ");
            }
            System.out.print("\n");
        }
    }

    public String DNF(int[][] array) {
        int X4 = 3;
        int F = 4;
        StringBuilder strBuilder = new StringBuilder();
        for (int i = 0; i < sizeI; i++) {
            if (array[i][F] == 1) {
                strBuilder.append("( ");
                for (int j = 0; j < X4; j++) {
                    if (array[i][j] == 1) {
                        strBuilder.append("X").append(j + 1).append(" * ");
                    }
                    else {
                        strBuilder.append("-X").append(j + 1).append(" * ");
                    }
                }
                if (array[i][X4] == 1) {
                    strBuilder.append("X4");
                }
                else {
                    strBuilder.append("-X4");
                }
                strBuilder.append(" ) + ");
            }
        }
        strBuilder.delete(strBuilder.toString().length() - 3, strBuilder.toString().length() - 1);
        return strBuilder.toString();
    }

    public String KNF(int[][] array) {
        int X4 = 3;
        int F = 4;
        StringBuilder strBuilder = new StringBuilder();

        for (int i = 0; i < sizeI; i++)
            if (array[i][F] == 0) {
                strBuilder.append("( ");
                for (int j = 0; j < X4; j++) {
                    if (array[i][j] == 0) {
                        strBuilder.append("X").append(j + 1).append(" + ");
                    }
                    else {
                        strBuilder.append("-X").append(j + 1).append(" + ");
                    }
                }
                if (array[i][X4] == 0) {
                    strBuilder.append("X4");
                }
                else {
                    strBuilder.append("-X4");
                }
                strBuilder.append(" ) * ");
            }

        strBuilder.delete(strBuilder.toString().length() - 3, strBuilder.toString().length() - 1);
        return strBuilder.toString();
    }
}

public class NumberSystemsConverter {

    //константы для удобства работы с символами
    final int LETTER_SHIFT = 55;
    final int DIGIT_SHIFT = 48;
    final String ERROR_MESSAGE = "Ошибка! Введены некорректные данные.";

    //метод для перевода числа в 10 с.с.
    public int convertToDecimalSystem(char[] originalNumber, int originalSystem) {
        int numberInDecimal = 0;
        int power = originalNumber.length - 1;
        if (originalSystem != 10) {
            for (char c : originalNumber) {
                if (Character.isDigit(c)) {
                    numberInDecimal += ((int) c - DIGIT_SHIFT) * Math.pow(originalSystem, power);
                } else if (Character.isLetter(c)) {
                    numberInDecimal += ((int) c - LETTER_SHIFT) * Math.pow(originalSystem, power);
                }
                power--;
            }
            return numberInDecimal;
        }
        else return Integer.parseInt(new String(originalNumber));
    }

    //метод для проверки соответствия заявленной исходной с.с. и исходного числа
    public boolean checkInputData(char[] originalNumber, int originalSystem) {
        for (char c : originalNumber) {
            if (c > (char) (originalSystem + DIGIT_SHIFT)) {
                System.out.println(ERROR_MESSAGE);
                return false;
            }
        }
        return  true;
    }

    //метод для перевода числа в конечную с.с.
    public String convertToFinalSystem(int originalNumber, int finalSystem) {
        StringBuilder finalNumber = new StringBuilder();
        while (originalNumber >= finalSystem) {
            if (originalNumber % finalSystem < 10) {
                finalNumber.append(originalNumber % finalSystem);
            }
            else if (originalNumber % finalSystem < 16) {
                finalNumber.append((char) (originalNumber % finalSystem + LETTER_SHIFT));
            }
            else return ERROR_MESSAGE;
            originalNumber /= finalSystem;
        }
        if (originalNumber > 10) {
            finalNumber.append((char) (originalNumber + LETTER_SHIFT));
        }
        else {
            finalNumber.append(originalNumber);
        }
        finalNumber.reverse();
        return finalNumber.toString();
    }
}

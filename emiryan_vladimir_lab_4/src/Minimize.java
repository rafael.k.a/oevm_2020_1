import java.util.Random;

public class Minimize {
    private final static int ROWS_NUMBER = 16;
    private final static int COLS_NUMBER = 5;
    private final static char[] LETTERS = {'A', 'B', 'C', 'D'};
    static int[][] tableOfTrue = new int[ROWS_NUMBER][COLS_NUMBER];

    public void fillTable() {
        Random random = new Random();
        for(int i = 0; i < ROWS_NUMBER; i++) {
            for(int j = 0; j < COLS_NUMBER; j++){
                tableOfTrue[i][j] = random.nextInt(2);
            }
        }
    }

    public void printTable() {
        for(int i = 0; i < ROWS_NUMBER; i++) {
            for(int j = 0; j < COLS_NUMBER; j++){
                System.out.print(tableOfTrue[i][j] + " ");
            }
            System.out.println();
        }
    }

    public String makeDNF() {
        String resultOfDNF = "";
        for(int i = 0; i < ROWS_NUMBER; i++) {
            if(tableOfTrue[i][COLS_NUMBER - 1] == 1) {
                if(resultOfDNF != "") {
                    resultOfDNF += " + ";
                }
                resultOfDNF += "(";
                for(int j = 0; j < COLS_NUMBER - 1; j++){
                    if(tableOfTrue[i][j] == 0) {
                        resultOfDNF += "(-" + LETTERS[j] + ")";
                    } else {
                        resultOfDNF += LETTERS[j];
                    }
                    if(j < COLS_NUMBER - 2) {
                        resultOfDNF += " * ";
                    }
                }
                resultOfDNF += ")";
            }
        }
        return resultOfDNF;
    }
}

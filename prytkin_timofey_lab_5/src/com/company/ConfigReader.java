package com.company;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

public class ConfigReader {

    private final Properties props;

    public ConfigReader() throws IOException {
        props = new Properties();
        props.load(new FileInputStream(new File("config")));
    }

    public HashMap<String, Integer> getConfig(){
        HashMap<String, Integer> map = new HashMap<>();

        map.put("countOfRows", Integer.parseInt(props.getProperty("countOfRows")));
        map.put("countOfColumns", Integer.parseInt(props.getProperty("countOfColumns")));
        map.put("heightAndWidthOfCells", Integer.parseInt(props.getProperty("heightAndWidthOfCells")));
        map.put("distanceFromTop", Integer.parseInt(props.getProperty("distanceFromTop")));
        map.put("distanceFromLeft", Integer.parseInt(props.getProperty("distanceFromLeft")));
        map.put("distanceFromBottomOfCell", Integer.parseInt(props.getProperty("distanceFromBottomOfCell")));
        map.put("distanceFromLeftOfCell", Integer.parseInt(props.getProperty("distanceFromLeftOfCell")));
        map.put("fontSize", Integer.parseInt(props.getProperty("fontSize")));

        return map;
    }
}

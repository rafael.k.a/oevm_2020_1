package com.company;

import javax.swing.*;
import java.io.IOException;

public class MainWindow {

    private TruthTable truthTable = new TruthTable();
    private JFrame frame;
    private JButton buttonDNF;
    private JButton buttonKNF;
    private JButton buttonNextStep;

    public MainWindow() throws IOException {
        initialize();
    }

    public static void main(String[] args) throws IOException {
        MainWindow mainWindow = new MainWindow();
        mainWindow.frame.setVisible(true);
    }

    private void initialize() {

        frame = new JFrame("Прыткин Тимофей Лаба 5");
        frame.setBounds(100, 100, 593, 810);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        JPanel panel = new Panel(truthTable);
        panel.setBounds(10, 11, 557, 596);
        frame.getContentPane().add(panel);

        JButton buttonCreateTruthTable = new JButton("Create Truth Table");
        buttonCreateTruthTable.addActionListener(e -> {
            truthTable.fillTruthTable();
            panel.repaint();
            truthTable.reset();
            buttonDNF.setEnabled(true);
            buttonKNF.setEnabled(true);
            buttonNextStep.setEnabled(false);
        });
        buttonCreateTruthTable.setBounds(10, 618, 557, 40);
        buttonCreateTruthTable.setEnabled(true);
        buttonCreateTruthTable.setFocusable(false);
        frame.getContentPane().add(buttonCreateTruthTable);

        buttonDNF = new JButton("DNF");
        buttonDNF.addActionListener(e -> {
            truthTable.chooseDNF();
            buttonDNF.setEnabled(false);
            buttonKNF.setEnabled(false);
            buttonNextStep.setEnabled(true);
        });
        buttonDNF.setBounds(10, 669, 275, 40);
        buttonDNF.setEnabled(true);
        buttonDNF.setFocusable(false);
        frame.getContentPane().add(buttonDNF);

        buttonKNF = new JButton("KNF");
        buttonKNF.addActionListener(e -> {
            truthTable.chooseKNF();
            buttonDNF.setEnabled(false);
            buttonKNF.setEnabled(false);
            buttonNextStep.setEnabled(true);
        });
        buttonKNF.setBounds(295, 669, 272, 40);
        buttonKNF.setEnabled(true);
        buttonKNF.setFocusable(false);
        frame.getContentPane().add(buttonKNF);

        buttonNextStep = new JButton("Next Step");
        buttonNextStep.addActionListener(e -> {
            if (!truthTable.getEndOfStepStatus()) { panel.repaint(); }
            buttonNextStep.setEnabled(!truthTable.getEndOfStepStatus());
        });
        buttonNextStep.setBounds(10, 720, 557, 40);
        buttonNextStep.setEnabled(false);
        buttonNextStep.setFocusable(false);
        frame.getContentPane().add(buttonNextStep);
    }
}


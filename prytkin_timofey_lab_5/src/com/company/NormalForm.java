package com.company;

import java.awt.*;

public class NormalForm {

    private final int countOfRows;//Количество строк
    private final int countOfColumns;//Количество столбцов
    public final String[] variables= {"X1", "X2", "X3", "X4" };//Перечень переменных

    public NormalForm(int countOfRows, int countOfColumns){
        this.countOfRows = countOfRows;
        this.countOfColumns = countOfColumns;
    }

    /**
     * Этот метод выыедет ДНФ до конкретного шага
     * @param truthTable Число в десятичной системае счисления
     * @param step Текущий шаг
     * @param g Полотно
     */
    public void drawDisjunctiveNormalForm(int[][] truthTable, int step, Graphics g) {
        boolean printPlus = false;
        StringBuilder stringBuilder;
        int countTerm = -1;

        for (int i = 0; i < step; ++i) {
            stringBuilder = new StringBuilder();
            if (truthTable[i][countOfColumns - 1] == 1) {
                if (printPlus) { stringBuilder.append(" + "); }
                countTerm++;
                stringBuilder.append("(");
                printPlus = true;

                for (int j = 0; j < countOfColumns - 1; ++j) {
                    if (truthTable[i][j] == 0) { stringBuilder.append("!"); }
                    stringBuilder.append(variables[j]);
                    if (j < countOfColumns - 2) { stringBuilder.append(" * "); }
                    else { stringBuilder.append(")"); }
                }
            }
            g.drawString(stringBuilder.toString(), 300, 50 + (countTerm * 40));
            if(i == countOfRows - 1){
                g.drawLine(300, 50 + ((countTerm + 1 ) * 40), 500, 50 + ((countTerm + 1) * 40));
            }
        }
    }

    /**
     * Этот метод выыедет КНФ до конкретного шага
     * @param truthTable Число в десятичной системае счисления
     * @param step Текущий шаг
     * @param g Полотно
     */
    public void drawConjunctiveNormalForm(int[][] truthTable, int step, Graphics g) {
        boolean printStar = false;
        StringBuilder stringBuilder;
        int countTerm = -1;

        for (int i = 0; i < step; ++i) {
            stringBuilder = new StringBuilder();
            if (truthTable[i][countOfColumns - 1] == 0) {

                if (printStar) { stringBuilder.append(" * "); }
                countTerm++;
                stringBuilder.append("(");
                printStar = true;

                for (int j = 0; j < countOfColumns - 1; ++j) {
                    if (truthTable[i][j] == 1) { stringBuilder.append("!"); }
                    stringBuilder.append(variables[j]);
                    if (j < countOfColumns - 2) { stringBuilder.append(" + "); }
                    else { stringBuilder.append(")"); }
                }
            }
            g.drawString(stringBuilder.toString(), 300, 50 + (countTerm * 40));
            if(i == countOfRows - 1){
                g.drawLine(300, 50 + ((countTerm + 1 ) * 40), 500, 50 + ((countTerm + 1) * 40));
            }
        }
    }
}

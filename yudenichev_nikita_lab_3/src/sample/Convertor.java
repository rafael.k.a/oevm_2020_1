package sample;

public class Convertor {

    public static long convertToDec(int initial, char[] firstNumber) {
        long numeral10 = 0;

        for (int i = 0; i < firstNumber.length; i++) {
            int currentNumeral = 1;
            if (firstNumber[i] >= '0' && firstNumber[i] <= '9') {
                currentNumeral = firstNumber[i] - '0';
            } else if (Character.isLetter(firstNumber[i])) {
                currentNumeral = 10 + firstNumber[i] - 'A';
            }
            numeral10 = (long) ((double) numeral10 + (double) currentNumeral * Math.pow(initial, firstNumber.length - i - 1));
        }
        return numeral10;
    }

    public static String convertToBinary(long numberDec) {
        int finalSystem = 2;
        StringBuilder result = new StringBuilder();
        long residue;

        for (String currentChar; numberDec > 0; numberDec /= finalSystem) {
            residue = numberDec % (long) finalSystem;
            currentChar = Long.toString(residue);
            result.insert(0, currentChar);
        }
        return result.toString();
    }
}



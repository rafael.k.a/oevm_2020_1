package com.company;

import java.io.FileReader;
import java.io.IOException;

public class ProgramsReader {
    public String read(){

        StringBuilder string = new StringBuilder();

        try(FileReader reader = new FileReader("Program.pas"))
        {
            int ch;
            while((ch = reader.read()) != -1){
                string.append((char) ch);
            }
        }
        catch(IOException ex){
            System.out.println("Ошибка при чтении");
        }

        return string.toString().replaceAll(";", ";\n");
    }

}

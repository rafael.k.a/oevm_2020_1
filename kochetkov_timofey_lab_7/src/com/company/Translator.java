package com.company;

import java.util.ArrayList;
import java.util.regex.*;

public class Translator {

    ProgramsWriter programsWriter = new ProgramsWriter();

    private String textProgramPascal;
    private String variables;
    private String code;
    private ArrayList<String> stringsOfVariables = new ArrayList<>();
    private ArrayList<String> stringsOfCode = new ArrayList<>();
    private ArrayList<String> arrayVariables = new ArrayList<>();

    public Translator(String string) {
        this.textProgramPascal = string;
    }

    public void writeProgram() {
        programsWriter.write();
    }

    public void splitCode() {

        Pattern patternVariables = Pattern.compile("(?<=var)[\\s\\S]*(?=begin)");
        Matcher matcherVariables = patternVariables.matcher(textProgramPascal);

        while (matcherVariables.find()) {
            variables = textProgramPascal.substring(matcherVariables.start(), matcherVariables.end());
        }

        Pattern patternCode = Pattern.compile("(?<=begin)[\\s\\S]*(?=end.)");
        Matcher matcherCode = patternCode.matcher(textProgramPascal);

        while (matcherCode.find()) {
            code = textProgramPascal.substring(matcherCode.start(), matcherCode.end());
        }

        convertVariables();
        convertCode();
    }

    public void convertVariables() {

        Pattern patternVariables = Pattern.compile("([a-zA-Z]([a-zA-Z0-9_]*[\\s]*,[\\s]*)*)([a-zA-Z][a-zA-Z0-9_]*)[\\s]*:[\\s]*integer[\\s]*;");
        Matcher matcherVariables = patternVariables.matcher(variables);

        while (matcherVariables.find()) {
            String variable = variables.substring(matcherVariables.start(), matcherVariables.end());
            stringsOfVariables.add(variable);
        }

        convertVariablesToInt();
    }

    public void convertVariablesToInt() {
        String variable;
        Pattern patternVariables = Pattern.compile("[a-zA-Z][a-zA-Z0-9_]*");
        Matcher matcherVariables;

        for (int i = 0; i < stringsOfVariables.size(); i++) {
            String stringVar = stringsOfVariables.get(i);
            matcherVariables = patternVariables.matcher(stringVar);

            while (matcherVariables.find()) {
                variable = stringVar.substring(matcherVariables.start(), matcherVariables.end());
                if (!variable.equals("integer")) {
                    arrayVariables.add(variable);
                }
            }
        }

        programsWriter.setArray(arrayVariables);
    }

    public void convertCode() {

        Pattern patternCode = Pattern.compile("[a-zA-Z].*;");
        Matcher matcherCode = patternCode.matcher(code);

        while (matcherCode.find()) {
            String codes = code.substring(matcherCode.start(), matcherCode.end());
            stringsOfCode.add(codes);
        }

        createCode();
    }

    public void createCode() {

        Pattern patternOperation = Pattern.compile("([a-z][a-zA-Z0-9]*)[\\s]*[\\s]*:=[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*([+\\-*/])[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*;");
        Pattern patternWrite = Pattern.compile("write\\('(.*)'\\);");
        Pattern patternWriteLn = Pattern.compile("writeln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");
        Pattern patternReadLn = Pattern.compile("readln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");


        for (int i = 0; i < stringsOfCode.size(); i++) {
            String string = stringsOfCode.get(i);

            if (string.matches(patternWrite.toString())) {
                Matcher matcherForCode = patternWrite.matcher(string);

                if (matcherForCode.find()) {
                    programsWriter.addToCodeWrite(matcherForCode.group(1));
                }
            } else if (string.matches(patternReadLn.toString())) {
                Matcher matcherForCode = patternReadLn.matcher(string);

                if (matcherForCode.find()) {
                    programsWriter.addToCodeReadLn(matcherForCode.group(1));
                }
            } else if (string.matches(patternOperation.toString())) {
                Matcher matcherForCode = patternOperation.matcher(string);

                if (matcherForCode.find()) {
                    programsWriter.addToCodeOperation(matcherForCode.group(1), matcherForCode.group(2), matcherForCode.group(3), matcherForCode.group(4));
                }
            } else if (string.matches(patternWriteLn.toString())) {
                Matcher matcherForCode = patternWriteLn.matcher(string);

                if (matcherForCode.find()) {
                    programsWriter.addToCodeWriteLn(matcherForCode.group(1));
                }
            }
            else {

            }
        }

    }


}

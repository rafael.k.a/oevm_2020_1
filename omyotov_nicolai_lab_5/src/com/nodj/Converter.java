package com.nodj;

import java.awt.*;

public class Converter {
    private final int[][] matrix = new int[16][5];
    private final int F = 5;
    private final int ROWS = 16;
    private boolean switcher = true; // true - KNF, false - DNF

    public Converter() {
        int container = 1;
        int divider = 8;
        for (int i = 0; i < F - 1; i++) {
            for (int j = 0; j < ROWS; j++) {
                if (j % divider == 0) {
                    container = container == 1 ? 0 : 1;
                }
                matrix[j][i] = container;
            }
            divider /= 2;
        }
        generate();
    }

    public void generate() {
        for (int j = 0; j < ROWS; j++) {
            matrix[j][F - 1] = (int) Math.round(Math.random());
        }
    }

    public void setSwitcher(boolean switcher) {
        this.switcher = switcher;
    }

    public void toPictureMatrix(Graphics g) {
        int h = 20, w = 20;
        int l = 30;
        int r = 30;
        g.setColor(new Color(100, 100, 100));
        for (int i = 0; i < F - 1; i++) {
            g.drawRect(l + i * w, r, w, h);
            g.drawString("x" + (i + 1), l + i * w + w / 3, r + h * 2 / 5);
        }

        g.drawRect(l + (F - 1) * w, r, w, h);
        g.drawString("F", l + (F - 1) * w + w / 3, r + h * 2 / 5);

        for (int i = 0; i < ROWS; i++) {
            if ((matrix[i][F - 1] == 0) == switcher)
                g.setColor(new Color(0, 100, 200));
            else
                g.setColor(new Color(100, 100, 100));
            for (int j = 0; j < F; j++) {
                g.drawRect(l + j * w, r + (i + 1) * h, w, h);
                g.drawString(matrix[i][j] + "", l + j * w + w / 3, r + h / 2 + (i + 1) * h);
            }
        }
    }

    public String showKNF() {
        boolean start = true;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < ROWS; i++) {
            if (matrix[i][F - 1] == 0) {
                if (start) {
                    sb.append("(");
                    start = false;
                } else {
                    sb.append(" *")
                            .append('\n')
                            .append('(');
                }

                for (int j = 0; j < F - 1; j++) {
                    if (j == 0) {
                        if (matrix[i][j] == 1)
                            sb.append("-x").append(j);
                        else
                            sb.append("x").append(j);
                    } else {
                        if (matrix[i][j] == 1)
                            sb.append(" + -x").append(j);
                        else
                            sb.append(" + x").append(j);
                    }
                }
                sb.append(")");
            }

        }

        return sb.toString();
    }

    public String showDNF() {
        boolean start = true;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < ROWS; i++) {
            if (matrix[i][F - 1] == 1) {
                if (start) {
                    start = false;
                } else {
                    sb.append(" +").append('\n');
                }

                for (int j = 0; j < F - 1; j++) {
                    if (j == 0) {
                        if (matrix[i][j] == 0)
                            sb.append("-x").append(j);
                        else
                            sb.append("x").append(j);
                    } else {
                        if (matrix[i][j] == 0)
                            sb.append(" -x").append(j);
                        else
                            sb.append(" x").append(j);
                    }
                }
            }

        }

        return sb.toString();
    }
}

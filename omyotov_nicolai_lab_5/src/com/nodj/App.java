package com.nodj;

import javax.swing.*;
import javax.swing.border.BevelBorder;

public class App {
    public JFrame frame;
    private final JButton generateButton = new JButton("Generate");
    private final Converter converter = new Converter();
    private final BoolPanel panel = new BoolPanel(converter);

    /**
     * Launch the application.
     */
    App() {
        initialize();
    }

    private void initialize() {
        int width = 200;
        int height = 450;
        frame = new JFrame();
        frame.setBounds(100, 100, 500, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        panel.setBorder(new BevelBorder(BevelBorder.LOWERED,
                null, null, null, null));
        panel.setBounds(10, 11, width, height);
        frame.getContentPane().add(panel);
        generateButton.addActionListener(e -> {
            converter.generate();
            panel.repaint();
        });
        generateButton.setBounds(250, 10, 100, 50);
        frame.getContentPane().add(generateButton);

        JTextArea functionTextArea = new JTextArea();
        functionTextArea.setBounds(250, 200, 200, 300);
        frame.getContentPane().add(functionTextArea);

        JButton showKNFButton = new JButton("show KNF");
        showKNFButton.setBounds(250, 70, 100, 50);
        frame.getContentPane().add(showKNFButton);
        showKNFButton.addActionListener(e -> {
            converter.setSwitcher(true);
            functionTextArea.setText(converter.showKNF());
            panel.repaint();
        });

        JButton showDNFButton = new JButton("show DNF");
        showDNFButton.setBounds(250, 130, 100, 50);
        frame.getContentPane().add(showDNFButton);
        showDNFButton.addActionListener(e -> {
            converter.setSwitcher(false);
            functionTextArea.setText(converter.showDNF());
            panel.repaint();
        });
    }
}

package com.company;

import java.util.Random;

public class MinKDNF {
    private final int[][] form = {
            {0, 0, 0, 0},
            {0, 0, 0, 1},
            {0, 0, 1, 0},
            {0, 0, 1, 1},
            {0, 1, 0, 0},
            {0, 1, 0, 1},
            {0, 1, 1, 0},
            {0, 1, 1, 1},
            {1, 0, 0, 0},
            {1, 0, 0, 1},
            {1, 0, 1, 0},
            {1, 0, 1, 1},
            {1, 1, 0, 0},
            {1, 1, 0, 1},
            {1, 1, 1, 0},
            {1, 1, 1, 1},
    };

    private final int[] randArr = new int[form.length];

    private void fillRand(){
        for(int i = 0; i < randArr.length; i++) {
            Random random = new Random();
            randArr[i] = random.nextInt(2);
        }
    }

    public void printForm(){
        StringBuilder strForm = new StringBuilder();
        fillRand();

        strForm.append("x1  x2  x3  x4  f" + '\n');

        for(int i = 0; i < form.length; i++){
            for(int j = 0; j < form[i].length; j++){
                strForm.append(form[i][j] + "   ");
            }
            strForm.append(randArr[i] + "   " + '\n');
        }
        System.out.println(strForm);
    }

    public void createDNF(){
        int cnt = 0;
        StringBuilder sbDNF = new StringBuilder();

        for(int i = 0; i < form.length; i++){
            if(randArr[i] == 1){
                if(cnt > 0){
                    sbDNF.append(" + ");
                }
                sbDNF.append("(");
                for(int j = 0; j < form[i].length; j++){
                    if(form[i][j] == 0) {
                        sbDNF.append("!");
                    }
                    sbDNF.append("x" + (j + 1));
                    if(j < form[i].length - 1){
                        sbDNF.append(" * ");
                    }
                }
                sbDNF.append(")");
                cnt++;
            }
        }
        sbDNF.append('\n');
        System.out.println(sbDNF);
    }

    public void createKNF(){
        int cnt = 0;
        StringBuilder sbKNF = new StringBuilder();

        for(int i = 0; i < form.length; i++){
            if(randArr[i] == 0){
                if(cnt > 0){
                    sbKNF.append(" * ");
                }
                sbKNF.append("(");
                for(int j = 0; j < form[i].length; j++){
                    if(form[i][j] == 1){
                        sbKNF.append("!");
                    }
                    sbKNF.append("x" + (j + 1));
                    if(j < form[i].length - 1){
                        sbKNF.append(" + ");
                    }
                }
                sbKNF.append(")");
                cnt++;
            }
        }
        sbKNF.append('\n');
        System.out.println(sbKNF);
    }

}

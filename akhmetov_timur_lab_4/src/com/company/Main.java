package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        MinKDNF kdnf = new MinKDNF();
        Scanner in = new Scanner(System.in);

        kdnf.printForm();

        System.out.println("Выберите ДНФ(1) или КНФ(2)");
        int form = in.nextInt();
        switch (form) {
            case 1 -> kdnf.createDNF();
            case 2 -> kdnf.createKNF();
            default -> System.out.println("Неверный ввод.");
        }


    }
}

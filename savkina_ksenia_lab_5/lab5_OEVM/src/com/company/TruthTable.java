package com.company;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.Random;

public class TruthTable {

    private int[][] truthTable;
    final public int CELL_HEIGHT = 30;
    final public int CELL_WIDTH = 40;
    private boolean tableIsCreate = false;
    private boolean dnfIsSet = false;
    private boolean knfIsSet = false;

    public TruthTable() {
        truthTable = new int[][] { { 0, 0, 0, 0, 0 }, { 0, 0, 0, 1, 0 },
                { 0, 0, 1, 0, 0 }, { 0, 0, 1, 1, 0 }, { 0, 1, 0, 0, 0 },
                { 0, 1, 0, 1, 0 }, { 0, 1, 1, 0, 0 }, { 0, 1, 1, 1, 0 },
                { 1, 0, 0, 0, 0 }, { 1, 0, 0, 1, 0 }, { 1, 0, 1, 0, 0 },
                { 1, 0, 1, 1, 0 }, { 1, 1, 0, 0, 0 }, { 1, 1, 0, 1, 0 },
                { 1, 1, 1, 0, 0 }, { 1, 1, 1, 1, 0 } };
    }

    /**
     * Создание таблицы
     */
    public void createTable() {
        Random random = new Random();
        int j = 4;
        for (int i = 0; i < truthTable.length; i++)
            truthTable[i][j] = random.nextInt(2);
        tableIsCreate = true;
        dnfIsSet = false;
        knfIsSet = false;
    }

    /**
     * Вывод таблицы
     */
    public void draw(Graphics g, int width, int height) {
        String[] header = { "x1", "x2", "x3", "x4", "F" };
        g.setFont(new Font("Courier", Font.BOLD + Font.ITALIC, 17));
        int top = 0;
        for (int j = 0; j < header.length; j++) {
            int left = j * CELL_WIDTH;
            g.drawRect(left, top, CELL_WIDTH, CELL_HEIGHT);
            if (tableIsCreate)
                g.drawString(header[j], left + CELL_WIDTH / 3, top + CELL_HEIGHT / 3 * 2);
        }
        g.setFont(new Font("Courier", Font.BOLD, 17));
        for (int i = 0; i < truthTable.length; i++) {
            top = (i + 1) * CELL_HEIGHT;
            for (int j = 0; j < truthTable[i].length; j++) {
                g.setColor(Color.black);
                int left = j * CELL_WIDTH;
                g.drawRect(left, top, CELL_WIDTH, CELL_HEIGHT);
                if (j == truthTable[i].length - 1)
                    g.setColor(Color.green);
                if (tableIsCreate)
                    g.drawString(String.valueOf(truthTable[i][j]), left + CELL_WIDTH / (7 / 2), top + CELL_HEIGHT / 3 * 2);
            }
        }
        g.setFont(new Font("Courier", Font.PLAIN, 15));
        g.setColor(Color.black);
        int y;
        if (dnfIsSet) {
            y = 75;
            for (String line : presentInDNF().split("\n"))
                g.drawString(line, 215, y += CELL_HEIGHT);
        }
        if (knfIsSet) {
            y = 325;
            for (String line : presentInKNF().split("\n"))
                g.drawString(line, 215, y += CELL_HEIGHT);
        }
    }

    /**
     * Представление в ДНФ
     */
    public String presentInDNF() {
        dnfIsSet = true;
        String str = "";
        int f;
        int k = 0;
        boolean isFirstPassage = true;
        for (int i = 0; i < truthTable.length; i++) {
            f = truthTable[0].length - 1;
            if (truthTable[i][f] == 1) {
                k++;
                if (isFirstPassage) {
                    str += "(";
                    isFirstPassage = false;
                } else
                    str += " + (";
                for (int j = 0; j < f; j++) {
                    if (truthTable[i][j] == 0)
                        str += "!" + getHeaderValue(j);
                    else
                        str += getHeaderValue(j);
                    if (j != f - 1)
                        str += " * ";
                }
                str += ")";
                if (k % 3 == 0)
                    str += "\n";
            }

        }
        return str;
    }

    /**
     * Представление в КНФ
     */
    public String presentInKNF() {
        knfIsSet = true;
        String str = "";
        int f;
        int k = 0;
        boolean isFirstPassage = true;
        for (int i = 0; i < truthTable.length; i++) {
            f = truthTable[0].length - 1;
            if (truthTable[i][f] == 0) {
                k++;
                if (isFirstPassage) {
                    str += "(";
                    isFirstPassage = false;
                } else
                    str += " * (";
                for (int j = 0; j < f; j++) {
                    if (truthTable[i][j] == 1)
                        str += "!" + getHeaderValue(j);
                    else
                        str += getHeaderValue(j);
                    if (j != f - 1)
                        str += " + ";
                }
                str += ")";
                if (k % 3 == 0)
                    str += "\n";
            }
        }
        return str;
    }

    /**
     * Получение переменной текеущего столбца
     *
     * @param j текущий столбец
     */
    private String getHeaderValue(int j) {
        switch (j) {
            case 0:
                return "x1";
            case 1:
                return "x2";
            case 2:
                return "x3";
            case 3:
                return "x4";
            default:
                return "ошибка";
        }
    }
}
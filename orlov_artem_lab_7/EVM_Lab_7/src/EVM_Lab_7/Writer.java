package EVM_Lab_7;

import java.io.FileWriter;

public class Writer {

    public static void createAssemblerFile(Translator translator) {
        try (FileWriter writer = new FileWriter("program.ASM")) {
            writer.write("format PE console\n\n" +
                    "entry start\n\n" +
                    "\tinclude 'win32a.inc'\n\n" +
                    "section '.data' data readable writable\n\n" +
                    "\tspaceString db '%d', 0\n" +
                    "\tnewLine db '%d', 0ah, 0\n");
            while (translator.hasVariable()) {
                writer.write("\t" + translator.removeVariable(0));
            }

            writer.write("\nsection '.code' data readable executable\n\n" + "\tstart:\n\n");

            while (translator.hasOperation()) {
                switch (translator.removeOperation()) {
                    case READ:
                        writer.write("\tpush " + translator.removeReadOperation() + "\n" + "\tpush spaceString\n" + "\tcall [scanf]\n\n");
                        break;
                    case WRITE:
                        writer.write("\tpush " + translator.removeWriteOperation() + "\n" + "\tcall [printf]\n\n");
                        break;
                    case WRITELN:
                        writer.write("\tpush [" + translator.removeWritelnOperation() + "]\n" + "\tpush newLine\n" + "\tcall [printf]\n\n");
                        break;
                    case MATHS:
                        String command = translator.removeMathsOperation();
                        String[] arguments = command.split(" ");
                        if (!command.contains("div")) {
                            writer.write("\tmov ecx, [" + arguments[1] + "]\n" +
                                    "\t" + arguments[3] + " ecx, [" + arguments[2] + "]\n" +
                                    "\tmov [" + arguments[0] + "], ecx\n\n");
                        } else {
                            writer.write("\tmov eax, [" + arguments[1] + "]\n" +
                                    "\tmov ecx, [" + arguments[2] + "]\n" +
                                    "\tdiv ecx\n" +
                                    "\tmov [" + arguments[0] + "], eax\n\n");
                        }
                        break;
                }
            }

            writer.write("\tcall [getch]\n" +
                    "\tpush NULL\n" +
                    "\tcall[ExitProcess]\n\n" +
                    "section '.idata' import data readable\n\n" +
                    "\tlibrary kernel, 'kernel32.dll',\\\n" +
                    "\t\tmsvcrt, 'msvcrt.dll'\n\n" +
                    "\timport kernel,\\\n" +
                    "\t\tExitProcess, 'ExitProcess'\n\n" +
                    "\timport msvcrt,\\\n" +
                    "\t\tprintf, 'printf',\\\n" +
                    "\t\tscanf, 'scanf',\\\n" +
                    "\t\tgetch, '_getch'");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

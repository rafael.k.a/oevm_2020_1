package EVM_Lab_7;

public class Main {

    public static void main(String[] args) {
        Translator translator = new Translator();
        Reader.readPascalFile("pascal.pas", translator);
        Writer.createAssemblerFile(translator);
        System.out.println("Done!");
    }
}


import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ной
 */
public class Principal {
    
     public static void main(String[] args) {
        // TODO code application logic here
        
        Scanner s = new Scanner(System.in);
        System.out.print("Укажите базу (2-16):   ");
        int ini = s.nextInt();
        System.out.print("Укажите число: ");
        char[] n1 = s.next().toCharArray();
        System.out.print("Укажите число: ");
        char[] n2 = s.next().toCharArray();
        System.out.print("Укажите операция  +, -, *, /  :  ");
        char sinal = s.next().charAt(0);
        long num1 = Secundaria.pDeci(ini, n1);
        char[] numb1 = Secundaria.pBin(num1).toCharArray();
        long num2 = Secundaria.pDeci(ini, n2);
        char[] numb2 = Secundaria.pBin(num2).toCharArray();

        switch (sinal) {
            case '+':
                System.out.println("Результат: " + Secundaria.Soma(numb1, numb2));
                break;
            case '-':
                System.out.println("Результат: " + Secundaria.Diferenca(numb1, numb2));
                break;
            case '*':
                System.out.println("Результат: " + Secundaria.Produto(numb1, numb2));
                break;
            case '/':
                System.out.println("Результат: " + Secundaria.Resto(numb1, numb2));
                break;
            default:
                System.out.println("Ошибка операция!");
                break;
        }
    }    
}

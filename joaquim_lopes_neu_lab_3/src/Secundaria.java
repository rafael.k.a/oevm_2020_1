
import java.util.Arrays;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ной
 */
public class Secundaria {
    
    public static long pDeci(int comecar, char[] n) {
        long numero = 0;
        for (int i = 0; i < n.length; ++i) {
            int atual = 1;
            if (n[i] >= '0' && n[i] <= '9') {
                atual = n[i] - '0';
            } else if (Character.isLetter(n[i])) {
                atual = 10 + n[i] - 'A';
            }
            numero = (long) ((double) numero + (double) atual * Math.pow(comecar, n.length - i - 1));
        }
        return numero;
    }

    public static String pBin(long numero) {
        long pensar;
        int fim = 2;
        StringBuilder result = new StringBuilder();

        for (String letra; numero > 0; numero /= fim) {
            pensar = numero % (long) fim;
            letra = Long.toString(pensar);
            result.insert(0, letra);
        }
        return result.toString();
    }

    public static String Soma(char[] n1, char[] n2) {
        StringBuilder n3 = new StringBuilder();
        char nchar;
        int n = 0;
        for (int i = n1.length - 1, j = n2.length - 1; i >= 0 || j >= 0 || n == 1; --i, --j) {
            int num1, num2;
            if (i < 0) {
                num1 = 0;
            } else if (n1[i] == '0') {
                num1 = 0;
            } else {
                num1 = 1;
            }

            if (j < 0) {
                num2 = 0;
            } else if (n2[j] == '0') {
                num2 = 0;
            } else {
                num2 = 1;
            }

            int somar = num1 + num2 + n;
            if (somar == 0) {
                nchar = '0';
                n = 0;
            } else if (somar == 1) {
                nchar = '1';
                n = 0;
            } else if (somar == 2) {
                nchar = '0';
                n = 1;
            } else {
                nchar = '1';
                n = 1;
            }
            n3.insert(0, nchar);
        }
        return n3.toString();
    }

    public static String Diferenca(char[] n1, char[] n2) {
        boolean dif = false;

        if (pDeci(2, n1) < pDeci(2, n2)) {
            dif = true;
            char[] aux = n2;
            n2 = n1;
            n1 = aux;
        } else if (pDeci(2, n1) == pDeci(2, n2)) {
            return "0";
        }

        StringBuilder resposta = new StringBuilder();

        int dn = n1.length - n2.length;
        char[] Isub = new char[n1.length];

        for (int i = 0; i < Isub.length; ++i) {
            if (i < dn) {
                Isub[i] = '1';
            } else {
                if (n2[i - dn] == '0') {
                    Isub[i] = '1';
                } else {
                    Isub[i] = '0';
                }
            }
        }

        char[] auxi = Soma(Isub, new char[]{'1'}).toCharArray();
        char[] djunta = Soma(auxi, n1).toCharArray();
        boolean test = true;

        for (int i = 1; i < djunta.length; ++i) {
            if (djunta[i] == '1') {
                test = false;
            } else {
                if (test) {
                    continue;
                }
            }
            resposta.append(djunta[i]);
        }
        if (dif) {
            resposta.insert(0, '-');
        }
        return resposta.toString();
    }

    public static String Produto(char[] n1, char[] n2) {
        char[] result = {};
        for (int i = 0; i < pDeci(2, n2); ++i) {
            result = (Soma(result, n1)).toCharArray();
        }
        return new String(result);
    }

    public static String Resto(char[] n1, char[] n2) {
        long resp = 0;
        while (pDeci(2, n1) >= pDeci(2, n2) && !Arrays.equals(n1, new char[]{'0'})) {
            n1 = (Diferenca(n1, n2)).toCharArray();
            resp++;
        }
        return pBin(resp);
    }
}

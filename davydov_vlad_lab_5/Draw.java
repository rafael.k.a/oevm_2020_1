package com.company;

import java.awt.*;

public class Draw {
    private final int rows = 16;
    private final int column = 5;
    private final int cellWidth = 30;
    private final int cellHeight = 30;
    private int posX = 10;
    private int posY = 10;

    public void draw(Graphics g, int array[][]) {
        for (int i = 0; i < column - 1; i++) {
            g.drawRect(posX + cellWidth * i, posY, cellWidth, cellHeight);
            g.drawString("X" + (i + 1), posX * 2 + cellWidth * i, posY * 3);
        }
        g.drawRect(posX + cellWidth * (column - 1), posY, cellWidth, cellHeight);
        g.drawString("F", posX + 10 + cellWidth * (column - 1), posY + 20);
        posY += 30;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < column; j++) {
                g.drawRect(posX + cellWidth * j, posY + cellHeight * i, cellWidth, cellHeight);
                g.drawString(array[i][j] + "", posX + 10 + cellWidth * j, posY + 20 + cellHeight * i);
            }
        }
        for (int i = 0; i < rows; i++) {
            if (Form.isDNF) {
                if (array[i][column - 1] == 1) {
                    g.setColor(Color.GREEN);
                } else {
                    g.setColor(Color.RED);
                }
            } else if (Form.isCNF) {
                if (array[i][column - 1] == 0) {
                    g.setColor(Color.GREEN);
                } else {
                    g.setColor(Color.RED);
                }
            }
            for (int j = 0; j < column; j++) {
                g.drawRect(posX + cellWidth * j, posY + cellHeight * i, cellWidth, cellHeight);
                g.drawString(array[i][j] + "", posX + 10 + cellWidth * j, posY + 20 + cellHeight * i);
            }
        }
    }
}


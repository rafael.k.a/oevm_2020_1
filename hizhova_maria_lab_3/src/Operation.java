import java.util.Arrays;

public class Operation{
    private static char signFirst;
    private static char signSecond;
    private static String binaryFirst;
    private static String binarySecond;
    private static String result = "";
    private static boolean reversed = false;

    public static String calculate(String oneN, String twoN, String operation) throws Exception {
        signFirst = oneN.toCharArray()[0];
        signSecond = twoN.toCharArray()[0];
        binaryFirst = oneN;
        binarySecond  = twoN;
        swipeNumbers();

        switch (operation) {
            case "-":
                getBinarySubtraction();
            case "+":
                getBinarySum();
                return result;
            case "*":
                getBinaryMultiplication();
                return result;
            case "/":
                getBinaryDivision();
                return result;
            default:
                throw new Exception();
        }
    }

    private static void getBinarySum() throws Exception {
        if (signFirst != '-' && signSecond != '-') {
            result += calculateSumBinaryNumbers(binaryFirst, binarySecond);
        } else if (signFirst == '-' && signSecond != '-') {
            reverseCode(binaryFirst, binarySecond);
            result += calculateSumBinaryNumbers(binaryFirst, binarySecond);
            result = deleteZero(result);
            result = new StringBuffer(result).reverse().toString();
            result += "-";
            result = new StringBuffer(result).reverse().toString();
        } else if (signFirst != '-') {
            reverseCode(binaryFirst, binarySecond);
            result += calculateSumBinaryNumbers(binaryFirst, binarySecond);
            result = deleteZero(result);
        } else {
            result += "-" + calculateSumBinaryNumbers(binaryFirst, binarySecond);
        }
    }

    private static void getBinarySubtraction() {
        if (reversed) {
            if (signFirst == '-') {
                signFirst = '+';
            } else {
                signFirst = '-';
            }
        } else {
            if (signSecond == '-') {
                signSecond = '+';
            } else {
                signSecond = '-';
            }
        }
    }

    private static void getBinaryMultiplication() {
        if ((signFirst == '-' && signSecond != '-') || (signFirst != '-' && signSecond == '-')) {
            result += "-";
        }
        result += calculateMultiplicationBinaryNumbers(binaryFirst, binarySecond);
    }

    private static void getBinaryDivision() throws Exception {
        if ((signFirst == '-' && signSecond != '-') || (signFirst != '-' && signSecond == '-')) {
            result += "-";
            result += calculateDivisionBinaryNumbers(binaryFirst, binarySecond);
            return;
        }
        result += calculateDivisionBinaryNumbers(binaryFirst, binarySecond);
    }

    private static String calculateSumBinaryNumbers(String firstNum, String secondNum) {
        String result = "";
        char[] firstNumArray = new StringBuffer(firstNum).reverse().toString().toCharArray();//реверс строки и перевод в массив символов
        char[] secondNumArray = new StringBuffer(secondNum).reverse().toString().toCharArray();

        int count = 0;
        char transfer = '0';

        for (int i = 0; i < firstNumArray.length; i++) {
            if (count < secondNumArray.length) {
                if (firstNumArray[i] == '1' && secondNumArray[i] == '1') {
                    result += transfer;
                    transfer = '1';
                    count++;
                }
                else if ((firstNumArray[i] == '1' && secondNumArray[i] == '0') ||
                        (firstNumArray[i] == '0' && secondNumArray[i] == '1')) {
                    if (transfer != '1') {
                        result += "1";
                    }
                    else {
                        result += "0";
                    }
                    count++;
                }
                else if (firstNumArray[i] == '0' && secondNumArray[i] == '0') {
                    result += transfer;
                    transfer = '0';
                    count++;
                }
            }
            else {
                if (firstNumArray[i] != '0' || transfer != '0') {
                    if ((firstNumArray[i] == '1' && transfer == '0') ||
                            (firstNumArray[i] == '0' && transfer == '1')) {
                        result += "1";
                        transfer = '0';
                    }
                    else if (firstNumArray[i] == '1' && transfer == '1') {
                        result += "0";
                    }
                }
                else {
                    result += "0";
                }
            }
        }
        if (transfer == '1') {
            result += "1";
        }
        return new StringBuffer(result).reverse().toString();
    }

    private static String calculateDivisionBinaryNumbers(String firstNum, String secondNum) throws Exception {
        String subtrahend = binaryFirst;
        String result = "0";
        reverseCode(firstNum, secondNum);

        if (secondNum.toCharArray().length == 1 && secondNum.toCharArray()[0] != '1') {
            throw new Exception();
        }
        else if (reversed) {
            return "0";
        }
        else
        {
            while (comparisonOfNumbers(subtrahend, secondNum)) {
                result = calculateSumBinaryNumbers(result, "1");
                subtrahend = calculateSumBinaryNumbers(subtrahend, binarySecond);
                subtrahend = deleteZero(subtrahend);
            }
            return result;
        }
    }

    private static String calculateMultiplicationBinaryNumbers(String firstNum, String secondNum) {
        char[] secondNumArray = new StringBuffer(secondNum).reverse().toString().toCharArray();
        String result = "0";
        int jIndex = 0;

        for (int i = 0; i < secondNumArray.length; i++) {
            if (secondNumArray[i] == '1') {
                for (int j = 0; j < i - jIndex; j++) {
                    firstNum += "0";
                }
                result = calculateSumBinaryNumbers(firstNum, result);
                jIndex = i;
            }
        }
        return result;
    }

    private static void reverseCode (String firstNum, String secondNum) {
        char[] firstNumArray = firstNum.toCharArray();
        char[] secondNumArray = new char[firstNumArray.length];
        char[] bufArray = secondNum.toCharArray();
        binarySecond = "";

        for (int i = 0; i < firstNumArray.length - bufArray.length; i++) {
            secondNumArray[i] = '0';
        }
        System.arraycopy(bufArray,0, secondNumArray,firstNumArray.length - bufArray.length, bufArray.length);
        for (char c : secondNumArray) {
            if (c == '1') {
                binarySecond += "0";
            }
            else {
                binarySecond += "1";
            }
        }
        binarySecond = calculateSumBinaryNumbers(binarySecond, "1");
    }

    private static String deleteZero(String value) {
        char[] valueArray = value.toCharArray();
        boolean firstElem = false;
        String result = "";
        for (int i = 1; i < valueArray.length; i++) {
            if (valueArray[i] == '1') {
                firstElem = true;
            }
            if (firstElem) {
                result += valueArray[i];
            }
        }
        if (result == "") {
            result = "0";
        }
        return result;
    }

    private static boolean comparisonOfNumbers(String firstNum, String secondNum) {
        char[] firstNumArray = firstNum.toCharArray();
        char[] secondNumArray = secondNum.toCharArray();

        if (firstNumArray.length > secondNumArray.length) {
            return true;
        }
        else if (firstNumArray.length < secondNumArray.length) {
            return false;
        }
        else {
            for(int i = 0; i < firstNumArray.length; i++) {
                if (firstNumArray[i] == '1' && secondNumArray[i] == '0') {
                    return true;
                }
                else if (firstNumArray[i] == '0' && secondNumArray[i] == '1'){
                    return false;
                }
            }
        }
        return true;
    }

    public static void swipeNumbers() {
        char[] firstArray = binaryFirst.toCharArray();
        char[] secondArray = binarySecond.toCharArray();

        if (firstArray.length < secondArray.length)
        {
            String tempStr = binaryFirst;
            char sign = signFirst;
            binaryFirst = binarySecond;
            signFirst = signSecond;
            binarySecond = tempStr;
            signSecond = sign;
            reversed = true;
            return;
        }
        if (firstArray.length == secondArray.length) {
            for (int i = 0; i < firstArray.length; i++) {
                if (firstArray[i] == '0' && secondArray[i] == '1') {
                    String tempStr = binaryFirst;
                    char sign = signFirst;
                    binaryFirst = binarySecond;
                    signFirst = signSecond;
                    binarySecond = tempStr;
                    signSecond = sign;
                    reversed = true;
                    return;
                }
            }
        }
    }
}



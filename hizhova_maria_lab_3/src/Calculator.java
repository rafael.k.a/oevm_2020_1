import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Calculator {

    private static final String symbols = "0123456789ABCDEF";
    private static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    private static int error = 0;
    private static int originalNumberSistem = 0;
    private static int resultingNumberSystem = 2;
    private static String operation="";
    private static String oneN;
    private static String twoN;

    public static void main(String[] args) throws Exception {
        originalNumberSistem=inputOfInitial(br,error,originalNumberSistem);
        char[] symbolsChar = symbols.substring(0, originalNumberSistem).toCharArray();
        String convertibleNumber = "";
        convertibleNumber=numberInput(br,error,convertibleNumber,symbolsChar);
        long charInt=conversion(convertibleNumber);
        oneN=convert(charInt, resultingNumberSystem, 0, "");
        convertibleNumber=numberInput(br,error,convertibleNumber,symbolsChar);
        charInt=conversion(convertibleNumber);
        twoN=convert(charInt, resultingNumberSystem, 0, "");
        operation=inputOperation(br,operation);

        System.out.println("Результат: " + Operation.calculate(oneN, twoN, operation));

    }

    public static long conversion(String convertibleNumber){
        long char10 = rebase(convertibleNumber, originalNumberSistem);
        long charInt = (int) char10;
        return charInt;
    }
    public static String inputOperation(BufferedReader br,String operation) throws IOException {
       System.out.print("Введите операцию(|+|-|*|/|): ");
       operation = br.readLine();
       return operation;
    }

    public static int inputOfInitial(BufferedReader br,int error,int originalNumberSistem){
        do {
            try {
                System.out.print("Введите основание исходной системы счисления: ");
                originalNumberSistem = Integer.parseInt(br.readLine());
                if (originalNumberSistem > 1 && originalNumberSistem < 17) {
                    error = 0;
                } else {
                    System.err.println("Основание системы должно находиться в диапазоне от 2 до 16 включительно");
                    error = 1;
                }
            } catch (Exception e) {
                System.err.println("Неверный формат основания системы");
                error = 1;
            }
        }
        while (error == 1);
        return originalNumberSistem;
    }
    public static String numberInput(BufferedReader br,int error,String convertibleNumber,char[] symbolsChar) throws IOException {
        do {
            System.out.print("Введите число: ");
            convertibleNumber = br.readLine().toUpperCase();
            char[] convertibleNumberArray =convertibleNumber.toCharArray();
            for (char Num :convertibleNumberArray) {
                if ((Arrays.binarySearch(symbolsChar, Num) >= 0)) {
                    error = 0;
                }
            }
            if (convertibleNumber.length() <= 0) {
                System.err.println("Введите значение");
                error = 1;
            }
        }
        while (error == 1);
        return convertibleNumber;
    }


    public static long rebase(String number, int base) {
        long result = 0;
        int position = number.length();
        for (char ch : number.toCharArray()) {
            int value = symbols.indexOf(ch);
            result += value * Math.pow(base, --position);
        }
        return result;
    }

    private static String convert(long charint, int base, int position, String result) {
        char[] symbolsC = symbols.toCharArray();
        if (charint < Math.pow(base, position + 1)) {
            return symbolsC[(int) (charint / (int) Math.pow(base, position))] + result;
        } else {
            int remainder = (int) (charint % (int) Math.pow(base, position + 1));
            return convert(charint - remainder, base, position + 1, symbolsC[remainder / (int) (Math.pow(base, position))] + result);
        }
    }

}

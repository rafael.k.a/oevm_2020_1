package com.company;

public class Convertor {
    public void convertToFiniteNotation(int initialNotation, int finiteNotation, String number) {
        long number_ = 0;
        int numberLength = number.length();
        for (int i = 0; i < numberLength; i++) {
            long symbol = number.charAt(i) - 48;
            if (symbol > 9) symbol -= 7;
            for (int j = numberLength - 1; j > i; j--) {
                symbol *= initialNotation;
            }
            number_ += symbol;
        }
        long auxiliaryForCount = number_;
        int count = 0;
        while (auxiliaryForCount > 0) {
            auxiliaryForCount /= finiteNotation;
            count++;
        }

        if (count == 0) count++;
        char[] resultArr = new char[count];
        for (int i = 0; i < count; i++) {
            resultArr[i] += number_ % finiteNotation;
            resultArr[i] += 48;
            if (resultArr[i] > 57) resultArr[i] += 7;
            number_ /= finiteNotation;
        }
        for (int i = count - 1; i >= 0; i--) {
            System.out.print(resultArr[i]);
        }
    }
}

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter the original number system : ");
		int NowO = in.nextInt();
		System.out.print("Enter a new number system : ");
		int NextO = in.nextInt();
		in.nextLine();
		System.out.print("Enter a number in the original number system : ");
		String numb = in.nextLine();
		int numbTen = Convert.toTen(numb, NowO); // Conversion to decimal system
		if (numbTen == -1) {
			return;
		}
		System.out.print(Convert.toNextO(numbTen, NextO));// Translation and output of a number in the required
																// number system
	}
}


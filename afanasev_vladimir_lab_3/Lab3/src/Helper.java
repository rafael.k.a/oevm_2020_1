public class Helper {

    private final char zero = '0';
    private final char one = '1';

    private int firstNumber = 0;
    private int secondNumber = 0;
    private int transfer = 0;

    private String firstNum;
    private String secondNum;

    private char action;

    private String firstNumToEndSS;
    private String secondNumToEndSS;

    public Helper(int startSS, String firstNumber, String secondNumber, char action) {
        this.firstNum = firstNumber;
        this.secondNum = secondNumber;
        this.action = action;
        translateToDecimalSS(startSS);
        translateToBinarySS();
    }

    private Boolean compare(String strFirst, String strSecond) {
        char[] strArrayFirst = new StringBuilder(strFirst).reverse().toString().toCharArray();
        char[] strArrayTwo = new StringBuilder(strSecond).reverse().toString().toCharArray();
        if (strArrayFirst.length > strArrayTwo.length) {
            return true;
        }
        if (strArrayFirst.length < strArrayTwo.length) {
            return false;
        }
        if (strArrayFirst.length == strArrayTwo.length) {
            for (int i = 0; i < strArrayFirst.length; i++) {
                if (strArrayFirst[i] == one && strArrayTwo[i] == zero) {
                    return true;
                }
                if (strArrayFirst[i] == zero && strArrayTwo[i] == one) {
                    return false;
                }
            }
        }
        return true;
    }

    private void translateToDecimalSS(int startSS) {
        char[] firstNumArray = firstNum.toCharArray();
        char[] secondNumArray = secondNum.toCharArray();
        for (int i = 0; i < firstNumArray.length; i++) {
            int temp;
            if (firstNumArray[i] >= '0' && firstNumArray[i] <= '9') {
                temp = firstNumArray[i] - '0';
            } else {
                temp = 10 + firstNumArray[i] - 'A';
            }
            firstNumber += temp * Math.pow(startSS, firstNumArray.length - i - 1);
        }
        for (int i = 0; i < secondNumArray.length; i++) {
            int temp;
            if (secondNumArray[i] >= '0' && secondNumArray[i] <= '9') {
                temp = secondNumArray[i] - '0';
            } else {
                temp = 10 + secondNumArray[i] - 'A';
            }
            secondNumber += temp * Math.pow(startSS, secondNumArray.length - i - 1);
        }
    }

    private void translateToBinarySS() {
        StringBuilder strFirstNum = new StringBuilder();
        StringBuilder strSecondNum = new StringBuilder();
        if (firstNumber == 0) {
            strFirstNum.append("0");
        }
        for (int i = 0; firstNumber > 0; i++) {
            strFirstNum.append((char) (firstNumber % 2 + '0'));
            firstNumber /= 2;
        }
        firstNumToEndSS = strFirstNum.toString();
        if (secondNumber == 0) {
            strSecondNum.append("0");
        }
        for (int i = 0; secondNumber > 0; i++) {
            strSecondNum.append((char) (secondNumber % 2 + '0'));
            secondNumber /= 2;
        }
        secondNumToEndSS = strSecondNum.toString();
    }

    public String calculate() {
        StringBuilder str = new StringBuilder();
        switch (action) {
            case '+':
                str.append(add(firstNumToEndSS, secondNumToEndSS));
                return str.reverse().toString();
            case '-':
                str.append(subtract(firstNumToEndSS, secondNumToEndSS)).reverse();
                return deleteZero(str.toString());
            case '*':
                str.append(multiplicate(firstNumToEndSS, secondNumToEndSS)).reverse();
                return str.toString();
            case '/':
                str.append(divide(firstNumToEndSS, secondNumToEndSS)).reverse();
                return str.toString();
        }
        throw new ArithmeticException("division by 0");
    }

    private String addFirstPart(char firstNum,char secondNum){
        if (transfer == 0) {
            if (firstNum == zero && secondNum == zero) {
                transfer = 0;
                return "0";
            }
            if ((firstNum == zero && secondNum == one) || (firstNum == one && secondNum == zero)) {
                transfer = 0;
                return "1";
            }
            if (firstNum == one && secondNum == one) {
                transfer = 1;
                return "0";
            }
        } else {
            if (firstNum == zero && secondNum == zero) {
                transfer = 0;
                return "1";
            }
            if ((firstNum == zero && secondNum == one) || (firstNum == one && secondNum == zero)) {
                transfer = 1;
                return "0";
            }
            if (firstNum == one && secondNum == one) {
                transfer = 1;
                return "1";
            }
        }
        return "Это никогда не случится";
    }
    private String addSecondPart(char firstNum){
        if (firstNum == zero && transfer == 0) {
            transfer = 0;
            return "0";
        }
        if ((firstNum == zero && transfer == 1) || (firstNum == one && transfer == 0)) {
            transfer = 0;
            return "1";
        }
        if (firstNum == one && transfer == 1) {
            transfer = 1;
            return "0";
        }
        return "Это никогда не случится";
    }

    private String add(String strFirst, String strSecond) {
        char[] firstNumArray = strFirst.toCharArray();
        char[] secondNumArray = strSecond.toCharArray();
        StringBuilder result = new StringBuilder();
        int i = 0;
        while (i < secondNumArray.length) {
            result.append( addFirstPart(firstNumArray[i],secondNumArray[i]));
            i++;
        }
        while (i < firstNumArray.length) {
            result.append( addSecondPart(firstNumArray[i]));
            i++;
        }
        if (transfer == 1) {
            result.append("1");
        }
        return result.toString();
    }

    private String subtract(String strFirst, String strSecond) {
        StringBuilder result = new StringBuilder();
        strSecond = upend(strSecond);
        result.append(add(strFirst, strSecond));
        result.deleteCharAt(result.length() - 1);
        return result.toString();
    }

    private String multiplicate(String strFirst, String strSecond) {
        char[] strArraySecond = strSecond.toCharArray();
        StringBuilder strArray1 = new StringBuilder(strFirst);
        String result = "0";
        int iIndex = 0;
        for (int i = 0; i < strArraySecond.length; i++) {
            if (strArraySecond[i] == one) {
                for (int j = 0; j < i - iIndex; j++) {
                    strArray1.insert(0, '0');
                }
                result = add(strArray1.toString(), result);
                iIndex = i;
            }
        }
        return result;
    }

    private String divide(String strFirst, String strSecond) {
        String comparison = strSecond;
        String result = "0";
        if (strSecond.toCharArray().length == 1 && strSecond.toCharArray()[0] != '1') {
            throw new ArithmeticException("division by 0");
        }
        while (compare(strFirst, comparison)) {
            result = add(result, "1");
            comparison = add(comparison, strSecond);
        }
        return result;
    }

    private String upend(String str) {
        StringBuilder string = new StringBuilder();
        StringBuilder result = new StringBuilder();
        char[] digitArray = str.toCharArray();
        int i = 0;
        while (i < secondNumToEndSS.length()) {
            if (digitArray[i] == one) {
                string.append("0");
            } else {
                string.append("1");
            }
            i++;
        }
        while (i < firstNumToEndSS.length()) {
            string.append("1");
            i++;
        }
        result.append(add(string.toString(), "1"));
        return result.toString();
    }

    private String deleteZero(String str) {
        StringBuilder result = new StringBuilder();
        char[] digitArray = str.toCharArray();
        int i = 0;
        while (digitArray[i] == zero) {
            i++;
            if (i == str.length()) {
                return "0";
            }
        }
        while (i < str.length()) {
            result.append(digitArray[i]);
            i++;
        }
        return result.toString();
    }
}
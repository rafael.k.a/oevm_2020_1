package com.company;

import javax.swing.*;
import java.awt.*;

public class PanelFunction extends JPanel {
    LogicFunction function;
    public PanelFunction(LogicFunction function) {
        this.function = function;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        function.draw(g);
    }
}

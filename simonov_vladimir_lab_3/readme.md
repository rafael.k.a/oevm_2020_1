## Лабораторная работа №3
**Выполнил Симонов Владимир**
**ПИбд-22**

Ссылка на видео: https://drive.google.com/file/d/1Xbtvj2hndXWC0eXnhT3KbqvMXgrmWTQV/view?usp=sharing<br>
<H3> Как запустить?</H3>
Запустить лабу через Main.java
<H3>Какие технологии использовались?</H3>
Использовалась IntelliJ IDEA и стандартные библиотеки Java. Использовал StringBuilder,ArrayList,HashMap
<H3>Функционал программы</H3>
Переводит два введенных числа в указанную с.с. и осуществляет арифметические операции над двоичными числами. 
Доступные операции: Сложение, вычитание, умножение, деление

<table bgcolor="#DCDCDC" border="2">
    <caption >
        Пример
    </caption>
    <tr>
        <th rowspan="2">№</th>
        <th colspan="4">Входные данные</th>
        <th colspan="3">Выходные данные</th>
    </tr>
    <tr>
			<th>с.с</th>
            <th>A</th>
            <th>B</th>
			<th>Операция</th>
			<th>А в 2 с.с</th>
            <th>B в 2 с.с</th>
            <th>Результат</th>
    </tr>
    <tr>
        <td>1.</td>
        <td>10</td>
        <td>154</td>
        <td>89</td>
		<td>*</td>
        <td>10011010</td>
        <td>1011001</td>
		<td>11010110001010</td>
    </tr>
    <tr>
        <td>2.</td>
        <td>16</td>
        <td>ABCD</td>
        <td>ABBD</td>
		<td>-</td>
        <td>1010101111001101</td>
        <td>1010101110111101</td>
		<td>10000</td>
    </tr>
    <tr>
        <td>3.</td>
        <td>8</td>
        <td>7654</td>
        <td>76</td>
		<td>+</td>
        <td>11111010110</td>
        <td>111110</td>
		<td>111111101010</td>
    </tr>
    <tr>
        <td>4.</td>
        <td>10</td>
        <td>874</td>
        <td>0</td>
		<td>/</td>
        <td>1101101010</td>
        <td>0</td>
		<td>Error! Делить на ноль нельзя</td>
    </tr>   
</table>
</body>
</html>
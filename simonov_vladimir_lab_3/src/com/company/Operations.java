package com.company;

public enum Operations {

    Sum,
    Sub,
    Multiplication,
    Division;

    public static Operations getOperations(String choice) {
        switch (choice) {
            case "+": {
                return Operations.Sum;
            }
            case "-": {
                return Operations.Sub;
            }
            case "*": {
                return Operations.Multiplication;
            }
            case "/": {
                return Operations.Division;
            }
        }
        return null;
    }
}



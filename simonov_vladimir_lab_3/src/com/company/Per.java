package com.company;

import static java.lang.Math.pow;

public class Per {

    private StringBuilder result;
    // Переменные для удобства переовда и проверки
    int n = 55;
    int m = 48;
    int d = 10;
    // Перевод строки в 10с.с
    public long translateTo10(int sSystem, String enter) {
        long dSystem = 0;
        for (int i = 0; i < enter.length(); i++) {
            if (Character.isLetter(enter.charAt(i))) {
                if (enter.charAt(i) - n < sSystem) {
                    dSystem += (enter.charAt(i) - n) * pow(sSystem, enter.length() - 1 - i);
                } else {
                    return 0;
                }
            } else {
                if (Character.getNumericValue(enter.charAt(i)) < sSystem) {
                    dSystem += (enter.charAt(i) - m) * pow(sSystem, enter.length() - 1 - i);
                } else {
                    return 0;
                }
            }
        }
        return dSystem;
    }
    // Перевод из стартовой с.с в конечную
    public String translate(int startSystem, int endSystem, String enter) {
        result = new StringBuilder();
        long decimalSystem = translateTo10(startSystem, enter);
        if (decimalSystem < endSystem)
        {
            if (decimalSystem < d) {
                this.result = this.result.append(decimalSystem);
            } else {
                this.result = this.result.append((char) (decimalSystem + n));
            }
        } else {
            while (decimalSystem / endSystem != 0) {
                if (decimalSystem % endSystem < d) {
                    this.result.append(decimalSystem % endSystem);
                } else {
                    this.result.append((char) (decimalSystem % endSystem + n));
                }
                decimalSystem /= endSystem;
            }
            if (decimalSystem < d) {
                this.result.append(decimalSystem);
            } else {
                this.result.append((char) (decimalSystem + n));
            }
        }
        if (this.result.length() == 0) {
            this.result.replace(0, result.length(), "Error! Неверный ввод");
        } else {
            this.result.reverse();
        }
        return this.result.toString();
    }
}


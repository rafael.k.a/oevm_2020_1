package com.company;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // write your code here
        Calculator calculator = new Calculator();
        calculator.list();

        Scanner in = new Scanner(System.in);
        int originalSystem = 0;
        String a = "";
        String b = "";
        String choice = "";
        System.out.println("Введите с.с");
        try {
            originalSystem = in.nextInt();
            System.out.println("Введите первое число");
            a = in.next();
            System.out.println("Введите второе число");
            b = in.next();
            System.out.println("Введите знак операции");
            choice = in.next();
        } catch (InputMismatchException e) {
            System.out.printf("Ошибка ввода");
            return;
        }
        in.close();

        calculator.setArguments(a, b, originalSystem);
        calculator.setChoice(choice);
        calculator.toCalculate();
    }
}

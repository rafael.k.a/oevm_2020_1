package com.company;

import java.util.ArrayList;
import static java.lang.Math.max;

public class Sum implements IArithmetic {
    private ArrayList<Integer> result;

    /**
     * Метод сложения 2х чисел в 2с.с, передающихся в ArrayList
     */
    @Override
    public ArrayList<Integer> startArithmeticOperation(ArrayList<Integer> num1, ArrayList<Integer> num2) {
        this.result = new ArrayList<>();
        int tmp = 0;
        for (int i = 0; i < max(num1.size(), num2.size()); i++) {
            if (i >= num1.size() || i >= num2.size()) {
                if (i >= num1.size()) {
                    tmp += num2.get(i);
                } else {
                    tmp += num1.get(i);
                }
            } else {
                tmp += num1.get(i) + num2.get(i);
            }
            switch (tmp) {
                case 0:
                case 1: {
                    this.result.add(tmp);
                    tmp = 0;
                    break;
                }
                case 2: {
                    tmp -= 2;
                    this.result.add(tmp);
                    tmp++;
                    break;
                }
                case 3: {
                    tmp -= 2;
                    this.result.add(tmp);
                    break;
                }
            }
        }
        if (tmp == 1) {
            this.result.add(1);
        }
        return this.result;
    }
}
package com.company;

public class TruthTable {
    int[][] truthTable;
    final int N;
    final int M;
    String[] x = {"x1", "x2", "x3", "x4"};

    TruthTable(int N, int M, int[][] truthTable) {
        for (int i = 0; i < N; ++i) {
            char[] strX = binaryСonversion(i);
            for (int j = 0; j < M - 1; ++j) {
                truthTable[i][j] = strX[j] - '0';
            }
            truthTable[i][M - 1] = (int) (Math.random() * 2);
        }
        this.truthTable = truthTable;
        this.N = N;
        this.M = M;
    }

    public char[] binaryСonversion(int number) {
        String result = "";
        int remainder = 1;
        int binaryNotation = 2;
        String currentSymbol = "";
        while (number > 0) {
            remainder = number % binaryNotation;
            currentSymbol = "";
            if (number % binaryNotation < 10) {
                currentSymbol = Integer.toString(remainder);
            } else {
                currentSymbol = currentSymbol + (char) ((int) ('A' + remainder - 10));
            }
            result = currentSymbol + result;
            number /= binaryNotation;
        }
        int amountOfSymbols = 4;
        while (result.length() < amountOfSymbols) {
            result = '0' + result;
        }
        return result.toCharArray();
    }

    public void printTruthTable(int[][] truthTable) {
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < M - 1; ++j) {
                System.out.print(truthTable[i][j] + " ");
            }
            System.out.println("= " + truthTable[i][M - 1]);
        }
    }

    public void toDisjunctiveNF(int[][] truthTable) {
        boolean DNF = true;
        for (int i = 0; i < N; ++i) {
            if (truthTable[i][M - 1] == 1) {
                if (!DNF) {
                    System.out.print(" + ");
                }
                System.out.print("(");
                DNF = false;
                for (int j = 0; j < M - 1; ++j) {
                    if (truthTable[i][j] == 0) {
                        System.out.print("!");
                    }
                    System.out.print(x[j]);
                    if (j < M - 2) {
                        System.out.print(" * ");
                    } else {
                        System.out.print(")");
                    }
                }
            }
        }
    }

    public void toConjunctiveNF(int[][] truthTable) {
        boolean CNF = true;
        for (int i = 0; i < N; ++i) {
            if (truthTable[i][M - 1] == 0) {
                if (!CNF) {
                    System.out.print(" * ");
                }
                System.out.print("(");
                CNF = false;
                for (int j = 0; j < M - 1; ++j) {
                    if (truthTable[i][j] == 1) {
                        System.out.print("!");
                    }
                    System.out.print(x[j]);
                    if (j < M - 2) {
                        System.out.print(" + ");
                    } else {
                        System.out.print(")");
                    }
                }
            }
        }
    }
}
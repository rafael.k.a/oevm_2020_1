package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int N = 16;
        int M = 5;
        int[][] truthTable = new int[N][M];
        int enter = 0;
        Scanner in = new Scanner(System.in);
        while (enter != 1 && enter != 2) {
            System.out.printf("Введите 1 для ДНФ\nВведите 2 для КНФ\nВводить тут: ");
            enter = in.nextInt();
        }
        TruthTable table = new TruthTable(N, M, truthTable);
        truthTable = table.truthTable;
        table.printTruthTable(truthTable);
        if (enter == 1) {
            System.out.print("\nДНФ: ");
            table.toDisjunctiveNF(truthTable);
        } else if (enter == 2) {
            System.out.print("\nКНФ: ");
            table.toConjunctiveNF(truthTable);
        }
    }
}
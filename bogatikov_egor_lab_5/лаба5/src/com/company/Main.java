package com.company;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;


public class Main {
    Calculations methods = new Calculations();
    StringBuilder strBuilder = new StringBuilder();
    static int[][] arrayOfNumbers = new int[new Variables().getPropertyValue("heightArray")+1][new Variables().getPropertyValue("widthArray") + 1]; // массив всех чисел
    JFrame frame;
    JButton buttonStart;
    JButton buttonDNF;
    JButton buttonKNF;
    JTextArea formula = new JTextArea();
    public JPanel ArrayPanel = new PanelOfArray(arrayOfNumbers);
    static boolean dnf = false;
    static boolean knf = false;

    public Main() {

        frame = new JFrame();
        frame.setSize(1000,700);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("My program");
        frame.setVisible(true);
        frame.getContentPane().setLayout(null);
        formula.setEditable(false);
        formula.setBounds(200, 60, 260, 300);
        frame.add(formula);

        buttonStart = new JButton("Start");
        buttonStart.setBounds(200,10,100,30);
        buttonStart.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ArrayPanel.setBounds(0, 0, 170, 600);
                frame.getContentPane().add(ArrayPanel);
                ArrayPanel.repaint();
                buttonStart.setEnabled(false);
                buttonDNF.setEnabled(true);
                buttonKNF.setEnabled(true);
            }
        });
        frame.add(buttonStart);

        buttonDNF = new JButton("DNF");
        buttonDNF.setBounds(310, 10, 70, 30);
        buttonDNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                formula.setText(strBuilder.append(methods.dnfСalculations(arrayOfNumbers)).toString());
                buttonDNF.setEnabled(false);
                buttonKNF.setEnabled(false);
                dnf = true;
                ArrayPanel.repaint();
            }
        });
        frame.add(buttonDNF);

        buttonKNF = new JButton("KNF");
        buttonKNF.setBounds(390, 10, 70, 30);
        buttonKNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                formula.setText(strBuilder.append(methods.knfСalculations(arrayOfNumbers)).toString());
                buttonDNF.setEnabled(false);
                buttonKNF.setEnabled(false);
                knf = true;
                ArrayPanel.repaint();
            }
        });
        frame.add(buttonKNF);
        buttonDNF.setEnabled(false);
        buttonKNF.setEnabled(false);
    }

    public static void main(String[] args) {
        Calculations.outputOnDisplay(arrayOfNumbers);
        System.out.print(new Variables().getPropertyValue("heightArray"));
        new Main();
    }
}

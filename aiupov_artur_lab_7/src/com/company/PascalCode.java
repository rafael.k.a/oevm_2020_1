package com.company;

import java.io.FileReader;
import java.io.IOException;

public class PascalCode {

    public String read(){
        StringBuilder string = new StringBuilder();

        try(FileReader reader = new FileReader("pascalProgram.pas"))
        {
            int c;
            while((c=reader.read())!=-1){

                string.append((char) c);
            }
        }

        catch(IOException ex){
            System.out.println(ex.getMessage());
        }

        return string.toString().replaceAll(";", ";\n");
    }
}

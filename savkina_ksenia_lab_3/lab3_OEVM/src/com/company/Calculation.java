package com.company;

public class Calculation {
    /**
     * Проверка входит ли число в данную систему счисления
     * @param originalNS Исходная СС
     * @param strNum Число для проверки
     */
    public boolean isNumOriginalNS(long originalNS, String strNum) {
        int k = 0;
        for (int i = 0; i < strNum.length(); i++) {
            if (Character.isDigit(strNum.charAt(i))) {
                if ((strNum.charAt(i) - '0') < originalNS)
                    k++;
            } else {
                if (Character.isUpperCase(strNum.charAt(i))) {
                    if ((int) strNum.charAt(i) - 55 < originalNS)
                        k++;
                }
            }
            if (k == strNum.length())
                return true;
        }
        return false;
    }

    /**
     * Перевод в число из строки
     * @param originalNS Исходная СС
     * @param strNum Число для перевода
     */
    private long convertToNumber(long originalNS, String strNum) {
        if (strNum.equals("0"))
            return 0;
        if (originalNS != 10) {
            long num = 0;
            long k = 0;
            for (int i = (strNum.length() - 1); i >= 0; i--) {
                if (!Character.isDigit(strNum.charAt(i)))
                    num += ((long) (strNum.charAt(i) - 55)) * (long) Math.pow(originalNS, k);
                else
                    num += ((long) (strNum.charAt(i) - 48)) * (long) Math.pow(originalNS, k);
                k++;
            }
            return num;
        }
        return Integer.valueOf(strNum);
    }

    /**
     * Перевод в между системами счисления
     * @param originalNS Исходная СС
     * @param finiteNS Конечная СС
     * @param result Число для перевода, на выходе в нужной СС
     */
    private String translate(long originalNS, long finiteNS, String result) {
        long num = convertToNumber(originalNS, result);
        result = "";
        while (num != 0) {
            long k = num % finiteNS;
            if (k >= 10) {
                char tmpNum = (char) (k + 55);
                result = tmpNum + result;
            } else {
                char tmpNum = (char) (k + 48);
                result = tmpNum + result;
            }
            num /= finiteNS;
        }
        return result;
    }

    /**
     * Сложение
     * @param strNum1 Первое слагаемое
     * @param strNum2 Второе слагаемое
     */
    private String add(String strNum1, String strNum2) {
        String result = "";
        boolean isHaveExtraNum = false;
        int smallLength;
        int largeLength;
        String strlargeLength;
        String strSmallLength;
        if (strNum1.length() < strNum2.length()) {
            smallLength = strNum1.length();
            largeLength = strNum2.length();
            strSmallLength = strNum1;
            strlargeLength = strNum2;
        } else {
            smallLength = strNum2.length();
            largeLength = strNum1.length();
            strSmallLength = strNum2;
            strlargeLength = strNum1;
        }
        int j = largeLength - 1;
        for (int i = smallLength - 1; i >= 0; i--) {
            if (strSmallLength.charAt(i) == '0' && strlargeLength.charAt(j) == '0') {
                if (isHaveExtraNum)
                    result = '1' + result;
                else
                    result = '0' + result;
                isHaveExtraNum = false;
            } else if (strSmallLength.charAt(i) == '1' && strlargeLength.charAt(j) == '1') {
                if (isHaveExtraNum)
                    result = '1' + result;
                else
                    result = '0' + result;
                isHaveExtraNum = true;
            } else if (strSmallLength.charAt(i) == '1' || strlargeLength.charAt(j) == '1') {
                if (isHaveExtraNum)
                    result = '0' + result;
                else
                    result = '1' + result;
            }
            j--;
        }
        for (int i = j; i >= 0; i--) {
            if (strlargeLength.charAt(i) == '1') {
                if (isHaveExtraNum)
                    result = '0' + result;
                else
                    result = '1' + result;
            } else if (strlargeLength.charAt(i) == '0') {
                if (isHaveExtraNum)
                    result = '1' + result;
                else
                    result = '0' + result;
                isHaveExtraNum = false;
            }
        }
        if (isHaveExtraNum) {
            result = '1' + result;
        }
        return result;
    }

    /**
     * Вычитание
     * @param strNum1 Уменьшаемое
     * @param strNum2 Вычитаемое
     */
    private String subtract(String strNum1, String strNum2) {
        StringBuilder result = new StringBuilder();
        strNum2 = convertToReverseCode(strNum2, 0);
        result.append(add(strNum1, strNum2));
        result = checkNumber(result);
        return result.toString();
    }

    /**
     * Проерка разрядов и удаление ненужных нулей
     * @param str Число для проверки
     */
    private StringBuilder checkNumber(StringBuilder str) {
        while (str.length() % 4 != 0)
            str.deleteCharAt(0);
        if (str.charAt(0) == '1') {
            str = new StringBuilder(convertToReverseCode(str.toString(), 1));
            str.insert(0, '-');
            while (str.charAt(1) == '0') {
                str.deleteCharAt(1);
            }
        } else {
            while (str.charAt(0) == '0') {
                str.deleteCharAt(0);
            }
        }
        return str;
    }

    /**
     * Определение количества разрядов у числа в двоичной СС
     * @param strNum Число для определения
     */
    private long setDigits(String strNum) {
        int degree = 3;
        long num = convertToNumber(10, translate(2, 10, strNum));
        long k = (long) Math.pow(2, degree);
        while (num > Math.pow(2, k) - 1) {
            degree++;
            k = (long) Math.pow(2, degree);
        }
        return k;
    }

    /**
     * Обратный код
     * @param str Число для определения
     * @param i Индекс с которого начинается инвертирование
     */
    private String convertToReverseCode(String str, int i) {
        long difference = 0;
        if (setDigits(str) > str.length()) {
            difference = setDigits(str) - str.length();
        }
        while (difference != 0) {
            str = '0' + str;
            difference--;
        }
        String result = "";
        while (i < str.length()) {
            if (str.charAt(i) == '1')
                result += '0';
            else
                result += '1';
            i++;
        }
        result = add(result, "1");
        return result;
    }

    /**
     * Умножение
     * @param strNum1 Первый множитель
     * @param strNum2 Вторый множитель
     */
    private String multiplyBy(String strNum1, String strNum2) {
        String result = "";
        for (int i = 0; i < convertToNumber(10, strNum2); ++i) {
            result = add(result, strNum1);
        }
        return result;
    }

    /**
     * Деление
     * @param strNum1 Делимое
     * @param strNum2 Делитель
     */
    private String divideBy (String strNum1, String strNum2) {
        long tmp = convertToNumber(10, strNum2);
        int k = 0;
        for (long i = convertToNumber(10, strNum1); i >= tmp; i -= tmp) {
            strNum1 = subtract(strNum1, strNum2);
            k++;
        }
        return translate(10, 2, String.valueOf(k));
    }

    /**
     * Выбор действия
     * @param ch Операция
     * @param strNum1 Первое число
     * @param strNum2 Второе число
     * @param originalNS Исходная СС
     */
    public String solve(char ch, String strNum1, String strNum2, long originalNS) {
        switch (ch) {
            case '+':
                return add(translate(originalNS, 2, strNum1), translate(originalNS, 2, strNum2));
            case '-':
                return subtract(translate(originalNS, 2, strNum1), translate(originalNS, 2, strNum2));
            case '*':
                if (strNum1.equals("0") || strNum2.equals("0"))
                    return "0";
                return multiplyBy(translate(originalNS, 2, strNum1), translate(originalNS, 10, strNum2));
            case '/':
                if (strNum2.equals("0"))
                    return "На ноль делить нельзя";
                if (strNum1.equals("0"))
                    return "0";
                return divideBy(translate(originalNS, 10, strNum1), translate(originalNS, 10, strNum2));
        }
        return null;
    }
}

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int endSystem;
        int sourceSystem;
        String number;
        System.out.print("Введите число (допускаются буквы A-F): ");
        number = scanner.nextLine();
        System.out.print("Введите начальную СС(от 2 до 16): ");
        sourceSystem = scanner.nextInt();
        System.out.print("Введите конечную СС(от 2 до 16): ");
        endSystem = scanner.nextInt();
        System.out.print(numberToNewDegree(sourceSystem, endSystem, number));
    }

    /**
     * Получение нового числа
     */
    private static String numberToNewDegree(int sourceSystem, int endSystem, String number) {
        String newNumber = "";
        if (endSystem >= 2 && endSystem <= 16 && sourceSystem >= 2 && sourceSystem <= 16) {
            if (endSystem == sourceSystem) {
                newNumber += number;
            } else if (endSystem == 10) {
                newNumber += ChangeNumber.transferToTenDegree(sourceSystem, number);
            } else {
                newNumber += ChangeNumber.transferAnyDegree(sourceSystem, number, endSystem);
            }

            return newNumber;
        } else return "Ошибка ввода. Введены некорректные данные";
    }
}

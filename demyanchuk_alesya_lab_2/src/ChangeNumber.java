import java.util.Scanner;

public class ChangeNumber {
    /**
     * Получение нового числа
     */
    private static String numberToNewDegree(int sourceSystem, int endSystem, String number ){
        String newNumber = "";
        if(endSystem >= 2 && endSystem <= 16 && sourceSystem >= 2 && sourceSystem <= 16){
            if(endSystem == sourceSystem){
                newNumber += number;
            }
            else if(endSystem == 10){
                newNumber += transferToTenDegree(sourceSystem, number);
            }
            else {
                newNumber += notTenSystem(sourceSystem, number, endSystem);;
            }

            return newNumber;
        }
        else return "Ошибка ввода. Введены некорректные данные";
    }

    /**
     * преобразование в 10 СС ( умножаем на основание в степени с конца)
     */
    private static int transferToTenDegree(int sourceSystem, String number){
        number=number.toUpperCase();
        String[] mass = number.split("");
        for (int i = 0; i < mass.length; i++){
            //преобразуем символы в числа
            switch (mass[i]) {
                case "A": {
                    if (sourceSystem >= 11)
                        mass[i] = String.valueOf(10);
                    else {
                        System.out.print("Ошибка ввода, данный символ отсутсвует в этой СС");
                        return 0;
                    }
                    break;
                }
                case "B": {
                    if (sourceSystem >= 12)
                        mass[i] = String.valueOf(11);
                    else {
                        System.out.print("Ошибка ввода, данный символ отсутсвует в этой СС");
                        return 0;
                    }
                    break;
                }
                case "C": {
                    if (sourceSystem >= 13)
                        mass[i] = String.valueOf(12);
                    else {
                        System.out.print("Ошибка ввода, данный символ отсутсвует в этой СС");
                        return 0;
                    }
                    break;
                }
                case "D": {
                    if (sourceSystem >= 14)
                        mass[i] = String.valueOf(13);
                    else {
                        System.out.print("Ошибка ввода, данный символ отсутсвует в этой СС");
                        return 0;
                    }
                    break;
                }
                case "E": {
                    if (sourceSystem >= 15)
                        mass[i] = String.valueOf(14);
                    else {
                        System.out.print("Ошибка ввода, данный символ отсутсвует в этой СС");
                        return 0;
                    }
                    break;
                }
                case "F": {
                    if (sourceSystem == 16)
                        mass[i] = String.valueOf(15);
                    else {
                        System.out.print("Ошибка ввода, данный символ отсутсвует в этой СС");
                        return 0;
                    }
                    break;
                }
                default: {
                    break;
                }
            }

        }
        int newNumber = 0;
        int m =0;
        for(int i = mass.length - 1; i >= 0; i--){
            int n = Integer.parseInt(mass[i]);
            newNumber= (int) (newNumber + (n * Math.pow(sourceSystem,m)));
            m++;
        }
        return newNumber;
    }
}

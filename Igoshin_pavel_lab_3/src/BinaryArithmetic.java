package com.example.program;

public class BinaryArithmetic {
    private char[] codeChar = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private int[] codeInt = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    private int _fromNS;                          // Система счисления (входные данные)
    private char _operation;                      // Знак операции (входные данные)
    private char _signValue1;                     // Знак первого числа
    private char _signValue2;                     // Знак второго числа
    private String _binaryFormatValue1 = "";      // Первое число в 2-СС
    private String _binaryFormatValue2 = "";      // Второе число в 2-СС
    private String _resultString = "";            // Результат
    private Boolean _reversed = false;            // Был ли произведен свап _bFV(1/2)

    public BinaryArithmetic(int fromNS, String value_1, String value_2, char operation) {
        _signValue1 = value_1.toCharArray()[0];
        _signValue2 = value_2.toCharArray()[0];
        _fromNS = fromNS;
        _operation = operation;
        _binaryFormatValue1 = convertToBinaryCode(unsignedFormat(value_1));
        _binaryFormatValue2 = convertToBinaryCode(unsignedFormat(value_2));

        swapVar();
    }

    /**
     * Присваивает _bFV1 и _bFV2 беззнаковые значения
      */
    private String unsignedFormat(String value) {
        String unsignedValue = "";

        if (value.toCharArray()[0] == '-') {
            char[] valueArray = value.toCharArray();

            for (int i = 1; i < valueArray.length; i++) {
                unsignedValue += valueArray[i];
            }
            return unsignedValue;
        } else
            return value;
    }

    /**
     * Конвертация из _fromNS-CC в 10-CC
      */
    private long convertTo10System(String value) {
        char[] valueCharArray = value.toCharArray();
        long valueLong = 0;

        for (int i = 0; i < valueCharArray.length; i++) {
            int power = valueCharArray.length - i - 1;

            for (int j = 0; j < _fromNS; j++) {
                if (valueCharArray[i] == codeChar[j]) {
                    valueLong += codeInt[j] * (int) Math.pow(_fromNS, power);
                    break;
                }
            }
        }
        return valueLong;
    }

    /**
     * Конвертация из 10-СС в 2-СС
      */
    private String convertToBinaryCode(String value) {
        long valueLong = convertTo10System(value);
        String binaryString = "";

        for (int i = 0; valueLong >= 2; i++) {
            binaryString += (valueLong % 2);
            valueLong /= 2;
        }
        binaryString += valueLong;

        return new StringBuffer(binaryString).reverse().toString();
    }

    /**
     * Происходит свап значений переменных _binaryFormatValue (на место _bFV1 становится значение наибольшее по модулю)
      */
    private void swapVar()
    {
        String bufStr;
        char bufChar;
        char[] valueArray1 = _binaryFormatValue1.toCharArray();
        char[] valueArray2 = _binaryFormatValue2.toCharArray();

        if (valueArray1.length < valueArray2.length)
        {
            bufStr = _binaryFormatValue1;
            bufChar = _signValue1;
            _binaryFormatValue1 = _binaryFormatValue2;
            _signValue1 = _signValue2;
            _binaryFormatValue2 = bufStr;
            _signValue2 = bufChar;
            _reversed = true;
            return;
        }
        if (valueArray1.length == valueArray2.length) {
            for (int i = 0; i < valueArray1.length; i++) {
                if (valueArray1[i] == '0' && valueArray2[i] == '1') {
                    bufStr = _binaryFormatValue1;
                    bufChar = _signValue1;
                    _binaryFormatValue1 = _binaryFormatValue2;
                    _signValue1 = _signValue2;
                    _binaryFormatValue2 = bufStr;
                    _signValue2 = bufChar;
                    _reversed = true;
                    return;
                }
            }
        }
    }

    /**
     * Обратный код (применяется перед вычитанием)
     */
    private void reverseCode (String value2, String value1)
    {
        char[] valueArray1 = value1.toCharArray();
        char[] valueArray2 = new char[valueArray1.length];
        char[] bufArray = value2.toCharArray();
        _binaryFormatValue2 = "";

        for (int i = 0; i < valueArray1.length - bufArray.length; i++)
            valueArray2[i] = '0';
        for (int i = 0; i < bufArray.length; i++)
            valueArray2[valueArray1.length - bufArray.length + i] = bufArray[i];

        for (int i = 0; i < valueArray2.length; i++)
        {
            if (valueArray2[i] == '1')
                _binaryFormatValue2 += "0";
            else
                _binaryFormatValue2 += "1";
        }
        _binaryFormatValue2 = binarySum(_binaryFormatValue2, "1");
    }

    /**
     * Удаление первой единицы и лишних нулей (применяется после вычитания)
      */
    private String removeZeros(String value)
    {
        Boolean firstElem = false;
        char[] valueArray = value.toCharArray();
        String result = "";

        for (int i = 1; i < valueArray.length; i++)
        {
            if (valueArray[i] == '1')
                firstElem = true;

            if (firstElem)
            {
                result += valueArray[i];
            }
        }
        if (result == "")
            result = "0";

        return result;
    }

    /**
     * Сравнение чисел в 2-СС (true - value1 больше, false - value2 больше)
     */
    private Boolean comparisonNum(String value1, String value2)
    {
        char[] valueArray1 = value1.toCharArray();
        char[] valueArray2 = value2.toCharArray();

        if (valueArray1.length > valueArray2.length)
            return true;
        if (valueArray1.length < valueArray2.length)
            return false;
        if (valueArray1.length == valueArray2.length)
        {
            for(int i = 0; i < valueArray1.length; i++)
            {
                if (valueArray1[i] == '1' && valueArray2[i] == '0')
                    return true;
                if (valueArray1[i] == '0' && valueArray2[i] == '1')
                    return false;
            }
        }
        return true;
    }

    /**
     * Сумма чисел
      */
    private String binarySum(String value1, String value2) {
        String result = "";
        char[] valueArray1 = new StringBuffer(value1).reverse().toString().toCharArray();
        char[] valueArray2 = new StringBuffer(value2).reverse().toString().toCharArray();

        int cnt = 0;
        char transfer = '0';

        for (int i = 0; i < valueArray1.length; i++) {
            if (cnt < valueArray2.length) {
                if (valueArray1[i] == '1' && valueArray2[i] == '1') {
                    result += transfer;
                    transfer = '1';
                    cnt++;
                    continue;
                }
                if ((valueArray1[i] == '1' && valueArray2[i] == '0') || (valueArray1[i] == '0' && valueArray2[i] == '1')) {
                    if (transfer == '1') {
                        result += "0";
                        cnt++;
                        continue;
                    } else {
                        result += "1";
                        cnt++;
                        continue;
                    }
                }
                if (valueArray1[i] == '0' && valueArray2[i] == '0') {
                    result += transfer;
                    transfer = '0';
                    cnt++;
                    continue;
                }
            } else {
                if (valueArray1[i] == '0' && transfer == '0') {
                    result += "0";
                    continue;
                }
                if ((valueArray1[i] == '1' && transfer == '0') || (valueArray1[i] == '0' && transfer == '1')) {
                    result += "1";
                    transfer = '0';
                    continue;
                }
                if (valueArray1[i] == '1' && transfer == '1') {
                    result += "0";
                    continue;
                }
            }
        }
        if (transfer == '1')
            result += "1";

        return new StringBuffer(result).reverse().toString();
    }

    /**
     * Умножение (основано на сумме result_(i-1) и value1_i с доп. нулями)
     */
    private String binaryMultiplication(String value1, String value2)
    {
        char[] valueArray2 = new StringBuffer(value2).reverse().toString().toCharArray();
        String result = "0";
        int jIndex = 0;

        for (int i = 0; i < valueArray2.length; i++)
        {
            if (valueArray2[i] == '1') {
                for (int j = 0; j < i - jIndex; j++)
                    value1 += "0";

                result = binarySum(value1, result);
                jIndex = i;
            }
        }

        return result;
    }

    /**
     * Деление (из делимого вычитается делитель, пока дилимое не станет меньше делителя)
     */
    private String binaryQuotient(String value1, String value2)
    {
        String subtrahend = _binaryFormatValue1;
        String result = "0";
        reverseCode(value2, value1);

        if (value2.toCharArray().length == 1 && value2.toCharArray()[0] != '1')
            return "Error!";

        if (_reversed)
            return "0";
        else
        {
            while (comparisonNum(subtrahend, value2)) {
                result = binarySum(result, "1");
                subtrahend = binarySum(subtrahend, _binaryFormatValue2);
                subtrahend = removeZeros(subtrahend);
            }
            return result;
        }
    }

    /**
     * Функция для выбора комбинации применяемых методов в зависимости от комбинации входных данных
      */
    public String binaryArithmetic ()
    {
        // Разность
        if (_operation == '-')
        {
            if (_reversed)
            {
                if (_signValue1 == '-') {
                    _signValue1 = '+';
                    _operation = '+';
                }
                else {
                    _signValue1 = '-';
                    _operation = '+';
                }
            }
            else
            {
                if (_signValue2 == '-') {
                    _signValue2 = '+';
                    _operation = '+';
                }
                else {
                    _signValue2 = '-';
                    _operation = '+';
                }
            }
        }

        // Сумма
        if (_operation == '+')
        {
            if (_signValue1 != '-' && _signValue2 != '-')
            {
                _resultString += binarySum(_binaryFormatValue1, _binaryFormatValue2);
                return _resultString;
            }
            if (_signValue1 == '-' && _signValue2 != '-')
            {
                reverseCode(_binaryFormatValue2, _binaryFormatValue1);
                _resultString += binarySum(_binaryFormatValue1, _binaryFormatValue2);
                _resultString = removeZeros(_resultString);
                _resultString = new StringBuffer(_resultString).reverse().toString();
                _resultString += "-";
                _resultString = new StringBuffer(_resultString).reverse().toString();
                return  _resultString;
            }
            if (_signValue1 != '-' && _signValue2 == '-')
            {
                reverseCode(_binaryFormatValue2, _binaryFormatValue1);
                _resultString += binarySum(_binaryFormatValue1, _binaryFormatValue2);
                _resultString = removeZeros(_resultString);
                return  _resultString;
            }
            if (_signValue1 == '-' && _signValue2 == '-')
            {
                _resultString += "-" + binarySum(_binaryFormatValue1, _binaryFormatValue2);
                return  _resultString;
            }
        }

        // Произведение
        if (_operation == '*')
        {
            if ((_signValue1 == '-' && _signValue2 != '-') || (_signValue1 != '-' && _signValue2 == '-'))
            {
                _resultString += "-";
                _resultString += binaryMultiplication(_binaryFormatValue1, _binaryFormatValue2);
                return _resultString;
            }
            if ((_signValue1 == '-' && _signValue2 == '-') || (_signValue1 != '-' && _signValue2 != '-'))
            {
                _resultString += binaryMultiplication(_binaryFormatValue1, _binaryFormatValue2);
                return _resultString;
            }
        }

        // Частное
        if (_operation == '/')
        {
            if ((_signValue1 == '-' && _signValue2 != '-') || (_signValue1 != '-' && _signValue2 == '-'))
            {
                _resultString += "-";
                _resultString += binaryQuotient(_binaryFormatValue1, _binaryFormatValue2);
                return _resultString;
            }
            if ((_signValue1 == '-' && _signValue2 == '-') || (_signValue1 != '-' && _signValue2 != '-'))
            {
                _resultString += binaryQuotient(_binaryFormatValue1, _binaryFormatValue2);
                return _resultString;
            }
        }
        return "Error!";
    }
}
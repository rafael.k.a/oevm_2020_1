package EVM_Lab_3;

public class Operations {
    public static String performAddition(String num1, String num2) {
        char[] num1Arr = num1.toCharArray();
        char[] num2Arr = num2.toCharArray();
        int endValue = 0;
        if (num1Arr.length >= num2Arr.length) {
            endValue = num1Arr.length + 1;
        } else {
            endValue = num2Arr.length + 1;
        }
        int num1Current = 0;
        if (num1Arr[num1Arr.length - 1] == '1') {
            num1Current = 1;
        }
        int num2Current = 0;
        if (num2Arr[num2Arr.length - 1] == '1') {
            num2Current = 1;
        }
        int dopCurrent = 0;
        StringBuilder result = new StringBuilder("");
        int i = 2;
        while (true) {
            int currentValue = num1Current + num2Current + dopCurrent;
            num1Current = 0;
            num2Current = 0;
            if (currentValue > 1) {
                result.append(currentValue % 2);
                dopCurrent = 1;
            } else {
                result.append(currentValue);
                dopCurrent = 0;
            }
            int possibleLength = num1Arr.length - i;
            if (possibleLength >= 0) {
                num1Current = 1;
                if (num1Arr[possibleLength] == '0') {
                    num1Current = 0;
                }
            }
            possibleLength = num2Arr.length - i;
            if (possibleLength >= 0) {
                num2Current = 1;
                if (num2Arr[possibleLength] == '0') {
                    num2Current = 0;
                }
            }
            if (i > endValue || (i == endValue && dopCurrent == 0)) {
                break;
            }
            i++;

        }
        return result.reverse().toString();
    }

    private static StringBuilder deleteZeros(StringBuilder num, boolean isNegative) {
        int zeros = 0;
        for (int i = 0; i < num.length(); i++) {
            if (num.charAt(i) == '1') {
                break;
            }
            zeros++;
        }
        if (isNegative) {
            num.delete(1, zeros);
        } else {
            num.delete(0, zeros);
        }
        return num;
    }


    public static String performSubtaction(String num1, String num2) {
        if (num1.equals(num2)) {
            return "0";
        }
        StringBuilder result = new StringBuilder("");
        boolean isNegative = false;
        if (Converter.convertToDec((byte) 2, num1) < Converter.convertToDec((byte) 2, num2)) {
            isNegative = true;
        }
        char[] num1Arr;
        char[] num2Arr;
        int endValue = num1.length();
        if (isNegative) {
            endValue = num2.length();
            num2Arr = num2.toCharArray();
            num1Arr = getDopCode(num1, num2Arr.length).toCharArray();

        } else {
            num1Arr = num1.toCharArray();
            num2Arr = getDopCode(num2, num1Arr.length).toCharArray();
        }
        int num1Current = 0;
        if (num1Arr[num1Arr.length - 1] == '1') {
            num1Current = 1;
        }
        int num2Current = 0;
        if (num2Arr[num2Arr.length - 1] == '1') {
            num2Current = 1;
        }
        int dopCurrent = 0;
        int i = 2;
        while (true) {
            int currentValue = num1Current + num2Current + dopCurrent;
            num1Current = 0;
            num2Current = 0;
            if (currentValue > 1) {
                result.append(currentValue % 2);
                dopCurrent = 1;
            } else {
                result.append(currentValue);
                dopCurrent = 0;
            }
            int possibleLength = num1Arr.length - i;
            if (possibleLength >= 0) {
                num1Current = 1;
                if (num1Arr[possibleLength] == '0') {
                    num1Current = 0;
                }
            }
            possibleLength = num2Arr.length - i;
            if (possibleLength >= 0) {
                num2Current = 1;
                if (num2Arr[possibleLength] == '0') {
                    num2Current = 0;
                }
            }
            if (i > endValue) {

                break;
            }
            i++;
        }
        if (isNegative) {
            result.append('-');
        }
        return deleteZeros(result.reverse(), isNegative).toString();
    }

    public static String getDopCode(String num, int len) {
        StringBuilder result = new StringBuilder("");
        char[] numArr = num.toCharArray();
        for (int i = 0; i < (len - numArr.length); i++) {
            result.append(1);
        }
        for (int i = 0; i < numArr.length; i++) {
            int currentNum = 0;

            if (numArr[i] == '0') {
                currentNum = 1;
            }
            result.append(currentNum);
        }
        return performAddition(result.toString(), "1");
    }


    public static String performMultiply(String num1, String num2) {
        long num1Dec = Converter.convertToDec((byte) 2, num1);
        long num2Dec = Converter.convertToDec((byte) 2, num2);
        if (num1Dec == 0 || num2Dec == 0) {
            return "0";
        }
        String result = num1;
        String multiplyValue = num1;
        long endMultiplyValue = num2Dec;
        if (num1Dec < num2Dec) {
            result = num2;
            multiplyValue = num2;
            endMultiplyValue = num1Dec;
        }
        endMultiplyValue--;
        for (long i = 0; i < endMultiplyValue; i++) {
            result = Operations.performAddition(result, multiplyValue);
        }
        return result;
    }


    public static String performDivide(String num1, String num2) {
        long num1Dec = Converter.convertToDec((byte) 2, num1);
        long num2Dec = Converter.convertToDec((byte) 2, num2);
        if (num2Dec == 0) {
            return "На ноль делить нельзя!";
        }
        if (num1Dec < num2Dec) {
            return "0";
        }
        if (num1Dec == num2Dec) {
            return "1";
        }
        if (num2Dec == 1) {
            return num1;
        }
        String appendResult = num2;

        long result = 0;
        while (Converter.convertToDec((byte) 2, appendResult) <= num1Dec) {
            appendResult = performAddition(appendResult, num2);
            result++;
        }
        return Converter.convertToNewNotation((byte) 10, (byte) 2, Long.toString(result));
    }

}

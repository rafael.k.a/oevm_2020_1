public class Converter {
    public static String convertToDnf(int[][] array) {
        StringBuilder answer = new StringBuilder();
        int valueIndex = 4;
        for (int i = 0; i < array.length; i++) {
            if (array[i][valueIndex] == 1) {
                if (answer.toString().equals("")) {
                    answer.append("(");
                } else {
                    answer.append(") + (");
                }
                for (int j = 0; j < array[i].length - 1; j++) {
                    if (array[i][j] == 0) {
                        answer.append("!");
                    }
                    answer.append("x").append(j + 1);
                    if (j < array[i].length - 2) {
                        answer.append(" * ");
                    }
                }
            }
        }
        answer.append(")");
        return answer.toString();
    }

    public static String convertToKnf(int[][] array) {
        StringBuilder answer = new StringBuilder();
        int valueIndex = 4;
        for (int i = 0; i < array.length; i++) {
            if (array[i][valueIndex] == 0) {
                if (answer.toString().equals("")) {
                    answer.append("(");
                } else {
                    answer.append(") * (");
                }
                for (int j = 0; j < array[i].length - 1; j++) {
                    if (array[i][j] == 1) {
                        answer.append("!");
                    }
                    answer.append("x").append(j + 1);
                    if (j < array[i].length - 2) {
                        answer.append(" + ");
                    }
                }
            }
        }
        answer.append(")");
        return answer.toString();
    }
}

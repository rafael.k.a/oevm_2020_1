import java.util.Arrays;
import java.util.Random;

public class TruthTable {
    private int[][] truthTableArray;

    public static TruthTable getTruthTable()
    {
        TruthTable truthTable = new TruthTable();
        Random random = new Random();
        truthTable.truthTableArray = new int[][]{
                {0,0,0,0,random.nextInt(2)},
                {0,0,0,1,random.nextInt(2)},
                {0,0,1,0,random.nextInt(2)},
                {0,0,1,1,random.nextInt(2)},
                {0,1,0,0,random.nextInt(2)},
                {0,1,0,1,random.nextInt(2)},
                {0,1,1,0,random.nextInt(2)},
                {0,1,1,1,random.nextInt(2)},
                {1,0,0,0,random.nextInt(2)},
                {1,0,0,1,random.nextInt(2)},
                {1,0,1,0,random.nextInt(2)},
                {1,0,1,1,random.nextInt(2)},
                {1,1,0,0,random.nextInt(2)},
                {1,1,0,1,random.nextInt(2)},
                {1,1,1,0,random.nextInt(2)},
                {1,1,1,1,random.nextInt(2)}
        };
        return truthTable;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        int rowsCount = 16;
        int columnsCount = 5;
        result.append("x1\tx2\tx3\tx4\tF\n");
        for (int i = 0; i < rowsCount; i++) {
            for (int j = 0; j < columnsCount; j++) {
                result.append(truthTableArray[i][j]).append("\t");
            }
            result.append("\n");
        }
        return result.toString();
    }

    public int[][] getTruthTableArray()
    {
        return truthTableArray;
    }
}

package com.company;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);//Ввод числа с клавиатуры
        System.out.println("Введите исходную систему счисления:");
        long notation1 = scanner.nextInt();
        System.out.println("Введите необходимую систему счисления:");
        long notation2 = scanner.nextInt();
        System.out.println("Введите число:");
        String number = scanner.next(); // Ввод числа в строку
        char[] number2 = number.toCharArray();//Представление числа в качестве массива символов
        char [] rezult = new char[20];//Массив для результата
        long numberIn10Natation = HelperClass.convertTo10Base(notation1, number2);
        HelperClass.convertToFinalBase(numberIn10Natation, notation2, rezult);
        int count = (int) rezult[0]; //Счетчик индексов в конечном массиве
        HelperClass.outputTheRezult(count, rezult); //Вывод
    }



}
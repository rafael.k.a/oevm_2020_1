public class Main {
    public static void main(String[] args){
        Pascal pascal = new Pascal("program.pas");
        Assembler assembler = new Assembler("assembler");
        Translator translator = new Translator(pascal.getPascalCode(), assembler);
        translator.startTranslate();
        assembler.createFile();
    }
}

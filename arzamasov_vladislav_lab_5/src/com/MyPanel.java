package com;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class MyPanel extends JPanel {
    int numColumn = 5;
    int numRows = 16;

    Random random = new Random();
    int[][] tableIst = {
            {0,0,0,0,random.nextInt(2)},
            {0,0,0,1,random.nextInt(2)},
            {0,0,1,0,random.nextInt(2)},
            {0,0,1,1,random.nextInt(2)},
            {0,1,0,0,random.nextInt(2)},
            {0,1,0,1,random.nextInt(2)},
            {0,1,1,0,random.nextInt(2)},
            {0,1,1,1,random.nextInt(2)},
            {1,0,0,0,random.nextInt(2)},
            {1,0,0,1,random.nextInt(2)},
            {1,0,1,0,random.nextInt(2)},
            {1,0,1,1,random.nextInt(2)},
            {1,1,0,0,random.nextInt(2)},
            {1,1,0,1,random.nextInt(2)},
            {1,1,1,0,random.nextInt(2)},
            {1,1,1,1,random.nextInt(2)},
    };

    private JLabel[] labelsAdd = new JLabel[numColumn * numRows];
    private JPanel table = new JPanel();
    private JTextPane rezultatDNF = new JTextPane();
    private JTextPane rezultatKNF = new JTextPane();

    MinBoolFunc minBoolFunc = new MinBoolFunc();

    private void printTabl() {
        String result = "";

        for (int i = 0; i < numRows; i++) {
            result = "";
            for (int j = 0; j < numColumn; j++) {
                result += Integer.toString(tableIst[i][j])+ " ";
            }
            labelsAdd[i] = new JLabel();
            labelsAdd[i].setText(result);
            table.add(labelsAdd[i]);
        }
    }
    private void createTable() {
        Random random = new Random();
        int[][] newTable = {
                {0, 0, 0, 0, random.nextInt(2)},
                {0, 0, 0, 1, random.nextInt(2)},
                {0, 0, 1, 0, random.nextInt(2)},
                {0, 0, 1, 1, random.nextInt(2)},
                {0, 1, 0, 0, random.nextInt(2)},
                {0, 1, 0, 1, random.nextInt(2)},
                {0, 1, 1, 0, random.nextInt(2)},
                {0, 1, 1, 1, random.nextInt(2)},
                {1, 0, 0, 0, random.nextInt(2)},
                {1, 0, 0, 1, random.nextInt(2)},
                {1, 0, 1, 0, random.nextInt(2)},
                {1, 0, 1, 1, random.nextInt(2)},
                {1, 1, 0, 0, random.nextInt(2)},
                {1, 1, 0, 1, random.nextInt(2)},
                {1, 1, 1, 0, random.nextInt(2)},
                {1, 1, 1, 1, random.nextInt(2)},};
        tableIst = newTable;
    }

    public MyPanel(){
        setLayout(null);

        JButton buttonDNF = new JButton("ДНФ");
        buttonDNF.setBounds(250, 500, 100, 50);
        buttonDNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String rez = "";
                rez = minBoolFunc.DNF(tableIst);
                table.removeAll();
                printTabl();
                rezultatDNF.setText(rez);
            }
        });
        add(buttonDNF);

        JButton buttonKNF = new JButton("КНФ");
        buttonKNF.setBounds(370, 500, 100, 50);
        buttonKNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String res = "";
                res = minBoolFunc.KNF(tableIst);
                table.removeAll();
                printTabl();
                rezultatKNF.setText(res);
            }
        });
        add(buttonKNF);

        JButton buttonObnTabl = new JButton("Обновить таблицу");
        buttonObnTabl.setBounds(50, 500, 150, 50);
        buttonObnTabl.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                createTable();
                table.removeAll();
                rezultatDNF.setText(null);
                rezultatKNF.setText(null);
                printTabl();
            }
        });
        add(buttonObnTabl);

        rezultatDNF.setBounds(150, 50, 150, 400);
        rezultatDNF.setEditable(false);
        add(rezultatDNF);

        rezultatKNF.setBounds(325, 50, 150, 400);
        rezultatKNF.setEditable(false);
        add(rezultatKNF);

        table.setBounds(25, 50, 100, 400);
        add(table);

        JLabel lblTable = new JLabel("Таблица:");
        lblTable.setBounds(25, 25, 100, 20);
        add(lblTable);

        JLabel lblResult = new JLabel("Результаты:");
        lblResult.setBounds(275, 5, 100, 20);
        add(lblResult);

        JLabel lblDNF = new JLabel("ДНФ:");
        lblDNF.setBounds(210, 30, 100, 20);
        add(lblDNF);

        JLabel lblKNF = new JLabel("КНФ:");
        lblKNF.setBounds(385, 30, 100, 20);
        add(lblKNF);
    }
}

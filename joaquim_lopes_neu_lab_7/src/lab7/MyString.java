package lab7;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyString {
	
	MyAssembler myassembler;

    private String stringOfFile;
    private String variables;
    private String code;
    private ArrayList<String> arrayVarsString = new ArrayList<>();
    private ArrayList<String> arrayCodeString = new ArrayList<>();
    private ArrayList<String> arrayVars = new ArrayList<>();

    public MyString(String str, MyAssembler myassembler) {

        this.stringOfFile = str;
        this.myassembler = myassembler;

    }

    public void parseVarAndBody() {

        Pattern pattern = Pattern.compile("(?<=var)[\\s\\S]*(?=begin)");
        Matcher matcher = pattern.matcher(stringOfFile);

        while (matcher.find()) {
            variables = stringOfFile.substring(matcher.start(), matcher.end());
        }

        Pattern patternCode = Pattern.compile("(?<=begin)[\\s\\S]*(?=end.)");
        Matcher matcherCode = patternCode.matcher(stringOfFile);

        while (matcherCode.find()) {
            code = stringOfFile.substring(matcherCode.start(), matcherCode.end());
        }

        parseVarsStr();
        parseCodeStr();
    }

    public void parseVarsStr() {

        Pattern pattern = Pattern.compile("([a-zA-Z]([a-zA-Z0-9_]*[\\s]*,[\\s]*)*)([a-zA-Z][a-zA-Z0-9_]*)[\\s]*:[\\s]*integer[\\s]*;");
        Matcher matcher = pattern.matcher(variables);

        while (matcher.find()) {
            arrayVarsString.add(variables.substring(matcher.start(), matcher.end()));
        }

        parseVarsInt();
    }

    public void parseVarsInt() {

        Pattern pattern = Pattern.compile("[a-zA-Z][a-zA-Z0-9_]*");
        Matcher matcher;
        String var;

        for (String string : arrayVarsString) {

            matcher = pattern.matcher(string);

            while (matcher.find()) {
                var = string.substring(matcher.start(), matcher.end());
                if (!var.equals("integer")) { arrayVars.add(var); }
            }
        }

        myassembler.addToVariables(arrayVars);
    }

    public void parseCodeStr() {

        Pattern patternCode = Pattern.compile("[a-zA-Z].*;");
        Matcher matcherCode = patternCode.matcher(code);

        while (matcherCode.find()) {
            arrayCodeString.add(code.substring(matcherCode.start(), matcherCode.end()));
        }

        createCode();
    }

    public void createCode() {

        Pattern patternWrite = Pattern.compile("write\\('(.*)'\\);");
        Pattern patternWriteLn = Pattern.compile("writeln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");

        Pattern patternReadLn = Pattern.compile("readln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");
        Pattern patternOperation = Pattern.compile("([a-z][a-zA-Z0-9]*)[\\s]*[\\s]*:=[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*([+\\-*/])[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*;");

        for (String string : arrayCodeString) {

            if(string.matches(patternWrite.toString())){
                Matcher matcherForCode = patternWrite.matcher(string);

                if (matcherForCode.find()) {
                	myassembler.addToCodeWrite(matcherForCode.group(1));
                }
            }

            else if(string.matches(patternReadLn.toString())){
                Matcher matcherForCode = patternReadLn.matcher(string);

                if (matcherForCode.find()) {
                	myassembler.addToCodeReadLn(matcherForCode.group(1));
                }
            }

            else if(string.matches(patternOperation.toString())){
                Matcher matcherForCode = patternOperation.matcher(string);

                if (matcherForCode.find()) {
                	myassembler.addToCodeOperation(matcherForCode.group(1), matcherForCode.group(2), matcherForCode.group(3), matcherForCode.group(4));
                }
            }

            else if(string.matches(patternWriteLn.toString())){
                Matcher matcherForCode = patternWriteLn.matcher(string);

                if (matcherForCode.find()) {
                	myassembler.addToCodeWriteLn(matcherForCode.group(1));
                }
            }
        }
    }
}

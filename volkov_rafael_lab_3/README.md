#Лабораторная работа №3 Моделирование выполнения арифметических операций

##ПИбд-22 Волков Рафаэль

ТЗ:
Разработать программный продукт для осуществления арифметических операций над двоичными числами. При запуске программы пользователь указывает:

* исходную систему счисления (2-16);
* два числа в исходной системе счисления (целые);
* арифметическую операцию (сложение, вычитание, умножение,деление)

Примеры:

![Пример работы1](https://sun9-59.userapi.com/impf/uGwGuzyCVHlm0oPL4JiikMNbrgU-zfr53UVupg/O7vktbw19Gw.jpg?size=299x265&quality=96&proxy=1&sign=4c70dac166927f417875bcd7f9866fb5&type=album)

![Пример работы2](https://sun9-10.userapi.com/impf/aK9PhxZHet_JtCua-BKV_4oQJX7ZLujs_g86BA/uudkYHu2mAc.jpg?size=363x260&quality=96&proxy=1&sign=2f602d51b6f284421e51b2fbd10f8e95&type=album)

Ссылка на [видео](https://drive.google.com/file/d/1OovfrJITrOYxZA5GPftxTBbndRmeEB8D/view?usp=sharing)
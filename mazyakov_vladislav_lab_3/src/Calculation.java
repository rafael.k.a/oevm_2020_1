import java.util.Arrays;

public class Calculation {
    TranslationNumber tNumber = new TranslationNumber();

    public String sum(char[] firstNumber, char[] secondNumber, boolean minus) {
        StringBuilder answer = new StringBuilder();
        char currentChar;
        int nextDigit = 0;//следующий разряд

        for (int i = firstNumber.length - 1, j = secondNumber.length - 1; i >= 0 || j >= 0 || nextDigit == 1; i--, j--) {
            int firstBinaryDigit, secondBinaryDigit;

            if (i < 0) {
                firstBinaryDigit = 0;
            }//нулевой разряд
            else if (firstNumber[i] == '0') {
                firstBinaryDigit = 0;
            } else {
                firstBinaryDigit = 1;
            }

            if (j < 0) {
                secondBinaryDigit = 0;
            } else if (secondNumber[j] == '0') {
                secondBinaryDigit = 0;
            } else {
                secondBinaryDigit = 1;
            }

            int currentSum = firstBinaryDigit + secondBinaryDigit + nextDigit;

            if (currentSum == 0) {
                currentChar = '0';
                nextDigit = 0;
            } else if (currentSum == 1) {
                currentChar = '1';
                nextDigit = 0;
            } else if (currentSum == 2) {
                currentChar = '0';
                nextDigit = 1;
            } else {
                currentChar = '1';
                nextDigit = 1;
            }//currentSum == 3

            answer.insert(0, currentChar);
        }
        if (minus) {
            answer.insert(0, "-");
        }
        return answer.toString();
    }

    public String subtraction(char[] minuend, char[] subtrahend, boolean minus) {
        StringBuilder difference = new StringBuilder();
        String minuendString = new String(minuend);
        String subtrahendString = new String(subtrahend);
        if (tNumber.transferToTheDecimal(2, minuendString) < tNumber.transferToTheDecimal(2, subtrahendString)) {
            char[] buffer = minuend;
            minuend = subtrahend;
            subtrahend = buffer;
            if (!minus) {
                minus = true;
            } else {
                minus = false;
            }
        } else if (tNumber.transferToTheDecimal(2, minuendString) == tNumber.transferToTheDecimal(2, subtrahendString)) {
            return "0";
        }

        int differenceInDigit = minuend.length - subtrahend.length;
        char[] additCodeSubtrahend = new char[minuend.length];

        //заполняем обратный код для вычитаемого
        for (int i = 0; i < additCodeSubtrahend.length; i++) {
            if (i < differenceInDigit) {
                additCodeSubtrahend[i] = '1';
            } else {
                if (subtrahend[i - differenceInDigit] == '0') {
                    additCodeSubtrahend[i] = '1';
                } else {
                    additCodeSubtrahend[i] = '0';
                }
            }
        }

        //преобразуем в дополнительный код
        additCodeSubtrahend = sum(additCodeSubtrahend, new char[]{'1'},false).toCharArray();

        char[] currentDif = sum(additCodeSubtrahend, minuend,false).toCharArray();

        //подготовка к выводу и сам вывод
        int i = 1;
        for (; currentDif[i] == '0'; i++) ;
        for (; i < currentDif.length; i++) {
            difference.append(currentDif[i]);
        }

        if (minus) {
            difference.insert(0, '-');
        }
        return difference.toString();
    }

    public String multiply(char[] firstNumberGet, char[] secondNumberGet,boolean minus) {
        char[] firstNumber = firstNumberGet;
        char[] secondNumber = secondNumberGet;
        if (firstNumberGet[0]=='-'|| secondNumberGet[0]=='-') {
            if (firstNumberGet[0] == '-') {
                firstNumber = new char[firstNumberGet.length - 1];
                System.arraycopy(firstNumberGet, 1, firstNumber, 0, firstNumberGet.length-1);
            }
            if (secondNumberGet[0] == '-') {
                secondNumber = new char[secondNumberGet.length - 1];
                System.arraycopy(secondNumberGet, 1, secondNumber, 0, secondNumberGet.length-1);
            }
        }
        char[] answer = {};
        String secondNumberString = new String(secondNumber);
        for (int i = 0; i < tNumber.transferToTheDecimal(2, secondNumberString); i++) {
            answer = (sum(answer, firstNumber,false)).toCharArray();
        }
        if (minus) {
            char[] answerMin = new char[answer.length+1];
            answerMin[0]='-';
            for (int i = 0; i <answer.length ; i++) {
                answerMin[i+1]=answer[i];
            }
            return new String(answerMin);
        }
        return new String(answer);
    }

    public String divide(char[] dividendGet, char[] dividerGet,boolean minus) {
        if (Arrays.equals(dividendGet, new char[]{'0'})) {
            return "Деление на 0";
        }
        String dividendString;
        String dividerString;
        char[] divider = dividerGet;
        if (dividendGet[0]=='-'|| dividerGet[0]=='-') {
            char[] dividend = dividendGet;
            if (dividendGet[0] == '-') {
                dividend = new char[dividendGet.length - 1];
                System.arraycopy(dividendGet, 1, dividend, 0, dividend.length);
            }
            if (dividerGet[0] == '-') {
                divider = new char[dividerGet.length - 1];
                System.arraycopy(dividerGet, 1, divider, 0, dividerGet.length-1);
            }
            dividendString = new String(dividend);
            dividerString = new String(divider);
        }
        else {
            dividendString = new String(dividendGet);
            dividerString = new String(dividerGet);
        }
        int answer = 0;
        //целочисленное деление
        while (tNumber.transferToTheDecimal(2, dividendString) >= tNumber.transferToTheDecimal(2, dividerString)) {
            dividendString = (subtraction(dividendString.toCharArray(), divider,false));
            answer++;
        }
        if (minus) {
            StringBuilder answerMin = tNumber.transferToTheBinary(answer);
            answerMin.insert(0,'-');
            return answerMin.toString();
        }
        return tNumber.transferToTheBinary(answer).toString();
    }
}

## Лабораторная работа №6
## ПИбд-21 Кочетков Тимофей

Разработать с помощью ассемблера FASM программу для сложения, вычитания,
умножения и деления двух целых чисел. Данные могут быть введены с
клавиатуры либо заданы в виде констант.

### Как работает:

Ссылка на видео: https://drive.google.com/drive/folders/1-u99H2r1Vn3Rw1qsDmCYbGICz1OYUEKx?usp=sharing
**Тест программы:**
```
Enter X = 7
Enter Y = 4
X + Y = <11>
X - Y = <3>
X * Y = <28>
X / Y = <1>

```
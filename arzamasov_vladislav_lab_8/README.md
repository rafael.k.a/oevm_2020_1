#Лабораторная работа №8

Выполнил студент группы ИСЭбд-21, *Арзамасов Владислав*.

**Задание:**

Разобрать и собрать системный блок ЭВМ.

**Этапы работы:**
1) компьютер перед разборкой: [фото1](https://drive.google.com/file/d/1y8xLlCIzKTFRu6WiyyhxQY_7xx3nti74/view?usp=sharing);
2) компьютер во время разборки: [фото1](https://drive.google.com/file/d/1_d6R-qMtLS3Mvw8JWCVc7uexaK5wJMk5/view?usp=sharing),
[фото2](https://drive.google.com/file/d/1MSPThy7KxC765n7RY7EadPrhC3mEq5ye/view?usp=sharing),
[фото3](https://drive.google.com/file/d/1ZW6vx1HJh8Rdth65F3a5WkHEbdsdvuHa/view?usp=sharing),
[фото4](https://drive.google.com/file/d/11-ng_h9IcgGUJSO11y6DRSIRSzUSXdoR/view?usp=sharing);
3) компьютер после сборки: [фото1](https://drive.google.com/file/d/1ajPrw0VwwCN9Z-YZXhavGLCt2BmSdAxX/view?usp=sharing).
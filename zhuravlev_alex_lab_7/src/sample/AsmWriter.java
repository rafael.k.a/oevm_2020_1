package sample;

import java.io.FileWriter;

public class AsmWriter {

    public static void writeASMFile(DataBase dataBase) {
        try (FileWriter writer = new FileWriter("C:/Users/user/IdeaProjects/zhuravlev_alex_lab_7/src/sample/AsmCalc.ASM")) {
            writer.write("format PE console\n\n" +
                    "entry start\n\n" +
                    "\tinclude 'win32a.inc'\n\n" +
                    "section '.data' data readable writable\n\n" +
                    "\tspaceString db '%d', 0\n" +
                    "\tnewLine db '%d', 0ah, 0\n");

            while (dataBase.hasVariable()) {
                writer.write("\t" + dataBase.removeVariable(0));
            }

            writer.write("\nsection '.code' data readable executable\n\n" +
                    "\tstart:\n\n");

            while (dataBase.hasCommand()) {
                switch (dataBase.removeCommand()) {
                    case READ -> writer.write("\tpush " + dataBase.removeReadCommand() + "\n" +
                            "\tpush spaceString\n" +
                            "\tcall [scanf]\n\n");
                    case WRITE -> writer.write("\tpush " + dataBase.removeWriteCommand() + "\n" +
                            "\tcall [printf]\n\n");
                    case WRITELN -> writer.write("\tpush [" + dataBase.removeWritelnCommand() + "]\n" +
                            "\tpush newLine\n" +
                            "\tcall [printf]\n\n");
                    case MATHS -> {
                        String command = dataBase.removeMathsCommand();
                        String[] arguments = command.split(" ");
                        if (!command.contains("div")) {
                            writer.write("\tmov ecx, [" + arguments[1] + "]\n" +
                                    "\t" + arguments[3] + " ecx, [" + arguments[2] + "]\n" +
                                    "\tmov [" + arguments[0] + "], ecx\n\n");
                        } else {
                            writer.write("\tmov eax, [" +  arguments[1] + "]\n" +
                                    "\tmov ecx, [" + arguments[2] + "]\n" +
                                    "\tdiv ecx\n" +
                                    "\tmov [" + arguments[0] + "], eax\n\n");
                        }
                    }
                }
            }

            writer.write("\tcall [getch]\n" +
                    "\tpush NULL\n" +
                    "\tcall[ExitProcess]\n\n" +
                    "section '.idata' import data readable\n\n" +
                    "\tlibrary kernel, 'kernel32.dll',\\\n" +
                    "\t\tmsvcrt, 'msvcrt.dll'\n\n" +
                    "\timport kernel,\\\n" +
                    "\t\tExitProcess, 'ExitProcess'\n\n" +
                    "\timport msvcrt,\\\n" +
                    "\t\tprintf, 'printf',\\\n" +
                    "\t\tscanf, 'scanf',\\\n" +
                    "\t\tgetch, '_getch'");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package com.company;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int start, next;
        Scanner in = new Scanner(System.in);
        System.out.print("Старая система счисления: ");
        start = in.nextInt();
        System.out.print("Новая сисетма счисления: ");
        next = in.nextInt();
        Scanner in1 = new Scanner(System.in);
        System.out.print("Введите число: ");
        String num = in1.nextLine();
        char[] number = num.toCharArray();

        // Проверка на систему счисления
        CorrectBase(start, next);
        //Проверка на принадлежность числа к СС
        CorrectNumber(start, number);
        //Перевод в десятичную систему
        convertTo10(start, number, next);
    }
    private static void CorrectBase(int initial, int next){
        if(initial < 2 || initial > 16  || next < 2  || next > 16  ){
            System.out.print("Не верно выбрана система счисления ");
            System.exit(1);
        }
    }
    private static void CorrectNumber(int initial, char[] number){
        for (int i = 0; i < number.length; i++){
            char digit = Character.toUpperCase(number[i]);
            if(Character.isDigit(digit) && (digit-'0')>= initial ){
                System.out.print(" Некорректное число");
                System.exit(1);
            }
            else if (Character.isLetter(digit) && (digit - 'A') +10 >= initial){
                System.out.print(" Некорректное число");
                System.exit(1);
            }
            else if (!Character.isLetter(digit) && !Character.isDigit(digit)){
                System.out.print(" Некорректное число");
                System.exit(1);
            }

        }
    }
    private static void convertTo10(int initial, char[] number, int next) {
        long decimal =0;
        int length= number.length;
        if (initial != 10) {
            for (int i = 0; i < length; i++) {
                char digit = Character.toUpperCase(number[i]);
                if (Character.isLetter(digit)) {
                    decimal += (digit - 'A' + 10) * (Math.pow(initial, length - 1 - i));
                } else if (Character.isDigit(digit)) {
                    decimal += (number[i] - '0') * (Math.pow(initial, length - 1 - i));
                }
                else {
                    System.exit(1);
                }
            }
        }
        Convert( next, decimal);
    }
    private static void Convert( int next, long decimal) {
        long divide= decimal;
        int quantity=1;
        while (Math.pow(next,quantity)<= decimal){
            quantity++;
        }
        int [] NewNum = new int[quantity];
        for (int k=0; k<quantity; k++ ){
            if(divide >= next ){
                NewNum[k]= (int)(divide%(long)next);
                divide= divide/next;
            }
        }
        NewNum[quantity-1]=(int)divide%next;

        char[] NewNumber = new char[quantity];
        for (int q=quantity-1; q>=0; q--) {
            if (NewNum[q] < 10) {
                int n = NewNum[q];
                NewNumber[quantity - q-1] = Character.forDigit(n, 10);
            } else if (NewNum[q] >= 10) {
                NewNumber[quantity - q-1] = (char) ('A' + NewNum[q] - 10);
            }
        }
        for(int p=0; p< NewNumber.length; p++){
            System.out.print( NewNumber[p]);
        }

    }
}

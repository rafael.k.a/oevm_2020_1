/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab_4;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Ной
 */
public class MyClass {

    private static int[][] generateArray() {
        Random r = new Random();
        int[][] matriz
                = {
                    {0, 0, 0, 0, r.nextInt(2)},
                    {0, 0, 0, 1, r.nextInt(2)},
                    {0, 0, 1, 0, r.nextInt(2)},
                    {0, 0, 1, 1, r.nextInt(2)},
                    {0, 1, 0, 0, r.nextInt(2)},
                    {0, 1, 0, 1, r.nextInt(2)},
                    {1, 1, 1, 0, r.nextInt(2)},
                    {0, 1, 1, 1, r.nextInt(2)},
                    {1, 0, 0, 0, r.nextInt(2)},
                    {1, 0, 0, 1, r.nextInt(2)},
                    {1, 0, 1, 0, r.nextInt(2)},
                    {1, 0, 1, 1, r.nextInt(2)},
                    {1, 1, 0, 0, r.nextInt(2)},
                    {1, 1, 0, 1, r.nextInt(2)},
                    {1, 1, 1, 0, r.nextInt(2)},
                    {1, 1, 1, 1, r.nextInt(2)},};
        return matriz;
    }

    private static void KNForm(int[][] RMatriz) {
        String output = new String();
        for (int[] n : RMatriz) {
            if (n[4] == 0) {
                if (output.isEmpty()) {
                    output += ((n[0] == 0) ? " (x1 + " : " (!x1 + ");
                } else {
                    output += ((n[0] == 0) ? " * (!x1 + " : " * (!x1 + ");
                }
                output += ((n[1] == 0) ? " x2 + " : " !x2 + ");
                output += ((n[2] == 0) ? " x3 + " : " !x3 + ");
                output += ((n[3] == 0) ? " x4)  " : " !x4)  ");
            }
        }
        System.out.println(output);
    }

    private static void DNForm(int[][] RMatriz) {
        String output = new String();
        for (int[] n : RMatriz) {
            if (n[4] == 1) {
                if (output.isEmpty()) {
                    output += ((n[0] == 0) ? "( !x1 * " : "( x1 * ");
                } else {
                    output += ((n[0] == 0) ? " + ( !x1 * " : " + ( x1 * ");
                }
                output += ((n[1] == 0) ? " !x2 * " : " x2 * ");
                output += ((n[2] == 0) ? " !x3 * " : " x3 * ");
                output += ((n[3] == 0) ? " !x4) "  : " x4)  ");
            }
        }
        System.out.println(output);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        Scanner scan = new Scanner(System.in);
        int[][] matriz = MyClass.generateArray();
        System.out.println("Число 0 КНФорма или 1 ДНФорма");
        String output = scan.next();

        if (output.equals("0")) {
            MyClass.KNForm(matriz);
            for (int[] n : matriz) {
                System.out.printf(" %d %d %d %d = %d%n ", n[0], n[1], n[2], n[3], n[4]);
            }
        } else if (output.equals("1")) {
            MyClass.DNForm(matriz);
            for (int[] n : matriz) {
                System.out.printf(" %d %d %d %d = %d%n ", n[0], n[1], n[2], n[3], n[4]);
            }
        }
    }
}

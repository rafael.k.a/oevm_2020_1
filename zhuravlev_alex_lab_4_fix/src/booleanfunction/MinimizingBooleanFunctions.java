package booleanfunction;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class MinimizingBooleanFunctions {
    private int[][] arrayTable = {
            {0, 0, 0, 0},
            {0, 0, 0, 1},
            {0, 0, 1, 0},
            {0, 0, 1, 1},
            {0, 1, 0, 0},
            {0, 1, 0, 1},
            {0, 1, 1, 0},
            {0, 1, 1, 1},
            {1, 0, 0, 0},
            {1, 0, 0, 1},
            {1, 0, 1, 0},
            {1, 0, 1, 1},
            {1, 1, 0, 0},
            {1, 1, 0, 1},
            {1, 1, 1, 0},
            {1, 1, 1, 1},
    };

    private int [] arrayRandom = new int [arrayTable.length];

    private void randomFilling() {
        for(int i = 0; i < arrayRandom.length; i++) {
            Random random = new Random();
            arrayRandom[i] = random.nextInt(2);
        }
    }

    public void printTable() {
        try {
            FileWriter writer = new FileWriter("site.html", false);
            writer.write("<!doctype html>\n" +
                    "<html lang=\"ru\">\n" +
                    "<head>\n" +
                    "    <meta charset=\"UTF-8\">\n" +
                    "    <meta name=\"viewport\"\n" +
                    "          content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">\n" +
                    "    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\n" +
                    "    <title>Document</title>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "<pre>\n");

            StringBuilder Table = new StringBuilder();
            randomFilling();
            Table.append("X1  X2  X3  X4  F" + '\n');
            writer.write("X1  X2  X3  X4  F" + "<br>\n");
            for(int i = 0; i < arrayTable.length; i++) {
                for(int j = 0; j < arrayTable[i].length; j++) {
                    Table.append(arrayTable[i][j] + "   ");
                    writer.write(arrayTable[i][j] + "   ");
                }
                Table.append(arrayRandom[i] + "   " + '\n');
                writer.write(arrayRandom[i] + "<br>\n");
            }
            System.out.println(Table);
            writer.write("</pre>\n" +
                    "</body>\n" +
                    "</html>");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void DNF(){
        int countEntry = 0;
        StringBuilder DNF = new StringBuilder();
        for(int i = 0; i < arrayTable.length; i++) {
            if(arrayRandom[i] == 1) {
                if(countEntry > 0) {
                    DNF.append(" + ");
                }
                DNF.append("(");
                for(int j = 0; j < arrayTable[i].length; j++) {
                    if(arrayTable[i][j] == 0) {
                        DNF.append("!");
                    }
                    DNF.append("X" + (j + 1)); //j + 1, так как нумерация Х начинается с 1
                    if(j < arrayTable[i].length - 1) {
                        DNF.append(" * ");
                    }
                }
                DNF.append(")");
                countEntry++;
            }
        }
        DNF.append('\n');
        System.out.println(DNF);
    }
    public void KNF() {
        int countEntry = 0;
        StringBuilder KNF = new StringBuilder();
        for(int i = 0; i < arrayTable.length; i++) {
            if(arrayRandom[i] == 0) {
                if(countEntry > 0) {
                    KNF.append(" * ");
                }
                KNF.append("(");
                for(int j = 0; j < arrayTable[i].length; j++) {
                    if(arrayTable[i][j] == 1) {
                        KNF.append("!");
                    }
                    KNF.append("X" + (j + 1)); //j + 1, т.к нумерация Х начинается с 1
                    if(j < arrayTable[i].length - 1) {
                        KNF.append(" + ");
                    }
                }
                KNF.append(")");
                countEntry++;
            }
        }
        KNF.append('\n');
        System.out.println(KNF);
    }
}

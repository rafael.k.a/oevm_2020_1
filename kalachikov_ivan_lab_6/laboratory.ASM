format PE console

entry start

include 'win32a.inc'
include 'encoding\win1251.inc'

section '.data' data readable writable

        getXString db '������� X: ', 0
        getYString db '������� Y: ', 0
        spaceString db '%d', 0

        resultSumString db 'X + Y: %d ', 0dh, 0ah, 0
        resultSubString db 'X - Y: %d ', 0dh, 0ah, 0
        resultMultString db 'X * Y: %d ', 0dh, 0ah,  0
        resultDivString db 'X / Y: %d ', 0dh, 0ah, 0
        exitString db '��������� ��������� ���� ������, ��� ������ ������� ������ ���������� ����������', 0dh, 0ah, 0

        X dd ?
        Y dd ?

        NULL = 0

section '.idata' import data readable

        library kernel, 'kernel32.dll',\
                msvcrt, 'msvcrt.dll'

        import kernel,\
               ExitProcess, 'ExitProcess',\
               SetConsoleOutputCP,'SetConsoleOutputCP',\
               SetConsoleCP,'SetConsoleCP'

        import msvcrt,\
               printf, 'printf',\
               scanf, 'scanf',\
               getch, '_getch'

section '.code' data readable executable

        start:
                invoke  SetConsoleOutputCP,1251
                invoke  SetConsoleCP,1251

                push getXString
                call [printf]
                push X
                push spaceString
                call [scanf]

                push getYString
                call [printf]
                push Y
                push spaceString
                call [scanf]

                mov ecx, [X]
                add ecx, [Y]
                push ecx
                push resultSumString
                call [printf]

                mov ecx, [X]
                sub ecx, [Y]
                push ecx
                push resultSubString
                call [printf]

                mov eax, [X]
                mul [Y]
                push eax
                push resultMultString
                call [printf]

                mov eax, [X]
                idiv [Y]
                push eax
                push resultDivString
                call [printf]

                push exitString
                call [printf]
                call [getch]
                push NULL
                call[ExitProcess]
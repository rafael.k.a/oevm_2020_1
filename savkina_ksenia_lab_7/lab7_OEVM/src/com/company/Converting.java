package com.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Converting {

    /**
     * Конвертация из .pas в .ASM
     *
     * @param pas путь к файлу
     */
    public String convert(String pas) {
        List<String> listVar = new ArrayList<>();
        List<String> listVarInt = new ArrayList<>();
        List<String> listCode = new ArrayList<>();
        HashMap<String, String> mapVarString = new HashMap<>();

        String strVar = "";
        String strCode = "";

        String str = "format PE console\n\n" + "entry start\n\n" + "include 'win32a.inc'\n\n" +
                "section '.data' data readable writable\n\n" + "        strSpace db '%d', 0\n" + "        strDop db '%d', 0ah, 0\n";

        Pattern patternVariables = Pattern.compile("(?<=var)[\\s\\S]*(?=begin)");
        Matcher matcherVariables = patternVariables.matcher(pas);

        while (matcherVariables.find()) {
            strVar = pas.substring(matcherVariables.start(), matcherVariables.end());
        }

        patternVariables = Pattern.compile("([a-zA-Z]([a-zA-Z0-9_]*[\\s]*,[\\s]*)*)([a-zA-Z][a-zA-Z0-9_]*)[\\s]*:[\\s]*integer[\\s]*;");
        matcherVariables = patternVariables.matcher(strVar);

        while (matcherVariables.find()) {
            listVar.add(strVar.substring(matcherVariables.start(), matcherVariables.end()));
        }

        patternVariables = Pattern.compile("[a-zA-Z][a-zA-Z0-9_]*");

        for (int i = 0; i < listVar.size(); i++) {
            matcherVariables = patternVariables.matcher(listVar.get(i));
            while (matcherVariables.find()) {
                if (!listVar.get(i).substring(matcherVariables.start(), matcherVariables.end()).equals("integer")) {
                    listVarInt.add(listVar.get(i).substring(matcherVariables.start(), matcherVariables.end()));
                }
            }
        }

        for (int i = 0; i < listVarInt.size(); i++) {
            str += "        " + listVarInt.get(i) + " dd ?\n";
        }

        Pattern patternCode = Pattern.compile("(?<=begin)[\\s\\S]*(?=end.)");
        Matcher matcherCode = patternCode.matcher(pas);

        while (matcherCode.find()) {
            strCode = pas.substring(matcherCode.start(), matcherCode.end());
        }

        patternCode = Pattern.compile("[a-zA-Z].*;");
        matcherCode = patternCode.matcher(strCode);

        while (matcherCode.find()) {
            listCode.add(strCode.substring(matcherCode.start(), matcherCode.end()));
        }

        Pattern patternWrite = Pattern.compile("write\\('(.*)'\\);");
        Pattern patternWriteLn = Pattern.compile("writeln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");
        Pattern patternReadLn = Pattern.compile("readln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");
        Pattern patternOperation = Pattern.compile("([a-z][a-zA-Z0-9]*)[\\s]*[\\s]*:=[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*([+\\-*/])[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*;");

        strCode = "";

        for (int i = 0; i < listCode.size(); i++) {
            if (listCode.get(i).matches(patternWrite.toString())) {
                matcherCode = patternWrite.matcher(listCode.get(i));
                if (matcherCode.find()) {
                    mapVarString.put("str" + (mapVarString.size() + 1), matcherCode.group(1));
                    strCode += "                push " + "str" + mapVarString.size() + "\n" +
                            "                call [printf]\n";
                }
            } else if (listCode.get(i).matches(patternReadLn.toString())) {
                matcherCode = patternReadLn.matcher(listCode.get(i));
                if (matcherCode.find()) {
                    if (listVarInt.contains(matcherCode.group(1))) {
                        strCode += "                push " + matcherCode.group(1) + "\n" +
                                "                push strSpace\n" +
                                "                call [scanf]\n\n";
                    }
                }
            } else if (listCode.get(i).matches(patternOperation.toString())) {
                matcherCode = patternOperation.matcher(listCode.get(i));
                if (matcherCode.find()) {
                    String strResult = matcherCode.group(1);
                    String strFirstNum = matcherCode.group(2);
                    String strSecondNum = matcherCode.group(4);
                    String strOperation = matcherCode.group(3);
                    ;
                    if (listVarInt.contains(strResult) && listVarInt.contains(strFirstNum) && listVarInt.contains(strSecondNum)) {
                        switch (strOperation) {
                            case "+":
                                strCode += "                mov ecx, [" + strFirstNum + "]\n" +
                                        "                add ecx, [" + strSecondNum + "]\n" +
                                        "                mov [" + strResult + "], ecx\n";
                                break;
                            case "-":
                                strCode += "                mov ecx, [" + strFirstNum + "]\n" +
                                        "                sub ecx, [" + strSecondNum + "]\n" +
                                        "                mov [" + strResult + "], ecx\n";
                                break;
                            case "*":
                                strCode += "                mov ecx, [" + strFirstNum + "]\n" +
                                        "                imul ecx, [" + strSecondNum + "]\n" +
                                        "                mov [" + strResult + "], ecx\n";
                                break;
                            case "/":
                                strCode += "                mov eax, [" + strFirstNum + "]\n" +
                                        "                mov ecx, [" + strSecondNum + "]\n" +
                                        "                div ecx\n" +
                                        "                mov [" + strResult + "], eax\n";
                                break;
                        }
                    }
                }
            } else if (listCode.get(i).matches(patternWriteLn.toString())) {
                matcherCode = patternWriteLn.matcher(listCode.get(i));
                if (matcherCode.find()) {
                    if (listVarInt.contains(matcherCode.group(1))) {
                        strCode += "                push [" + matcherCode.group(1) + "]\n" +
                                "                push strDop\n" +
                                "                call [printf]\n\n";
                    }
                }
            }
        }

        for (String key : mapVarString.keySet()) {
            str += "        " + key + " db '" + mapVarString.get(key) + "', 0\n";
        }

        str += "\nsection '.code' code readable executable\n\n" + "         start:\n" + strCode +
                "         finish:\n" +
                "\n" +
                "                call [getch]\n" +
                "\n" +
                "                call [ExitProcess]\n" +
                "\n" +
                "section '.idata' import data readable\n" +
                "\n" +
                "        library kernel, 'kernel32.dll',\\\n" +
                "                msvcrt, 'msvcrt.dll'\n" +
                "\n" +
                "        import kernel,\\\n" +
                "               ExitProcess, 'ExitProcess'\n" +
                "\n" +
                "        import msvcrt,\\\n" +
                "               printf, 'printf',\\\n" +
                "               scanf, 'scanf',\\\n" +
                "               getch, '_getch'";

        return str;
    }
}

import java.util.ArrayList;

public class DataBase {

    public enum Command {
        READ, WRITE, WRITELN, MATHS
    }

    private final ArrayList<Command> commandSequence;
    private final ArrayList<String> variableList;
    private final ArrayList<String> readCommandList;
    private final ArrayList<String> writeCommandList;
    private final ArrayList<String> writelnCommandList;
    private final ArrayList<String> mathsCommandList;

    public DataBase() {
        commandSequence = new ArrayList<>();
        variableList = new ArrayList<>();
        readCommandList = new ArrayList<>();
        writeCommandList = new ArrayList<>();
        writelnCommandList = new ArrayList<>();
        mathsCommandList = new ArrayList<>();
    }

    public void addVariable(String var) {
        variableList.add(var);
    }

    public String removeVariable(int index) {
        return variableList.remove(index);
    }

    public boolean hasVariable() {
        return !variableList.isEmpty();
    }

    public void addReadCommand(String var) {
        readCommandList.add(var);
        commandSequence.add(Command.READ);
    }

    public void addWriteCommand(String text) {
        writeCommandList.add(text);
        commandSequence.add(Command.WRITE);
    }

    public void addWritelnCommand(String text) {
        writelnCommandList.add(text);
        commandSequence.add(Command.WRITELN);
    }

    public void addMathsCommand(String operation) {
        mathsCommandList.add(operation);
        commandSequence.add(Command.MATHS);
    }

    public boolean hasCommand() {
        return !commandSequence.isEmpty();
    }

    public Command removeCommand() {
        return commandSequence.remove(0);
    }

    public String removeReadCommand() {
        return readCommandList.remove(0);
    }

    public String removeWriteCommand() {
        return writeCommandList.remove(0);
    }

    public String removeWritelnCommand() {
        return writelnCommandList.remove(0);
    }

    public String removeMathsCommand() {
        return mathsCommandList.remove(0);
    }
}

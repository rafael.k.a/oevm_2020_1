package com.company;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PascalLang {

    private StringBuilder _strFromGetter = null;
    String buffstring = "";

    public PascalLang(StringBuilder str){
        _strFromGetter = str;
    }

    public String [] getVars (){
        Pattern pattern = Pattern.compile("(?<=var)[\\S\\s]*(?=begin)");
        Matcher matcher = pattern.matcher(_strFromGetter);

        while (matcher.find()) {
            buffstring += matcher.group();
        }

        String[] varstypes = buffstring.split(":|;");
        buffstring = "";
        for (int i = 0; i < varstypes.length - 1; i++) {
            if (varstypes[i + 1].contains("integer")) {
                buffstring += varstypes[i];
            }
        }

        String varr = "";

        Pattern p = Pattern.compile("[A-Za-z][A-Za-z0-9]*");
        Matcher m = p.matcher(buffstring);
        while (m.find()) {
            varr += m.group() + "_";
        }

        return varr.split("_");
    }

    public String [] getCommandLines (){
        Pattern p1 = Pattern.compile("(?<=begin)[\\S\\s]*(?=end.)");
        Matcher m1 = p1.matcher(_strFromGetter);

        buffstring = "";

        while (m1.find()) {
            buffstring += m1.group();
        }

        String commandline = "";
        Pattern p2 = Pattern.compile("[A-Za-z][\\w:()'' =+*/-]*");
        Matcher m2 = p2.matcher(buffstring);
        while (m2.find()) {
            commandline += m2.group() + "_";
        }
        return commandline.split("_");
    }
}

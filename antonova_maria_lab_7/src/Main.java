package com.company;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;


public class Main {

    public static void main(String[] args) {
        try {
            FileInputStream fPascal = new FileInputStream("C://Users/Мария/Desktop/PascalProg.pas");
            FileOutputStream fAssembler = new FileOutputStream("C://Users/Мария/Desktop/fasmw17325/AssemblerProg.ASM");

            PascalGetter pg = new PascalGetter(fPascal);
            StringBuilder str = pg.getStrsFromPascalDoc();                                 //получаем код на Паскале из файла.pas
            PascalLang pascalTranslator = new PascalLang(str);                             //переводим его в удобный для работы формат

            String [] vars = pascalTranslator.getVars();                                   //получаем блок переменных
            String [] commandlines = pascalTranslator.getCommandLines();                   //получаем блок исполняемого кода

            AssemblerLang assemblerTranslator = new AssemblerLang(vars, commandlines);     //отправляем коды на перевод
            String assemblerCode = assemblerTranslator.GetAssemblerCode();                 //получаем строку кода на Ассемблере

            AssemblerSetter as = new AssemblerSetter(fAssembler);
            as.setStrsToAssemblerDoc(assemblerCode);                                       //записываем строку в файл.asm


        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}


package com.company;
import java.io.FileOutputStream;
import java.io.IOException;

public class AssemblerSetter {

    private FileOutputStream _fAssembler = null;

    public AssemblerSetter(FileOutputStream fAssembler){
        _fAssembler = fAssembler;
    }

    public void setStrsToAssemblerDoc (String assemblerCode){
        try {
            byte[] buffer = assemblerCode.getBytes();
            _fAssembler.write(buffer, 0, buffer.length);
            _fAssembler.close();
        }
        catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}

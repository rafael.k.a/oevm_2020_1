package com.company;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AssemblerLang {

    String [] _vars = null;
    String [] _commandlines = null;

    String beginning = "";
    String idata = "";
    String data = "";
    String code = "";
    String finish = "";

    public AssemblerLang(String [] vars, String [] commandlines) {
        _vars = vars;
        _commandlines = commandlines;
    }

    public String GetAssemblerCode () {
        beginning += "format PE console\n\n" +
                "entry Start\n\n" +
                "include 'win32a.inc'\n\n";

        idata += "section '.idata' import data readable\n\n" +
                "\tlibrary kernel, 'kernel32.dll',\\\n" +
                "\t\tmsvcrt, 'msvcrt.dll'\n\n" +
                "\timport kernel,\\\n" +
                "\t\tExitProcess, 'ExitProcess'\n\n" +
                "\timport msvcrt,\\\n" +
                "\t\tprintf, 'printf',\\\n" +
                "\t\tscanf, 'scanf',\\\n" +
                "\t\tgetch, '_getch'\n\n";

        data += "section '.data' data readable writable\n\n" +
                "\temptyStr db '%d', 0\n" +
                "\tnextStr db ' ', 0dh, 0ah, 0\n" +
                "\tpoint db ',', 0\n\n" +
                "\tbuf dd ? \n";

        for (String var : _vars) {
            data += "\t" + var + " dd ? \n";
        }
        data += "\n";

        code += "section '.code' code readable executable\n\n" +
                "\tStart:\n\n";

        Pattern patternForWriteStrs = Pattern.compile("(?<=write\\(')[^'^)]*(?='\\))");
        int i = 0;
        for (int k = 0; k < _commandlines.length; k++) {
            Matcher matcherForWriteStrs = patternForWriteStrs.matcher(_commandlines[k]);
            while (matcherForWriteStrs.find()) {
                data += "\t" + "msg" + i + " db " + "'" + matcherForWriteStrs.group() + "', 0\n";
                _commandlines[k] = "write msg" + i;
                i++;
            }
        }
        data += "\n";

        Boolean delenye = false;
        Pattern patternForWrite = Pattern.compile("(?<=write )[\\w]*");
        Pattern patternForReadln = Pattern.compile("(?<=readln\\()[^)]*(?=\\))");
        Pattern patternForWriteln = Pattern.compile("(?<=writeln\\()[^)]*(?=\\))");

        for (int k = 0; k < _commandlines.length; k++) {
            Matcher matcherForWrite = patternForWrite.matcher(_commandlines[k]);
            Matcher matcherForReadln = patternForReadln.matcher(_commandlines[k]);
            Matcher matcherForWriteln = patternForWriteln.matcher(_commandlines[k]);

            if (matcherForWrite.find()) {
                code += "\t\tpush " + matcherForWrite.group() + "\n" +
                        "\t\tcall [printf]\n\n";
            }
            if (matcherForReadln.find()) {
                code += "\t\tpush " + matcherForReadln.group() + "\n" +
                        "\t\tpush emptyStr\n" +
                        "\t\tcall [scanf]\n\n";
            }
            if (_commandlines[k].contains(":=")) {
                String [] calculate = _commandlines[k].split(" := ");
                String [] varrs = calculate[1].split (" [+|\\-|*|/] ");
                if (_commandlines[k].contains("+")){
                    code += "\t\tmov eax, [" + varrs[0] + "]\n" +
                            "\t\tadd eax, [" + varrs[1] + "]\n" +
                            "\t\tmov [" + calculate[0] + "], eax\n\n";
                }
                if (_commandlines[k].contains("-")){
                    code += "\t\tmov ebx, [" + varrs[0] + "]\n" +
                            "\t\tsub ebx, [" + varrs[1] + "]\n" +
                            "\t\tmov [" + calculate[0] + "], ebx\n\n";
                }
                if (_commandlines[k].contains("*")){
                    code += "\t\tmov ecx, [" + varrs[0] + "]\n" +
                            "\t\timul ecx, [" + varrs[1] + "]\n" +
                            "\t\tmov [" + calculate[0] + "], ecx\n\n";
                }
                if (_commandlines[k].contains("/")){
                    delenye = true;
                    code += "\t\tmov eax, [" + varrs[0] + "]\n" +
                            "\t\tmov ecx, [" + varrs[1] + "]\n" +
                            "\t\tcdq \n" +
                            "\t\t\tidiv ecx\n" +
                            "\t\t\tmov [buf], edx\n" +
                            "\t\t\tcmp [buf], 0\n" +
                            "\t\t\tjG bufNotOtric\n" +
                            "\t\t\t\tmov ecx, [buf]\n" +
                            "\t\t\t\timul ecx, -1\n" +
                            "\t\t\t\tmov [buf], ecx\n" +
                            "\t\t\tbufNotOtric:\n" +
                            "\t\t\tmov [" + calculate[0] + "], eax\n" +
                            "\t\t\tmov ebx, 0\n" +
                            "\t\t\tlp:\n" +
                            "\t\t\t\tmov eax, [buf]\n" +
                            "\t\t\t\tmov ecx, [" + varrs[1] + "]\n" +
                            "\t\t\t\tcmp [" + varrs[1] + "], 0\n" +
                            "\t\t\t\t\tjG num2NotOtric\n" +
                            "\t\t\t\t\timul ecx, -1\n" +
                            "\t\t\t\t\tmov [" + varrs[1] + "], ecx\n" +
                            "\t\t\t\t num2NotOtric:\n" +
                            "\t\t\t\timul eax, 10\n" +
                            "\t\t\t\tmov edx, 0\n" +
                            "\t\t\t\tdiv ecx\n" +
                            "\t\t\t\tmov [buf], eax\n" +
                            "\t\t\t\tadd ebx, 1\n" +
                            "\t\t\t\tcmp ebx, 1\n" +
                            "\t\t\tjne lp\n\n";
                }
            }
            if (matcherForWriteln.find()) {
                code += "\t\tpush [" + matcherForWriteln.group() + "]\n" +
                        "\t\tpush emptyStr\n" +
                        "\t\tcall [printf]\n\n";
                if (!delenye){
                    code += "\t\tpush nextStr\n" +
                            "\t\tcall [printf]\n\n";
                }
                if (delenye){
                    code += "\t\tpush point\n" +
                            "\t\tcall [printf]\n" +
                            "\t\tpush [buf]\n" +
                            "\t\tpush emptyStr\n" +
                            "\t\tcall [printf]\n\n";
                }
            }
        }
        data += "\n";

        finish += "                finish:\n" +
                "                push nextStr\n" +
                "                call [printf]\n" +
                "                call [getch]\n" +
                "                call [ExitProcess]\n";

        return beginning + idata + data + code + finish;
    }
}

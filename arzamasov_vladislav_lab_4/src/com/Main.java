package com;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int normalForm;
        int numColumn = 5;
        int numRows = 16;
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);

        int[][] tableIst = {
                {0,0,0,0,random.nextInt(2)},
                {0,0,0,1,random.nextInt(2)},
                {0,0,1,0,random.nextInt(2)},
                {0,0,1,1,random.nextInt(2)},
                {0,1,0,0,random.nextInt(2)},
                {0,1,0,1,random.nextInt(2)},
                {0,1,1,0,random.nextInt(2)},
                {0,1,1,1,random.nextInt(2)},
                {1,0,0,0,random.nextInt(2)},
                {1,0,0,1,random.nextInt(2)},
                {1,0,1,0,random.nextInt(2)},
                {1,0,1,1,random.nextInt(2)},
                {1,1,0,0,random.nextInt(2)},
                {1,1,0,1,random.nextInt(2)},
                {1,1,1,0,random.nextInt(2)},
                {1,1,1,1,random.nextInt(2)},
        };

        System.out.print("ДНФ - 1\nКНФ - 2\nВведите номер: ");
        normalForm = scanner.nextInt();
        MinBoolFunc minBoolFunc = new MinBoolFunc(numColumn,numRows,tableIst);
        minBoolFunc.printTab();

        if (normalForm == 1){
            System.out.println("Дизъюнктивная нормальная форма:");
            System.out.print(minBoolFunc.DNF());
        }
        else if (normalForm == 2){
            System.out.println("Конъюнктивная нормальная форма");
            System.out.print(minBoolFunc.KNF());
        }
    }
}

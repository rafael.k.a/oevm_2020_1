import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.print("Введите систему счисления : ");
        int startSS = in.nextInt();

        System.out.print("Введите первое число: ");
        String firstDigit = in.next();

        System.out.print("Введите второе число: ");
        String secondDigit = in.next();

        System.out.print("Знак операции (+,-,*,/) : ");
        char action = in.next().charAt(0);

        Methods methods = new Methods(startSS, firstDigit, secondDigit, action);
        System.out.print(methods.calculator());
    }
}

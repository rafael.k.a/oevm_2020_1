package com.company;

public class TransferToNumberSystem {

    public static int convertToDecimal(int firstNumberSystem, char[] digit) {
        int result = 0;
        for (int i = 0; i < digit.length; i++) {
            int tmp;
            if (digit[i] >= '0' && digit[i] <= '9') {
                tmp = digit[i] - '0';
            } else {
                tmp = 10 + digit[i] - 'A';
            }
            result += tmp * Math.pow(firstNumberSystem, digit.length - i - 1);
        }
        return result;
    }

    public static String convertToBinary(int number) {
        StringBuilder result = new StringBuilder();
        int binarySystem = 2;
        int other;
        for (String currentChar; number > 0; number /= binarySystem) {
            other = number % binarySystem;
            currentChar = Long.toString(other);
            result.insert(0, currentChar);
        }
        return result.toString();
    }
}

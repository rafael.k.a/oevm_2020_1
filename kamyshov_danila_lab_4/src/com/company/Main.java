package com.company;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Minimalization minimalization = new Minimalization();
        TruthTable truthTable = new TruthTable();
        System.out.println("X1 X2 X3 X4 F");
        truthTable.printTruthTable();
        int choise = 0;
        while (choise < 1 || choise > 2) {
            System.out.println("Выберите приведение: ДНФ - 1, КНФ - 2");
            choise = sc.nextInt();
        }
        if (choise == 1) {
            minimalization.convertToDisjunctiveNormalForm(truthTable.truthTable);
        }
        if (choise == 2) {
            minimalization.convertToConjuctiveNormalForm(truthTable.truthTable);
        }
    }
}

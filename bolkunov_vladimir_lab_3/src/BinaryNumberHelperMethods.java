public class BinaryNumberHelperMethods {
    public static boolean[] shiftLeft(boolean[] num, int amount) {
        if (amount < 0) {
            amount = -amount;
        }

        boolean[] res = new boolean[num.length + amount];
        System.arraycopy(num, 0, res, 0, num.length);
        return res;
    }

    public static boolean[] shiftRight(boolean[] num, int amount) {
        if (amount < 0) {
            amount = -amount;
        }

        boolean[] res = new boolean[num.length - amount];
        System.arraycopy(num, 0, res, 0, num.length - amount);
        return res;
    }

    public static int findLeftNextTrue(boolean[] num, int searchStart) throws IndexOutOfBoundsException {
        if (searchStart >= num.length && searchStart >= 0) {
            throw new IndexOutOfBoundsException("searchStart was out of bounds in findBiggerNextTrue");
        } else {
            for (int i = searchStart; i >= 0; --i) {
                if (num[i]) {
                    return i;
                }
            }

            return -1;
        }
    }

    public static boolean[] trim(boolean[] num) {
        int i;
        for (i = 0; i < num.length; ++i) {
            if (num[i]) {
                break;
            }
        }

        boolean[] res = new boolean[num.length - i];
        System.arraycopy(num, i, res, 0, num.length - i);
        return res;
    }


    public static int charToNum(char c) {
        switch (c) {
            case '0':
                return 0;
            case '1':
                return 1;
            case '2':
                return 2;
            case '3':
                return 3;
            case '4':
                return 4;
            case '5':
                return 5;
            case '6':
                return 6;
            case '7':
                return 7;
            case '8':
                return 8;
            case '9':
                return 9;
            case 'A':
                return 10;
            case 'B':
                return 11;
            case 'C':
                return 12;
            case 'D':
                return 13;
            case 'E':
                return 14;
            case 'F':
                return 15;
            default:
                return 0;
        }
    }

    public static char numToChar(int n) {
        switch (n) {
            case 0:
                return '0';
            case 1:
                return '1';
            case 2:
                return '2';
            case 3:
                return '3';
            case 4:
                return '4';
            case 5:
                return '5';
            case 6:
                return '6';
            case 7:
                return '7';
            case 8:
                return '8';
            case 9:
                return '9';
            case 10:
                return 'A';
            case 11:
                return 'B';
            case 12:
                return 'C';
            case 13:
                return 'D';
            case 14:
                return 'E';
            case 15:
                return 'F';
            default:
                return ' ';
        }
    }

    public static BinaryNumber decimalToBinary(int decimal) {
        int testNum = 2;

        int leng;
        for (leng = 1; testNum <= decimal; ++leng) {
            testNum *= 2;
        }

        boolean[] value = new boolean[leng];

        for (int i = 0; i < leng; ++i) {
            if (decimal % 2 == 0) {
                value[leng - 1 - i] = false;
            } else {
                value[leng - 1 - i] = true;
            }

            decimal /= 2;
        }

        return new BinaryNumber(value);
    }
}

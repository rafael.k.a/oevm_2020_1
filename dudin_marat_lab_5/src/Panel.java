import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class Panel extends JPanel {
    Random random= new Random();

    private boolean KNF = false;
    private boolean DNF = false;
    private int sizeI = 16;
    private int sizeJ = 5;
    private int indent = 10;
    private int squareSize = 30;
    private int[][] array = {
            {0, 0, 0, 0, random.nextInt(2)},
            {0, 0, 0, 1, random.nextInt(2)},
            {0, 0, 1, 0, random.nextInt(2)},
            {0, 0, 1, 1, random.nextInt(2)},
            {0, 1, 0, 0, random.nextInt(2)},
            {0, 1, 0, 1, random.nextInt(2)},
            {0, 1, 1, 0, random.nextInt(2)},
            {0, 1, 1, 1, random.nextInt(2)},
            {1, 0, 0, 0, random.nextInt(2)},
            {1, 0, 0, 1, random.nextInt(2)},
            {1, 0, 1, 0, random.nextInt(2)},
            {1, 0, 1, 1, random.nextInt(2)},
            {1, 1, 0, 0, random.nextInt(2)},
            {1, 1, 0, 1, random.nextInt(2)},
            {1, 1, 1, 0, random.nextInt(2)},
            {1, 1, 1, 1, random.nextInt(2)},
    };

    public void paint(Graphics g) {
        super.paint(g);
        Color LeadLine = new Color(255,0,0);
        Color UnnecessaryLine = new Color(0,0,0);
        for (int i = 0; i < sizeI; i++) {
            for (int j = 0; j < sizeJ; j++) {
                g.setColor(UnnecessaryLine);
                if(KNF) {
                    if (array[i][sizeJ-1] == 0) {
                        g.setColor(LeadLine);
                    }
                }
                if(DNF) {
                    if (array[i][sizeJ-1] == 1) {
                        g.setColor(LeadLine);
                    }
                }
                g.drawRect(indent + j * squareSize, indent + i * squareSize, squareSize, squareSize);
                g.drawString(array[i][j] + "", indent * 2 + j * squareSize, indent * 3  + i * squareSize);
            }
        }
    }

    public void createNewArray() {
        DNF = false;
        KNF = false;
        int[][] newArray = {
                {0, 0, 0, 0, random.nextInt(2)},
                {0, 0, 0, 1, random.nextInt(2)},
                {0, 0, 1, 0, random.nextInt(2)},
                {0, 0, 1, 1, random.nextInt(2)},
                {0, 1, 0, 0, random.nextInt(2)},
                {0, 1, 0, 1, random.nextInt(2)},
                {0, 1, 1, 0, random.nextInt(2)},
                {0, 1, 1, 1, random.nextInt(2)},
                {1, 0, 0, 0, random.nextInt(2)},
                {1, 0, 0, 1, random.nextInt(2)},
                {1, 0, 1, 0, random.nextInt(2)},
                {1, 0, 1, 1, random.nextInt(2)},
                {1, 1, 0, 0, random.nextInt(2)},
                {1, 1, 0, 1, random.nextInt(2)},
                {1, 1, 1, 0, random.nextInt(2)},
                {1, 1, 1, 1, random.nextInt(2)},
        };
        array = newArray;
    }

    public void setDNF() {
        DNF = true;
        KNF = false;
    }
    public void setKNF() {
        KNF = true;
        DNF = false;
    }
    public int [][] getArray() {
        return array;
    }
}

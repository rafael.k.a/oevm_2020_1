public class Converter {

    private int sizeI = 16;

    private String toDNF(int step, int[][] array) {
        int X1 = 0;
        int X4 = 3;
        int F = 4;

        StringBuilder strBuilder = new StringBuilder();

        if (array[step][F] == 1) {
            strBuilder.append("( ");
            for (int j = X1; j < X4; j++) {
                if (array[step][j] == 1) {
                    strBuilder.append("X").append(j + 1).append(" * ");
                } else {
                    strBuilder.append("-X").append(j + 1).append(" * ");
                }
            }
            if (array[step][X4] == 1) {
                strBuilder.append("X4");
            } else {
                strBuilder.append("-X4");
            }
            strBuilder.append(" ) + ");
            return strBuilder.append("\n").toString();
        }
        return "";
    }

    private String toKNF(int step, int[][] array) {
        int X1 = 0;
        int X4 = 3;
        int lastInd = 4;

        StringBuilder stringBuilder = new StringBuilder();

        if (array[step][lastInd] == 0) {
            stringBuilder.append("( ");
            for (int j = X1; j < X4; j++) {
                if (array[step][j] == 0) {
                    stringBuilder.append("X").append(j + 1).append(" + ");
                } else {
                    stringBuilder.append("-X").append(j + 1).append(" + ");
                }
            }
            if (array[step][X4] == 0) {
                stringBuilder.append("X4");
            } else {
                stringBuilder.append("-X4");
            }
            stringBuilder.append(" ) * ");
            return stringBuilder.append("\n").toString();
        }
        return "";
    }

    public String convertFunction(String type, int[][] array) {
        StringBuilder result = new StringBuilder();
        switch (type) {
            case "DNF":
                for (int i = 0; i < sizeI; i++) {
                    result.append(toDNF(i, array));
                }
                result.setLength(result.length() - 3);
                return result.toString();
            case "KNF":
                for (int i = 0; i < sizeI; i++) {
                    result.append(toKNF(i, array));
                }
                result.setLength(result.length() - 3);
                return result.toString();
        }
        return "";
    }
}

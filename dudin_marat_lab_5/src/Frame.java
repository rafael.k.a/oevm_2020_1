import javax.swing.*;

public class Frame {

        Converter converter = new Converter();
        private JFrame frame;
        private Panel myPanel;
        private JTextArea boolFunctions = new JTextArea();
        private JButton buttonNewArray = new JButton("Новый массив");
        private JButton buttonToDNF = new JButton("Привести к ДНФ");
        private JButton buttonToKNF = new JButton("Привести к КНФ");

        Frame() {
            frame = new JFrame();
            frame.setBounds(100, 100, 380, 540);
            frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            frame.setVisible(true);
            frame.setResizable(false);
            frame.setLayout(null);

            boolFunctions.setBounds(180, 10, 150, 360);
            frame.getContentPane().add(boolFunctions);

            myPanel = new Panel();
            myPanel.setBounds(0, 0, 180, 500);
            frame.getContentPane().add(myPanel);

            buttonNewArray.addActionListener(e -> newArray());
            buttonNewArray.setBounds(180, 380, 150, 30);
            frame.getContentPane().add(buttonNewArray);

            buttonToDNF.addActionListener(e -> cutToDNF());
            buttonToDNF.setBounds(180, 420, 150, 30);
            frame.getContentPane().add(buttonToDNF);

            buttonToKNF.addActionListener(e -> cutToKNF());
            buttonToKNF.setBounds(180, 460, 150, 30);
            frame.getContentPane().add(buttonToKNF);
        }

        private void newArray() {
            boolFunctions.setText("");
            myPanel.createNewArray();
            myPanel.repaint();
        }

        private void cutToKNF() {
            myPanel.setKNF();
            myPanel.repaint();
            boolFunctions.setText(converter.convertFunction("KNF", myPanel.getArray()));
        }

        private void cutToDNF() {
            myPanel.setDNF();
            myPanel.repaint();
            boolFunctions.setText(converter.convertFunction("DNF", myPanel.getArray()));
        }
}
public class Function {
    static int lengthI = 16;
    static int lengthJ = 5;
    static int LastTableColumn = 3;
    static int FColumn = 4;

    public void printTable(int[][] array){
        for(int i = 0; i< lengthI; i++){
            for(int j = 0; j< lengthJ; j++){
                if(j == lengthJ - 1){
                    System.out.print("\t");
                }
                System.out.print("\t" + array[i][j]+" ");
            }
            System.out.print("\n");
        }
    }

    public String DNF(int[][] array) {
        int count = 0;
        StringBuilder strBuilder = new StringBuilder();
        for (int ind = 0; ind < lengthI; ind++) {
            if (array[ind][FColumn] == 1) {
                count++;
                if(count > 1){
                    strBuilder.append(" + ");
                }
                strBuilder.append("(");
                for (int j = 0; j <= LastTableColumn; j++) {
                    if(j != LastTableColumn){
                        if (array[ind][j] == 1) {
                            strBuilder.append("X").append(j + 1).append(" * ");
                        } else {
                            strBuilder.append("(-X").append(j + 1 + ")").append(" * ");
                        }
                    }else{
                        if (array[ind][j] == 1) {
                            strBuilder.append("X").append(j + 1);
                        } else {
                            strBuilder.append("(-X").append(j + 1 + ")");
                        }
                    }
                }
                strBuilder.append(")");
            }
        }
        return strBuilder.toString();
    }

    public String KNF(int[][] array) {
        int count = 0;
        StringBuilder strBuilder = new StringBuilder();
        for (int ind = 0; ind < lengthI; ind++)
            if (array[ind][FColumn] == 0) {
                count++;
                if(count > 1){
                    strBuilder.append(" * ");
                }
                strBuilder.append("(");
                for (int j = 0; j <= LastTableColumn; j++) {
                    if(j != LastTableColumn){
                        if (array[ind][j] == 1) {
                            strBuilder.append("X").append(j + 1).append(" + ");
                        } else {
                            strBuilder.append("(-X").append(j + 1 + ")").append(" + ");
                        }
                    }else{
                        if (array[ind][j] == 1) {
                            strBuilder.append("X").append(j + 1);
                        } else {
                            strBuilder.append("(-X").append(j + 1 + ")");
                        }
                    }
                }
                strBuilder.append(")");
            }
        return strBuilder.toString();
    }
}

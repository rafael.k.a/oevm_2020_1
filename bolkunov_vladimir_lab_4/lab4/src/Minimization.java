import jdk.nashorn.api.tree.SwitchTree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Random;

public class Minimization {
    private FunctionDefenition[] funcs;

    public Minimization() {
        Random rnd = new Random();
        funcs = new FunctionDefenition[16];

        for (int i = 0; i < 16; i++) {
            Boolean x1 = i >= 8;
            Boolean x2 = i % 8 >= 4;
            Boolean x3 = i % 4 >= 2;
            Boolean x4 = i % 2 == 1;
            //Это я так строю таблицу значений переменных (0000, 0001, ..., 1110, 1111)
            funcs[i] = new FunctionDefenition(x1, x2, x3, x4, false);
        }

        for (int i = 0; i < 16; i++) {
            if (rnd.nextBoolean()) {
                FunctionDefenition cur = funcs[i];
                funcs[i] = new FunctionDefenition(cur.getX1(), cur.getX2(), cur.getX3(), cur.getX4(), true);
            }
        }
    }

    public String minimize(boolean DNF) {
        ArrayList<FunctionDefenition> curFuncs = new ArrayList<FunctionDefenition>();
        for (int i = 0; i < 16; i++) {
            if (funcs[i].getF() == DNF) {
                curFuncs.add(funcs[i]);
            }
        }

        FunctionDefenition[] curFuncArr = new FunctionDefenition[curFuncs.size()];
        curFuncs.toArray(curFuncArr);

        curFuncArr = manageFuncs(curFuncArr, true);
        curFuncArr = manageFuncs(curFuncArr, false);

        String res = funcsToString(curFuncArr, DNF);
        return res;
    }

    //Выполняет процедуру склеивания(glue) или объеденения(merge) в зависимости от параметра isGlueing
    private FunctionDefenition[] manageFuncs(FunctionDefenition[] values, boolean isGlueing) {
        ArrayList<FunctionDefenition> result = new ArrayList<FunctionDefenition>();
        for (int i = 0; i < values.length; i++) {
            if (values[i] != null && !values[i].isEmpty())
                result.add(values[i]);
        }

        boolean managed;
        do {
            managed = false;
            for (int i = 0; i < result.size(); i++) {
                if (result.get(i) == null || result.get(i).isEmpty()) {
                    result.remove(result.get(i));
                    managed = true;
                    break;
                }

                for (int j = 0; j < result.size(); j++) {
                    if (i == j) continue;
                    if (result.get(j) == null || result.get(j).isEmpty()) {
                        result.remove(result.get(j));
                        managed = true;
                        break;
                    }

                    if ((result.get(i).canGlue(result.get(j)) && isGlueing) ||
                            (result.get(i).canMerge(result.get(j)) && !isGlueing)) {
                        FunctionDefenition curI = result.get(i);
                        FunctionDefenition curJ = result.get(j);
                        FunctionDefenition f;
                        if (isGlueing)
                            f = curI.glue(curJ);
                        else
                            f = curI.merge(curJ);
                        if (f != null && !f.isEmpty())
                            result.add(f);
                        result.remove(curI);
                        result.remove(curJ);
                        managed = true;
                        break;
                    }
                }

                if (managed)
                    break;
            }
        }
        while (managed);

        FunctionDefenition[] res = new FunctionDefenition[result.size()];
        result.toArray(res);
        return res;
    }

    public String tableToString() {
        System.out.println("x1|x2|x3|x4|F");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 16; i++) {
            sb.append(booleanToString(funcs[i].getX1()) + " |");
            sb.append(booleanToString(funcs[i].getX2()) + " |");
            sb.append(booleanToString(funcs[i].getX3()) + " |");
            sb.append(booleanToString(funcs[i].getX4()) + " |");
            sb.append(booleanToString(funcs[i].getF()));
            sb.append("\n");
        }
        return sb.toString();
    }

    private String booleanToString(Boolean bool) {
        if (bool == null)
            return "-";
        else if (bool == true)
            return "1";
        else
            return "0";
    }

    @Override
    public String toString() {
        return funcsToString(funcs, true);
    }

    private String funcsToString(FunctionDefenition[] fs, boolean DNF) {
        char c;
        if (DNF) {
            c = '∨';
        } else {
            c = '∧';
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < fs.length; i++) {
            FunctionDefenition f = fs[i];
            if (i != 0) {
                sb.append(c);
            }
            sb.append(f.toString(DNF));
        }
        if (DNF) {
            sb.append("=1");
        } else {
            sb.append("=0");
        }
        return sb.toString();
    }
}

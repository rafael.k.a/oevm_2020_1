package com.company;

import java.io.*;

public class Pascal {

    public String read(){

        StringBuilder builder = new StringBuilder();

        try(FileReader reader = new FileReader("PascalProgram.pas"))
        {
            int ch;
            while((ch = reader.read()) != -1){
                builder.append((char) ch);
            }
        }

        catch(IOException ex){
            System.out.println(ex.getMessage());
        }

        return builder.toString().replaceAll(";", ";\n");
    }
}

package com.company;

public class Main {

    public static void main(String[] args) {
        ReadingFromPascal reader = new ReadingFromPascal();

        AssemblerWriter writer = new AssemblerWriter();

        StringParser parser = new StringParser(reader.read(), writer);
        parser.parse();

        writer.write();
    }
}

package com.company;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringParser {
    private AssemblerWriter writer;

    private String curString;

    private ArrayList<String> varsString = new ArrayList<>();
    private ArrayList<String> codsString = new ArrayList<>();
    private ArrayList<String> vars = new ArrayList<>();

    private String varsBlock;
    private String codsBlock;

    public StringParser(String str, AssemblerWriter writer) {
        curString = str;
        this.writer = writer;
    }

    public void parse() {
        Pattern patternVariables = Pattern.compile("(?<=var)[\\s\\S]*(?=begin)");
        Matcher matcherVariables = patternVariables.matcher(curString);

        while (matcherVariables.find()) {
            varsBlock = curString.substring(matcherVariables.start(), matcherVariables.end());
        }

        Pattern patternCode = Pattern.compile("(?<=begin)[\\s\\S]*(?=end.)");
        Matcher matcherCode = patternCode.matcher(curString);

        while (matcherCode.find()) {
            codsBlock = curString.substring(matcherCode.start(), matcherCode.end());
        }

        parseVarsStrings();
        parseCodsStrings();
    }

    public void parseVarsStrings() {
        Pattern patternVariables = Pattern.compile("([a-zA-Z]([a-zA-Z0-9_]*[\\s]*,[\\s]*)*)([a-zA-Z][a-zA-Z0-9_]*)[\\s]*:[\\s]*integer[\\s]*;");
        Matcher matcherVariables = patternVariables.matcher(varsBlock);

        while (matcherVariables.find()) {
            varsString.add(varsBlock.substring(matcherVariables.start(), matcherVariables.end()));
        }
        parseVarsInteger();
    }

    public void parseVarsInteger() {
        Pattern patternVariables = Pattern.compile("[a-zA-Z][a-zA-Z0-9_]*");
        Matcher matcherVariables;
        String var;

        for (String string : varsString) {
            matcherVariables = patternVariables.matcher(string);

            while (matcherVariables.find()) {
                var = string.substring(matcherVariables.start(), matcherVariables.end());
                if (!var.equals("integer")) { vars.add(var); }
            }
        }
        writer.addToVariables(vars);
    }

    public void parseCodsStrings(){
        Pattern stringPattern = Pattern.compile("[a-zA-Z].*;");
        Matcher stringMatcher = stringPattern.matcher(codsBlock);

        while (stringMatcher.find()) {
            codsString.add(codsBlock.substring(stringMatcher.start(), stringMatcher.end()));
        }
        createCode();
    }

    public void createCode(){
        Pattern patternWrite = Pattern.compile("write\\('(.*)'\\);");
        Pattern patternWriteLn = Pattern.compile("writeln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");

        Pattern patternReadLn = Pattern.compile("readln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");
        Pattern patternOperation = Pattern.compile("([a-z][a-zA-Z0-9]*)[\\s]*[\\s]*:=[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*([+\\-*/])[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*;");

        for (String string : codsString) {
            if (string.matches(patternWrite.toString())) {
                Matcher matcherCode = patternWrite.matcher(string);

                if (matcherCode.find()) {
                    writer.addWrite(matcherCode.group(1));
                }
            }
            else if (string.matches(patternReadLn.toString())) {
                Matcher matcherCode = patternReadLn.matcher(string);

                if (matcherCode.find()) {
                    writer.addReadLn(matcherCode.group(1));
                }
            }
            else if (string.matches(patternOperation.toString())) {
                Matcher matcherCode = patternOperation.matcher(string);

                if (matcherCode.find()) {
                    writer.addOperation(matcherCode.group(1), matcherCode.group(2), matcherCode.group(3), matcherCode.group(4));
                }
            }
            else if (string.matches(patternWriteLn.toString())) {
                Matcher matcherCode = patternWriteLn.matcher(string);

                if (matcherCode.find()) {
                    writer.addWriteLn(matcherCode.group(1));
                }
            }
        }
    }
}

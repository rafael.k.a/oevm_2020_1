package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите исходное основаиние числа");
        int from = in.nextInt();           // Вводим исходное основание числа
        System.out.println("Введите итоговое основаиние числа");
        int to = in.nextInt();          // водим итоговое основание числа
        System.out.println("Введите само число");
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();       // также вводим само число

        if (from > 0 && to < 17) {
            Convert convert = new Convert(from, to);
            int ten = convert.convertToTen(input);
            StringBuffer otvet;
            otvet = convert.convertTenTo(ten);
            System.out.println("\nИтоговое число: " + otvet);
        }
        in.close();
    }
}

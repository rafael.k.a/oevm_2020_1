public class Methods {

    private int firstNumber = 0;
    private int secondNumber = 0;

    private String firstDigit;
    private String secondDigit;

    private char action;
    private String firstDigit2SS;
    private String secondDigit2SS;


    public Methods(int startSS, String firstDigit, String secondDigit, char action) {

        this.firstDigit = firstDigit;
        this.secondDigit = secondDigit;
        this.action = action;

        digitsTo10SS(startSS);
        digitsTo2SS();

    }

    private Boolean comparison(String str1, String str2) {
        char[] strArray1 = new StringBuilder(str1).reverse().toString().toCharArray();
        char[] strArray2 = new StringBuilder(str2).reverse().toString().toCharArray();

        if (strArray1.length > strArray2.length) {
            return true;
        }
        if (strArray1.length < strArray2.length) {
            return false;
        }
        if (strArray1.length == strArray2.length) {

            for (int i = 0; i < strArray1.length; i++) {

                if (strArray1[i] == '1' && strArray2[i] == '0') {
                    return true;
                }
                if (strArray1[i] == '0' && strArray2[i] == '1') {
                    return false;
                }
            }
        }
        return true;
    }

    private void digitsTo10SS(int startSS) {
        char[] firstDigitArray = firstDigit.toCharArray();
        char[] secondDigitArray = secondDigit.toCharArray();
        for (int i = 0; i < firstDigitArray.length; i++) {
            int temp;
            if (firstDigitArray[i] >= '0' && firstDigitArray[i] <= '9') {
                temp = firstDigitArray[i] - '0';
            } else {
                temp = 10 + firstDigitArray[i] - 'A';
            }
            firstNumber += temp * Math.pow(startSS, firstDigitArray.length - i - 1);
        }

        for (int i = 0; i < secondDigitArray.length; i++) {
            int temp;
            if (secondDigitArray[i] >= '0' && secondDigitArray[i] <= '9') {
                temp = secondDigitArray[i] - '0';
            } else {
                temp = 10 + secondDigitArray[i] - 'A';
            }
            secondNumber += temp * Math.pow(startSS, secondDigitArray.length - i - 1);
        }
    }

    private void digitsTo2SS() {
        StringBuilder strFirstDigit = new StringBuilder();
        StringBuilder strSecondDigit = new StringBuilder();

        if (firstNumber == 0) {
            strFirstDigit.append("0");
        }

        for (int i = 0; firstNumber > 0; i++) {

            strFirstDigit.append((char) (firstNumber % 2 + '0'));
            firstNumber /= 2;
        }

        firstDigit2SS = strFirstDigit.toString();

        if (secondNumber == 0) {
            strSecondDigit.append("0");
        }

        for (int i = 0; secondNumber > 0; i++) {

            strSecondDigit.append((char) (secondNumber % 2 + '0'));
            secondNumber /= 2;
        }

        secondDigit2SS = strSecondDigit.toString();
    }

    public String calculator() {
        StringBuilder str = new StringBuilder();
        switch (action) {
            case '+':
                str.append(addition(firstDigit2SS, secondDigit2SS));
                return str.reverse().toString();
            case '-':
                str.append(subtraction(firstDigit2SS, secondDigit2SS)).reverse();
                return deleteZerows(str.toString());
            case '*':
                str.append(multiplication(firstDigit2SS, secondDigit2SS)).reverse();
                return str.toString();
            case '/':
                str.append(division(firstDigit2SS, secondDigit2SS)).reverse();
                return str.toString();
        }
        return "error";
    }

    private String addition(String str1, String str2) {

        char[] firstDigitArray = str1.toCharArray();
        char[] secondDigitArray = str2.toCharArray();

        StringBuilder result = new StringBuilder();

        int transfer = 0;
        int i = 0;

        while (i < secondDigitArray.length) {
            if (transfer == 0) {
                if (firstDigitArray[i] == '0' && secondDigitArray[i] == '0') {
                    result.append("0");
                    transfer = 0;
                }
                if ((firstDigitArray[i] == '0' && secondDigitArray[i] == '1') || (firstDigitArray[i] == '1' && secondDigitArray[i] == '0')) {
                    result.append("1");
                    transfer = 0;
                }
                if (firstDigitArray[i] == '1' && secondDigitArray[i] == '1') {
                    result.append("0");
                    transfer = 1;
                }
            } else {
                if (firstDigitArray[i] == '0' && secondDigitArray[i] == '0') {
                    result.append("1");
                    transfer = 0;
                }
                if ((firstDigitArray[i] == '0' && secondDigitArray[i] == '1') || (firstDigitArray[i] == '1' && secondDigitArray[i] == '0')) {
                    result.append("0");
                    transfer = 1;
                }
                if (firstDigitArray[i] == '1' && secondDigitArray[i] == '1') {
                    result.append("1");
                    transfer = 1;
                }
            }
            i++;
        }

        while (i < firstDigitArray.length) {
            if (firstDigitArray[i] == '0' && transfer == 0) {
                result.append("0");
                transfer = 0;
            }
            if ((firstDigitArray[i] == '0' && transfer == 1) || (firstDigitArray[i] == '1' && transfer == 0)) {
                result.append("1");
                transfer = 0;
            }
            if (firstDigitArray[i] == '1' && transfer == 1) {
                result.append("0");
                transfer = 1;
            }
            i++;
        }

        if (transfer == 1) {
            result.append("1");
        }

        return result.toString();
    }

    private String subtraction(String str1, String str2) {

        StringBuilder result = new StringBuilder();
        str2 = reverse(str2);
        result.append(addition(str1, str2));
        result.deleteCharAt(result.length() - 1);
        return result.toString();
    }

    private String multiplication(String str1, String str2) {

        char[] strArray2 = str2.toCharArray();
        StringBuilder strArray1 = new StringBuilder(str1);
        String result = "0";
        int iIndex = 0;
        for (int i = 0; i < strArray2.length; i++) {
            if (strArray2[i] == '1') {
                for (int j = 0; j < i - iIndex; j++) {
                    strArray1.insert(0, '0');

                }
                result = addition(strArray1.toString(), result);
                iIndex = i;
            }
        }

        return result;
    }

    private String division(String str1, String str2) {
        String comparison = str2;
        String result = "0";

        if (str2.toCharArray().length == 1 && str2.toCharArray()[0] != '1') {
            return "Error!";
        }
        while (comparison(str1, comparison)) {
            result = addition(result, "1");
            comparison = addition(comparison, str2);
        }
        return result;

    }

    private String reverse(String str) {

        StringBuilder string = new StringBuilder();
        StringBuilder result = new StringBuilder();
        char[] digitArray = str.toCharArray();

        int i = 0;
        while (i < secondDigit2SS.length()) {
            if (digitArray[i] == '1') {
                string.append("0");
            } else {
                string.append("1");
            }
            i++;
        }
        while (i < firstDigit2SS.length()) {
            string.append("1");
            i++;
        }
        result.append(addition(string.toString(), "1"));
        return result.toString();
    }

    private String deleteZerows(String str) {
        StringBuilder result = new StringBuilder();
        char[] digitArray = str.toCharArray();
        int i = 0;
        while (digitArray[i] == '0') {
            i++;
            if (i == str.length()) {
                return "0";
            }
        }
        while (i < str.length()) {
            result.append(digitArray[i]);
            i++;
        }
        return result.toString();
    }

}
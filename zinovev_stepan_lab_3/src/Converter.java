public class Converter {
    static int tenthNumSystem = 0;
    static int fromASCIIToDigit = 48;
    static int fromASCIIToLetter = 55;
    static boolean negative = false;

    static String fault = "Ошибка.";

    public static String convert(int ourNumSystem, int secondNumSystem, String value) {
        char[] valueString = value.toCharArray();

        for (int i = 0; i < valueString.length; i++) {
            double powNumSystem = Math.pow(ourNumSystem, valueString.length - 1 - i);
            if (Character.isDigit(valueString[i])) {
                if (valueString[i] - fromASCIIToDigit < ourNumSystem && valueString[i] - fromASCIIToDigit >= 0) {
                    tenthNumSystem += (valueString[i] - fromASCIIToDigit) * powNumSystem;
                } else {
                    return fault;
                }
            } else if (Character.isLetter(valueString[i])) {
                if (valueString[i] - fromASCIIToLetter < ourNumSystem && valueString[i] - fromASCIIToLetter >= 0) {
                    tenthNumSystem += (valueString[i] - fromASCIIToLetter) * powNumSystem;
                } else {
                    return fault;
                }
            } else {
                if (i == 0 && valueString[i] == '-') {
                    negative = true;
                } else {
                    return fault;
                }
            }
        }
        return convertToSecondNumSystem(secondNumSystem);
    }

    public static String convertToSecondNumSystem(int secondNumSystem) {
        StringBuilder valueSecondNumSystem = new StringBuilder();
        String answer;

        if (tenthNumSystem == 0) {
            return "0";
        } else {
            while (tenthNumSystem > 0) {
                if (Character.isDigit(tenthNumSystem % secondNumSystem + fromASCIIToDigit)) {
                    valueSecondNumSystem.append((char) (tenthNumSystem % secondNumSystem + fromASCIIToDigit));
                } else {
                    valueSecondNumSystem.append((char) (tenthNumSystem % secondNumSystem + fromASCIIToLetter));
                }
                tenthNumSystem /= secondNumSystem;
            }
            if (negative) {
                valueSecondNumSystem.append("-(");
                valueSecondNumSystem.insert(0, ')');
            }
            answer = valueSecondNumSystem.reverse().toString();
            return answer;
        }
    }
}

# Лабораторная работа №8

## ПИбд-21 Фахрутдинов Азат

8) Физические компоненты ЭВМ

**Задача:**
Разобрать и собрать системный блок ЭВМ.

Задачу по разборке и сборке ЭВМ я выполнил с помощью своего стационарного ПК.
(В лабораторной работе №1 я уже описал его характеристики.)

Данная задача не показалась мне сложной, так как я уже имел опыт в подборе и сборке ПК по просьбам родственников и друзей. 

P.S. Благодаря этой лабораторной работе удалось совместить приятное с полезным :) - почистил все комплектующие в тех местах, куда не удавалось подобраться без разборки ПК.

Прикладываю ссылку на Гугл Диск с фотографиями разборки/сборки ПК:

https://drive.google.com/drive/folders/1xsTE16ZuCzGfbh_RrVxxPDRao_tSfXgM?usp=sharing
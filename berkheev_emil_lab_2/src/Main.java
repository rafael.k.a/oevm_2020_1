import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {

    //Simple converter for alphabets without Letters
    public static long convertToDecimal(long ch, long osn) {
        long k = 0;
        int razr = (Long.toString(ch)).length();
        for (int i = 0; i < razr; i++) {
            k = (k + ((ch % 10) * (int) (java.lang.Math.pow(osn, i))));
            ch = ch / 10;
        }
        return k;
    }

    //Complex converter with alphabet initialization
    public static StringBuilder convertToSelected(long decimalNumber, int radix, StringBuilder result1) throws Exception {
        final int bitslong = 64;
        final String digits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        //exception
        if (radix < 2 || radix > digits.length()) {
            throw new Exception("The radix must be >= 2 and <= " + digits.length());
        }
        //simplifier
        if (decimalNumber == 0) {
            result1.append("0");
            return result1;
        }
        //0-63
        int index = bitslong - 1;
        long currentNumber = Math.abs(decimalNumber);
        char[] charArray = new char[bitslong];

        while (currentNumber != 0) {
            int remainder = (int) (currentNumber % radix);
            charArray[index--] = digits.charAt(remainder);
            currentNumber = currentNumber / radix;
        }


        result1.append(charArray, index + 1, bitslong - index - 1);
        //Privative numbers
        if (decimalNumber < 0) {
            result1.insert(1, "-");
        }
        return result1;
    }

    //Using inserted data and StringBuilder we are operating on char[],to change alphabetical measures
    public static void originToDestiny(int origin, int destiny, long k, String numb, StringBuilder result) throws Exception {
        if (origin > 10) {
            char[] ceil = numb.toCharArray();
            for (int i1 = numb.length() - 1, i4 = 0; i1 >= 0; i1--, i4++) {

                if (ceil[i1] > 47 && ceil[i1] < 58) {
                    k += (ceil[i1] - 48) * (long) Math.pow(origin, i4);

                } else if (ceil[i1] > 64 && ceil[i1] < 91) {
                    k += (ceil[i1] - 65 + 10) * (long) (Math.pow(origin, i4));

                } else if (ceil[i1] > 96 && ceil[i1] < 123) {
                    k += (ceil[i1] - 97 + 10) * (long) (Math.pow(origin, i4));
                }
            }
            result = convertToSelected(k, destiny, result);
        } else {
            long ceil = Long.parseUnsignedLong(numb);
            long k1 = convertToDecimal(ceil, origin);
            result = convertToSelected(k1, destiny, result);
        }
        System.out.println(result);
    }


    public static void main(String Args[]) throws Exception {
        int origin;
        int destiny;
        long k = 0;
        StringBuilder result
                = new StringBuilder();
        /*
        Buffer declaration + initialization
        */
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(System.in));
        origin = Integer.parseInt(reader.readLine());
        destiny = Integer.parseInt(reader.readLine());
        String numb = reader.readLine();
        originToDestiny(origin, destiny, k, numb, result);
    }

}

# Лабораторная работа №5 "Изучение простейших логических элементов" 

* Использовала Swing для создания десктопного приложения, которое приводит таблицу истинности к ДНФ и КНФ  
* Запускается через среду разработки IDEA  
* Тесты:

![Картинка](https://media.discordapp.net/attachments/691905536722993196/771469162215505950/unknown.png)  

[Видео](https://drive.google.com/file/d/1gXiDX6I3H0leTQzpUbPlmC8Zz24aoqem/view?usp=sharing)

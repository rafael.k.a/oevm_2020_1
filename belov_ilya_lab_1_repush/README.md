## Лабораторная работа №1
## Выполнил студент группы ИСЭбд-21 Белов Илья

### Сборка: 

1. **Процессор Intel Core i5-9400 OEM** - процессор сокета LGA 1151-v2 , хороший за свои деньги

2. **Материнская плата GIGABYTE H310M S2H 2.0** - подходит для данного сокета

3. **Корпус Aerocool Tomahawk-S [ACCM-PV17014.11] черный** - просто хороший чёрный корпус

4. **Видеокарта MSI GeForce GTX 1050 Ti 4GT [GTX 1050 Ti 4GT OCV1]** - 4 гб хватит для программирования и большинства игр(не CS:G0)

5. **Кулер для процессора DEEPCOOL GAMMAXX 300 [DP-MCH3-GMX300]** - кулер достаточен для охлаждения процессора, надеюсь не сгорит))

6. **Оперативная память Kingston HyperX FURY Black [HX424C15FB3K2/16] 16 ГБ** - 16 гб хватит для программирования, игр, ещё и браузер с дискордом(не zoom) можно открыть

7. **250 ГБ SSD-накопитель Samsung 860 EVO [MZ-76E250BW]** - просто samsung

8. **Блок питания Zalman WATTBIT 500W [ZM500-XE]** - ну это мне посоветовали добрые люди

9. ***Общая сумма комплектующих 45000. Не очень дорогой, но неплохо работающий камплюдахтер.***
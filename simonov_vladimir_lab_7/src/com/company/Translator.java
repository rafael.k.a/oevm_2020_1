package com.company;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Translator {
    private String pascalCode;
    private String vars;
    private String code;
    private ArrayList<String> varLines = new ArrayList<>();
    private ArrayList<String> codeLines = new ArrayList<>();
    private ArrayList<String> arrayVars = new ArrayList<>();
    private Assembler assembler;

    public Translator(String pascalCode, Assembler assembler) {
        this.pascalCode = pascalCode;
        this.assembler = assembler;
    }

    public void startTranslate() {
        splitIntoBlocks();
        selectVarLines();
        selectVars();
        selectCodeLines();
        translateCode();
    }

    private void splitIntoBlocks() {
        Pattern patternOfVar = Pattern.compile("(?<=var)[\\s\\S]*(?=begin)");
        Matcher matcherOfVar = patternOfVar.matcher(pascalCode);
        while (matcherOfVar.find()) {
            vars = pascalCode.substring(matcherOfVar.start(), matcherOfVar.end());
        }

        Pattern patternOfCode = Pattern.compile("(?<=begin)[\\s\\S]*(?=end.)");
        Matcher matcherOfCode = patternOfCode.matcher(pascalCode);
        while (matcherOfCode.find()) {
            code = pascalCode.substring(matcherOfCode.start(), matcherOfCode.end());
        }
    }

    private void selectVarLines() {
        Pattern patternOfVar = Pattern.compile("([a-zA-Z]([a-zA-Z0-9_]*[\\s]*,[\\s]*)*)([a-zA-Z][a-zA-Z0-9_]*)[\\s]*:[\\s]*integer[\\s]*;");
        Matcher matcherOfVar = patternOfVar.matcher(vars);

        while (matcherOfVar.find()) {
            varLines.add(vars.substring(matcherOfVar.start(), matcherOfVar.end()));
        }
    }

    private void selectVars() {
        Pattern patternOfVar = Pattern.compile("[a-zA-Z][a-zA-Z0-9_]*");
        Matcher matcherOfVar;
        String var;
        for (String line : varLines) {
            matcherOfVar = patternOfVar.matcher(line);
            while (matcherOfVar.find()) {
                var = line.substring(matcherOfVar.start(), matcherOfVar.end());
                if (!var.equals("integer")) {
                    arrayVars.add(var);
                }
            }
        }
        assembler.setArrayVar(arrayVars);
    }

    private void selectCodeLines() {
        Pattern patternOfCode = Pattern.compile("[a-zA-Z].*?;");
        Matcher matcherOfCode = patternOfCode.matcher(code);
        while (matcherOfCode.find()) {
            codeLines.add(code.substring(matcherOfCode.start(), matcherOfCode.end()));
        }
    }

    private void translateCode() {
        Pattern patternWrite = Pattern.compile("write\\('(.*)'\\);");
        Pattern patternWriteLn = Pattern.compile("writeln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");
        Pattern patternReadLn = Pattern.compile("readln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");
        Pattern patternOperation = Pattern.compile("([a-z][a-zA-Z0-9]*)[\\s]*[\\s]*:=[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*([+\\-*/])[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*;");
        for (String line : codeLines) {
            if (line.matches(patternWrite.toString())) {
                Matcher matcherForCode = patternWrite.matcher(line);
                if (matcherForCode.find()) {
                    assembler.addWrite(matcherForCode.group(1));
                }
            } else if (line.matches(patternWriteLn.toString())) {
                Matcher matcherForCode = patternWriteLn.matcher(line);
                if (matcherForCode.find()) {
                    assembler.addWriteLn(matcherForCode.group(1));
                }
            } else if (line.matches(patternReadLn.toString())) {
                Matcher matcherForCode = patternReadLn.matcher(line);
                if (matcherForCode.find()) {
                    assembler.addReadLn(matcherForCode.group(1));
                }
            } else if (line.matches(patternOperation.toString())) {
                Matcher matcherForCode = patternOperation.matcher(line);
                if (matcherForCode.find()) {
                    assembler.addOperation(matcherForCode.group(1), matcherForCode.group(2), matcherForCode.group(3), matcherForCode.group(4));
                }
            }
        }
    }
}

package com.company;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class Assembler {
    private StringBuilder[] assemblerCode;
    private String filename;

    ArrayList<String> arrayVar = new ArrayList<>();
    HashMap<String, String> hashMapVar = new HashMap<>();

    public Assembler(String filename) {
        this.filename = filename;
        initialize();
    }

    public void setArrayVar(ArrayList<String> arrayVar) {
        this.arrayVar = arrayVar;
    }

    private void initialize() {
        assemblerCode = new StringBuilder[4];

        for (int i = 0; i < 4; i++) {
            assemblerCode[i] = new StringBuilder();
        }

        //start program
        assemblerCode[0].append("format PE console\n" +
                "\n" + "entry start\n" +
                "\n" + "include 'win32a.inc'\n" +
                "\n" + "section '.data' data readable writable\n");

        //variables program
        assemblerCode[1].append("\tstrSpace db '%d', 0\n" +
                "\tstrEnter db '%d', 0ah, 0\n");

        //code program
        assemblerCode[2].append("section '.code' code readable executable\n" +
                "\n" + "\t start:\n");

        //end program
        assemblerCode[3].append("\t finish:\n" +
                "\n" + "\t\t call [getch]\n" +
                "\n" + "\t\t call [ExitProcess]\n" +
                "\n" + "section '.idata' import data readable\n" +
                "\n" + "\t library kernel, 'kernel32.dll',\\\n" +
                "\t msvcrt, 'msvcrt.dll'\n" +
                "\n" + "\t import kernel,\\\n" +
                "\t ExitProcess, 'ExitProcess'\n" +
                "\n" + "\t import msvcrt,\\\n" +
                "\t printf, 'printf',\\\n" +
                "\t scanf, 'scanf',\\\n" +
                "\t getch, '_getch'");
    }

    public void createFile() {
        addVar();
        try {
            FileWriter fileWriter = new FileWriter(filename + ".asm");
            for (int i = 0; i < assemblerCode.length; i++) {
                fileWriter.write(assemblerCode[i].toString());
            }
            fileWriter.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private void addVar() {
        for (String string : arrayVar) {
            assemblerCode[1].append("\t" + string + " dd ?\n");
        }

        for (String key : hashMapVar.keySet()) {
            assemblerCode[1].append("\t" + key + " db '" + hashMapVar.get(key) + "', 0\n");
        }
    }

    public void addWrite(String string) {
        hashMapVar.put("str" + (hashMapVar.size() + 1), string);
        assemblerCode[2].append("\t\t push " + "str" + hashMapVar.size() + "\n" +
                "\t\t call [printf]\n");
    }

    public void addReadLn(String string) {
        if (arrayVar.contains(string)) {
            assemblerCode[2].append("\t\t push " + string + "\n" +
                    "\t\t push strSpace\n" +
                    "\t\t call [scanf]\n\n");
        }
    }

    public void addWriteLn(String string) {
        if (arrayVar.contains(string)) {
            assemblerCode[2].append("\t\t push [" + string + "]\n" +
                    "\t\t push strEnter\n" +
                    "\t\t call [printf]\n\n");
        }
    }

    public void addOperation(String var, String number1, String operation, String number2) {
        if (arrayVar.contains(var) && arrayVar.contains(number1) && arrayVar.contains(number2)) {
            switch (operation) {
                case "+":
                    assemblerCode[2].append("\t\t mov ecx, [" + number1 + "]\n" +
                            "\t\t add ecx, [" + number2 + "]\n" +
                            "\t\t mov [" + var + "], ecx\n");
                    break;
                case "-":
                    assemblerCode[2].append("\t\t mov ecx, [" + number1 + "]\n" +
                            "\t\t sub ecx, [" + number2 + "]\n" +
                            "\t\t mov [" + var + "], ecx\n");
                    break;
                case "*":
                    assemblerCode[2].append("\t\t mov ecx, [" + number1 + "]\n" +
                            "\t\t imul ecx, [" + number2 + "]\n" +
                            "\t\t mov [" + var + "], ecx\n");
                    break;
                case "/":
                    assemblerCode[2].append("\t\t mov eax, [" + number1 + "]\n" +
                            "\t\t mov ecx, [" + number2 + "]\n" +
                            "\t\t div ecx\n" +
                            "\t\t mov [" + var + "], eax\n");
                    break;
            }
        }
    }
}

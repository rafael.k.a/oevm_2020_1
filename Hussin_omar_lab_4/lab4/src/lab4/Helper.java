package lab4;
import java.util.Random;
import java.util.Scanner;

public class Helper {
	 public static void main(String[] args) {
	        Scanner scanner = new Scanner(System.in);
	        int[][] array = Helper.generateArray();
	        System.out.println("to get DNF number 1 if  KNF number 2");
	        String Result = scanner.next();
	        
	            if (Result.equals("1")) {
	                Helper.knf(array);
	                for (int[] number : array)
	                {
	                    System.out.printf("%d %d %d %d = %d%n", number[0],number[1], number[2], number[3], number[4]);
	                   
	                }
	               
	            }
	           
	            else   if (Result.equals("2")) {
	               Helper.dnf(array);
	                for (int[] number : array) {
	                    System.out.printf("%d %d %d %d = %d%n", number [0], number[1], number[2], number[3], number [4]);
	                }
	                
	                
	            }
	      
	        }
	        
    public static int[][] generateArray() {
        Random random = new Random();
        int[][] array =
        	{
                {0, 0, 0, 0, random.nextInt(2)},
                {0, 0, 0, 1, random.nextInt(2)},
                {0, 0, 1, 0, random.nextInt(2)},
                {0, 0, 1, 1, random.nextInt(2)},
                {0, 1, 0, 0, random.nextInt(2)},
                {0, 1, 0, 1, random.nextInt(2)},
                {1, 1, 1, 0, random.nextInt(2)},
                {0, 1, 1, 1, random.nextInt(2)},
                {1, 0, 0, 0, random.nextInt(2)},
                {1, 0, 0, 1, random.nextInt(2)},
                {1, 0, 1, 0, random.nextInt(2)},
                {1, 0, 1, 1, random.nextInt(2)},
                {1, 1, 0, 0, random.nextInt(2)},
                {1, 1, 0, 1, random.nextInt(2)},
                {1, 1, 1, 0, random.nextInt(2)},
                {1, 1, 1, 1, random.nextInt(2)},
        };
        return array;
    }

    public static void knf(int[][] randomArray) {
        String Result = new String();
        for (int[] T : randomArray) {
            if (T[4] == 0) {
                if (Result.isEmpty()) {
                    Result += ((T[0] == 0) ? "(x1 + " : "(!x1 + ");
                } else {
                    Result += ((T[0] == 0) ? " * (!x1 + " : " * (!x1 + ");
                }
                Result += ((T[1] == 0) ? "x2 + " : " !x2 + ");
                Result += ((T[2] == 0) ? "x3 + " : " !x3 + ");
                Result += ((T[3] == 0) ? "x4)" : " !x4)");
            }
        }
        System.out.println(Result);
}
    public static void dnf(int[][] randomArray) {
        String Result = new String();
        for (int[] T : randomArray) {
            if (T[4] == 1) {
                if (Result.isEmpty()) {
                    Result += ((T[0] == 0) ? "(!x1 * " : "(x1 * ");
                } else {
                    Result += ((T[0] == 0) ? " + (!x1 * " : " + (x1 * ");
                }
                Result += ((T[1] == 0) ? "!x2 * " : " x2 * ");
                Result += ((T[2] == 0) ? "!x3 * " : " x3 * ");
                Result += ((T[3] == 0) ? "!x4)" : " x4)");
            }
        }
        System.out.println(Result);
    }
}
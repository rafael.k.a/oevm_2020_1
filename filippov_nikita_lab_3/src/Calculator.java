import java.util.ArrayList;
import java.util.HashMap;

public class Calculator {
    private Addition addition;
    private Subtraction subtraction;
    private Multiplication multiplication;
    private Division division;
    private Compare compare;
    private Translator tranlator = new Translator();
    private Operations operations;

    private HashMap<Integer, String> descriptionMethods;
    private ArrayList<Integer> aBinary = new ArrayList();
    private ArrayList<Integer> bBinary = new ArrayList<>();
    private ArrayList<Integer> result = new ArrayList();
    private StringBuilder A = new StringBuilder("");
    private StringBuilder B = new StringBuilder("");

    private boolean orientResult = true;
    private String choice;
    private int scaleOfNotation = -1;

    /**
     * Конструктор, инициализирующий все вспомогательные классы и описание функций калькулятора
     */
    public Calculator() {
        addition = new Addition();
        subtraction = new Subtraction();
        multiplication = new Multiplication();
        division = new Division();
        compare = new Compare();
        descriptionMethods = new HashMap<>();
        initDesc();
    }

    /**
     * Сеттер выбора арф.операции
     */
    public void setChoice(String choice) {
        this.choice = choice;
    }

    /**
     * Метод, принимающий А и В ввиде строк, и с.с
     */
    public void setArguments(String first, String second, int ScaleOfNotation) {
        this.A.append(first);
        this.B.append(second);
        if (ScaleOfNotation <= 0) {
            System.out.printf("Ошибка введения с.с");
            return;
        }
        this.scaleOfNotation = ScaleOfNotation;
        convertToArrayList();
    }

    /**
     * Приватный метод инициализации хэш-таблицы, хранящей описание функций калькулятора
     */
    private void initDesc() {
        descriptionMethods.put(1, "+ - Сложение A и B");
        descriptionMethods.put(2, "- - Разность A и B");
        descriptionMethods.put(3, "* - Произведение A и B");
        descriptionMethods.put(4, "/ - Частное A и B");
    }

    /**
     * Метод вывода в консоль всех функций калькулятора
     */
    public void list() {
        for (int i = 1; i <= descriptionMethods.size(); i++) {
            System.out.println(descriptionMethods.get(i));
        }
        System.out.print("\n");
    }

    /**
     * Метод запуска арифметических действий
     */
    public void toCalculate() {
        this.result = new ArrayList<>();
        if (!checkInvalidInput()) {
            getBinaryAandB();
            switch (operations.getOperations(choice)) {
                case Addition: {
                    this.result = addition.startArithmeticOperation(aBinary, bBinary);
                    break;
                }
                case Subtraction: {
                    if (compare.equals(aBinary, bBinary, 0) == -1) {
                        this.orientResult = false;
                        ArrayList<Integer> tmp = aBinary;
                        aBinary = bBinary;
                        bBinary = tmp;
                    } else {
                        this.orientResult = true;
                    }
                    this.result = subtraction.startArithmeticOperation(aBinary, bBinary);
                    break;
                }
                case Multiplication: {
                    this.result = multiplication.startArithmeticOperation(aBinary, bBinary);
                    break;
                }
                case Division: {
                    this.result = division.startArithmeticOperation(aBinary, bBinary);
                    break;
                }
                default:
                    break;
            }
            getResult();
        }
    }

    /**
     * Приватный метод проверки всех переменных на неверные значения
     */
    private boolean checkInvalidInput() {
        return (aBinary == null || bBinary == null || scaleOfNotation == -1);
    }

    /**
     * * Метод конвертации строкового ввода в ArrayList. 2ий код записан задом-наперед
     */
    private void convertToArrayList() {
        String A_BinaryStr = tranlator.translate(scaleOfNotation, 2, A.toString());
        String B_BinaryStr = tranlator.translate(scaleOfNotation, 2, B.toString());
        for (int i = A_BinaryStr.length() - 1; i >= 0; i--) {
            if (A_BinaryStr.charAt(i) == '0' || A_BinaryStr.charAt(i) == '1') {
                this.aBinary.add(Character.getNumericValue(A_BinaryStr.charAt(i)));
            } else {
                aBinary = null;
                return;
            }
        }
        for (int i = B_BinaryStr.length() - 1; i >= 0; i--) {
            if (B_BinaryStr.charAt(i) == '0' || B_BinaryStr.charAt(i) == '1') {
                this.bBinary.add(Character.getNumericValue(B_BinaryStr.charAt(i)));
            } else {
                bBinary = null;
                return;
            }
        }
    }

    /**
     * Метод вывода в консоль А и В в 2с.с
     */
    private void getBinaryAandB() {
        System.out.println("A в 2 с.с");
        for (int i = aBinary.size() - 1; i >= 0; i--) {
            System.out.print(aBinary.get(i));
        }
        System.out.printf("\n");
        System.out.println("B в 2 с.с");
        for (int i = bBinary.size() - 1; i >= 0; i--) {
            System.out.print(bBinary.get(i));
        }
        System.out.printf("\n");
    }

    /**
     * Метод получения результата арифметической операции
     */
    public void getResult() {
        if (this.result.size() != 0) {
            System.out.println("Результат:");
            for (int i = this.result.size() - 1; ((i >= 0) && (this.result.get(i) == 0)); i--) {
                this.result.remove(i);
            }
            if (this.result.size() == 0) {
                result.add(0);
            }
            if (!orientResult) {
                System.out.printf("-");
                this.orientResult = true;
            }
            for (int i = this.result.size() - 1; i >= 0; i--) {
                System.out.print(this.result.get(i));
            }
            System.out.printf("\n");
        }
    }
}

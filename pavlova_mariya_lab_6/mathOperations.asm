format PE console

entry Start

include 'win32a.inc'

section '.data' data readable writeable

        strFirstNum db 'X = ', 0
        strSecondNum db 'Y = ', 0

        srtResAdd db 'X + Y = %d', 0dh, 0ah, 0
        srtResSub db 'X - Y = %d', 0dh, 0ah, 0
        srtResMul db 'X * Y = %d', 0dh, 0ah, 0
        srtResDiv db 'X / Y = %d', 0dh, 0ah, 0
        strSpace db ' %d', 0

        firstNum dd ?
        secondNum dd ?

section '.idata' import data readable
        library kernel, 'kernel32.dll',\
                msvctr, 'msvcrt.dll'

        import kernel,\
               ExitProcess, 'ExitProcess'

        import msvctr,\
               printf, 'printf',\
               getch, '_getch',\
               scanf, 'scanf'

section '.code' code readable executable
        Start:
                push strFirstNum
                call [printf]

                push firstNum
                push strSpace
                call [scanf]

                push strSecondNum
                call [printf]

                push secondNum
                push strSpace
                call [scanf]

                ;��������
                mov ecx, [firstNum]
                add ecx, [secondNum]

                push ecx
                push srtResAdd
                call [printf]

                ;���������
                mov ecx, [firstNum]
                sub ecx, [secondNum]

                push ecx
                push srtResSub
                call [printf]

                ;���������
                mov ecx, [firstNum]
                imul ecx, [secondNum]

                push ecx
                push srtResMul
                call [printf]

                ;�������
                mov eax, [firstNum]
                cdq
                mov ecx, [secondNum]
                div ecx

                push eax
                push srtResDiv
                call [printf]

                call [getch]

                call [ExitProcess]
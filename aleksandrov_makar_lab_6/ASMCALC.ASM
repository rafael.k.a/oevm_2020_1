format PE console

entry start

include 'win32a.inc'

section '.data' data readable writable

        enterX db 'X = ', 0
        enterY db 'Y = ', 0
        spaceStr db ' %d', 0

        addStr db 'X + Y = %d', 0dh, 0ah, 0
        subStr db 'X - Y = %d', 0dh, 0ah, 0
        mulStr db 'X * Y = %d', 0dh, 0ah, 0
        divStr db 'X / Y = %d', 0

        X dd ?
        Y dd ?

        NULL = 0

section '.code' code readable executable

        start:
                ;���� � �������
                push enterX
                call [printf]

                push X
                push spaceStr
                call [scanf]

                push enterY
                call [printf]

                push Y
                push spaceStr
                call [scanf]

                ;��������
                    mov ecx, [X]
                    add ecx, [Y]

                    push ecx
                    push addStr
                    call [printf]

                ;���������
                    mov ecx, [X]
                    sub ecx, [Y]

                    push ecx
                    push subStr
                    call [printf]

                ;���������
                    mov ecx, [X]
                    imul ecx, [Y]

                    push ecx
                    push mulStr
                    call [printf]

                ;�������
                    mov eax, [X]
                    cdq
                    mov ecx, [Y]
                    idiv ecx

                    push eax
                    push divStr
                    call [printf]

                call [getch];���������� ����� ��� ������� �� ����� �������

                push NULL
                call [ExitProcess]

section '.idata' import data readable

        library kernel, 'kernel32.dll',\
                msvcrt, 'msvcrt.dll'

        import kernel,\
               ExitProcess, 'ExitProcess'

        import msvcrt,\
               printf, 'printf',\
               scanf, 'scanf',\
               getch, '_getch'
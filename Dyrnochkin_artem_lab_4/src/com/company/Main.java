package com.company;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException {

        Table table = new Table();
        table.fillTable();
        table.printTable();

        System.out.println();
        System.out.println("выберите номер один для ДНФ таблицы | выберите номер два для КНФ таблицы");

        BufferedReader BufReader = new BufferedReader(new InputStreamReader(System.in));
        String normalForm = BufReader.readLine();
        NormalForm Nform = new NormalForm();
        switch (normalForm) {
            case "1":
                Nform.convertDisjunctiveForm(table.getTable());
                break;
            case "2":
                Nform.convertConjunctiveForm(table.getTable());
                break;
        }

    }
}

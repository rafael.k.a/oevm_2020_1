package com;

public class Helper
{
    private String result="";
    final int _sizeCol;
    final int _sizeStr;
    private int X1=0;
    private int X2=1;
    private int X3=2;
    private int X4=3;
    private int [][] _table;

    public Helper(int numberForm,int sizeCol, int sizeStr, int[][] table) {
        _sizeCol = sizeCol;
        _sizeStr = sizeStr;
        _table = table;
        printTable();

        switch (numberForm)
        {
            case 1:
                DNF();
                break;
            case 2:
                KNF();
                break;
        }
    }

    private String DNF()
    {
        int count = 0;
        for (int i = 0; i < _sizeStr; i++) {
            if (_table[i][_sizeCol - 1] == 1){
                if(count > 0){
                    result = result + " + ";
                }
                if(_table[i][X1] == 0){
                    result = result + "( - X1 ";
                }
                else {
                    result = result + "( X1 ";
                }
                if(_table[i][X2] == 0){
                    result = result + " * -X2 ";
                }
                else {
                    result = result + " * X2 ";
                }
                if(_table[i][X3] == 0){
                    result = result + " * -X3 ";
                }
                else {
                    result = result + " * X3 ";
                }
                if(_table[i][X4] == 0){
                    result = result + "* -X4 )";
                }
                else {
                    result = result + "* X4 )";
                }
                count++;
            }
        }
        System.out.print(result);
        return result;
    }

    private String KNF()
    {
        int count = 0;
        for (int i = 0; i < _sizeStr; i++) {
            if (_table[i][_sizeCol - 1] == 0){
                if(count > 0){
                    result = result + " * ";
                }
                if(_table[i][X1] == 1){
                    result = result + "( - X1 ";
                }
                else {
                    result = result + " ( X1 ";
                }
                if(_table[i][X2] == 1){
                    result = result + " - X2 ";
                }
                else {
                    result = result + " + X2 ";
                }
                if(_table[i][X3] == 1){
                    result = result + " - X3 ";
                }
                else {
                    result = result + " + X3 ";
                }
                if(_table[i][X4] == 1){
                    result = result + " - X4 )";
                }
                else {
                    result = result + " + X4 )";
                }
                count++;
            }
        }
        System.out.print(result);
        return result;
    }

    private void printTable()
    {
        for (int i=0;i<_sizeStr;i++)
        {
            for (int j = 0; j < _sizeCol; j++)
            {
            System.out.print(_table[i][j]+" ");
            }
            System.out.print("\n");
        }
    }
}

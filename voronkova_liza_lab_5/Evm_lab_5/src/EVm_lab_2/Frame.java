package EVm_lab_2;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class Frame {
    private Random random = new Random();
    JFrame frame;
    Panel myPanel;

    private JTextArea reducedFunction = new JTextArea();

    private int stringSize = 16;
    private int columnSize = 5;

    private int[][] table = {
            {0, 0, 0, 0, random.nextInt(2)},
            {0, 0, 0, 1, random.nextInt(2)},
            {0, 0, 1, 0, random.nextInt(2)},
            {0, 0, 1, 1, random.nextInt(2)},
            {0, 1, 0, 0, random.nextInt(2)},
            {0, 1, 0, 1, random.nextInt(2)},
            {0, 1, 1, 0, random.nextInt(2)},
            {0, 1, 1, 1, random.nextInt(2)},
            {1, 0, 0, 0, random.nextInt(2)},
            {1, 0, 0, 1, random.nextInt(2)},
            {1, 0, 1, 0, random.nextInt(2)},
            {1, 0, 1, 1, random.nextInt(2)},
            {1, 1, 0, 0, random.nextInt(2)},
            {1, 1, 0, 1, random.nextInt(2)},
            {1, 1, 1, 0, random.nextInt(2)},
            {1, 1, 1, 1, random.nextInt(2)}
    };

    Frame(){
        frame = new JFrame();
        frame.setBounds(100, 100, 700, 540);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setLayout(null);

        myPanel = new Panel();
        myPanel.setArray(table);
        myPanel.setBounds(0, 0, 180, 500);
        frame.getContentPane().add(myPanel);

        JButton buttonGenerate = new JButton("Сгенерировать");
        buttonGenerate.addActionListener(e -> madeNewArray());
        buttonGenerate.setBounds(180, 10, 150, 30);
        frame.getContentPane().add(buttonGenerate);

        JButton buttonDNF = new JButton("ДНФ");
        buttonDNF.addActionListener(e -> drawDNF());
        buttonDNF.setBounds(180, 50, 150, 30);
        frame.getContentPane().add(buttonDNF);

        JButton buttonKNF = new JButton("КНФ");
        buttonKNF.addActionListener(e -> drawKNF());
        buttonKNF.setBounds(180, 90, 150, 30);
        frame.getContentPane().add(buttonKNF);


        reducedFunction.setBounds(450, 10, 200, 480);
        frame.getContentPane().add(reducedFunction);
    }

    private void madeNewArray(){
        table = new int[][] {
                {0, 0, 0, 0, random.nextInt(2)},
                {0, 0, 0, 1, random.nextInt(2)},
                {0, 0, 1, 0, random.nextInt(2)},
                {0, 0, 1, 1, random.nextInt(2)},
                {0, 1, 0, 0, random.nextInt(2)},
                {0, 1, 0, 1, random.nextInt(2)},
                {0, 1, 1, 0, random.nextInt(2)},
                {0, 1, 1, 1, random.nextInt(2)},
                {1, 0, 0, 0, random.nextInt(2)},
                {1, 0, 0, 1, random.nextInt(2)},
                {1, 0, 1, 0, random.nextInt(2)},
                {1, 0, 1, 1, random.nextInt(2)},
                {1, 1, 0, 0, random.nextInt(2)},
                {1, 1, 0, 1, random.nextInt(2)},
                {1, 1, 1, 0, random.nextInt(2)},
                {1, 1, 1, 1, random.nextInt(2)}
        };
        myPanel.setArray(table);
        frame.repaint();
    }
    private void drawKNF() {
        myPanel.setKNF();
        myPanel.repaint();
        reducedFunction.setText(Function.KNF(table, stringSize));
    }

    private void drawDNF() {
        myPanel.setDNF();
        myPanel.repaint();
        reducedFunction.setText(Function.DNF(table, stringSize));
    }
}
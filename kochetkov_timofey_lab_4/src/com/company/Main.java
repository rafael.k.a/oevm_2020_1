package com.company;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int ROWS = 16;
        int COLUMNS = 5;

        System.out.println("Input number, please: 1 or 0");
        System.out.println("0 - DNF, 1 - KNF");

        Scanner scanner = new Scanner(System.in);
        int variationForm = scanner.nextInt();
        int[][] table = new int[ROWS][COLUMNS];

        OperationClass operationClass = new OperationClass(table, variationForm);

        operationClass.filling();
        operationClass.output();
        operationClass.runForm();
    }
}

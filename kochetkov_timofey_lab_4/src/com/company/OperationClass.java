package com.company;

public class OperationClass {
    private boolean variationForm;
    private int[][] table;
    private int widthTable;
    private int heightTable;
    private char symbolForm;

    public OperationClass(int[][] table, int variationForm) {
        if (variationForm == 0) {
            this.variationForm = false;
            symbolForm = '+';
        } else {
            this.variationForm = true;
            symbolForm = '*';
        }
        this.table = table;
        heightTable = table.length;
        widthTable = table[0].length - 1; // не трогаем последний столбец
    }

    public void filling() {

        for (int i = 0; i < heightTable; i++) {
            for (int j = 0; j < widthTable; j++) {
                table[i][j] = (int) (Math.random() * 2);
            }
        }
    }


    public void output() {

        for (int i = 0; i < heightTable; i++) {
            for (int j = 0; j < widthTable; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }
    }

    public void runForm() {
        if (variationForm) {
            knfForm();
        } else {
            dnfForm();
        }
        widthTable++;
        finalOutput();
    }

    private void knfForm() {
        boolean step;
        for (int i = 0; i < heightTable; i++) {
            step = true;
            for (int j = 0; j < widthTable; j++) {
                if (table[i][j] == 0) {
                    step = false;
                }
            }
            if(step) {
                table[i][widthTable] = 1;
            }
            else {
                table[i][widthTable] = 0;
            }
        }
    }

    private void dnfForm() {
        boolean step;
        for (int i = 0; i < heightTable; i++) {
            step = false;
            for (int j = 0; j < widthTable; j++) {
                if (table[i][j] == 1) {
                    step = true;
                }
            }
            if(step) {
                table[i][widthTable] = 1;
            }
            else {
                table[i][widthTable] = 0;
            }
        }
    }

    private void finalOutput() {
        System.out.println();
        System.out.println("Your result:");
        char stepHelpChar = symbolForm;
        for (int i = 0; i < heightTable; i++) {
            stepHelpChar = symbolForm;
            for (int j = 0; j < widthTable; j++) {
                if(j == widthTable - 2) {
                    stepHelpChar = '=';
                }
                else if(j == widthTable - 1) {
                    stepHelpChar = ' ';
                }
                System.out.print(table[i][j] + " ");
                System.out.print(stepHelpChar + " ");
            }
            System.out.println();
        }
    }
}

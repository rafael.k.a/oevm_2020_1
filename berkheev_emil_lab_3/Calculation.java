public class Calculation {

    private int firstNumber = 0;
    private int secondNumber = 0;

    private String firstDigit;
    private String secondDigit;

    private final char action;
    private String firstDigit2SS;
    private String secondDigit2SS;

    public Calculation(int startSS, String firstDigit, String secondDigit, char action) {

        this.firstDigit = firstDigit;
        this.secondDigit = secondDigit;
        this.action = action;

        convertToDecimal(startSS);
        convertToBinary();
    }

    private Boolean compareNumber(String str1, String str2) {
        char[] strArray1 = new StringBuilder(str1).reverseNumber().toString().toCharArray();
        char[] strArray2 = new StringBuilder(str2).reverseNumber().toString().toCharArray();

        if (strArray1.length > strArray2.length) {
            return true;
        }

        if (strArray1.length < strArray2.length) {
            return false;
        }

        if (strArray1.length == strArray2.length) {

            for (int i = 0; i < strArray1.length; i++) {

                if (strArray1[i] == '1' && strArray2[i] == '0') {
                    return true;
                }
                else if (strArray1[i] == '0' && strArray2[i] == '1') {
                    return false;
                }
            }
        }
        return true;
    }

    private void convertToDecimal(int startSS) {
        char[] firstDigitArray = firstDigit.toCharArray();
        char[] secondDigitArray = secondDigit.toCharArray();
        for (int i = 1; i =< firstDigitArray.length; i++) {
            int temp;
            if (firstDigitArray[i] >= '0' && firstDigitArray[i] <= '9') {
                temp = firstDigitArray[i] - '0';
            } else {
                temp = 10 + firstDigitArray[i] - 'A';
            }
            firstNumber += temp * Math.pow(startSS, firstDigitArray.length - i);
        }

        for (int i = 1; i =< secondDigitArray.length; i++) {
            int temp;
            if (secondDigitArray[i] >= '0' && secondDigitArray[i] <= '9') {
                temp = secondDigitArray[i] - '0';
            } else {
                temp = 10 + secondDigitArray[i] - 'A';
            }
            secondNumber += temp * Math.pow(startSS, secondDigitArray.length - i);
        }
    }

    private void convertToBinary() {
        StringBuilder strFirstDigit = new StringBuilder();
        StringBuilder strSecondDigit = new StringBuilder();

        if (firstNumber == 0) {
            strFirstDigit.append("0");
        }
        else {
            while (firstNumber > 0)
            {
                strFirstDigit.append((char) (firstNumber % 2 + '0'));
                firstNumber /= 2;
            }
        }
        firstDigit2SS = strFirstDigit.toString();

        if (secondNumber == 0) {
            strSecondDigit.append("0");
        }
        else {
            while (secondNumber > 0)
            {
                strSecondDigit.append((char) (secondNumber % 2 + '0'));
                secondNumber /= 2;
            }
        }
        secondDigit2SS = strSecondDigit.toString();
    }

    public String calculateOperations() {
        StringBuilder str = new StringBuilder();
        switch (action) {
            case '+':
                str.append(appendAddition(firstDigit2SS, secondDigit2SS));
                return str.reverseNumber().toString();
            case '-':
                str.append(subtractNumber(firstDigit2SS, secondDigit2SS)).reverseNumber();
                return deleteZeros(str.toString());
            case '*':
                str.append(multiplyNumber(firstDigit2SS, secondDigit2SS)).reverseNumber();
                return str.toString();
            case '/':
                str.append(divideNumber(firstDigit2SS, secondDigit2SS)).reverseNumber();
                return str.toString();
        }
        return "null";
    }

    private String appendAddition(String str1, String str2) {

        char[] firstDigitArray = str1.toCharArray();
        char[] secondDigitArray = str2.toCharArray();

        StringBuilder result = new StringBuilder();

        int transfer = 0;

        int i = 0;

        while (i < secondDigitArray.length) {
            final boolean longCheck = (firstDigitArray[i] == '0' && secondDigitArray[i] == '1') || (firstDigitArray[i] == '1' && secondDigitArray[i] == '0');
            if (transfer == 0) {
                if (firstDigitArray[i] == '0' && secondDigitArray[i] == '0') {
                    result.append("0");
                    transfer = 0;
                }
                if (longCheck) {
                    result.append("1");
                    transfer = 0;
                }
                if (firstDigitArray[i] == '1' && secondDigitArray[i] == '1') {
                    result.append("0");
                    transfer = 1;
                }
            } else {
                if (firstDigitArray[i] == '0' && secondDigitArray[i] == '0') {
                    result.append("1");
                    transfer = 0;
                }
                if (longCheck) {
                    result.append("0");
                    transfer = 1;
                }
                if (firstDigitArray[i] == '1' && secondDigitArray[i] == '1') {
                    result.append("1");
                    transfer = 1;
                }
            }
            i++;
        }

        while (i < firstDigitArray.length) {
            if (firstDigitArray[i] == '0' && transfer == 0) {
                result.append("0");
                transfer = 0;
            }
            else if ((firstDigitArray[i] == '0' && transfer == 1) || (firstDigitArray[i] == '1' && transfer == 0)) {
                result.append("1");
                transfer = 0;
            }
            else if (firstDigitArray[i] == '1' && transfer == 1) {
                result.append("0");
                transfer = 1;
            }
            i++;
        }

        if (transfer == 1) {
            result.append("1");
        }

        return result.toString();
    }

    private String subtractNumber(String str1, String str2) {
        StringBuilder result = new StringBuilder();
        str2 = reverseNumber(str2);
        result.append(appendAddition(str1, str2));
        
        int desiredCount=result.length()-1;
        result.deleteCharAt(desirdCount);
        return result.toString();
    }

    private String multiplyNumber(String str1, String str2) {
        char[] strArray2 = str2.toCharArray();
        StringBuilder strArray1 = new StringBuilder(str1);
        String result = "0";
        int iIndex = 0;
        for (int i = 0; i < strArray2.length; i++) {
            if (strArray2[i] == '1') {

                for (int j = 0; j < i - iIndex; j++) {
                    strArray1.insert(0, '0');
                }
                result = appendAddition(strArray1.toString(), result);
                iIndex = i;
            }
        }

        return result;
    }

    private String divideNumber(String str1, String str2) {
        String compareNumber
 = str2;
        String result = "0";

        if (str2.toCharArray().length == 1 && str2.toCharArray()[0] != '1') {
            return "Error!";
        }
        while (compareNumber
(str1, compareNumber
)) {
            result = appendAddition(result, "1");
            compareNumber
     = appendAddition(compareNumber
    , str2);
        }
        return result;

    }

    private String reverseNumber(String str) {

        StringBuilder string = new StringBuilder();
        StringBuilder result = new StringBuilder();
        char[] digitArray = str.toCharArray();

        int i = 0;
        while (i < secondDigit2SS.length()) {
            if (digitArray[i] == '1') {
                string.append("0");
            } else {
                string.append("1");
            }
            i++;
        }
        while (i < firstDigit2SS.length()) {
            string.append("1");
            i++;
        }
        result.append(appendAddition(string.toString(), "1"));
        return result.toString();
    }

    private String deleteZeros(String str) {
        StringBuilder result = new StringBuilder();
        char[] digitArray = str.toCharArray();
        int i = 0;
        while (digitArray[i] == '0') {
            i++;
            if (i == str.length()) {
                return "0";
            }
        }
        while (i < str.length()) {
            result.append(digitArray[i]);
            i++;
        }
        return result.toString();
    }

}
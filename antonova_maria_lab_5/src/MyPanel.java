package com.company;
import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel {

    private int array [][];
    Methods methods = new Methods();
    TruthTable table;

    public  MyPanel(TruthTable table){
        this.array = table._truthTable;
        this.table = table;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        methods.draw(Main.func, Main.step, array, g, table);
    }
}

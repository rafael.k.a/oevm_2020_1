public class Methods {
    public void print(int[][] array, int lengthI, int lengthJ) {
        for (int i = 0; i < lengthI; i++) {
            for (int j = 0; j < lengthJ; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.print("\n");
        }
    }

    public String disNF(int[][] array, int lengthI, int lengthJ) {
        int X4 = 3;
        int k = 4;
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < lengthI; i++) {
            if (array[i][k] == 1) {
                builder.append("( ");
                for (int j = 0; j < X4; j++) {
                    if (array[i][j] == 1) {
                        builder.append("X").append(j + 1).append(" * ");
                    } else {
                        builder.append("-X").append(j + 1).append(" * ");
                    }
                }
                if (array[i][X4] == 1) {
                    builder.append("X4");
                } else {
                    builder.append("-X4");
                }
                builder.append(" ) + ");
            }
        }

        builder.delete(builder.toString().length() - 3, builder.toString().length() - 1);
        return builder.toString();
    }

    public String conNF(int[][] array, int lengthI, int lengthJ) {
        int X4 = 3;
        int k = 4;
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < lengthI; i++)
            if (array[i][k] == 0) {
                builder.append("( ");
                for (int j = 0; j < X4; j++) {
                    if (array[i][j] == 0) {
                        builder.append("X").append(j + 1).append(" + ");
                    } else {
                        builder.append("-X").append(j + 1).append(" + ");
                    }
                }
                if (array[i][X4] == 0) {
                    builder.append("X4");
                } else {
                    builder.append("-X4");
                }
                builder.append(" ) * ");
            }

        builder.delete(builder.toString().length() - 3, builder.toString().length() - 1);
        return builder.toString();
    }
}


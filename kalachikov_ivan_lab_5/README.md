# Лабораторная работа 5
# Изучение простейших логических элементов
***
* Лабораторная работа запускается через IDEA с конфигурацией **Main**
* Программа создает таблицу истинности (матрицу) и приводит ее к ДНФ и КНФ, визуализируя этот процесс  
* Пример работы: 

![Картинка 1](https://sun9-33.userapi.com/PNYDK49b2pkJLEjcnAhF0fd_EqA3OywADUMPbA/4j6nG5snHQU.jpg)
![Картинка 2](https://sun9-68.userapi.com/xpUXLw3XaDbKLnpNJRFAfKuR_Km1Nky8TkSlKg/1h9V2Umoy44.jpg)

[Сслыка на видео](https://drive.google.com/file/d/1yvoaHn4VyavKSSazv9vgWjtLMz7WMkOR/view?usp=sharing)
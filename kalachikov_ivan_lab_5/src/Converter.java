public class Converter {

    private static final int indexF = 4;
    private static int count = 0;

    public static void refreshCount() {
        count = 0;
    }

    private static String convertToDNF(int[][] truthTable) {
        StringBuilder result = new StringBuilder();
        if (truthTable[count][indexF] == 1) {
            result.append('(');
            for (int j = 0; j < truthTable[count].length - 1; j++) {
                if (truthTable[count][j] == 0) {
                    result.append("!");
                }
                result.append("X").append(j + 1);
                if (j != truthTable[count].length - 2) {
                    result.append(" * ");
                }
            }
            result.append(")");
            count++;
        }
        return result.toString();
    }

    private static String convertToKNF(int[][] truthTable) {
        StringBuilder result = new StringBuilder();
        if (truthTable[count][indexF] == 0) {
            result.append('(');
            for (int j = 0; j < truthTable[count].length - 1; j++) {
                if (truthTable[count][j] == 1) {
                    result.append("!");
                }
                result.append("X").append(j + 1);
                if (j != truthTable[count].length - 2) {
                    result.append(" + ");
                }
            }
            result.append(")");
            count++;
        }
        return result.toString();
    }

    public static String convertTable(int[][] truthTable) {
        String result;
        result = convertToDNF(truthTable);
        if (result.equals("")) {
            result = convertToKNF(truthTable);
        }
        return result;
    }
}

import javax.swing.*;

public class Frame {

    private JFrame frame;
    private final int frameWidth = 630;
    private final int frameHeight = 850;

    private JFrame frameInit() {
        JFrame frame = new JFrame("Converter");
        frame.setSize(frameWidth, frameHeight);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setLayout(null);
        return frame;
    }

    private void addElementsToFrame() {
        DrawPanel drawPanel = new DrawPanel(frame);
        JButton buttonRefresh = new JButton("Новая таблица");
        JButton buttonStartAnimation = new JButton("Минимизировать");

        frame.getContentPane().add(buttonRefresh);
        frame.getContentPane().add(buttonStartAnimation);
        frame.getContentPane().add(drawPanel);

        buttonRefresh.setBounds(30, 760, 200, 30);
        buttonStartAnimation.setBounds(260, 760, 340, 30);
        drawPanel.setBounds(0, 0, frameWidth, frameHeight);

        buttonRefresh.addActionListener(e -> {
            drawPanel.refreshTable();
            buttonStartAnimation.setEnabled(true);
            drawPanel.stopAnimation();
            frame.repaint();
        });

        buttonStartAnimation.addActionListener(e -> {
            drawPanel.startAnimation();
            buttonStartAnimation.setEnabled(false);
        });

        frame.repaint();
    }

    public void showFrame() {
        frame = frameInit();
        addElementsToFrame();
    }
}

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class DrawPanel extends JPanel {

    private TruthTable truthTable;
    private ArrayList<AnimatedString> animatedStrings;
    private final JFrame frame;
    private final Timer timerFill;
    private final Timer timerMoving;

    private final int cellSize = 40;
    private final int margin = 30;
    private final int rows = 17;
    private final int columns = 5;

    public DrawPanel(JFrame frame) {
        truthTable = new TruthTable();
        animatedStrings = new ArrayList<>();
        timerFill = new Timer(150, e -> fillAnimationStrings());
        timerMoving = new Timer(10, e -> move());
        this.frame = frame;
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g2.setColor(Color.white);
        g2.fillRect(0, 0, frame.getWidth(), frame.getHeight());

        drawTable(g2);
        fillTable(g2);
        drawAnimatedStrings(g2);
    }

    private void drawTable(Graphics g) {
        g.setColor(Color.BLACK);
        g.setFont(new Font("Consolas", Font.PLAIN, 18));
        g.drawString("Таблица истинности", margin + 10, margin + 10);
        for (int i = 0; i <= rows; i++) {
            g.drawLine(margin, margin * 2 + i * cellSize, margin + cellSize * columns, margin * 2 + i * cellSize);
            for (int j = 0; j <= columns; j++) {
                g.drawLine(margin + j * cellSize, margin * 2, margin + j * cellSize, margin * 2 + cellSize * rows);
            }
        }
    }

    private void fillTable(Graphics g) {
        g.setFont(new Font("Consolas", Font.PLAIN, 22));
        g.setColor(Color.BLACK);

        int littlePosFix = 7;
        g.drawString("X1", margin + littlePosFix, cellSize * 2 + littlePosFix);
        g.drawString("X2", margin + littlePosFix + cellSize, cellSize * 2 + littlePosFix);
        g.drawString("X3", margin + littlePosFix + cellSize * 2, cellSize * 2 + littlePosFix);
        g.drawString("X4", margin + littlePosFix + cellSize * 3, cellSize * 2 + littlePosFix);
        g.drawString("F", margin + littlePosFix * 2 + cellSize * 4, cellSize * 2 + littlePosFix);

        for (int i = 0; i < rows - 1; i++) {
            for (int j = 0; j < columns; j++) {
                g.setFont(new Font("Consolas", Font.PLAIN, 24));
                if (j == columns - 1) {
                    g.setFont(new Font("Consolas", Font.BOLD, 24));
                }
                g.drawString(Integer.toString(truthTable.getTruthTable()[i][j]), margin + littlePosFix * 2 + j * cellSize, margin * 4 + littlePosFix + i * cellSize);
            }
        }
    }

    private void drawAnimatedStrings(Graphics g) {
        g.setFont(new Font("Consolas", Font.PLAIN, 24));
        for (AnimatedString animatedString : animatedStrings) {
            g.drawString(animatedString.getString(), 260, animatedString.getPosY());
        }
    }

    public void refreshTable() {
        truthTable = new TruthTable();
        animatedStrings = new ArrayList<>();
        Converter.refreshCount();
    }

    private void fillAnimationStrings() {
        if (animatedStrings.size() < rows - 1) {
            String result = Converter.convertTable(truthTable.getTruthTable());
            if (!result.equals("")) {
                animatedStrings.add(new AnimatedString(result, 5 + (animatedStrings.size() + 3) * 40));
            }
        } else {
            sortAnimationStrings();
            timerMoving.start();
            timerFill.stop();
        }
        frame.repaint();
    }

    private void move() {
        for (AnimatedString string : animatedStrings) {
            if (string.getPosY() > string.getLimitY()) {
                string.decreaseY();
            } else if (string.getPosY() < string.getLimitY()) {
                string.increaseY();
            }
        }
        frame.repaint();
    }

    private void sortAnimationStrings() {
        int marginForStrings = 85;
        animatedStrings.add(new AnimatedString("ДНФ", -50, cellSize));
        AnimatedString stringAddLastSymbol = null;
        int count = 0;
        for (AnimatedString string : animatedStrings) {
            if (string.getString().indexOf('*') != -1) {
                string.setLimitY(count * cellSize + marginForStrings);
                if (stringAddLastSymbol != null) {
                    stringAddLastSymbol.addPlusToDNF();
                }
                stringAddLastSymbol = string;
                count++;
            }
        }
        animatedStrings.add(new AnimatedString("КНФ", 900, count * cellSize + marginForStrings));
        count++;
        stringAddLastSymbol = null;
        for (AnimatedString string : animatedStrings) {
            if (string.getString().indexOf('+') != -1 && !(string.getString().indexOf('+') == string.getString().length() - 1)) {
                string.setLimitY(count * cellSize + marginForStrings);
                if (stringAddLastSymbol != null) {
                    stringAddLastSymbol.addMultiplyToKNF();
                }
                stringAddLastSymbol = string;
                count++;
            }
        }
    }

    public void startAnimation() {
        timerFill.start();
    }

    public void stopAnimation() {
        timerFill.stop();
    }
}

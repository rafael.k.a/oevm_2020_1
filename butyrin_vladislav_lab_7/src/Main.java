

public class Main {

    public static void main(String[] args) {
        PascalCode PascalCode = new PascalCode();

        AssemblerCode assemblerCode = new AssemblerCode();

        Parser Parser = new Parser(PascalCode.read(), assemblerCode);
        Parser.parseBody();
        assemblerCode.create();
    }
}

package com.company;

public class NumberSystemConverter {

    private static char[] number;
    private static long decimalNumber = 0;

    public static String convert(String value, int originalNS, int finalNS) {


        if (originalNS == finalNS) {
            return value;
        }

        number = value.toCharArray();

        if (!validateInput(originalNS)) {
           return null;
        }

        toDecimalNotation(originalNS);
        return toOtherNumberSystem(finalNS);
    }

    private static boolean validateInput(int originalNS) {

        // Можно использовать нижние подчеркивания при передачи числа в метод convert для большей читаемости.
        // Здесь они будут игнорироваться.

        for (int i = 0; i < number.length; i++) {
            if (number[i] == '_')
                continue;
            int a = toNumeric(number[i]);
            if (a == -1 || a > originalNS - 1) {
                return false;
            }
        }
        return true;
    }

    private static String toOtherNumberSystem(int otherNumberSystem) {

        // Типичный перевод делением числа в десетичной СС на основание новый СС.
        // В reversedNumber записываются остатки от деления.
        // В result записывается результат деления.

        StringBuilder reversedNumber = new StringBuilder();
        long result = 0;
        if(decimalNumber < otherNumberSystem){
            return fromNumeric((int)decimalNumber) + "";
        }

        while (decimalNumber >= otherNumberSystem) {
            result = decimalNumber / otherNumberSystem;
            reversedNumber.append(fromNumeric((int) (decimalNumber - otherNumberSystem * result)));
            decimalNumber = result;
        }
        reversedNumber.append(fromNumeric((int) result));
        return reversedNumber.reverse().toString();
    }


    private static void toDecimalNotation(int originalNS) {

        // Текущая позиция считается справа налево. В это число будет возводиться основание СС.
        int currentPosition = 0;
        decimalNumber = 0;
        for (int i = number.length - 1; i >= 0; i--) {

            // Можно использовать нижние подчеркивания при передачи числа в метод convert для большей читаемости.
            // Здесь они будут игнорироваться.

            if (number[i] != '_') {
                int a = toNumeric(number[i]);
                decimalNumber += a * Math.pow(originalNS, currentPosition);
                currentPosition++;
            }
        }
    }

    private static char fromNumeric(int number) {
        if (number >= 0 && number <= 9) {
            return (char) ('0' + number);
        } else if (number <= 15) {
            return (char) ('A' + number - 10);
        }
        return 0;
    }

    private static int toNumeric(char numberSymbol) {

        if (numberSymbol >= '0' && numberSymbol <= '9') {
            return numberSymbol - '0';
        }
        if (numberSymbol >= 'a' && numberSymbol <= 'f') {
            return numberSymbol - 'a' + 10;
        }
        if (numberSymbol >= 'A' && numberSymbol <= 'F') {
            return numberSymbol - 'A' + 10;
        }
        return -1;
    }
}

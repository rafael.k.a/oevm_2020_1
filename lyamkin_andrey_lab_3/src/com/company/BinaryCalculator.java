package com.company;

import org.w3c.dom.ls.LSOutput;

import java.util.Arrays;

public class BinaryCalculator {


    private static boolean isNegative = false;

    public static String calculate(String val1, String val2, int numberSystem, String operator) {

        // Здесь переводим в 2 СС.
        char[] binaryVal1 = NumberSystemConverter.convert(val1, numberSystem, 2).toCharArray();
        char[] binaryVal2 = NumberSystemConverter.convert(val2, numberSystem, 2).toCharArray();

        // Тут если прилетает массив (то есть число в 2 СС) с длиной больше 31, говорим, что слишком много.
        if (!validateInputDate(binaryVal1, binaryVal2)) {
            System.out.println("Числа слишком большое!");
           return null;
        }

        if (operator.equals("+") || operator.toLowerCase().equals("сложение")) {
            return arrToString(plus(binaryVal1, binaryVal2)) ;
        } else if (operator.equals("-") || operator.toLowerCase().equals("вычитание")) {
            char[] res = minus(binaryVal1, binaryVal2);
            if(isNegative){
                return "-" + arrToString(res);
            }
            return arrToString(res);
        } else if (operator.equals("*") || operator.toLowerCase().equals("умножение")) {
            if(isZero(binaryVal2)){
                return "0";
            }
            return arrToString(mult(binaryVal1, binaryVal2));
        } else if (operator.equals("/") || operator.toLowerCase().equals("деление")) {
            if(isZero(binaryVal2)){
                return "Деление на 0";
            }
            return arrToString(div(binaryVal1, binaryVal2));
        } else {
            System.out.println("Такой операции не существует!");
            return null;
        }
    }
    private static boolean isZero(char[] val){
        for (int i = 0; i < val.length; i++) {
            if(val[i] =='1')
                return false;
        }
        return true;
    }

    private static char[] div(char[] value1, char[] value2) {
        // Деление все портит, функция перевода в дополнительный
        // код сразу дополняет незначащими нулями, что здесь не нужно. Опять костыли одни

        StringBuilder result = new StringBuilder();
        char[] remains = toFixedLength(value1); // Остатки (Алгоритм из лекции).

        // Выполняем подготовку к делению.
        char[] addCode = new char[32];
        char[] directCode = new char[32];

        Arrays.fill(addCode, '0');
        Arrays.fill(directCode, '0');
        addCode[0] = '1';
        for (int i = 0; i < value2.length; i++) {
            // См. особенности функции перевода в дополнительный код.
            addCode[i+1] = toAdditionalCode(value2)[32 - value2.length + i];
            directCode[i+1] = value2[i];
        }
        // Начинаем вычислять.
        shiftToLeft(remains);
        remains = plus(remains, addCode);

        for (int i = 0;i < 32-value2.length -1; i++) {
            if(remains[0] == '1'){
                result.append('0');
                shiftToLeft(remains);
                remains = plus(remains, directCode);
            }else if(remains[0] == '0'){
                result.append('1');
                shiftToLeft(remains);
                remains = plus(remains, addCode);
            }
        }
        return result.toString().toCharArray();
    }

    private static char[] mult(char[] value1, char[] value2) {
        // я не отвечаю за то, если кто-то ввел слишком большие числа. Все сломается.
        char[] cumulativeAmount = new char[]{'0'};
        char[] val1 = toFixedLength(value1);
        for (int i = 0; i < value2.length; i++) {
            if (value2[value2.length - 1 - i] == '1') {
                cumulativeAmount = plus(cumulativeAmount, val1);
            }
            shiftToLeft(val1);
        }
        return cumulativeAmount;
    }

    private static char[] minus(char[] value1, char[] value2) {
        isNegative = false;
        char[] result = plus(value1, toAdditionalCode(value2));
        if(more(value2, value1)){
            isNegative = true;
            return fromAdditionalCode(result);
        }
        return result;
    }


    private static char[] plus(char[] value1, char[] value2) {

        char[] val1 = toFixedLength(value1);
        char[] val2 = toFixedLength(value2);
        char[] result = new char[32];
        Arrays.fill(result, '0');
        boolean shift = false;

        for (int i = result.length - 1; i >= 0; i--) {
            // Это было логически продумано
            if(val1[i]=='1' && val2[i]=='1' && shift){
                result[i] = '1';

            }else if((val1[i] == '1' && val2[i] == '1') || (val1[i] == '1' && shift) || (shift && val2[i] == '1')){
                result[i] = '0';
                shift = true;
            }else if(!shift && val1[i]=='0' && val2[i] =='0'){
                result[i] = '0';
                shift = false;
            }
            else{
                result[i] = '1';
                shift = false;
            }
        }
        return result;
    }

    private static void shiftToLeft(char[]value){
        for (int i = 0; i < value.length-1; i++) {
            value[i] = value[i+1];
        }
        value[value.length-1] = '0';
    }

    private static char[] toAdditionalCode(char[] val){

        char[] value = toFixedLength(val);
        for (int i = 0; i < value.length; i++) {
            if(value[i] == '1'){
                value[i] ='0';
            }else value[i] ='1';
        }
        return plus(value, new char[]{'1'});
    }
    private static char[] fromAdditionalCode(char[] value){

        char[] result = minus(value, new char[]{'1'});
        for (int i = 0; i < result.length; i++) {
            if(result[i] == '1'){
                result[i] ='0';
            }else result[i] ='1';
        }
        return result;
    }

    private static char[] toFixedLength(char[] value){
        // Храним  числа в массиве на 32 элемента, чтобы их длины были одинаковыми.
        if(value.length == 32)
            return value;

        char[] binaryVal = new char[32];
        Arrays.fill(binaryVal, '0');
        for (int i = 0; i < value.length; i++) {
            binaryVal[31 - i] = value[value.length - 1 - i];
        }
        return binaryVal;
    }

    private static boolean more(char[] value1, char[] value2){
        // Возвращает тру, если первое число больше (числа положительные).

        char[] v1 = toFixedLength(value1);
        char[] v2 = toFixedLength(value2);
        for (int i = 0; i < v1.length; i++) {
           if(v1[i] == '1' && v2[i] == '0'){
               return true;
           }else if(v1[i] == '0' && v2[i] == '1'){
               return false;
           }
        }
        return false;
    }

    private static boolean validateInputDate(char[] val1, char[] val2) {
        if (val1.length > 32 || val2.length > 32) {
            return false;
        }
        return true;
    }
    private static String arrToString(char[] arr){
        StringBuilder sb = new StringBuilder(32);
        int i = 0;
        while(arr[i] == '0'){
            i++;
        }
        while(i < arr.length) {
            sb.append(arr[i]);
            i++;
        }
        return sb.toString();
    }
}

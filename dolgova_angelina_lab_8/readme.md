## Лабораторная работа №8 "Физические компоненты ЭВМ"

Выполнила студентка группы **ПИбд-22 Долгова Ангелина**


### Задание:

Разобрать и собрать системный блок ЭВМ. 

*В качестве подопытного был выбран старый ПК.*

Фото до:

![Pic1](https://i.ibb.co/jf0G1Qz/Ae-KLneq3-BDU.jpg)

Фото после (извиняюсь за качество):

![Pic2](https://i.ibb.co/xssKgKH/yc-Lhb1a7i-LU.jpg)

---------------------------------------------------------------

**Список комплектующих:**

| **Деталь** | **Изображение** | **Описание** 
| :---: | :---: | :---: | 
| MSI A55M-P33 | ![Pic3](https://i.ibb.co/rwktSQX/1.jpg) | Древняя материнская плата
| AMD A4-3400 2700 MHz | ![Pic4](https://i.ibb.co/3NzRSxk/2.jpg) | Древний двухядерный процессор со встроенным графическим ядром Radeon HD 6410D 
| Qori 500CG | ![Pic5](https://i.ibb.co/Wcc5Q4y/3.jpg) | Блок питания на 500 Вт
| QUMO [QUM3U-4G1600С11L] 4 ГБ | ![Pic6](https://i.ibb.co/M2bTV9D/4.jpg) | Низкочастотная оперативная память со сниженным энергопотреблением
| Western Digital 250G | ![Pic6](https://i.ibb.co/XZr8JLZ/5.jpg) | Жесткий диск с запасом в 250 ГБ

А также куллер и радиатор, обозначения которых не были обнаружены.
 


---------------------------------------------------------------



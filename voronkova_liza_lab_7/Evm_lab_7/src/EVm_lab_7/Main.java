package EVm_lab_7;

public class Main {

    public static void main(String[] args) {
        PReader PascalCode = new PReader();
        AWriter assemblerCode = new AWriter();
        Commands Parser = new Commands(PascalCode.read(), assemblerCode);
        Parser.parseBody();
        assemblerCode.create();
    }

}

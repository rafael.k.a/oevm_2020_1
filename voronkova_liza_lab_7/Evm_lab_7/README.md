# Лабораторная работа №7

## ИСэбд-21 Воронкова Лиза

Разработать программный продукт для перевода кода Pascal 
в код Assembler

Программа на языке Pascal:
var
    x, y: integer;
    res1, res2, res3, res4: integer;
begin
    write('input x: '); readln(x);
    write('input y: '); readln(y);
    res1 := x + y; write('x + y = '); writeln(res1);
    res2 := x - y; write('x - y = '); writeln(res2);
    res3 := x * y; write('x * y = '); writeln(res3);
    res4 := x / y; write('x / y = '); writeln(res4);
end.

Программа на языке ассемблер 

format PE console

entry start

include 'win32a.inc'

section '.data' data readable writable
        spaceStr db '%d', 0
        dopStr db '%d', 0ah, 0
        x dd ?
        y dd ?
        res1 dd ?
        res2 dd ?
        res3 dd ?
        res4 dd ?
        str5 db 'x * y = ', 0
        str6 db 'x / y = ', 0
        str3 db 'x + y = ', 0
        str4 db 'x - y = ', 0
        str1 db 'input x: ', 0
        str2 db 'input y: ', 0
section '.code' code readable executable

         start:
                 push str1
                 call [printf]
                 push x
                 push spaceStr
                 call [scanf]

                 push str2
                 call [printf]
                 push y
                 push spaceStr
                 call [scanf]

                 mov ecx, [x]
                 add ecx, [y]
                 mov [res1], ecx
                 push str3
                 call [printf]
                 push [res1]
                 push dopStr
                 call [printf]

                 mov ecx, [x]
                 sub ecx, [y]
                 mov [res2], ecx
                 push str4
                 call [printf]
                 push [res2]
                 push dopStr
                 call [printf]

                 mov ecx, [x]
                 imul ecx, [y]
                 mov [res3], ecx
                 push str5
                 call [printf]
                 push [res3]
                 push dopStr
                 call [printf]

                 mov eax, [x]
                 mov ecx, [y]
                 div ecx
                 mov [res4], eax
                 push str6
                 call [printf]
                 push [res4]
                 push dopStr
                 call [printf]

         finish:

                 call [getch]

                 call [ExitProcess]

section '.idata' import data readable

         library kernel, 'kernel32.dll',\
         msvcrt, 'msvcrt.dll'

         import kernel,\
         ExitProcess, 'ExitProcess'

         import msvcrt,\
         printf, 'printf',\
         scanf, 'scanf',\
         getch, '_getch'

Видео: https://cloud.mail.ru/public/9fwo/5ckyCQKdc
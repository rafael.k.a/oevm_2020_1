package com.nodj;


import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class AssemblerMaker {
    HashMap<String, ArrayList<String>> varsByType;
    ArrayList<String> programLines;
    StringBuilder fullCode = new StringBuilder();
    HashMap<String, String> varString = new HashMap<>();

    public AssemblerMaker(PascalParser pp) {
        varsByType = pp.getVarsByType();
        programLines = pp.getProgramLines();
        makeStart();
        makeProgram();
        makeFinish();
        writeToFile();
    }

    private void makeStart() {
        fullCode.append("format PE console\n")
                .append("entry start\n")
                .append("include 'win32a.inc'\n")
                .append("section '.data' data readable writable\n")
                .append("\tkeeperInputString db '%d', 0\n")
                .append("\tkeeperResult db '%d', 0ah, 0\n");

        for (String var : varsByType.get("integer")) {
            fullCode.append("\t").append(var).append(" dd ?\n");
        }

        for (int i = 0; i < programLines.size(); i++) {
            String line = programLines.get(i);
            if (line.contains("'")) {
                String parser = line.substring(line.indexOf('\'') + 1, line.indexOf('\'', line.indexOf('\'') + 1));
                varString.put(parser, "str" + (i + 1));
                fullCode.append("\tstr")
                        .append(i + 1).append(" db '")
                        .append(parser)
                        .append("', 0\n");
            }
        }

    }

    private void makeProgram() {
        fullCode.append("section '.code' code readable executable\n")
                .append("\t start:\n");
        for (String line :
                programLines) {
            if (line.contains(":=")) {
                makeOperation(line);
            } else {
                makeMethod(line);
            }
        }
    }

    private void makeFinish() {
        fullCode.append("\t finish:\n")
                .append("\n")
                .append("\t\t call [getch]\n")
                .append("\n")
                .append("\t\t call [ExitProcess]\n")
                .append("\n")
                .append("section '.idata' import data readable\n")
                .append("\n")
                .append("\t library kernel, 'kernel32.dll',\\\n")
                .append("\t msvcrt, 'msvcrt.dll'\n")
                .append("\n")
                .append("\t import kernel,\\\n")
                .append("\t ExitProcess, 'ExitProcess'\n")
                .append("\n")
                .append("\t import msvcrt,\\\n")
                .append("\t printf, 'printf',\\\n")
                .append("\t scanf, 'scanf',\\\n")
                .append("\t getch, '_getch'");
    }

    private void writeToFile() {
        try (FileWriter writer = new FileWriter("FromPascalToAssembler.ASM", false)) {
            writer.write(fullCode.toString());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

    }

    public void makeMethod(String method) {
        String[] raw = method.split("\\(");
        String instruction = raw[0];
        String args = raw[1];

        switch (instruction) {
            case "writeln":
                fullCode.append("\t\t push [")
                        .append(args, 0, args.length() - 1)
                        .append("]\n\t\t push keeperResult\n")
                        .append("\t\t call [printf]\n\n");
                break;
            case "readln":
                fullCode.append("\t\t push ")
                        .append(args, 0, args.length() - 1)
                        .append("\n\t\t push keeperInputString\n")
                        .append("\t\t call [scanf]\n\n");
                break;
            case "write":
                fullCode.append("\t\t push ").append(varString.get(args.substring(args.indexOf('\'') + 1, args.indexOf('\'', args.indexOf('\'') + 1)))).append("\n\t\t call [printf]\n");
                break;
        }
    }

    void makeOperation(String operation) {
        String[] raw = operation.split(" ");
        String resultVar = raw[0];
        String fVar = raw[2];
        String operand = raw[3];
        String sVar = raw[4];

        switch (operand) {
            case "+":
                fullCode.append("\t\t mov ecx, [").append(fVar).append("]\n").append("\t\t add ecx, [").append(sVar).append("]\n").append("\t\t mov [").append(resultVar).append("], ecx\n");
                break;
            case "-":
                fullCode.append("\t\t mov ecx, [").append(fVar).append("]\n").append("\t\t sub ecx, [").append(sVar).append("]\n").append("\t\t mov [").append(resultVar).append("], ecx\n");
                break;
            case "*":
                fullCode.append("\t\t mov ecx, [").append(fVar).append("]\n").append("\t\t imul ecx, [").append(sVar).append("]\n").append("\t\t mov [").append(resultVar).append("], ecx\n");
                break;
            case "/":
                fullCode.append("\t\t mov eax, [").append(fVar).append("]\n").append("\t\t mov ecx, [").append(sVar).append("]\n").append("\t\t div ecx\n").append("\t\t mov [").append(resultVar).append("], eax\n");
                break;
        }
    }
}

public class Converter {
    final String ERROR_MESSAGE = "Ошибка! Введены некорректные данные.\nЧисло не соответствует заявленной с.с.";
    final int LETTER_SHIFT = 55;
    final int DIGIT_SHIFT = 48;

    //Метод для проверки введённых чисел на соответствие заявленной с.с.
    public boolean checkInputData(char[] number, int originalSystem) {
        for (char c : number) {
            if (Character.isDigit(c)) {
                if ((c >= (char) (originalSystem + DIGIT_SHIFT))) {
                    System.out.println(ERROR_MESSAGE);
                    return false;
                }
            }
            else if (Character.isLetter(c)) {
                if ((c >= (char) (originalSystem + LETTER_SHIFT))) {
                    System.out.println(ERROR_MESSAGE);
                    return false;
                }
            }
            else if (c != (char) 45) {
                System.out.println(ERROR_MESSAGE);
                return false;
            }
        }
        return true;
    }

    public char[] convertToDecimalSystem(char[] originalNumber, int originalSystem) {
        int numberInDecimal = 0;
        boolean isNegative = false;

        if (originalNumber.length > 0) {
            if (originalNumber[0] == '-') {
                isNegative = true;
                StringBuilder buf = new StringBuilder();
                buf.append(String.valueOf(originalNumber));
                buf.delete(0, 1);
                originalNumber = buf.toString().toCharArray();
            }
        }

        int power = originalNumber.length - 1;

        if (originalSystem != 10) {
            for (char c : originalNumber) {
                if (Character.isDigit(c)) {
                    numberInDecimal += ((int) c - DIGIT_SHIFT) * Math.pow(originalSystem, power);
                } else if (Character.isLetter(c)) {
                    numberInDecimal += ((int) c - LETTER_SHIFT) * Math.pow(originalSystem, power);
                }
                power--;
            }
            if (isNegative) {
                StringBuilder negativeAnswer = new StringBuilder();
                negativeAnswer.append(numberInDecimal);
                negativeAnswer.insert(0, "-");
                return negativeAnswer.toString().toCharArray();
            }
            return Integer.toString(numberInDecimal).toCharArray();
        }
        else {
            if (isNegative) {
                StringBuilder negativeAnswer = new StringBuilder();
                negativeAnswer.append(originalNumber);
                negativeAnswer.insert(0, "-");
                return negativeAnswer.toString().toCharArray();
            }
                return originalNumber;
        }
    }

    public char[] convertToBinarySystem(char[] originalNumber) {
        boolean isNegative = false;

        if (originalNumber[0] == '-') {
            isNegative = true;
            StringBuilder buf = new StringBuilder();
            buf.append(String.valueOf(originalNumber));
            buf.delete(0, 1);
            originalNumber = buf.toString().toCharArray();
        }
        int originalNumberInt = Integer.parseInt(String.valueOf(originalNumber));
        StringBuilder finalNumber = new StringBuilder();
        while (originalNumberInt >= 2) {
            finalNumber.append(originalNumberInt % 2);
            originalNumberInt /= 2;
        }
        finalNumber.append(originalNumberInt);
        finalNumber.reverse();
        if (isNegative) {
            finalNumber.insert(0, "-");
        }
        return finalNumber.toString().toCharArray();
    }
}
import java.util.Arrays;

public class BinaryCalculator {
    Converter converter = new Converter();

    //Метод для сложения двоичных чисел
    public String addNumbers(char[] firstNumber, char[] secondNumber) {
        StringBuilder answer = new StringBuilder();
        char currentChar;
        int transferDigit = 0;
        boolean negativeNumbers = false;

        //Обработка отрицательных чисел
        if (firstNumber.length > 0 && secondNumber.length > 0) {
            if (firstNumber[0] == '-' && secondNumber[0] == '-') {
                negativeNumbers = true;
                StringBuilder buf = new StringBuilder();
                buf.append(String.valueOf(firstNumber));
                buf.delete(0, 1);
                firstNumber = buf.toString().toCharArray();
                buf.delete(0, firstNumber.length);
                buf.append(String.valueOf(secondNumber));
                buf.delete(0, 1);
                secondNumber = buf.toString().toCharArray();
            } else if (firstNumber[0] == '-') {
                StringBuilder buf = new StringBuilder();
                buf.append(String.valueOf(firstNumber));
                buf.delete(0, 1);
                firstNumber = buf.toString().toCharArray();
                return subtractNumbers(secondNumber, firstNumber);
            } else if (secondNumber[0] == '-') {
                StringBuilder buf = new StringBuilder();
                buf.append(String.valueOf(secondNumber));
                buf.delete(0, 1);
                secondNumber = buf.toString().toCharArray();
                return subtractNumbers(firstNumber, secondNumber);
            }
        }

        for (int i = firstNumber.length - 1 , j = secondNumber.length - 1; i >= 0 || j >= 0 || transferDigit == 1; i--, j--) {
            int firstDigit, secondDigit;
            if (i < 0) {
                firstDigit = 0;
            }
            else if (firstNumber[i] == '0') {
                firstDigit = 0;
            }
            else {
                firstDigit = 1;
            }

            if (j < 0) {
                secondDigit = 0;
            }
            else if (secondNumber[j] == '0') {
                secondDigit = 0;
            }
            else {
                secondDigit = 1;
            }

            int currentSum = firstDigit + secondDigit + transferDigit;

            if (currentSum == 0) {
                currentChar = '0';
                transferDigit = 0;
            }
            else if (currentSum == 1) {
                currentChar = '1';
                transferDigit = 0;
            }
            else if (currentSum == 2) {
                currentChar = '0';
                transferDigit = 1;
            }
            else {
                currentChar = '1';
                transferDigit = 1;
            }
            answer.insert(0, currentChar);
        }
        if (negativeNumbers) {
            answer.insert(0, '-');
        }
        return answer.toString();
    }

    //Метод для вычитания двоичных чисел
    public String subtractNumbers(char[] firstNumber, char[] secondNumber) {
        StringBuilder answer = new StringBuilder();
        boolean isNegative = false;
        boolean insignificantZeros = true;

        //Обработка отрицательных чисел
        if ((firstNumber[0] == '-' && secondNumber[0] == '-') || (secondNumber[0] == '-')) {
            StringBuilder buf = new StringBuilder();
            buf.append(String.valueOf(secondNumber));
            buf.delete(0, 1);
            secondNumber = buf.toString().toCharArray();
            return addNumbers(firstNumber, secondNumber);
        }
        else if (firstNumber[0] == '-') {
            StringBuilder buf = new StringBuilder();
            buf.append(String.valueOf(secondNumber));
            buf.insert(0, "-");
            secondNumber = buf.toString().toCharArray();
            return addNumbers(firstNumber, secondNumber);
        }
        if (Integer.parseInt(String.valueOf(converter.convertToDecimalSystem(firstNumber, 2))) < Integer.parseInt(String.valueOf(converter.convertToDecimalSystem(secondNumber, 2)))) {
            isNegative = true;
            char[] buf = secondNumber;
            secondNumber = firstNumber;
            firstNumber = buf;

        }
        else if (converter.convertToDecimalSystem(firstNumber, 2) == converter.convertToDecimalSystem(secondNumber, 2)) {
            return "0";
        }

        int difference = firstNumber.length - secondNumber.length;
        char[] reverseCodeFirst = new char[firstNumber.length];

        for (int i = 0; i < reverseCodeFirst.length; i++) {
            if (i < difference) {
                reverseCodeFirst[i] = '1';
            }
            else {
                if (secondNumber[i - difference] == '0') {
                    reverseCodeFirst[i] = '1';
                }
                else {
                    reverseCodeFirst[i] = '0';
                }
            }
        }

        char[] additionalCode = addNumbers(reverseCodeFirst, new char[] {'1'}).toCharArray();
        char[] additionalSum = addNumbers(additionalCode, firstNumber).toCharArray();

        for (int i = 1; i < additionalSum.length; ++i) {
            if (additionalSum[i] == '1') {
                insignificantZeros = false;
            }
            else {
                if (insignificantZeros) {
                    continue;
                }
            }
            answer.append(additionalSum[i]);
        }
        if (isNegative) {
            answer.insert(0, '-');
        }
        return answer.toString();
    }

    //Метод для умножения двоичных чисел
    public String multiplyNumbers(char[] firstNumber, char[] secondNumber) {
        char[] answer = {};
        boolean isNegative = false;

        //Обработка отрицательных чисел
        if (firstNumber[0] == '-' && secondNumber[0] == '-') {
            StringBuilder buf = new StringBuilder();
            buf.append(String.valueOf(firstNumber));
            buf.delete(0, 1);
            firstNumber = buf.toString().toCharArray();
            buf.delete(0, firstNumber.length);
            buf.append(String.valueOf(secondNumber));
            buf.delete(0, 1);
            secondNumber = buf.toString().toCharArray();
        }
        else if (firstNumber[0] == '-') {
            isNegative = true;
            StringBuilder buf = new StringBuilder();
            buf.append(String.valueOf(firstNumber));
            buf.delete(0, 1);
            firstNumber = buf.toString().toCharArray();
        }
        else if (secondNumber[0] == '-') {
            isNegative = true;
            StringBuilder buf = new StringBuilder();
            buf.append(String.valueOf(secondNumber));
            buf.delete(0, 1);
            secondNumber = buf.toString().toCharArray();
        }

        for (int i = 0; i < Integer.parseInt(String.valueOf(converter.convertToDecimalSystem(secondNumber, 2))); i++) {
            answer = (addNumbers(answer, firstNumber)).toCharArray();
        }
        if (isNegative) {
            StringBuilder negativeAnswer = new StringBuilder();
            negativeAnswer.append(String.valueOf(answer));
            negativeAnswer.insert(0, "-");
            return negativeAnswer.toString();
        }
        return String.valueOf(answer);
    }

    public String divideNumbers(char[] firstNumber, char[] secondNumber) {
        int answer = 0;
        boolean isNegative = false;

        //Обработка отрицательных чисел
        if (firstNumber[0] == '-' && secondNumber[0] == '-') {
            StringBuilder buf = new StringBuilder();
            buf.append(String.valueOf(firstNumber));
            buf.delete(0, 1);
            firstNumber = buf.toString().toCharArray();
            buf.delete(0, firstNumber.length);
            buf.append(String.valueOf(secondNumber));
            buf.delete(0, 1);
            secondNumber = buf.toString().toCharArray();
        }
        else if (firstNumber[0] == '-') {
            isNegative = true;
            StringBuilder buf = new StringBuilder();
            buf.append(String.valueOf(firstNumber));
            buf.delete(0, 1);
            firstNumber = buf.toString().toCharArray();
        }
        else if (secondNumber[0] == '-') {
            isNegative = true;
            StringBuilder buf = new StringBuilder();
            buf.append(String.valueOf(secondNumber));
            buf.delete(0, 1);
            secondNumber = buf.toString().toCharArray();
        }

        while (Integer.parseInt(String.valueOf(converter.convertToDecimalSystem(firstNumber, 2))) >= Integer.parseInt(String.valueOf(converter.convertToDecimalSystem(secondNumber, 2))) && !Arrays.equals(firstNumber, new char[]{'0'})) {
            firstNumber = (subtractNumbers(firstNumber, secondNumber)).toCharArray();
            answer++;
        }
        if (isNegative) {
            StringBuilder negativeAnswer = new StringBuilder();
            negativeAnswer.append(converter.convertToBinarySystem(Integer.toString(answer).toCharArray()));
            if (negativeAnswer.length() > 1 || negativeAnswer.charAt(0) != '0') {
                negativeAnswer.insert(0, "-");
            }
            return negativeAnswer.toString();
        }
        return String.valueOf(converter.convertToBinarySystem(Integer.toString(answer).toCharArray()));
    }
}
package com.company;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.lang.Integer;

public class Variables {
    public int getPropertyValueInt(String propertyName) {
        int propertyValue = 0;
        Properties properties = new Properties();

        try (InputStream inputStream = this.getClass().getResourceAsStream("config.properties")) {
            properties.load(inputStream);
            propertyValue = Integer.valueOf(properties.getProperty(propertyName));
        } catch (IOException e) {
            System.out.print(e);
        }
        return propertyValue;
    }
}
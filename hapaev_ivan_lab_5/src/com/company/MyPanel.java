package com.company;
import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel {

    private int truthTable[][];
    HelperClass methods = new HelperClass();

    public MyPanel(int array[][]) {
        this.truthTable = array;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        methods.draw(truthTable, g);
    }

}

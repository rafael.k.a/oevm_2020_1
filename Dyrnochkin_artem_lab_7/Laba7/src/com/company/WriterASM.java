package com.company;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;


public class WriterASM {

    ReaderPascal pfr = new ReaderPascal();
    private ArrayList<String> variablesArray = pfr.getVariables();
    private ArrayList<String> writeArray = pfr.getWrite();
    private ArrayList<String> writelnArray = pfr.getWriteln();
    private ArrayList<String> readArray = pfr.getRead();
    private ArrayList<String> arithmeticCommandsArray = pfr.getArithmeticCommands();
    private ArrayList<String> outStrings = new ArrayList<>();
    private ArrayList<String> readStrings = new ArrayList<>();
    private String starting;
    private String ending;
    private String section;
    private String variablesString;
    private String writeString;
    private String body;

    public WriterASM() throws FileNotFoundException {
        pfr.startReading("PascalFile.txt");
        compileStarting();
        compileWrite();
        compileVariables();
        compileSection();
        compileBody();
        compileEnding();
        writeFile();
    }

    public void compileStarting() {
        starting =
                "format PE console\n" +
                        "\n" +
                        "entry start\n" +
                        "\n" +
                        "include 'win32a.inc'\n" +
                        "\n" +
                        "section '.data' data readable writable\n";
    }
    public void compileEnding() {
        ending = "         finish:\n" +
                "\n" +
                "                call [getch]\n" +
                "\n" +
                "                call [ExitProcess]\n" +
                "\n" +
                "section '.idata' import data readable\n" +
                "\n" +
                "        library kernel, 'kernel32.dll',\\\n" +
                "                msvcrt, 'msvcrt.dll'\n" +
                "\n" +
                "        import kernel,\\\n" +
                "               ExitProcess, 'ExitProcess'\n" +
                "\n" +
                "        import msvcrt,\\\n" +
                "               printf, 'printf',\\\n" +
                "               scanf, 'scanf',\\\n" +
                "               getch, '_getch'";
    }
    public void compileSection() {
        section = "section '.code' code readable executable\n" +
                "   start:\n";
    }

    public void compileWrite() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < writeArray.size(); i++) {
            sb.append("string").append(i).append(" db ").append(writeArray.get(i)).append(", 0").append("\n");
            if (i<readArray.size()) {
                readStrings.add("string" + i);
            }
            if (i>=readArray.size()) {
                outStrings.add("string" + i);
            }
        }
        writeString=sb.toString();
    }

    public void compileVariables() {
        StringBuilder sb = new StringBuilder();
        sb.append("spaceStr db '%d', 0\ndopStr db '%d', 0ah, 0 \n");
        for (int i = 0; i < variablesArray.size(); i++) {
            sb.append(variablesArray.get(i)).append("\n");
        }
        variablesString=sb.toString();
    }

    public void compileBody() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < readArray.size(); i++) {
            sb.append("push ").append(readStrings.get(i)+"\n").append("call [printf]\n").append("push ").append(readArray.get(i)).append("\npush spaceStr\n").append("call [scanf]\n\n");
        }
        for (int i = 0; i < arithmeticCommandsArray.size(); i++) {
            if (arithmeticCommandsArray.get(i).contains("+")) {
                sb.append("mov ecx, ").append("[").append(readArray.get(0)).append("]\n").append("add").append(" ecx, ")
                        .append("[").append(readArray.get(1)).append("]\n").append("mov [").append(writelnArray.get(i)).append("], ecx\npush ")
                        .append(outStrings.get(i)).append("\ncall [printf]\npush [").append(writelnArray.get(i)).append("]\npush dopStr\ncall [printf]\n\n");
            }
            if (arithmeticCommandsArray.get(i).contains("-")) {
                sb.append("mov ecx, ").append("[").append(readArray.get(0)).append("]\n").append("sub").append(" ecx, ")
                        .append("[").append(readArray.get(1)).append("]\n").append("mov [").append(writelnArray.get(i)).append("], ecx\npush ")
                        .append(outStrings.get(i)).append("\ncall [printf]\npush [").append(writelnArray.get(i)).append("]\npush dopStr\ncall [printf]\n\n");
            }
            if (arithmeticCommandsArray.get(i).contains("*")) {
                sb.append("mov ecx, ").append("[").append(readArray.get(0)).append("]\n").append("imul").append(" ecx, ")
                        .append("[").append(readArray.get(1)).append("]\n").append("mov [").append(writelnArray.get(i)).append("], ecx\npush ")
                        .append(outStrings.get(i)).append("\ncall [printf]\npush [").append(writelnArray.get(i)).append("]\npush dopStr\ncall [printf]\n\n");
            }
            if (arithmeticCommandsArray.get(i).contains("/")) {
                sb.append("mov eax, ").append("[").append(readArray.get(0)).append("]\n").append("mov").append(" ecx, ")
                        .append("[").append(readArray.get(1)).append("]\n").append("div ecx\n").append("mov [").append(writelnArray.get(i)).append("], eax\npush ")
                        .append(outStrings.get(i)).append("\ncall [printf]\npush [").append(writelnArray.get(i)).append("]\npush dopStr\ncall [printf]\n\n");
            }
        }
        body = sb.toString();
    }


    public void writeFile() {
        try(FileWriter writer = new FileWriter("Lab7.ASM", false))
        {
            writer.write(starting);
            writer.write(writeString);
            writer.write(variablesString);
            writer.write(section);
            writer.write(body);
            writer.write(ending);
            writer.flush();
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
    }


}

package com.company;
import java.util.Arrays;

public class Сalculation {

    translatingNumber tNumber = new translatingNumber();

    public String sum(char[] firstNumber, char[] secondNumber, boolean minus) {
        StringBuilder strBuilder = new StringBuilder();
        char presentChar;
        int nextDischarge = 0;

        for (int i = firstNumber.length - 1, j = secondNumber.length - 1; i >= 0 || j >= 0 || nextDischarge == 1; i--, j--) {
            int firstbinaryDigit, secondbinaryDigit;

            if (i < 0) {
                firstbinaryDigit = 0;
            }
            else if (firstNumber[i] == '0') {
                firstbinaryDigit = 0;
            } else {
                firstbinaryDigit = 1;
            }

            if (j < 0) {
                secondbinaryDigit = 0;
            } else if (secondNumber[j] == '0') {
                secondbinaryDigit = 0;
            } else {
                secondbinaryDigit = 1;
            }

            int presentSum = firstbinaryDigit + secondbinaryDigit + nextDischarge;

            if (presentSum == 0) {
                presentChar = '0';
                nextDischarge = 0;
            } else if (presentSum == 1) {
                presentChar = '1';
                nextDischarge = 0;
            } else if (presentSum == 2) {
                presentChar = '0';
                nextDischarge = 1;
            } else {
                presentChar = '1';
                nextDischarge = 1;
            }

            strBuilder.insert(0, presentChar);
        }
        if (minus) {
            strBuilder.insert(0, "-");
        }
        return strBuilder.toString();
    }

    public String operationMinus(char[] minuend, char[] subtrahend, boolean minus) {
        StringBuilder difference = new StringBuilder();
        String minuendString = new String(minuend);
        String subtrahendString = new String(subtrahend);
        if (tNumber.transferDecimal(2, minuendString) < tNumber.transferDecimal(2, subtrahendString)) {
            char[] buffer = minuend;
            minuend = subtrahend;
            subtrahend = buffer;
            if (!minus) {
                minus = true;
            } else {
                minus = false;
            }
        } else if (tNumber.transferDecimal(2, minuendString) == tNumber.transferDecimal(2, subtrahendString)) {
            return "0";
        }

        int differenceDigit = minuend.length - subtrahend.length;
        char[] additSubtrahend = new char[minuend.length];

        for (int i = 0; i < additSubtrahend.length; i++) {
            if (i < differenceDigit) {
                additSubtrahend[i] = '1';
            } else {
                if (subtrahend[i - differenceDigit] == '0') {
                    additSubtrahend[i] = '1';
                } else {
                    additSubtrahend[i] = '0';
                }
            }
        }

        additSubtrahend = sum(additSubtrahend, new char[]{'1'},false).toCharArray();

        char[] currentDif = sum(additSubtrahend, minuend,false).toCharArray();

        int i = 1;
        for (; currentDif[i] == '0'; i++) ;
        for (; i < currentDif.length; i++) {
            difference.append(currentDif[i]);
        }

        if (minus) {
            difference.insert(0, '-');
        }
        return difference.toString();
    }

    public String multiply(char[] firstnumberGet, char[] secondnumberGet,boolean minus) {
        char[] firstNumber = firstnumberGet;
        char[] secondNumber = secondnumberGet;
        if (firstnumberGet[0]=='-'|| secondnumberGet[0]=='-') {
            if (firstnumberGet[0] == '-') {
                firstNumber = new char[firstnumberGet.length - 1];
                System.arraycopy(firstnumberGet, 1, firstNumber, 0, firstnumberGet.length-1);
            }
            if (secondnumberGet[0] == '-') {
                secondNumber = new char[secondnumberGet.length - 1];
                System.arraycopy(secondnumberGet, 1, secondNumber, 0, secondnumberGet.length-1);
            }
        }
        char[] answer = {};
        String secondnumberString = new String(secondNumber);
        for (int i = 0; i < tNumber.transferDecimal(2, secondnumberString); i++) {
            answer = (sum(answer, firstNumber,false)).toCharArray();
        }
        if (minus) {
            char[] answerMin = new char[answer.length+1];
            answerMin[0]='-';
            for (int i = 0; i <answer.length ; i++) {
                answerMin[i+1]=answer[i];
            }
            return new String(answerMin);
        }
        return new String(answer);
    }

    public String divide(char[] dividendGet, char[] dividerGet,boolean minus) {
        if (Arrays.equals(dividendGet, new char[]{'0'})) {
            return "Деление на 0";
        }
        String dividendString;
        String dividerString;
        char[] divider = dividerGet;
        if (dividendGet[0]=='-'|| dividerGet[0]=='-') {
            char[] dividend = dividendGet;
            if (dividendGet[0] == '-') {
                dividend = new char[dividendGet.length - 1];
                System.arraycopy(dividendGet, 1, dividend, 0, dividend.length);
            }
            if (dividerGet[0] == '-') {
                divider = new char[dividerGet.length - 1];
                System.arraycopy(dividerGet, 1, divider, 0, dividerGet.length-1);
            }
            dividendString = new String(dividend);
            dividerString = new String(divider);
        }
        else {
            dividendString = new String(dividendGet);
            dividerString = new String(dividerGet);
        }
        int answer = 0;

        while (tNumber.transferDecimal(2, dividendString) >= tNumber.transferDecimal(2, dividerString)) {
            dividendString = (operationMinus(dividendString.toCharArray(), divider,false));
            answer++;
        }
        if (minus) {
            StringBuilder answerMin = tNumber.transferBinary(answer);
            answerMin.insert(0,'-');
            return answerMin.toString();
        }
        return tNumber.transferBinary(answer).toString();
    }

}

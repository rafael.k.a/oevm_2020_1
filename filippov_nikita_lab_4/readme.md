<!DOCTYPE html>
<html>
<head>
    <meta charset ="utf-8">
</head>
<body>
Лабораторная работа №4<br>
выполнил: Филиппов Никита<br>
группа: ПИбд-22<br>

Ссылка на видео: https://drive.google.com/file/d/1Cx4XmMwpV03WL6ktQWiXOhSfNhl7hIqW/view?usp=sharing<br>
<H3> Как запустить?</H3>
Запустить лабу через Main.java
<H3>Какие технологии использовались?</H3>
Использовалась IntelliJ IDEA и стандартные библиотеки Java. Использовал StringBuilder
<H3>Функционал программы</H3>
Выполняет минимизацию булевой функции из 4х переменных. Конечную форму логической функции указывает пользователь

<table bgcolor="#DCDCDC" border="2">
    <caption >
        Пример КНФ
    </caption>
    <tr>
        <td width="25px">0</td>
		<td width="25px">0</td>
        <td width="25px">0</td>
        <td width="25px">0</td>
		<td width="25px">0</td>
		<td>( X1 + X2 + X3 + X4 )</td>
    </tr>
	<tr>
        <td>0</td>
		<td>0</td>
        <td>0</td>
        <td>1</td>
		<td>0</td>
		<td>* ( X1 + X2 + X3 + -X4 )</td>
    </tr>
	<tr>
        <td>0</td>
		<td>0</td>
        <td>1</td>
        <td>0</td>
		<td>0</td>
		<td>* ( X1 + X2 + -X3 + X4 )</td>
    </tr>
	<tr>
        <td>0</td>
		<td>0</td>
        <td>1</td>
        <td>1</td>
		<td>1</td>
    </tr>
	<tr>
        <td>0</td>
		<td>1</td>
        <td>0</td>
        <td>0</td>
		<td>0</td>
		<td>* ( X1 + -X2 + X3 + X4 ))</td>
    </tr>
	<tr>
        <td>0</td>
		<td>1</td>
        <td>0</td>
        <td>1</td>
		<td>0</td>
		<td>* ( X1 + -X2 + X3 + -X4 )</td>
    </tr>
	<tr>
        <td>0</td>
		<td>1</td>
        <td>1</td>
        <td>0</td>
		<td>1</td>
    </tr>
	<tr>
        <td>0</td>
		<td>1</td>
        <td>1</td>
        <td>1</td>
		<td>0</td>
		<td>* ( X1 + -X2 + -X3 + -X4 )</td>
    </tr>
	<tr>
        <td>1</td>
		<td>0</td>
        <td>0</td>
        <td>0</td>
		<td>0</td>
		<td>* ( -X1 + X2 + X3 + X4 )</td>
    </tr>
	<tr>
        <td>1</td>
		<td>0</td>
        <td>0</td>
        <td>1</td>
		<td>1</td>
    </tr>
	<tr>
        <td>1</td>
		<td>0</td>
        <td>1</td>
        <td>0</td>
		<td>1</td>
    </tr>
	<tr>
        <td>1</td>
		<td>0</td>
        <td>1</td>
        <td>1</td>
		<td>1</td>
    </tr>
	<tr>
        <td>1</td>
		<td>1</td>
        <td>0</td>
        <td>0</td>
		<td>1</td>
    </tr>
	<tr>
        <td>1</td>
		<td>1</td>
        <td>0</td>
        <td>1</td>
		<td>0</td>
		<td>* ( -X1 + -X2 + X3 + -X4 )</td>
	</tr>
	<tr>
        <td>1</td>
		<td>1</td>
        <td>1</td>
        <td>0</td>
		<td>1</td>
    </tr>
	<tr>
        <td>1</td>
		<td>1</td>
        <td>1</td>
        <td>1</td>
		<td>0</td>
		<td>* ( -X1 + -X2 + -X3 + -X4 )</td>
    </tr>
</table>

<table bgcolor="#DCDCDC" border="2">
    <caption >
        Пример ДНФ
    </caption>
    <tr>
        <td width="25px">0</td>
		<td width="25px">0</td>
        <td width="25px">0</td>
        <td width="25px">0</td>
		<td width="25px">0</td>
    </tr>
	<tr>
        <td>0</td>
		<td>0</td>
        <td>0</td>
        <td>1</td>
		<td>1</td>
		<td>( -X1 * -X2 * -X3 * X4 )</td>
    </tr>
	<tr>
        <td>0</td>
		<td>0</td>
        <td>1</td>
        <td>0</td>
		<td>1</td>
		<td>+ ( -X1 * -X2 * X3 * -X4 )</td>
    </tr>
	<tr>
        <td>0</td>
		<td>0</td>
        <td>1</td>
        <td>1</td>
		<td>1</td>
		<td>+ ( -X1 * -X2 * X3 * X4 )</td>
    </tr>
	<tr>
        <td>0</td>
		<td>1</td>
        <td>0</td>
        <td>0</td>
		<td>0</td>
    </tr>
	<tr>
        <td>0</td>
		<td>1</td>
        <td>0</td>
        <td>1</td>
		<td>1</td>
		<td>+ ( -X1 * X2 * -X3 * X4 )</td>
    </tr>
	<tr>
        <td>0</td>
		<td>1</td>
        <td>1</td>
        <td>0</td>
		<td>1</td>
		<td>+ ( -X1 * X2 * X3 * -X4 )</td>
    </tr>
	<tr>
        <td>0</td>
		<td>1</td>
        <td>1</td>
        <td>1</td>
		<td>1</td>
		<td>+ ( -X1 * X2 * X3 * X4 )</td>
    </tr>
	<tr>
        <td>1</td>
		<td>0</td>
        <td>0</td>
        <td>0</td>
		<td>1</td>
		<td>+ ( X1 * -X2 * -X3 * -X4 )</td>
    </tr>
	<tr>
        <td>1</td>
		<td>0</td>
        <td>0</td>
        <td>1</td>
		<td>0</td>
    </tr>
	<tr>
        <td>1</td>
		<td>0</td>
        <td>1</td>
        <td>0</td>
		<td>0</td>
    </tr>
	<tr>
        <td>1</td>
		<td>0</td>
        <td>1</td>
        <td>1</td>
		<td>1</td>
		<td>+ ( X1 * -X2 * X3 * X4 )</td>
    </tr>
	<tr>
        <td>1</td>
		<td>1</td>
        <td>0</td>
        <td>0</td>
		<td>0</td>
    </tr>
	<tr>
        <td>1</td>
		<td>1</td>
        <td>0</td>
        <td>1</td>
		<td>0</td>
	</tr>
	<tr>
        <td>1</td>
		<td>1</td>
        <td>1</td>
        <td>0</td>
		<td>0</td>
    </tr>
	<tr>
        <td>1</td>
		<td>1</td>
        <td>1</td>
        <td>1</td>
		<td>1</td>
		<td>+ ( X1 * X2 * X3 * X4 )</td>
    </tr>
</table>



</body>
</html>
package com.company;

import java.io.*;
import java.util.*;

public class AssemblerWrite {

    ArrayList<String> arrayListOfVariables = new ArrayList<>();

    HashMap<String, String> arrayOfStringVariables = new HashMap<>();

    String start = "format PE console\n" +
            "\n" +
            "entry start\n" +
            "\n" +
            "include 'win32a.inc'\n" +
            "\n" +
            "section '.data' data readable writable\n";

    String variables = "\tspaceStr db '%d', 0\n" +
            "\tdopStr db '%d', 0ah, 0\n";

    String code = "section '.code' code readable executable\n" +
            "\n" +
            "\tstart:\n";

    String finish = "\tfinish:\n" +
            "\n" +
            "\t\tcall [getch]\n" +
            "\n" +
            "\t\tcall [ExitProcess]\n" +
            "\n" +
            "section '.idata' import data readable\n" +
            "\n" +
            "\tlibrary kernel, 'kernel32.dll',\\\n" +
            "\t\tmsvcrt, 'msvcrt.dll'\n" +
            "\n" +
            "\timport kernel,\\\n" +
            "\t\tExitProcess, 'ExitProcess'\n" +
            "\n" +
            "\timport msvcrt,\\\n" +
            "\t\tprintf, 'printf',\\\n" +
            "\t\tscanf, 'scanf',\\\n" +
            "\t\tgetch, '_getch'\n";


    public void write(){

        addAllVariables();

        try(FileWriter writer = new FileWriter("AssemblerProgramm.ASM", false))
        {
            writer.write(start);
            writer.write(variables);
            writer.write(code);
            writer.write(finish);

            writer.flush();
        }
        
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
    }

    public void addToVariables(ArrayList<String> arrayList) {
        arrayListOfVariables = arrayList;
    }

    public void addAllVariables() {
        for (String string : arrayListOfVariables) {
            variables += "\t" + string + " dd ?\n";
        }

        for (String key: arrayOfStringVariables.keySet()) {
            variables += "\t" + key + " db '" + arrayOfStringVariables.get(key) + "', 0\n";
        }
    }

    public void addToCodeWrite(String string) {
        arrayOfStringVariables.put("str" + (arrayOfStringVariables.size() + 1), string);

        code += "\t\tpush " + "str" + arrayOfStringVariables.size() + "\n" +
                "\t\tcall [printf]\n";
    }

    public void addToCodeWriteLn(String string) {
        if (arrayListOfVariables.contains(string)) {

            code += "\t\tpush [" + string + "]\n" +
                    "\t\tpush dopStr\n" +
                    "\t\tcall [printf]\n\n";
        }
    }

    public void addToCodeReadLn(String string) {
        if(arrayListOfVariables.contains(string)){
            code += "\t\tpush " + string + "\n" +
                    "\t\tpush spaceStr\n" +
                    "\t\tcall [scanf]\n\n";
        }
    }

    public void addToCodeOperation(String res, String firstNum, String operator, String secondNum) {

        if (arrayListOfVariables.contains(res) && arrayListOfVariables.contains(firstNum) && arrayListOfVariables.contains(secondNum)) {


            switch (operator) {
                case "+":
                    code += "\t\tmov ecx, [" +  firstNum + "]\n" +
                            "\t\tadd ecx, [" +  secondNum + "]\n" +
                            "\t\tmov [" + res + "], ecx\n";
                    break;
                case "-":
                    code += "\t\tmov ecx, [" +  firstNum + "]\n" +
                            "\t\tsub ecx, [" +  secondNum + "]\n" +
                            "\t\tmov [" + res + "], ecx\n";
                    break;
                case "*":
                    code += "\t\tmov ecx, [" +  firstNum + "]\n" +
                            "\t\timul ecx, [" +  secondNum + "]\n" +
                            "\t\tmov [" + res + "], ecx\n";
                    break;
                case "/":
                    code += "\t\tmov eax, [" +  firstNum + "]\n" +
                            "\t\tmov ecx, [" +  secondNum + "]\n" +
                            "\t\tdiv ecx\n" +
                            "\t\tmov [" + res + "], eax\n";
                    break;
            }
        }
    }
}


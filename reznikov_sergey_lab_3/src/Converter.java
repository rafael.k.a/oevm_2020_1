import java.io.IOException;
import java.util.Arrays;

public class Converter {

    char[] firstnumber;
    char[] secondnumber;
    char[] result;
    char[] flexnumber;
    final int DECIMAL_BASE = 10;
    final int INTEGER_SIZE = 16;
    boolean mistake = false;
    int base;

    private void convertToIntForm() throws IOException {
        if (firstnumber.length > 15 || secondnumber.length > 15) {
            throw new IOException("Number is too large for signed int type");
        }

        char[] tmp = new char[INTEGER_SIZE];

        for (int index = 0; index < tmp.length; index++) {
            tmp[index] = '0';
        }
        for (int index = firstnumber.length - 1; index > 0; index--) {
            tmp[tmp.length - 1 - (firstnumber.length - 1 - index)] = firstnumber[index];
        }

        if (firstnumber[0] == '0') {
            tmp[0] = '0';
        } else {
            tmp[0] = '1';
        }

        firstnumber = tmp;

        tmp = new char[INTEGER_SIZE];

        for (int index = 0; index < tmp.length; index++) {
            tmp[index] = '0';
        }
        for (int index = secondnumber.length - 1; index > 0; index--) {
            tmp[tmp.length - 1 - (secondnumber.length - 1 - index)] = secondnumber[index];
        }

        if (secondnumber[0] == '0') {
            tmp[0] = '0';
        } else {
            tmp[0] = '1';
        }

        secondnumber = tmp;
    }

    public Converter() {
    }

    public Converter(int b, String num1, String num2) throws IOException {
        if (Math.abs(convertToDecimal(num1.toCharArray(), b)) > 32767 || Math.abs(convertToDecimal(num2.toCharArray(), b)) > 32768)
            this.base = b;
        this.firstnumber = findBinary(b, num1);
        this.secondnumber = findBinary(b, num2);
        result = new char[16];
        convertToIntForm();
        prepare();
    }

    public void setNumbers(int b, String num1, String num2) throws IOException {
        this.base = b;
        this.firstnumber = findBinary(b, num1);
        this.secondnumber = findBinary(b, num2);
        result = new char[INTEGER_SIZE];
        convertToIntForm();
        prepare();
    }

    private char[] findBinary(int base1, String num) throws IOException {
        int decimal;

        if (base1 == DECIMAL_BASE) {
            decimal = Integer.parseInt(num);
        } else {
            decimal = convertToDecimal(num.toCharArray(), base1);
        }

        return convertFromDecimalToBinary(decimal).toCharArray();
    }

    public void sumNumbers() throws IOException {
        if (result[0] == flexnumber[0]) {
            sumPoint(result.length - 1, 0);
        } else {
            flexnumber[0] = result[0];
            dimNumbers();
        }
    }

    public void sumNumbersBuffer() {

    }

    public void dimNumbers() throws IOException {
        if (result[0] == flexnumber[0]) {
            DimPoint(result.length - 1, 0);
        } else {
            if (secondnumber[0] == '1') secondnumber[0] = '0';
            else secondnumber[0] = '1';
            prepare();
            sumNumbers();
        }
    }

    private void DimPoint(int index, int i) {
        if (index < 1) {
            return;
        }

        if (i == 0) {
            if (result[index] == flexnumber[index]) {
                result[index] = '0';
                DimPoint(index - 1, 0);
            } else if (result[index] == '1') {
                result[index] = '1';
                DimPoint(index - 1, 0);
            } else {
                result[index] = '1';
                DimPoint(index - 1, 1);
            }
        } else {
            if (result[index] == flexnumber[index]) {
                result[index] = '1';
                DimPoint(index - 1, 1);
            } else if (result[index] == '1') {
                result[index] = '0';
                DimPoint(index - 1, 0);
            } else {
                result[index] = '0';
                DimPoint(index - 1, 1);
            }
        }
    }

    private void prepare() {
        for (int index = 1; index < firstnumber.length; index++) {
            if (firstnumber[index] != secondnumber[index]) {
                if (firstnumber[index] == '0') {
                    result = secondnumber.clone();
                    flexnumber = firstnumber.clone();
                    return;
                } else {
                    result = firstnumber.clone();
                    flexnumber = secondnumber.clone();
                    return;
                }
            }
        }
        result = firstnumber.clone();
        flexnumber = secondnumber.clone();
    }

    private void sumPoint(int index, int i) throws IOException {

        if (index < 1) {
            if (i > 0) {
                throw new IOException("Number is larger then we expected");
            }
            return;
        }

        if (i == 0) {
            if (result[index] != flexnumber[index]) {
                result[index] = '1';
                sumPoint(index - 1, 0);
            } else if (result[index] == flexnumber[index] && flexnumber[index] == '1') {
                result[index] = '0';
                sumPoint(index - 1, 1);
            } else {
                result[index] = '0';
                sumPoint(index - 1, 0);
            }
        } else {
            if (result[index] != flexnumber[index]) {
                result[index] = '0';
                sumPoint(index - 1, 1);
            } else if (result[index] == flexnumber[index] && flexnumber[index] == '1') {
                result[index] = '1';
                sumPoint(index - 1, 1);
            } else {
                result[index] = '1';
                sumPoint(index - 1, 0);
            }
        }
    }

    public int getDecimalResult() {
        return convertFromBinaryToDecimal(result);
    }

    public String getResult() {
        return Arrays.toString(result);
    }

    private int convertFromBinaryToDecimal(char[] num) {
        int base = 2;
        boolean minus = false;
        if (num[0] == '1') {
            minus = true;
        }

        int result = 0;
        for (int index = 1; index < num.length; index++) {
            int tmp;
            tmp = num[index] - '0';
            result += tmp * Math.pow(base, num.length - index - 1);
        }

        if (minus) {
            return -result;
        } else {
            return result;
        }
    }

    private String convertFromDecimalToBinary(int number) {
        int base = 2;
        int num = Math.abs(number);
        boolean minus = false;
        StringBuilder result = new StringBuilder("");

        if (number == 0) {
            return "0";
        }

        if (number < 0) {
            minus = true;
        }

        while (num > 0) {
            int tmp = num % base;
            result.append(tmp);
            num = num / base;
        }

        if (minus) {
            result.append('1');
        } else {
            result.append('0');
        }

        return result.reverse().toString();
    }

    private int convertToDecimal(char[] num, int base) throws IOException {

        boolean minus = false;
        if (num[0] == '-') {
            minus = true;
        }

        int result = 0;
        for (int index = 0; index < num.length; index++) {
            int tmp;
            if (num[index] >= 'A' && num[index] <= 'F') {
                tmp = num[index] - 'A' + 10;
            } else if (num[index] == '-' || num[index] == '+') {
                tmp = 0;
            } else {
                tmp = num[index] - '0';
            }
            if (tmp >= base) {
                throw new IOException("Ввод неверный");
            }
            result += tmp * Math.pow(base, num.length - index - 1);
        }

        if (minus) {
            return -result;
        } else {
            return result;
        }
    }

    public void multiplyNumbers() throws IOException {
        if (result[0] == flexnumber[0]) {
            result[0] = '0';
        } else {
            result[0] = '1';
        }
        int iterations_number = Math.abs(convertFromBinaryToDecimal(flexnumber));
        flexnumber = result.clone();
        for (int count = 1; count < iterations_number; count++) {
            sumPoint(result.length - 1, 0);
        }
    }

    public void splitNumbers() throws IOException {
        result = new char[INTEGER_SIZE];
        Arrays.fill(result, '0');
        flexnumber = secondnumber.clone();
        if (firstnumber[0] == secondnumber[0]) {
            result[0] = '0';
        } else {
            result[0] = '1';
        }

        int res = 0;
        while (true) {
            if (Math.abs(convertFromBinaryToDecimal(result)) < Math.abs(convertFromBinaryToDecimal(firstnumber))) {
                sumPoint(result.length - 1, 0);
                res++;
            } else {
                res--;
                break;
            }
        }

        this.result = convertFromDecimalToBinary(res).toCharArray();
    }
}

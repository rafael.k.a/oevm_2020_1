public class Minimization {

    private int X1 = 0;
    private int X2 = 1;
    private int X3 = 2;
    private int X4 = 3;
    private int F = 4;

    public String disNF(int[][] array, int sizeI, int sizeJ) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < sizeI; i++) {
            if (array[i][F] == 1) {
                builder.append("( ");
                for (int j = 0; j < X4; j++) {
                    if (array[i][j] == 1) {
                        builder.append("X").append(j + 1).append(" * ");
                    } else {
                        builder.append("-X").append(j + 1).append(" * ");
                    }
                }
                if (array[i][X4] == 1) {
                    builder.append("X4");
                } else {
                    builder.append("-X4");
                }
                builder.append(" ) + ");
                builder.append("\n");
            }
        }
        builder.delete(builder.toString().length() - 3, builder.toString().length() - 1);
        return builder.toString();
    }

    public String conNF(int[][] array, int sizeI, int sizeJ) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < sizeI; i++) {
            if (array[i][F] == 0) {
                builder.append("( ");
                for (int j = 0; j < X4; j++) {
                    if (array[i][j] == 0) {
                        builder.append("X").append(j + 1).append(" + ");
                    } else {
                        builder.append("-X").append(j + 1).append(" + ");
                    }
                }
                if (array[i][X4] == 0) {
                    builder.append("X4");
                } else {
                    builder.append("-X4");
                }
                builder.append(" ) * ");
                builder.append("\n");
            }
        }
        builder.delete(builder.toString().length() - 3, builder.toString().length() - 1);
        return builder.toString();
    }
}

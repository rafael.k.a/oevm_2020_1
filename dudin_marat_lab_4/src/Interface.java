import java.util.Random;
import java.util.Scanner;

public class Interface {
    int[][] tableOfTruth;

    private int[][] array() {
        Random random = new Random();
        int[][] array = new int[][]{
                {0,0,0,0,random.nextInt(2)},
                {0,0,0,1,random.nextInt(2)},
                {0,0,1,0,random.nextInt(2)},
                {0,0,1,1,random.nextInt(2)},
                {0,1,0,0,random.nextInt(2)},
                {0,1,0,1,random.nextInt(2)},
                {0,1,1,0,random.nextInt(2)},
                {0,1,1,1,random.nextInt(2)},
                {1,0,0,0,random.nextInt(2)},
                {1,0,0,1,random.nextInt(2)},
                {1,0,1,0,random.nextInt(2)},
                {1,0,1,1,random.nextInt(2)},
                {1,1,0,0,random.nextInt(2)},
                {1,1,0,1,random.nextInt(2)},
                {1,1,1,0,random.nextInt(2)},
                {1,1,1,1,random.nextInt(2)}
        };
        printTable(array);
        return array;
    }

    private void printTable(int[][] tableOfTruth) {
        System.out.println("Таблица истинности: ");
        System.out.println("X1| X2| X3| X4| F");
        for (int[] ints : tableOfTruth) {
            for (int j = 0; j < ints.length; j++) {
                System.out.print(ints[j]);
                if (j != ints.length - 1) {
                    System.out.print(" | ");
                }
            }
            System.out.println();
        }
        System.out.println();
    }

    public void console() {
        Scanner scanner = new Scanner(System.in);
        int digit;
        while (true) {
            tableOfTruth = array();
            manual();
            System.out.print("Введите команду: ");
            digit = scanner.nextInt();

            if (digit == 1) {
                System.out.println(Converter.converterToDNF(tableOfTruth));
                System.out.println("Таблица приведена в ДНФ.");
            } else if (digit == 2) {
                System.out.println(Converter.converterToKNF(tableOfTruth));
                System.out.println("Таблица приведена в КНФ.");
            } else if (digit == 0) {
                tableOfTruth = array();
                System.out.println("Сгенерирована новая таблица истинности.");
            } else if (digit == -1) {
                System.exit(0);
            } else {
                System.out.println("Пожалуйста, введите корректное число.");
                manual();
            }
        }
    }

    private void manual() {
        System.out.println("Для перевода в ДНФ введите 1, в КНФ 2");
        System.out.println("Для генерирования новой таблицы введите 0");
        System.out.println("Для выхода из программы введите -1");
    }
}

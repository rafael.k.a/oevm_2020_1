public class Converter {
    private static final int index = 4;

    public static String converterToDNF(int[][] truthTable) {
        StringBuilder outcome = new StringBuilder("(");
        boolean inProcess = false;
        for (int[] table : truthTable) {
            if (table[index] == 1) {
                if (inProcess) {
                    outcome.append(") + (");
                }
                for (int j = 0; j < table.length - 1; j++) {
                    if (table[j] == 0) {
                        outcome.append("-");
                    }
                    outcome.append("X").append(j + 1);
                    if (j != table.length - 2) {
                        outcome.append(" * ");
                    }
                }
                inProcess = true;
            }
        }
        outcome.append(")");
        return outcome.toString();
    }

    public static String converterToKNF(int[][] truthTable) {
        StringBuilder outcome = new StringBuilder("(");
        boolean inProcess = false;
        for (int[] table : truthTable) {
            if (table[index] == 0) {
                if (inProcess) {
                    outcome.append(") * (");
                }
                for (int j = 0; j < table.length - 1; j++) {
                    if (table[j] == 1) {
                        outcome.append("-");
                    }
                    outcome.append("X").append(j + 1);
                    if (j != table.length - 2) {
                        outcome.append(" + ");
                    }
                }
                inProcess = true;
            }
        }
        outcome.append(")");
        return outcome.toString();
    }
}
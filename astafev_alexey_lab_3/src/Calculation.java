public class Calculation {
    private String firstDigit;
    private String secondDigit;
    private String firstDigitTwo;
    private String secondDigitTwo;

    private int firstNumber = 0;
    private int secondNumber = 0;

    private char operation;

    public Calculation(int systemNumber, String firstDigit, String secondDigit, char operation) {

        this.firstDigit = firstDigit;
        this.secondDigit = secondDigit;
        this.operation = operation;

        conversionToDecimalNotation(systemNumber);
        conversionToBinaryNotation();
    }

    private void conversionToBinaryNotation() {
        StringBuilder strFirstDigit = new StringBuilder();
        StringBuilder strSecondDigit = new StringBuilder();

        if (firstNumber == 0) {
            strFirstDigit.append("0");
        }

        for (int i = 0; firstNumber > 0; i++) {

            strFirstDigit.append((char) (firstNumber % 2 + '0'));
            firstNumber /= 2;
        }

        firstDigitTwo = strFirstDigit.toString();

        if (secondNumber == 0) {
            strSecondDigit.append("0");
        }

        for (int i = 0; secondNumber > 0; i++) {

            strSecondDigit.append((char) (secondNumber % 2 + '0'));
            secondNumber /= 2;
        }

        secondDigitTwo = strSecondDigit.toString();
    }

    private void conversionToDecimalNotation(int systemNumber) {
        char[] firstDigitArray = firstDigit.toCharArray();
        char[] secondDigitArray = secondDigit.toCharArray();
        for (int i = 0; i < firstDigitArray.length; i++) {
            int temp;
            if (firstDigitArray[i] >= '0' && firstDigitArray[i] <= '9') {
                temp = firstDigitArray[i] - '0';
            } else {
                temp = 10 + firstDigitArray[i] - 'A';
            }
            firstNumber += temp * Math.pow(systemNumber, firstDigitArray.length - i - 1);
        }

        for (int i = 0; i < secondDigitArray.length; i++) {
            int temp;
            if (secondDigitArray[i] >= '0' && secondDigitArray[i] <= '9') {
                temp = secondDigitArray[i] - '0';
            } else {
                temp = 10 + secondDigitArray[i] - 'A';
            }
            secondNumber += temp * Math.pow(systemNumber, secondDigitArray.length - i - 1);
        }
    }

    private Boolean compareTheNumbers(String str1, String str2) {
        char[] strArray1 = new StringBuilder(str1).reverse().toString().toCharArray();
        char[] strArray2 = new StringBuilder(str2).reverse().toString().toCharArray();

        if (strArray1.length > strArray2.length) {
            return true;
        }
        if (strArray1.length < strArray2.length) {
            return false;
        }
        if (strArray1.length == strArray2.length) {

            for (int i = 0; i < strArray1.length; i++) {

                if (strArray1[i] == '1' && strArray2[i] == '0') {
                    return true;
                }
                if (strArray1[i] == '0' && strArray2[i] == '1') {
                    return false;
                }
            }
        }
        return true;
    }

    public String methodCalculation() {
        StringBuilder str = new StringBuilder();
        switch (operation) {
            case '+':
                str.append(addingNumbers(firstDigitTwo, secondDigitTwo));
                return str.reverse().toString();
            case '-':
                str.append(subtractionOfNumbers(firstDigitTwo, secondDigitTwo)).reverse();
                return deletionOfZeros(str.toString());
            case '*':
                str.append(multiplicationOfNumbers(firstDigitTwo, secondDigitTwo)).reverse();
                return str.toString();
            case '/':
                str.append(divisionOfNumbers(firstDigitTwo, secondDigitTwo)).reverse();
                return str.toString();
        }
        return "error";
    }

    private String addingNumbers(String str1, String str2) {

        char[] firstDigitArray = str1.toCharArray();
        char[] secondDigitArray = str2.toCharArray();

        StringBuilder result = new StringBuilder();

        transferOfTheNumber(firstDigitArray,secondDigitArray,result);

        return result.toString();
    }

    private void transferOfTheNumber( char[] firstDigitArray, char[] secondDigitArray, StringBuilder result){
        int transfer = 0;

        for(int i = 0; i < secondDigitArray.length; i++) {
            if (transfer == 0) {
                if (firstDigitArray[i] == '0' && secondDigitArray[i] == '0') {
                    result.append("0");
                    transfer = 0;
                }
                if ((firstDigitArray[i] == '0' && secondDigitArray[i] == '1') || (firstDigitArray[i] == '1' && secondDigitArray[i] == '0')) {
                    result.append("1");
                    transfer = 0;
                }
                if (firstDigitArray[i] == '1' && secondDigitArray[i] == '1') {
                    result.append("0");
                    transfer = 1;
                }
            } else {
                if (firstDigitArray[i] == '0' && secondDigitArray[i] == '0') {
                    result.append("1");
                    transfer = 0;
                }
                if ((firstDigitArray[i] == '0' && secondDigitArray[i] == '1') || (firstDigitArray[i] == '1' && secondDigitArray[i] == '0')) {
                    result.append("0");
                    transfer = 1;
                }
                if (firstDigitArray[i] == '1' && secondDigitArray[i] == '1') {
                    result.append("1");
                    transfer = 1;
                }
            }
        }

        for(int i = firstDigitArray.length; i < secondDigitArray.length; i++) {
            if (firstDigitArray[i] == '0' && transfer == 0) {
                result.append("0");
                transfer = 0;
            }
            if ((firstDigitArray[i] == '0' && transfer == 1) || (firstDigitArray[i] == '1' && transfer == 0)) {
                result.append("1");
                transfer = 0;
            }
            if (firstDigitArray[i] == '1' && transfer == 1) {
                result.append("0");
                transfer = 1;
            }
            i++;
        }
        if (transfer == 1) {
            result.append("1");
        }
    }

    private String subtractionOfNumbers(String str1, String str2) {

        StringBuilder result = new StringBuilder();
        str2 = reverseOfNumbers(str2);
        result.append(addingNumbers(str1, str2));
        result.deleteCharAt(result.length() - 1);
        return result.toString();
    }

    private String multiplicationOfNumbers(String str1, String str2) {

        char[] strArray2 = str2.toCharArray();
        StringBuilder strArray1 = new StringBuilder(str1);
        String result = "0";
        int iIndex = 0;
        for (int i = 0; i < strArray2.length; i++) {
            if (strArray2[i] == '1') {
                for (int j = 0; j < i - iIndex; j++) {
                    strArray1.insert(0, '0');

                }
                result = addingNumbers(strArray1.toString(), result);
                iIndex = i;
            }
        }
        return result;
    }

    private String divisionOfNumbers(String str1, String str2) {
        String comparison = str2;
        String result = "0";

        if (str2.toCharArray().length == 1 && str2.toCharArray()[0] != '1') {
            return "Error!";
        }
        while (compareTheNumbers(str1, comparison)) {
            result = addingNumbers(result, "1");
            comparison = addingNumbers(comparison, str2);
        }
        return result;

    }

    private String reverseOfNumbers(String str) {

        StringBuilder string = new StringBuilder();
        StringBuilder result = new StringBuilder();
        char[] digitArray = str.toCharArray();

        int i = 0;
        while (i < secondDigitTwo.length()) {
            if (digitArray[i] == '1') {
                string.append("0");
            } else {
                string.append("1");
            }
            i++;
        }
        while (i < firstDigitTwo.length()) {
            string.append("1");
            i++;
        }
        result.append(addingNumbers(string.toString(), "1"));
        return result.toString();
    }

    private String deletionOfZeros(String str) {
        StringBuilder result = new StringBuilder();
        char[] digitArray = str.toCharArray();
        int i = 0;
        while (digitArray[i] == '0') {
            i++;
            if (i == str.length()) {
                return "0";
            }
        }
        while (i < str.length()) {
            result.append(digitArray[i]);
            i++;
        }
        return result.toString();
    }
}
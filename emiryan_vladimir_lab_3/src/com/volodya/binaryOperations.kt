package com.volodya

fun sum(firstInputDigit: String, secondInputDigit: String): String {
    var result: String = "";
    var firstDigit: String = firstInputDigit
    var secondDigit: String = secondInputDigit
    var extraBit: Boolean = false;

    if(secondDigit == isOver(firstDigit, secondDigit)) {
        val temp = secondDigit;
        secondDigit = firstDigit;
        firstDigit = temp;
    }

    for(i in 0 until firstDigit.length - secondDigit.length) {
        secondDigit = "0$secondDigit"
    }

    for(i in  isOver(firstDigit, secondDigit).length - 1 downTo 0) {
        var plusStatement: String = firstDigit[i].toString() + secondDigit[i].toString()
        when(plusStatement){
            "00" -> {
                if (!extraBit) {
                    result += "0"
                } else {
                    result += "1"
                    extraBit = false
                }
            }
            "01", "10" -> {
                if (!extraBit) {
                    result += "1"
                } else {
                    result += "0"
                }
            }
            "11" -> {
                if (!extraBit) {
                    result += "0"
                    extraBit = true
                } else {
                    result += "1"
                }
            }
        }
    }
    if(extraBit) {
        result += "1";
    }

    return result.reversed()
}

fun diff(firstInputDigit: String, secondInputDigit: String): String {
    var result: String = "";
    var firstDigit: String = firstInputDigit
    var secondDigit: String = secondInputDigit
    var extraBit: Boolean = false;

    if(secondDigit == isOver(firstDigit, secondDigit)) {
        val temp = secondDigit;
        secondDigit = firstDigit;
        firstDigit = temp;
    }

    for(i in 0 until firstDigit.length - secondDigit.length) {
        secondDigit = "0$secondDigit"
    }

    for(i in  isOver(firstDigit, secondDigit).length - 1 downTo 0) {
        var minusStatement: String = firstDigit[i].toString() + secondDigit[i].toString()
        when(minusStatement){
            "00" -> {
                if (!extraBit) {
                    result += "0"
                } else {
                    result += "1"
                }
            }
            "01" -> {
                if (!extraBit) {
                    result += "1"
                    extraBit = true
                } else {
                    result += "0"
                    extraBit = false
                }
            }
            "10" -> {
                if (!extraBit) {
                    result += "1"
                } else {
                    result += "0"
                    extraBit = false
                }
            }
            "11" -> {
                if (!extraBit) {
                    result += "0"
                } else {
                    result += "1"
                    extraBit = true
                }
            }
        }
    }
    result = result.dropLastWhile{
        it == '0'
    }

    return result.reversed()
}

fun multiply(firstInputDigit: String, secondInputDigit: String): String {
    var result: String = ""
    var firstDigit: String = firstInputDigit
    var secondDigit: Long = toDecimal(secondInputDigit, 2)
    for(i in 1..secondDigit) {
        result = sum(result, firstDigit)
    }
    return result
}

fun divide(firstInputDigit: String, secondInputDigit: String): String {
    var difference: Int = 0;
    var result: String = firstInputDigit
    var divider: String = secondInputDigit
    var count: Long = toDecimal(secondInputDigit, 2)
    while (isBigger(result, divider)) {
        result = diff(result, divider)
        difference++
    }
    difference++
    return toX(difference.toLong(), 2)
}
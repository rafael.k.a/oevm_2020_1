package com.company;
import java.awt.*;
import java.util.Scanner;
import java.lang.StringBuffer;

public class Main {
    static final char[] SYMBOL = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    public static long numberToTen(String result, long finalNumber, int originalNumberSystem) {
        for (int i = 0; i<result.length();i++) { // перевожу число в десятеричную систему счисления
            for (int j = 0; j<=15;j++) {
                if(result.charAt(i) == SYMBOL[j]) { // проверяю, если символы сходятся, считаю число в десятичной системе
                    finalNumber += j * Math.pow(originalNumberSystem,i); // перебираю числа для того, чтобы получилось десятичное число
                }
            }
        }
        return finalNumber;
    }

    public static String convert(long finalNumber, int finalNumberSystem, String notFinalResult, char symbolNFS) {
        while(((finalNumber / finalNumberSystem != 0)) || (finalNumber % finalNumberSystem != 0)) { // пока остаток не ноль делаю рил ток вещи
            if (finalNumber % finalNumberSystem >= 10) { // если получается символ, то заходим в оператор (IF)
                symbolNFS = (char) ((finalNumber % finalNumberSystem) + 55); // пытаюсь использовать ASCII
                // 55 = A - 10 (остаток от деления)
                notFinalResult += symbolNFS; // прибавляю символ аски
                finalNumber = finalNumber / finalNumberSystem; // делю нацело, чтобы цикл не заклинило
                continue;
            }
            notFinalResult += finalNumber % finalNumberSystem; // строка к которой на каждой
            // итерации прибавляется остаток от деления
            finalNumber = finalNumber / finalNumberSystem; // делю нацело, чтобы цикл не заклинило
        }
        return notFinalResult;
    }

    public static void main(String[] args) {
	// write your code here
        System.out.println("Input a number of original number system: "); // обычный вывод на консоль, ничего необычного
        Scanner original = new Scanner(System.in); // сканер сканирует (считывает)
        int originalNumberSystem = original.nextInt(); // вводим изначальное основание

        System.out.println("Input a number of final number system: "); // обычный вывод на консоль, ничего необычного
        Scanner finalN = new Scanner(System.in); // сканер сканирует (считывает)
        int finalNumberSystem = finalN.nextInt(); // вводим конечное основание

        System.out.println("Input a number: "); // обычный вывод на консоль, ничего необычного
        Scanner number = new Scanner(System.in); // сканер сканирует (считывает)
        String justNumber = number.nextLine(); // вводим исходное число
        String result = new StringBuffer(justNumber).reverse().toString(); // строка, в которой запишу перевёрнутую изначальную строку

        long finalNumber = 0; // Нужное мне число (в десятичной системе, из которой я всё буду переводить в другие)
        String notFinalResult = ""; // нужная мне строка, которую в конце переворачиваю
        char symbolNFS = ' '; // чаровая переменная, так как аски со строками не работают

        finalNumber = numberToTen(result, finalNumber, originalNumberSystem); //функция перевода в 10 систему
        notFinalResult = convert(finalNumber, finalNumberSystem, notFinalResult, symbolNFS);//функция конвертирования

        String finalResult = new StringBuffer(notFinalResult).reverse().toString();
        System.out.println("Final Number: "+finalResult); // вывод финального результата строки
    }
}

import java.io.FileReader;
import java.io.IOException;

public class Reader {
    public String read(){
        StringBuilder stringBuilder = new StringBuilder();

        try(FileReader reader = new FileReader("Pascal.pas")) {
            int ch;
            while((ch = reader.read()) != - 1) {
                stringBuilder.append((char) ch);
            }
        }
        catch(IOException ex) {
            System.out.println(ex.getMessage());
        }
        return stringBuilder.toString().replaceAll(";", ";\n");
    }
}

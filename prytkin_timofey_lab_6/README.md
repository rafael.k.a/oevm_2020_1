## Лабораторная работа №6 "Основы программирования на языке ассемблера"

Выполнил студент группы **ПИбд-22 Прыткин Тимофей**

### Задание:

*Разработать с помощью ассемблера FASM (http://flatassembler.net/)
 программу для сложения, вычитания, умножения и деления двух целых
 чисел. Данные могут быть введены с клавиатуры либо заданы в виде
 констант. Результатом работы программы служит текстовый
 вывод следующего вида:*
 
 X = <num1>;  
 Y = <num2>;  
 X + Y = <res1>;  
 X – Y = <res2>;  
 X * Y = <res3>;  
 X / Y = <res4>;  

---------------------------------------------------------------

**Чтобы запустить лабораторную работу нужно запустить файл prytkin_timofey_lab_6.EXE*

При выполнении лабораторной раблоты использовался язык программирования **Assembler** и среда разработки **Flat assembler**

**Входные данные:**
- *Два целых числа X и Y*

**Результат программы:**
- *Выводится сумма, разность, произведение и частное чисел X и Y*

---------------------------------------------------------------

**Тесты:**

| X | Y | X + Y | X – Y | X * Y | X / Y |
| :---: | :---: | :---: | :---: | :---: | :---: |
| 5 | 5 | 10 | 0 | 25 | 1 |
| 167 | 3 | 170 | 164 | 501 | 55 |
| 45 | -64 | -19| 109 | -2880 | 0 |

---------------------------------------------------------------

[Ссылка на видео](https://yadi.sk/i/poGVP1coOJOisg)

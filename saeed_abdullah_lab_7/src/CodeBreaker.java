import java.util.ArrayList;
import java.util.regex.*;

public class CodeBreaker {
    TO_Fasm Assembler_Maker;
    private String Code_String;
    private String Variables_String;
    private String Body_String;


    private ArrayList<String> Variables_Array = new ArrayList<>();
    private ArrayList<String> Codes_Array = new ArrayList<>();
    private ArrayList<String> Variables = new ArrayList<>();

    public CodeBreaker(String str, TO_Fasm Assembler_Maker) {
        this.Code_String = str;
        this.Assembler_Maker = Assembler_Maker;
    }

    public void Code_Hasher() {

        Pattern patternForVariable = Pattern.compile("(?<=var)[\\s\\S]*(?=begin)");
        Matcher matcherVariables = patternForVariable.matcher(Code_String);

        while (matcherVariables.find()) {
            Variables_String = Code_String.substring(matcherVariables.start(), matcherVariables.end());
        }

        Pattern patternBody = Pattern.compile("(?<=begin)[\\s\\S]*(?=end.)");
        Matcher matcherBody = patternBody.matcher(Code_String);

        while (matcherBody.find()) {
            Body_String = Code_String.substring(matcherBody.start(), matcherBody.end());
        }

        Variables_Hasher();
        Assembler_Maker.addToVarList(Variables);

        Body_Hasher();
    }

    public void Variables_Hasher() {

        Pattern patternDefineVariables = Pattern.compile("([a-zA-Z]([a-zA-Z0-9_]*[\\s]*,[\\s]*)*)([a-zA-Z][a-zA-Z0-9_]*)[\\s]*:[\\s]*integer[\\s]*;");
        Matcher matcherDefineVariables = patternDefineVariables.matcher(Variables_String);

        while (matcherDefineVariables.find()) {
            Variables_Array.add(Variables_String.substring(matcherDefineVariables.start(), matcherDefineVariables.end()));
        }

        Pattern patternVariables = Pattern.compile("[a-zA-Z][a-zA-Z0-9_]*");
        Matcher matcherVariables;
        String var;

        for (String string : Variables_Array) {

            matcherVariables = patternVariables.matcher(string);
            while (matcherVariables.find()) {
                var = string.substring(matcherVariables.start(), matcherVariables.end());
                if (!var.equals("integer")) { Variables.add(var); }
            }
        }
    }

    public void Body_Hasher() {

        Pattern patternBody = Pattern.compile("[a-zA-Z].*;");
        Matcher matcherBody = patternBody.matcher(Body_String);

        while (matcherBody.find()) {
            Codes_Array.add(Body_String.substring(matcherBody.start(), matcherBody.end()));
        }

        Body_Maker();
    }

    public void Body_Maker() {

        Pattern patternWrite = Pattern.compile("write\\('(.*)'\\);");
        Pattern patternWriteLn = Pattern.compile("writeln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");

        Pattern patternReadLn = Pattern.compile("readln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");
        Pattern patternOperation = Pattern.compile("([a-z][a-zA-Z0-9]*)[\\s]*[\\s]*:=[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*([+\\-*/])[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*;");

        for (String string : Codes_Array) {

            if (string.matches(patternWrite.toString())) {
                Matcher matcherForCode = patternWrite.matcher(string);

                if (matcherForCode.find()) {
                    Assembler_Maker.addWrite(matcherForCode.group(1));
                }

            } else if (string.matches(patternReadLn.toString())) {
                Matcher matcherForCode = patternReadLn.matcher(string);

                if (matcherForCode.find()) {
                    Assembler_Maker.addReadLn(matcherForCode.group(1));
                }

            } else if (string.matches(patternWriteLn.toString())) {
                Matcher matcherForCode = patternWriteLn.matcher(string);

                if (matcherForCode.find()) {
                    Assembler_Maker.addWriteLn(matcherForCode.group(1));
                }

            } else if (string.matches(patternOperation.toString())) {
                Matcher matcherForCode = patternOperation.matcher(string);
                if (matcherForCode.find()) {
                    Assembler_Maker.addOperation(matcherForCode.group(1), matcherForCode.group(2), matcherForCode.group(3), matcherForCode.group(4));
                }
            }
        }
    }
}

public class Main {

    public static void main (String string[]){

        From_Pascal InPascal = new From_Pascal();
        TO_Fasm assembler_Maker = new TO_Fasm();
        CodeBreaker code_hasher = new CodeBreaker(InPascal.read(), assembler_Maker);
        code_hasher.Code_Hasher();
        assembler_Maker.write();
    }

}

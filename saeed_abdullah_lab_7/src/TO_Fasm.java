import java.io.*;
import java.util.*;

public class TO_Fasm {

    ArrayList<String> Variables_List = new ArrayList<>();

    HashMap<String, String> Variables_String_Array = new HashMap<>();

    String start = "format PE console\n" +
            "entry start\n" +
            "\n" +
            "include 'c:\\FASM\\include\\win32a.inc'\n";

    String library = "\nsection '.idata' import data readable\n" +
            "\n" +
            "\tlibrary kernel, 'kernel32.dll',\\\n" +
            "\t\tmsvcrt, 'msvcrt.dll'\n" +
            "\n" +
            "\timport kernel,\\\n" +
            "\t\tExitProcess, 'ExitProcess'\n" +
            "\n" +
            "\timport msvcrt, printf, 'printf',\\\n" +
            "\t\tscanf, 'scanf',\\\n" +
            "\t\tgetch, '_getch'\n";

    String variables = "\nsection '.data' data readable writable\n" +
            "\n" +
            "\tspaceStr db '%d', 0\n" +
            "\tdopStr db '%d', 0ah, 0\n" +
            "\temptyStr db '%d', 0\n" +
            "\tpoint db ',', 0\n" +
            "\tspace db ' ',0dh, 0ah, 0\n";

    String code = "\nsection '.code' code readable executable\n" +
            "\n" +
            "\tstart:\n";

    String finish = "\n" +
            "\t\tcall [getch]\n" +
            "\n" +
            "\t\tpush 0\n" +
            "\t\tcall [ExitProcess]\n" +
            "\n";

    public void write(){

        addEntireVariables();

        try(FileWriter fw = new FileWriter("Calculator.ASM", false))
        {
            fw.write(start);
            fw.write(library);
            fw.write(variables);
            fw.write(code);
            fw.write(finish);
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
    }

    public void addEntireVariables() {
        for (String string : Variables_List) {
            variables += "\t" + string + " dd ?\n";
        }
        for (String key: Variables_String_Array.keySet()) {
            variables += "\t" + key + " db '" + Variables_String_Array.get(key) + "', 0\n";
        }


    }

    public void addToVarList(ArrayList<String> arrayList) {
        Variables_List = arrayList;
    }

    public void addReadLn(String string) {

        if(Variables_List.contains(string)){
            code += "\t\tpush " + string + "\n" +
                    "\t\tpush spaceStr\n" +
                    "\t\tcall [scanf]\n\n";
        }
    }

    public void addWrite(String string) {
        Variables_String_Array.put("str" + (Variables_String_Array.size() + 1), string);

        code += "\t\tpush " + "str" + Variables_String_Array.size() + "\n" +
                "\t\tcall [printf]\n";
    }

    public void addWriteLn(String string) {
        if (Variables_List.contains(string)) {

            code += "\t\tpush [" + string + "]\n" +
                    "\t\tpush dopStr\n" +
                    "\t\tcall [printf]\n\n";
        }
    }

    public void addOperation(String res, String firstNum, String operator, String secondNum ) {

        if (Variables_List.contains(res) && Variables_List.contains(firstNum) && Variables_List.contains(secondNum)) {

            switch (operator) {
                case "+":
                    code += "\t\tmov ecx, [" +  firstNum + "]\n" +
                            "\t\tadd ecx, [" +  secondNum + "]\n" +
                            "\t\tmov [" + res + "], ecx\n";
                    break;
                case "-":
                    code += "\t\tmov ecx, [" +  firstNum + "]\n" +
                            "\t\tsub ecx, [" +  secondNum + "]\n" +
                            "\t\tmov [" + res + "], ecx\n";
                    break;
                case "*":
                    code += "\t\tmov ecx, [" +  firstNum + "]\n" +
                            "\t\timul ecx, [" +  secondNum + "]\n" +
                            "\t\tmov [" + res + "], ecx\n";
                    break;
                case "/":
                    code += "\t\t mov eax, [" + firstNum + "]\n" +
                            "\t\t mov ecx, [" + secondNum + "]\n" +
                            "\t\t div ecx\n" +
                            "\t\t mov [" + res + "], eax\n";

                    break;
            }
        }
    }
}

import java.util.Scanner;

public class Main {
    /**
     * Method to transform from all numerals system to decimal
     */
    private static int convertTodic(int val, String num) {
        String Number = num.toUpperCase();
        char[] number = Number.toCharArray();

        int counter = 0;
        for (int i = 0; i < number.length; i++) {
            int T=0;
             //Convert binary and octal to decimal.
            if (number[i] >= '0' && number[i] <= '9') {
                T = number[i] - 48;
            }
             //Convert hex to decimal.
            else if(number[i] >= 'A' && number[i] <= 'F') {
                T = number[i] - 55;
            }
            counter += T * Math.pow(val, number.length - i - 1);
        }
        return counter;
    }
    /**
     * Method to transform from decimal system to all numerals system.
     */
    private static String convertToall(int val, int T, String num) {
        StringBuilder str = new StringBuilder();
        int coun = convertTodic(val, num);
        if (coun == 0) {
            str.append("0");
            str.toString();
        }
        int convert_for_2_8 = 48 , convert_for_16 = 55;
        for (int i = 0; coun > 0; i++) {
            //Convert decimal to binary and octal.
            if (coun % T <= 9) {
                str.append((char) (coun % T + convert_for_2_8)) ;
            }
            //Convert decimal to hex.
            else if (coun % T <= 15){
                str.append((char) (coun % T + convert_for_16)) ;
            }
            coun /= T;
        }
        return str.reverse().toString();
    }

    public static void main (String string[]) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter the input numeral system: ");
        int var = input.nextInt();
        System.out.print("Enter the output numeral system: ");
        int var_2 = input.nextInt();
        System.out.print("Enter the number for convert: ");
        String num = input.next();
        System.out.print("The Result: ");
        String res =convertToall(var, var_2, num);
        System.out.print(res);

    }
}

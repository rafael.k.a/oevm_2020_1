import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class DrawPanel extends JPanel {

    private TableOfTruth tableOfTruth;
    private ArrayList<DrawElements> drawElements;
    private final JFrame frame;

    private final int cellSize = 40;
    private final int margin = 30;
    private final int rows = 17;
    private final int columns = 5;

    public DrawPanel(JFrame frame) {
        tableOfTruth = new TableOfTruth();
        drawElements = new ArrayList<>();
        this.frame = frame;
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g2.setColor(Color.white);
        g2.fillRect(0, 0, frame.getWidth(), frame.getHeight());

        drawTable(g2);
        fillTable(g2);
        drawStrings(g2);
    }

    private void drawTable(Graphics g) {
        g.setColor(Color.BLACK);
        g.setFont(new Font("Calibri", Font.PLAIN, 22));
        g.drawString("Table of truth", margin + 40, margin + 10);
        for (int i = 0; i <= rows; i++) {
            g.drawLine(margin, margin * 2 + i * cellSize, margin + cellSize * columns, margin * 2 + i * cellSize);
            for (int j = 0; j <= columns; j++) {
                g.drawLine(margin + j * cellSize, margin * 2, margin + j * cellSize, margin * 2 + cellSize * rows);
            }
        }
    }

    private void fillTable(Graphics g) {
        g.setFont(new Font("Calibri", Font.PLAIN, 22));
        g.setColor(Color.BLACK);

        int littlePosFix = 7;
        g.drawString("X1", margin + littlePosFix, cellSize * 2 + littlePosFix);
        g.drawString("X2", margin + littlePosFix + cellSize, cellSize * 2 + littlePosFix);
        g.drawString("X3", margin + littlePosFix + cellSize * 2, cellSize * 2 + littlePosFix);
        g.drawString("X4", margin + littlePosFix + cellSize * 3, cellSize * 2 + littlePosFix);
        g.drawString("F", margin + littlePosFix * 2 + cellSize * 4, cellSize * 2 + littlePosFix);

        for (int i = 0; i < rows - 1; i++) {
            for (int j = 0; j < columns; j++) {
                g.setFont(new Font("Calibri", Font.PLAIN, 24));
                if (j == columns - 1) {
                    g.setFont(new Font("Calibri", Font.BOLD, 24));
                }
                g.drawString(Integer.toString(tableOfTruth.getTableOfTruth()[i][j]), margin + littlePosFix * 2 + j * cellSize, margin * 4 + littlePosFix + i * cellSize);
            }
        }
    }

    private void drawStrings(Graphics g) {
        g.setFont(new Font("Calibri", Font.PLAIN, 24));
        for (DrawElements animatedString : drawElements) {
            g.drawString(animatedString.getString(), 260, animatedString.getPosY());
        }
    }

    public void refreshTable() {
        tableOfTruth = new TableOfTruth();
        drawElements = new ArrayList<>();
        Converter.refreshCount();
    }

    public void fillStrings() {
        for (int i = 0; i < 16; i++) {
            String result = Converter.convertTable(tableOfTruth.getTableOfTruth());
            if (!result.equals("")) {
                drawElements.add(new DrawElements(result, 5 + (drawElements.size() + 3) * 40));
            }
        }
        drawStringElements();
        frame.repaint();
    }

    private void drawStringElements() {
        int marginForStrings = 85;
        drawElements.add(new DrawElements("DNF:", cellSize));
        DrawElements stringAddLastSymbol = null;
        int count = 0;
        for (DrawElements string : drawElements) {
            if (string.getString().indexOf('*') != -1) {
                string.setPosY(count * cellSize + marginForStrings);
                if (stringAddLastSymbol != null) {
                    stringAddLastSymbol.addPlusToDNF();
                }
                stringAddLastSymbol = string;
                count++;
            }
        }
        drawElements.add(new DrawElements("KNF:", count * cellSize + marginForStrings));
        count++;
        stringAddLastSymbol = null;
        for (DrawElements string : drawElements) {
            if (string.getString().indexOf('+') != -1 && !(string.getString().indexOf('+') == string.getString().length() - 1)) {
                string.setPosY(count * cellSize + marginForStrings);
                if (stringAddLastSymbol != null) {
                    stringAddLastSymbol.addMultiplyToKNF();
                }
                stringAddLastSymbol = string;
                count++;
            }
        }
    }
}

public class Main {

    public static void main(String[] args) {

        Pascal pascal = new Pascal();
        Assembler assembler = new Assembler();

        Parser parser = new Parser(pascal.read(), assembler);
        parser.parseProgram();
        assembler.createCode();
    }
}

package com.company;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // write your code here
        int startSS;
        int endSS;
        String numStartString;

        Scanner in = new Scanner(System.in);

        System.out.print("Введите исходную с.с.: ");
        startSS = in.nextInt();

        System.out.print("Введите конечную с.с.: ");
        endSS = in.nextInt();

        System.out.print("Введите число в исходной с.с.: ");
        numStartString = in.next();

        TransferSS transfer = new TransferSS(startSS, endSS, numStartString);
        transfer.transferSS();
    }
}

package com.company;

import org.w3c.dom.ls.LSOutput;

import java.math.BigInteger;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        System.out.println((char)('0' + '0'));


        String inputData;
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.println("Введите исходную СС (2-16)");
            int in1 = scanner.nextInt();
            System.out.println("Введите конечную СС (2-16)");
            int in2 = scanner.nextInt();
            System.out.println("Введите число в исходной СС");
            String in3 = scanner.next();
            try {
                System.out.println("Число в " + in2 + " СС: " + NumberSystemConverter.convert(in3, in1, in2));
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println("Продолжить? ");
            inputData = scanner.next();
        } while (!inputData.equals("EXIT"));


    }
}


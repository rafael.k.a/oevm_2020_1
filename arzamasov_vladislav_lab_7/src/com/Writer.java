package com;

import java.io.FileWriter;
import java.util.ArrayList;

public class Writer {
    private static ArrayList<String> peremenList = new ArrayList<>();
    private static ArrayList<String> commandList = new ArrayList<>();
    private static ArrayList<String> readCommandList = new ArrayList<>();
    private static ArrayList<String> writeCommandList = new ArrayList<>();
    private static ArrayList<String> matematicCommandList = new ArrayList<>();
    public Writer(ArrayList<String> peremen, ArrayList<String> command, ArrayList<String> read, ArrayList<String> write, ArrayList<String> maths){
        peremenList = peremen;
        commandList = command;
        readCommandList = read;
        writeCommandList = write;
        matematicCommandList = maths;
        writeASMFile();
    }

    public static void writeASMFile() {
        try (FileWriter writer = new FileWriter("program.ASM")) {
            writer.write("format PE console\n\n" +
                    "entry start\n\n" +
                    "\tinclude 'win32a.inc'\n\n" +
                    "section '.data' data readable writable\n\n" +
                    "\tspaceString db '%d', 0\n" +
                    "\tstrSymbol db 'Enter symbol:', 0\n" +
                    "\tsum db 'X + Y = %d', 0dh, 0ah, 0 \n" +
                    "\tsubtraction  db 'X - Y = %d', 0dh, 0ah, 0\n" +
                    "\tmultiplication db 'X * Y = %d', 0dh, 0ah, 0\n" +
                    "\tdividing db 'X / Y = %d', 0dh, 0ah, 0\n" +
                    "\tnewLine db '%d', 0ah, 0\n");

            while (!peremenList.isEmpty()) {
                writer.write("\t" + peremenList.remove(0));
            }

            writer.write("\nsection '.code' data readable executable\n\n" +
                    "\tstart:\n\n");

            while (!commandList.isEmpty()) {
                switch (commandList.remove(0)) {
                    case "READ" -> writer.write("\tpush strSymbol\n" +
                            "\tcall [printf]\n\n" +
                            "\tpush " + readCommandList.remove(0) + "\n" +
                            "\tpush spaceString\n" +
                            "\tcall [scanf]\n\n");
                    case "WRITE" -> writer.write("\tpush " + writeCommandList.remove(0) + "\n" +
                            "\tcall [printf]\n\n");
                    case "MATHS" -> {
                        String command = matematicCommandList.remove(0);
                        String[] arguments = command.split(" ");
                        if (!command.contains("div")) {
                            writer.write("\tmov ecx, [" + arguments[1] + "]\n" +
                                    "\t" + arguments[3] + " ecx, [" + arguments[2] + "]\n" +
                                    "\tmov [" + arguments[0] + "], ecx\n\n");
                        } else {
                            writer.write("\tmov eax, [" +  arguments[1] + "]\n" +
                                    "\tmov ecx, [" + arguments[2] + "]\n" +
                                    "\tidiv ecx\n" +
                                    "\tmov [" + arguments[0] + "], eax\n\n");
                        }
                    }
                }
            }

            writer.write("\n\tpush [res1]\n"+
                    "\tpush sum\n"+
                    "\tcall [printf]\n\n"+
                    "\tpush [res2]\n"+
                    "\tpush subtraction\n"+
                    "\tcall [printf]\n\n"+
                    "\tpush [res3]\n"+
                    "\tpush multiplication\n"+
                    "\tcall [printf]\n\n"+
                    "\tpush [res4]\n"+
                    "\tpush dividing\n"+
                    "\tcall [printf]\n\n"+
                    "\tcall [getch]\n" +
                    "\tpush NULL\n" +
                    "\tcall[ExitProcess]\n\n" +
                    "section '.idata' import data readable\n\n" +
                    "\tlibrary kernel, 'kernel32.dll',\\\n" +
                    "\t\tmsvcrt, 'msvcrt.dll'\n\n" +
                    "\timport kernel,\\\n" +
                    "\t\tExitProcess, 'ExitProcess'\n\n" +
                    "\timport msvcrt,\\\n" +
                    "\t\tprintf, 'printf',\\\n" +
                    "\t\tscanf, 'scanf',\\\n" +
                    "\t\tgetch, '_getch'");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

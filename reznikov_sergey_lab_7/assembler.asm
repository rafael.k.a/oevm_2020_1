format PE console

entry Start

include 'win32a.inc'

section '.data' data readable writable

x dd ?
y dd ?
res1 dd ?
res2 dd ?
res3 dd ?
res4 dd ?
str0 db 'input x: ', 0
str1 db 'input y: ', 0
str2 db 'x + y = ', 0
str3 db 'x - y = ', 0
str4 db 'x * y = ', 0
str5 db 'x / y = ', 0
empstr db '%d', 0
artstr db ' ',0xA, 0
NULL=0

section '.code' code readable executable 

Start:
push str0
call [printf]

push x
push empstr
call [scanf]

push str1
call [printf]

push y
push empstr
call [scanf]

mov ecx, [ x ]
add ecx, [ y]
mov [res1], ecx

push str2
call [printf]

push [res1]
push empstr
call [printf]

push artstr
call [printf]

mov ecx, [ x ]
sub ecx, [ y]
mov [res2], ecx

push str3
call [printf]

push [res2]
push empstr
call [printf]

push artstr
call [printf]

mov ecx, [ x ]
imul ecx, [ y]
mov [res3], ecx

push str4
call [printf]

push [res3]
push empstr
call [printf]

push artstr
call [printf]

mov eax, [ x ]
mov ecx, [ y]
idiv ecx

mov [res4], eax

push str5
call [printf]

push [res4]
push empstr
call [printf]

push artstr
call [printf]

finish:

                call[getch]

                push NULL
                call[ExitProcess]


section '.idata' import data readable

        library kernel, 'kernel32.dll',\
                msvcrt, 'msvcrt.dll'

        import  kernel,\
                ExitProcess, 'ExitProcess'

        import msvcrt,\
               printf, 'printf',\
               scanf, 'scanf',\
               getch,'_getch'